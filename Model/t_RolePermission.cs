//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class t_RolePermission
    {
        public int RolePermissionId { get; set; }
        public Nullable<int> RoleId { get; set; }
        public Nullable<int> PermissionId { get; set; }
    }
}
