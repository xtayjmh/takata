﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// 系统整合界面所需要的Mode，SystemIntegration
    /// </summary>
    public class SysItnModel:t_Video
    {
        public string Date { get; set; }//最后一个分析视频的日期
        public string WorkType { get; set; }
        public string WorkLine { get; set; }
        public string ProdName { get; set; }
        public string CarType { get; set; }
        public string SeatNo { get; set; }
        public string ProdType { get; set; }
        public List<t_Video> Videos { get; set; }
        public string HmoaReport { get; set; }//人机操作分析报表
        public string ImproveReport { get; set; }
        public string PartNo { get; set; }//品号
        public bool HasImproveReport { get; set; }
        public bool HasHumanMachineReport { get; set; }
        public bool HasImproveReportEdit { get; set; }
        public string ZoneName { get; set; }
    }
}
