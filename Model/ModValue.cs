﻿namespace Model
{
    public static class ModValue
    {
        public static double GeneralVal => 0.129;//正常值。能量消耗最小的动作
        public static double HighlyActiveVal => 0.1;//高效值。熟练工人的高水平动作时间值
        public static double TiredVal => 0.143;//包括疲劳恢复时间的10.7%在内的动作时间
        public static double FasterVal => 0.12;//快速值，比正常值快7%左右
    }
}
