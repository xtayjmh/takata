﻿namespace Model
{
    public class RankItem
    {
        public string Text;

        public int Value;

        public RankItem(string _Text, int _Value)
        {
            Text = _Text;
            Value = _Value;
        }

        public override string ToString()
        {
            return Text;
        }

        public enum RankItemEnum
        {
            第一项 = 1,
            第二项 = 2,
            第三项 = 3
        }
    }

}
