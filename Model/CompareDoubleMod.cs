﻿namespace Model
{
    public class CompareDoubleMod
    {
        public string IdA { get; set; }
        public string IdB { get; set; }
        public string NameA { get; set; }
        public string NameB { get; set; }
        public decimal? TimeA { get; set; }
        public decimal? TimeB { get; set; }
        public decimal? TimeDiff => TimeA - TimeB;
    }
}
