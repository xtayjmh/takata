﻿using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Configuration.Install;
using System.IO;
using System.Security.AccessControl;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Diagnostics;

namespace Library
{
    [RunInstaller(true)]
    public partial class Installer : System.Configuration.Install.Installer
    {
        public Installer()
        {
            InitializeComponent();
        }
        private void RunBat(string batPath)
        {
            Process pro = new Process();
            FileInfo file = new FileInfo(batPath);
            pro.StartInfo.WorkingDirectory = file.Directory.FullName;
            pro.StartInfo.FileName = batPath;
            pro.StartInfo.CreateNoWindow = false;
            pro.Start();
            pro.WaitForExit();
        }
        // Override the 'OnAfterInstall' method.
        protected override void OnAfterInstall(IDictionary savedState)
        {
            //System.Diagnostics.Debugger.Launch();//启动调试
            //            base.OnAfterInstall(savedState);
            var ftpaddr = Context.Parameters["ftpaddr"].Replace("\\\\", "\\"); //ftp地址
            var ftpuser = Context.Parameters["ftpuser"]; //ftp用户
            var ftppwd = Context.Parameters["ftppwd"]; //ftp用户密码
            var dbaddr = Context.Parameters["dbaddr"].Replace("\\\\", "\\"); //数据库地址
            var dbuser = Context.Parameters["dbuser"]; //数据库用户
            var dbpwd = Context.Parameters["dbpwd"]; //数据库用户密码
            var dbname = Context.Parameters["dbname"];//数据库名称
            var path = Context.Parameters["targetdir"]; //软件安装目录
            //如果什么都不填的话，从注册表里读取这些信息，省掉还要在编辑配置信息的地方点击一次确定
            RegistryKey regKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\\JoysonRegedit", false);
            if (regKey != null)
            {
                if (string.IsNullOrEmpty(ftpaddr) && regKey.GetValue("ftpaddr") != null)
                {
                    ftpaddr = regKey.GetValue("ftpaddr").ToString();
                }
                if (string.IsNullOrEmpty(ftpuser) && regKey.GetValue("ftpuser") != null)
                {
                    ftpuser = regKey.GetValue("ftpuser").ToString();
                }

                if (string.IsNullOrEmpty(ftppwd) && regKey.GetValue("ftppwd") != null)
                {
                    ftppwd = regKey.GetValue("ftppwd").ToString();
                }
                if (string.IsNullOrEmpty(dbaddr) && regKey.GetValue("dbaddr") != null)
                {
                    dbaddr = regKey.GetValue("dbaddr").ToString();
                }
                if (string.IsNullOrEmpty(dbuser) && regKey.GetValue("dbuser") != null)
                {
                    dbuser = regKey.GetValue("dbuser").ToString();
                }
                if (string.IsNullOrEmpty(dbpwd) && regKey.GetValue("dbpwd") != null)
                {
                    dbpwd = regKey.GetValue("dbpwd").ToString();
                }
                if (string.IsNullOrEmpty(dbname) && regKey.GetValue("dbname") != null)
                {
                    dbname = regKey.GetValue("dbname").ToString();
                }
            }


            Configuration config = ConfigurationManager.OpenExeConfiguration(path + "Joyson.exe");
            config.AppSettings.Settings["ftpaddr"].Value = ftpaddr;
            config.AppSettings.Settings["ftpuser"].Value = ftpuser;
            config.AppSettings.Settings["ftppwd"].Value = ftppwd;
            config.AppSettings.Settings["dbaddr"].Value = dbaddr;
            config.AppSettings.Settings["dbuser"].Value = dbuser;
            config.AppSettings.Settings["dbpwd"].Value = dbpwd;
            config.AppSettings.Settings["dbname"].Value = dbname;

            config.ConnectionStrings.ConnectionStrings["TakataEntities"].ConnectionString = $"metadata=res://*/Takata.csdl|res://*/Takata.ssdl|res://*/Takata.msl;provider=System.Data.SqlClient;provider connection string=\"data source={dbaddr};initial catalog={dbname};persist security info=True;user id={dbuser};password={dbpwd};MultipleActiveResultSets=True;App=EntityFramework\"";
            config.ConnectionStrings.ConnectionStrings["Takata.Properties.Settings.TakataConnectionString"].ConnectionString = $"Data Source={dbaddr};Initial Catalog={dbname};Persist Security Info=True;User ID={dbuser};Password={dbpwd}";

            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
            ConfigurationManager.RefreshSection("connectionStrings");
            AddSecurityControllFolder(path);
            RunBat(path + "//APlayerSDK//install.bat");
        }
        /// <summary>
        /// 为文件夹添加User，EveryOne用户组的完全控制权限
        /// </summary>
        /// <param name="filaPath"></param>
        void AddSecurityControllFolder(string filaPath)
        {
            //获取文件夹信息
            var dir = new DirectoryInfo(filaPath);
            //获取该文件夹的所有访问权限
            var dirSecurity = dir.GetAccessControl(AccessControlSections.All);
            //设定文件ACL继承
            var inherits = InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit;
            //添加everyone用户组的访问权限规则，完全控制
            var everyOneFileSystemAccessRule = new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, inherits, PropagationFlags.None, AccessControlType.Allow);
            //添加Users用户组的访问权限控制，完全控制
            var usersFileSystemAccessRule = new FileSystemAccessRule("Users", FileSystemRights.FullControl, inherits, PropagationFlags.None, AccessControlType.Allow);
            var isModified = false;
            dirSecurity.ModifyAccessRule(AccessControlModification.Add, everyOneFileSystemAccessRule, out isModified);
            dirSecurity.ModifyAccessRule(AccessControlModification.Add, usersFileSystemAccessRule, out isModified);
            dir.SetAccessControl(dirSecurity);//设置访问权限
        }
    }
}
