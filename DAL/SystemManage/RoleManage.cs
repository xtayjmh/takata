﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace DAL.SystemManage
{
    public class RoleManage:BaseManage
    {
        
        /// <summary>
        /// 获取权限列表
        /// </summary>
        /// <returns></returns>
        public static List<t_Role> GetRoleList()
        {
            var roleList = Common.GetList<t_Role>(null);
            return roleList;
        }
        /// <summary>
        /// 添加权限
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public static bool AddNewRole(t_Role role)
        {
            return Common.AddOrUpdateObject(role);
        }
        /// <summary>
        /// 删除权限，删除成功的话返回true，否则返回false
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public static bool DeleteRole(int roleId)
        {
            var role = Db.t_Role.FirstOrDefault(i => i.RoleId == roleId);
            if (role == null) return false;
            Db.t_Role.Remove(role);
            return Db.SaveChanges() > 0;
        }

        public static List<int?> GetUsedRoleIdList()
        {
            var usedRoleIdList = Db.t_User.Select(i => i.RoleId).Distinct().ToList();
            return usedRoleIdList;
        }
        /// <summary>
        /// 编辑权限
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public static bool UpdateRole(t_Role role)
        {
            Db.Set<t_Role>().AddOrUpdate(role);
            return Db.SaveChanges() > 0;
        }
    }
}
