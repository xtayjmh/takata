﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Model;

namespace DAL.SystemManage
{
    public class WorkLineManage : BaseManage
    {
        public static List<t_WorkLine> GetWorkLines(int id)
        {
            var list = Db.t_WorkLine.Where(w => w.WorkShopId == id && !w.IsDel).ToList();
            return list;
        }

        public static bool DeleteWorkLine(int id)
        {
            var model = Db.t_WorkLine.FirstOrDefault(i => i.WorkLineId == id);
            if (model == null) return false;
            model.IsDel = true;
            var carTypeList = Db.t_CarType.Where(c => c.WorkLineId == id && !c.IsDel);
            carTypeList.ToList().ForEach(c =>
            {
                var prodList = Db.t_Prod.Where(p => p.CarTypeId == c.TypeId && !p.IsDel);
                prodList.ToList().ForEach(p =>
                {
                    var stepList = Db.t_WorkStep.Where(s => s.ProdId == p.ProdId && !s.IsDel);
                    stepList.ToList().ForEach(s =>
                    {
                        s.IsDel = true;
                    });
                    p.IsDel = true;
                });
                c.IsDel = true;
            });
            return Db.SaveChanges() > 0;
        }

        public static bool AddWorkLine(t_WorkLine mode)
        {
            return Common.AddOrUpdateObject(mode);
        }

        public static bool EditWorkLine(t_WorkLine mode)
        {
            Db.Set<t_WorkLine>().AddOrUpdate(mode);
            return Db.SaveChanges() > 0;
        }

        public static bool DeleteCarType(int id)
        {
            var mode = Db.t_CarType.FirstOrDefault(i => i.TypeId == id);
            if (mode == null) return false;
            mode.IsDel = true;

            var prodList = Db.t_Prod.Where(p => p.CarTypeId == id && !p.IsDel);
            prodList.ToList().ForEach(p =>
            {
                var stepList = Db.t_WorkStep.Where(s => s.ProdId == p.ProdId && !s.IsDel);
                stepList.ToList().ForEach(s =>
                {
                    s.IsDel = true;
                });
                p.IsDel = true;
            });

            return Db.SaveChanges() > 0;
        }

        public static List<t_CarType> GetCarTypes(int wid)
        {
            var list = Db.t_CarType.Where(i => i.WorkLineId == wid && !i.IsDel).ToList();
            return list;
        }
    }
}
