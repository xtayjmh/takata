﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Model;

namespace DAL.SystemManage
{
    public class SingleActionManage : BaseManage
    {
        public static List<t_SingleAction> GetSingleActions()
        {
            var list = Common.GetList<t_SingleAction>(null);
            return list;
        }

        public static bool DeleteSingleAction(int id)
        {
            var model = Db.t_SingleAction.FirstOrDefault(i => i.SingleActionId == id);
            if (model == null) return false;
            return Common.DeleteObj(model);
        }

        public static t_SingleAction AddSingleAction(t_SingleAction mode)
        {
            return Common.AddNewObjReturnObj(mode);
        }

        public static bool EditSingleAction(t_SingleAction mode)
        {
            Db.Set<t_SingleAction>().AddOrUpdate(mode);
            return Db.SaveChanges() > 0;
        }
    }
}
