﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using Model;

namespace DAL.SystemManage
{
    public class UserManage:BaseManage
    {
        public bool AddUser(t_User user)
        {
            return Common.AddOrUpdateObject(user);
        }

        public List<string> GetLoginNameList()
        {
            return Common.GetList<t_User>(null).Select(i => i.LoginName).ToList();
        }
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public t_User Login(t_User user)
        {
            user.UserPwd = Common.EncryptMd5(user.UserPwd);
            var mode = Common.GetSingleModel<t_User>(i => i.LoginName == user.LoginName && i.UserPwd == user.UserPwd && i.IsActive == 1);
            return mode;
        }
        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool DeleteUser(int userId)
        {
            var user = Db.t_User.FirstOrDefault(u => u.UserId == userId);
            if (user == null) return false;
            user.IsActive = 0;
            var r = Db.SaveChanges();
            return r > 0;

        }
        //获取所有用户的列表
        public List<t_User> GetUserList()
        {
            var list = Common.GetList<t_User>(i => i.IsActive == 1);
            var roleList = Common.GetList<t_Role>(null);
            list.ForEach(i => i.RoleName = roleList.FirstOrDefault(j => j.RoleId == i.RoleId)?.RoleName);
            var zoneList = Common.GetList<t_Zone>(null);
            list.ForEach(i => i.ZoneName = zoneList.FirstOrDefault(j => j.ZoneId == i.ZoneId && j.IsActive == true)?.ZoneName);

            return list;
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="user"></param>
        /// <param name="newPwd"></param>
        /// <returns></returns>
        public bool UpdateUserPwd(t_User user, string newPwd)
        {
//            user.UserPwd = Common.EncryptMd5(user.UserPwd);
            using (var db = new TakataEntities())
            {
                var model = db.t_User.FirstOrDefault(i => i.LoginName == user.LoginName && i.UserPwd == user.UserPwd);
                if (model == null) return false;
                model.UserPwd = Common.EncryptMd5(newPwd);
                db.Set<t_User>().AddOrUpdate(model);
                return db.SaveChanges() > 0;
            }
        }

        public bool UpdateUser(t_User user)
        {
            Db.Set<t_User>().AddOrUpdate(user);
            return Db.SaveChanges()>0;
        }
    }
}
