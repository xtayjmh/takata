﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using Model;

namespace DAL.SystemManage
{
    public class ZoneManage : BaseManage
    {
        public bool AddZone(t_Zone zone)
        {
            return Common.AddOrUpdateObject(zone);
        }
 
        public bool DeleteZone(int zoneId)
        {
            var zone = Db.t_Zone.FirstOrDefault(u => u.ZoneId == zoneId);
            if (zone == null) return false;
            zone.IsActive = false;
            var r = Db.SaveChanges();
            return r > 0;

        }
        //获取所有 区域的列表
        public List<t_Zone> GetZoneList()
        {
            var list = Common.GetList<t_Zone>(i=>i.IsActive==true);
            return list;
        }

        public t_Zone GetSingleZone(int ZoneId)
        {
            return Db.t_Zone.FirstOrDefault(u => u.ZoneId == ZoneId);
        }

        public bool UpdateZone(t_Zone zone)
        {
            Db.Set<t_Zone>().AddOrUpdate(zone);
            return Db.SaveChanges()>0;
        }
    }
}
