﻿using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Model;

namespace DAL.SystemManage
{
    public class WorkShopManage : BaseManage
    {
        public static List<t_WorkShop> GetWorkShops()
        {
            var list = Db.t_WorkShop.Where(w =>!w.IsDel).ToList();
            return list;
        }
        public static List<t_WorkShop> GetWorkShops(int id)
        {
            var list = Db.t_WorkShop.Where(w => w.FactoryId == id && !w.IsDel).ToList();
            return list;
        }

        public static bool DeleteWorkShop(int id)
        {
            var model = Db.t_WorkShop.FirstOrDefault(i => i.WorkShopId == id);
            if (model == null) return false;
            model.IsDel = true;

            var workLineList = Db.t_WorkLine.Where(w => w.WorkShopId == id && !w.IsDel);
            workLineList.ToList().ForEach(w => {
                var carTypeList = Db.t_CarType.Where(c => c.WorkLineId == w.WorkLineId && !c.IsDel);
                carTypeList.ToList().ForEach(c => {
                    var prodList = Db.t_Prod.Where(p => p.CarTypeId == c.TypeId && !p.IsDel);
                    prodList.ToList().ForEach(p => {
                        var stepList = Db.t_WorkStep.Where(s => s.ProdId == p.ProdId && !s.IsDel);
                        stepList.ToList().ForEach(s => {
                            s.IsDel = true;
                        });
                        p.IsDel = true;
                    });
                    c.IsDel = true;
                });
                w.IsDel = true;
            });


            return Db.SaveChanges() > 0;
        }

        public static bool AddWorkShop(t_WorkShop mode)
        {
            return Common.AddOrUpdateObject(mode);
        }

        public static bool EditWorkShop(t_WorkShop mode)
        {
            Db.Set<t_WorkShop>().AddOrUpdate(mode);
            return Db.SaveChanges() > 0;
        }

        public static List<t_Prod> GetProds()
        {
            var list = Db.t_Prod.Where(i => !i.IsDel).ToList();
            return list;
        }

        public static List<t_Prod> GetProds(int wid)
        {
            var list = Db.t_Prod.Where(i => i.CarTypeId == wid && !i.IsDel).ToList();
            return list;
        }

        public static bool DeleteProd(int id)
        {
            var model = Db.t_Prod.FirstOrDefault(i => i.ProdId == id);
            if (model == null) return false;
            model.IsDel = true;

            var stepList = Db.t_WorkStep.Where(s => s.ProdId == id && !s.IsDel);
            stepList.ToList().ForEach(s =>
            {
                s.IsDel = true;
            });

            return Db.SaveChanges() > 0;
        }

        public static void EditProd(t_Prod sa)
        {
            throw new System.NotImplementedException();
        }

        public static void AddProd(t_Prod sa)
        {
            throw new System.NotImplementedException();
        }

    }
}
