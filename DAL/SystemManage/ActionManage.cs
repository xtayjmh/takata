﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace DAL.SystemManage
{
    public class ActionManage:BaseManage
    {
        public static List<t_Action> GetActions(int videoId)
        {
            var listAction = Common.GetList<t_Action>(i => i.VideoId == videoId).OrderBy(i=>i.ActionId).ToList(); // updated by Ted on 20180312 for sorting
            listAction.ForEach(l => {
                l.DisplayActionTime = l.ActionTime / 1000;
                l.DisplayManualStandardTime = l.ManualStandardTime / 1000;
            });
            return listAction;
        }

        public static bool DeleteAction(int actionId)
        {
            var model = Db.t_Action.FirstOrDefault(i => i.ActionId == actionId);
            if (model == null) return false;
            var upAction = Db.t_Action.FirstOrDefault(a => a.EndTime == model.StartTime && a.VideoId == model.VideoId) ?? Db.t_Action.OrderByDescending(i => i.EndTime).FirstOrDefault(i => i.VideoId == model.VideoId && i.EndTime < model.StartTime);
            if (upAction != null)
            {
                upAction.EndTime = model.EndTime;
                upAction.ActionTime = model.EndTime - upAction.StartTime;
                if (upAction.Speed != null)
                {
                    var at = Math.Round((double) (upAction.ActionTime / upAction.Speed), 2);
                    upAction.ManualStandardTime = (decimal) at;
                    upAction.AutoStandardTime = (decimal) at;
                }

                Db.Set<t_Action>().AddOrUpdate(upAction);
            }
            Db.Set<t_Action>().Remove(model);

            return Db.SaveChanges() > 0;
        }
        /// <summary>
        /// 删除第一条标记，时间向下合并2018-6-11，Johnson
        /// </summary>
        /// <param name="actionId"></param>
        /// <returns></returns>
        public static bool DeleteFirstAction(int actionId)
        {
            var model = Db.t_Action.FirstOrDefault(i => i.ActionId == actionId);
            if (model == null) return false;
            var endTime = decimal.Parse(model.EndTime.ToString("#0.0000"));
            var dt = Math.Round(model.EndTime, 4);

            var downAction = Db.t_Action.FirstOrDefault(a => a.StartTime == model.EndTime && a.VideoId == model.VideoId) ?? Db.t_Action.OrderBy(i => i.EndTime).FirstOrDefault(i => i.VideoId == model.VideoId && i.StartTime > model.EndTime);
            if (downAction != null)
            {
                downAction.StartTime = 0;
                downAction.ActionTime = downAction.EndTime - 0;
                if (downAction.Speed != null)
                {
                    var at = Math.Round((double)(downAction.ActionTime / downAction.Speed), 2);
                    downAction.ManualStandardTime = (decimal)at;
                    downAction.AutoStandardTime = (decimal)at;
                }

                Db.Set<t_Action>().AddOrUpdate(downAction);
            }
            Db.Set<t_Action>().Remove(model);

            return Db.SaveChanges() > 0;
        }


        public static bool DeleteActionByVideo(int VideoId)
        {
            var models = Db.t_Action.Where(i => i.VideoId == VideoId);
            if (models.Any())
            {
                foreach (var model in models)
                {
                    Db.t_Action.Remove(model);
                }
                return Db.SaveChanges() > 0;
            }
            else
                return true;
        }

        public static bool DeleteVideo(int videoId)
        {
            var model = Db.t_Video.FirstOrDefault(i => i.VideoId == videoId);
            return model != null && Common.DeleteObj(model);
        }

        public static List<string> GetOperationName(int currentVideoVideoId)
        {
            var list = Db.t_Action.Where(j => j.VideoId == currentVideoVideoId).Select(i => i.OperationName).Distinct().ToList();
            return list;
        }

        public static bool AddOrUpdate(t_Action action)
        {
            Db.Set<t_Action>().AddOrUpdate(action);
            return Db.SaveChanges() > 0;
        }
        public static t_Action SplitAction(t_Action currentAction, t_Action newAction)
        {
            Db.Set<t_Action>().AddOrUpdate(currentAction);//被拆分的动作
            Db.Set<t_Action>().Add(newAction);//拆分出来的新动作
            Db.SaveChanges();
            return newAction;
        }

        public static void BatchUpdateOperationName(t_Action tAction, string oldOperationName)
        {
            Db.Set<t_Action>().AddOrUpdate(tAction);
            Db.SaveChanges();
            var mode = Db.t_Action.FirstOrDefault(i => i.ActionId == tAction.ActionId);
            var exc = Db.t_Action.FirstOrDefault(i => i.VideoId == tAction.VideoId && i.EndTime > tAction.EndTime && i.OperationName != oldOperationName);//要修改操作名称那一条后边
            var l = new List<t_Action>();
            if (exc != null)
            {
                l = Db.t_Action.Where(i => i.VideoId == tAction.VideoId && i.EndTime > tAction.EndTime && i.EndTime < exc.EndTime && i.OperationName == oldOperationName).ToList();
            }
            else
            {
                l = Db.t_Action.Where(i => i.VideoId == tAction.VideoId && i.EndTime > tAction.EndTime && i.OperationName == oldOperationName).ToList();
            }
            if (l.Count > 0)
            {
                l.ForEach(j =>
                {
                    j.OperationName = tAction.OperationName;
                    Db.Set<t_Action>().AddOrUpdate(j);
                });
                Db.SaveChanges();
            }
        }

        public static void BatchUpdateCycle(t_Action tAction, int oldCycle)
        {
            var mode = Db.t_Action.FirstOrDefault(i => i.ActionId == tAction.ActionId);
            var exc = Db.t_Action.FirstOrDefault(i => i.VideoId == tAction.VideoId && i.EndTime > tAction.EndTime && i.Cycle != oldCycle);//要修改操作名称那一条后边
            var l = new List<t_Action>();
            if (exc != null)
            {
                l = Db.t_Action.Where(i => i.VideoId == tAction.VideoId && i.EndTime > tAction.EndTime && i.EndTime < exc.EndTime && i.Cycle == oldCycle).ToList();
            }
            else
            {
                l = Db.t_Action.Where(i => i.VideoId == tAction.VideoId && i.EndTime > tAction.EndTime && i.Cycle == oldCycle).ToList();
            }
            l.ForEach(j =>
            {
                j.Cycle = tAction.Cycle;
                Db.Set<t_Action>().AddOrUpdate(j);
            });
            Db.Set<t_Action>().AddOrUpdate(tAction);
            Db.SaveChanges();
        }

        public static t_Action AddNewAction(t_Action mode, decimal videoEndTime)
        {
            if (videoEndTime > mode.EndTime)
            {
                var lastAction = new t_Action
                {
                    ActionTime = videoEndTime - mode.EndTime, //当前播放时间减去上个动作的
                    StartTime = mode.EndTime,
                    EndTime = videoEndTime,
                    VideoId = mode.VideoId,
                    Speed = 1.00m,
                    ManualStandardTime = videoEndTime - mode.EndTime,
                    AutoStandardTime = videoEndTime - mode.EndTime,
                    TypeId = 2,
                    ActionTypeId = 1,
                    Cycle = mode.Cycle
                };
                Db.Set<t_Action>().Add(lastAction);
            }

            Db.Set<t_Action>().Add(mode);
            Db.SaveChanges();
            return mode;
        }
    }
}
