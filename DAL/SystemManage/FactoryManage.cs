﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Model;

namespace DAL.SystemManage
{
    public class FactoryManage : BaseManage
    {
        public static List<t_Factory> GetFactorys()
        {
            var list = Common.GetList<t_Factory>(i => !i.IsDel);
            return list;
        }

        public static bool DeleteFactory(int id)
        {
            var model = Db.t_Factory.FirstOrDefault(i => i.FactoryId == id && !i.IsDel);
            if (model == null) return false;
            model.IsDel = true;
            var workShopList = Db.t_WorkShop.Where(i => i.FactoryId == id && !i.IsDel);
            //递归删除工厂下边的车间和产线数据，不做物理删除
            workShopList.ToList().ForEach(i =>
            {
                var workLineList = Db.t_WorkLine.Where(w => w.WorkShopId == i.WorkShopId && !w.IsDel);
                workLineList.ToList().ForEach(w => {
                    var carTypeList = Db.t_CarType.Where(c => c.WorkLineId == w.WorkLineId && !c.IsDel);
                    carTypeList.ToList().ForEach(c => {
                        var prodList = Db.t_Prod.Where(p => p.CarTypeId == c.TypeId && !p.IsDel);
                        prodList.ToList().ForEach(p => {
                            var stepList = Db.t_WorkStep.Where(s => s.ProdId == p.ProdId && !s.IsDel);
                            stepList.ToList().ForEach(s => {
                                s.IsDel = true;
                            });
                            p.IsDel = true;
                        });
                        c.IsDel = true;
                    });
                    w.IsDel = true;
                });
                i.IsDel = true;
            });
            return Db.SaveChanges() > 0;
//            return Common.AddOrUpdateObject(model);
        }

        public static bool AddFactory(t_Factory mode)
        {
            return Common.AddOrUpdateObject(mode);
        }

        public static bool EditFactory(t_Factory mode)
        {
            Db.Set<t_Factory>().AddOrUpdate(mode);
            return Db.SaveChanges() > 0;
        }
    }
}
