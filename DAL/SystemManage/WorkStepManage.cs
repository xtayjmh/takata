﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Model;

namespace DAL.SystemManage
{
    public class WorkStepManage : BaseManage
    {
        public static List<t_WorkStep> GetWorkSteps(int id)
        {
            var list = Db.t_WorkStep.Where(w => w.ProdId == id && !w.IsDel).ToList();
            return list;
        }

        public static bool DeleteWorkStep(int id)
        {
            var model = Db.t_WorkStep.FirstOrDefault(i => i.WorkStepId == id);
            if (model == null) return false;
            model.IsDel = true;
            return Db.SaveChanges() > 0;
        }

        public static bool AddWorkStep(t_WorkStep mode)
        {
            return Common.AddOrUpdateObject(mode);
        }

        public static bool EditWorkStep(t_WorkStep mode)
        {
            Db.Set<t_WorkStep>().AddOrUpdate(mode);
            return Db.SaveChanges() > 0;
        }
    }
}
