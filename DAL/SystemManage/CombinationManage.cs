﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Model;

namespace DAL.SystemManage
{
    public class CombinationManage : BaseManage
    {
        public static List<t_ModAPTS> GetCombinations()
        {
            var list = Db.t_ModAPTS.ToList();
            return list;
        }

        public static bool DeleteCombination(int id)
        {
            var model = Db.t_ModAPTS.FirstOrDefault(i => i.ModAPTSId == id);
            if (model == null) return false;
            return Common.DeleteObj(model);
        }

        public static bool AddCombination(t_ModAPTS mode)
        {
            return Common.AddOrUpdateObject(mode);
        }

        public static bool EditCombination(t_ModAPTS mode)
        {
            Db.Set<t_ModAPTS>().AddOrUpdate(mode);
            return Db.SaveChanges() > 0;
        }
    }
}
