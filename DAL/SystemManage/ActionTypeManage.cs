﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Model;

namespace DAL.SystemManage
{
    public class ActionTypeManage:BaseManage
    {
        public static List<t_ActionType> GetActionTypes()
        {
            var list = Common.GetList<t_ActionType>(null);
            return list;
        }

        public static bool DeleteActionType(int id)
        {
            var model = Db.t_ActionType.FirstOrDefault(i => i.ActionTypeId == id);
            if (model == null) return false;
            return Common.DeleteObj(model);
        }

        public static bool AddActionType(t_ActionType mode)
        {
            return Common.AddOrUpdateObject(mode);
        }

        public static bool EditActionType(t_ActionType mode)
        {
            Db.Set<t_ActionType>().AddOrUpdate(mode);
            return Db.SaveChanges() > 0;
        }
    }
}
