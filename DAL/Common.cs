﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using DAL.SystemManage;
using Model;

namespace DAL
{
    public class Common:BaseManage
    {
        /// <summary>
        /// 新增一条记录
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <returns></returns>
        public static bool AddOrUpdateObject<T>(T t) where T : class
        {
//            Db.Set<T>().Add(t);
            Db.Set<T>().AddOrUpdate(t);
            var r = Db.SaveChanges();
            return r > 0;
        }

        public static T AddNewObjReturnObj<T>(T t) where T : class
        {
            Db.Set<T>().Add(t);
            Db.SaveChanges();
            return t;
        }
        /// <summary>
        /// 根据查询条件获取一条记录
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="whereLambda"></param>
        /// <returns></returns>
        //示例 var model = Common.GetSingleModel<t_User>(i => i.LoginName == "xtayjmh");
        public static T GetSingleModel<T>(Expression<Func<T,bool>> whereLambda) where T : class
        {
            return Db.Set<T>().Where(whereLambda).AsNoTracking().FirstOrDefault();
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="whereLambda"></param>
        /// <returns></returns>
        public static List<T> GetList<T>(Expression<Func<T, bool>> whereLambda) where T : class
        {
            return whereLambda != null ? Db.Set<T>().Where(whereLambda).AsNoTracking().ToList() : Db.Set<T>().AsNoTracking().ToList();
        }
        /// <summary>
        /// 获取全部列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static List<T> GetList<T>() where T : class
        {
            return Db.Set<T>().AsNoTracking().ToList();
        }
        /// <summary>
        /// 获取MD5加密的字符串32位的
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string EncryptMd5(string s)
        {
            var md5 = new MD5CryptoServiceProvider();
            var result = "";
            if (!string.IsNullOrWhiteSpace(s))
            {
                result = BitConverter.ToString(md5.ComputeHash(Encoding.UTF8.GetBytes(s.Trim())));
            }
            return result.Replace("-","");
        }

        public static bool DeleteObj<T>(T t) where T :class 
        {
            Db.Set<T>().Remove(t);
            return Db.SaveChanges() > 0;
        }

        public static bool ExecuteSql(string sql)
        {
            return Db.Database.ExecuteSqlCommand(sql) > 0;
        }

        public static void ErrorLog(string errMsg)
        {
            if (!File.Exists(Environment.CurrentDirectory + "\\SystemError.txt")) File.Create(Environment.CurrentDirectory + "\\SystemError.txt");
            using (var sw = new StreamWriter(Environment.CurrentDirectory + "\\SystemError.txt", true))
            {
                sw.WriteLine($"{errMsg},{DateTime.Now}...");
                sw.Flush();
            }
        }
    }
}
