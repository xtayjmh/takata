﻿using Utility;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using Microsoft.Office.Interop.Excel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExcelApplication = Microsoft.Office.Interop.Excel.Application;
namespace UnitTestReport
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestImproveReport()
        {
            var model = new ImproveReportModel()
            {
                Title = "角度+包装工位（工序名称）动作分析",
                SheetName = "TestSheetName",
                HeaderModel = new ImproveReportModel.HeaderInfoModel()
                {
                    ActionName = "角度+包装",
                    CreateDate = DateTime.Now.ToString("yyyy.MM.dd"),
                    ProductName = "X156（品号）",
                    Researcher = "（平衡度分析人员，系统登录人员）",
                    WorkDepart = "制造部制造科",
                    WorkLine = "TSB-12",
                    WorkMan = "视频制作初期自定义录入的人员）"
                },
                DataList = new System.Collections.Generic.List<ImproveReportModel.DataBodyModel>() {
                    new ImproveReportModel.DataBodyModel() {
                        Index=1,
                        Before_ActionDes="右手移至织带处",
                        Before_StandTime=0.3m,
                        After_ActionDes="伸手",
                        After_Instruction="说明",
                        After_Time=0.19m,
                        Anaysis_ActionType="伸手",
                        Anaysis_IsValid="有效",
                        Anaysis_ECRS ="ECRS"
                 },new ImproveReportModel.DataBodyModel() {
                        Index=2,
                        Before_ActionDes="右手移至织带处1",
                        Before_StandTime=0.4m,
                        After_ActionDes="伸手1",
                        After_Instruction="说明1",
                        After_Time=0.1m,
                        Anaysis_ActionType="伸手1",
                        Anaysis_IsValid="有效1",
                        Anaysis_ECRS ="ECRS1"
                 }
                }

            };
            var model2 = new ImproveReportModel()
            {
                Title = "角度+包装工位（工序名称）动作分析",
                SheetName = "TestSheetName2",
                HeaderModel = new ImproveReportModel.HeaderInfoModel()
                {
                    ActionName = "角度+包装",
                    CreateDate = DateTime.Now.ToString("yyyy.MM.dd"),
                    ProductName = "X156（品号）",
                    Researcher = "（平衡度分析人员，系统登录人员）",
                    WorkDepart = "制造部制造科",
                    WorkLine = "TSB-12",
                    WorkMan = "视频制作初期自定义录入的人员）"
                },
                DataList = new System.Collections.Generic.List<ImproveReportModel.DataBodyModel>() {
                    new ImproveReportModel.DataBodyModel() {
                        Index=1,
                        Before_ActionDes="右手移至织带处",
                        Before_StandTime=0.2m,
                        After_ActionDes="伸手",
                        After_Instruction="说明",
                        After_Time=0.19m,
                        Anaysis_ActionType="伸手",
                        Anaysis_IsValid="有效",
                        Anaysis_ECRS ="ECRS"
                 },new ImproveReportModel.DataBodyModel() {
                        Index=2,
                        Before_ActionDes="右手移至织带处1",
                        Before_StandTime=0.31m,
                        After_ActionDes="伸手1",
                        After_Instruction="说明1",
                        After_Time=0.6m,
                        Anaysis_ActionType="伸手1",
                        Anaysis_IsValid="有效1",
                        Anaysis_ECRS ="ECRS1"
                 }
                }

            };
            ExcelHelper.ImproveReport(new System.Collections.Generic.List<ImproveReportModel>() { model, model2 }, "E://test.xlsx");
            var excel = new ExcelApplication();
            try
            {
                Workbook xBook = excel.Workbooks.Open("E://test.xlsx", Missing.Value, Missing.Value,
                    Missing.Value, Missing.Value, Missing.Value, Missing.Value
                    , Missing.Value, Missing.Value, Missing.Value, Missing.Value
                    , Missing.Value, Missing.Value, Missing.Value, Missing.Value);


                excel.Visible = false; // 不显示 Excel 文件,如果为 true 则显示 Excel 文件
                var sheet = (Worksheet)xBook.Sheets[xBook.Sheets.Count];//最后一个

                Range r = null;
                r = sheet.get_Range("A1:Z5");
                r.Columns.AutoFit();
                xBook.Save();
            }
            catch (Exception ex)
            {
                
            }
            finally
            {
                excel.Quit();
                excel = null;
            }
            Process.Start("E://test.xlsx");

        }

        [TestMethod]
        public void TestMatchingReport()
        {
            var model = new HumanMatchingModel()
            {
                Title = "角度+包装工位：人-机分析",
                SheetName = "TestSheetName",
                HeaderModel = new HumanMatchingModel.HeaderInfoModel()
                {
                    ActionName = "角度+包装",
                    CreateDate = DateTime.Now.ToString("yyyy.MM.dd"),
                    ProductName = "X156（品号）",
                    Researcher = "（平衡度分析人员，系统登录人员）",
                    WorkDepart = "制造部制造科",
                    WorkLine = "TSB-12",
                    WorkMan = "视频制作初期自定义录入的人员）"
                },
                DataList = new System.Collections.Generic.List<HumanMatchingModel.DataBodyModel>() {
                    new HumanMatchingModel.DataBodyModel() {
                        Index=1,
                        Operator_ActionDes="动作叙述",
                        Operator_StandTime=0.3m,
                        Operator_ActionType="伸手",
                        Operator_Status= HumanMatchingModel.Operator_StatusEnum.Second,
                        Matching_Status= HumanMatchingModel.Matching_StatusEnum.Free,
                        Matching_Time=0.21m,
                        Matching_ActionDes="空闲"
                 },new HumanMatchingModel.DataBodyModel() {
                         Index=2,
                        Operator_ActionDes="动作叙述1",
                        Operator_StandTime=0.311m,
                        Operator_ActionType="伸手1",
                        Operator_Status= HumanMatchingModel.Operator_StatusEnum.First,
                        Matching_Status= HumanMatchingModel.Matching_StatusEnum.Running,
                        Matching_Time=0.21m,
                        Matching_ActionDes="空闲1"
                 }
                },
                Summary = new HumanMatchingModel.SummaryModel()
                {
                    Matching_ActionTime = "12",
                    Matching_FreeTime = "23",
                    Matching_Rate = "34",
                    Operater_ActionTime = "45",
                    Operater_FreeTime = "56",
                    Operater_Rate = "67"
                }

            };
            var file = "E://test2/" + Guid.NewGuid().ToString("N") + ".xlsx";
            ExcelHelper.HumanMatchingReport(new List<HumanMatchingModel>(){model}, file);
            Process.Start(file);
        }

        [TestMethod]
        public void TestReSaveReport()
        {
            
        }
    }
}
