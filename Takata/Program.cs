﻿using System;
using System.Windows.Forms;
using DAL;
using Model;

namespace Takata
{
    static class Program
    {
        public static t_User CurrentUser;
        public static t_Zone CurrentZone;
        public static FormMain FormMain;
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            CurrentUser = new t_User();
            FormLogin login = new FormLogin();
            //            Application.Run(new FormSystemIntegration());

            if (login.ShowDialog() == DialogResult.OK)
            {
                //                Splasher.Show(typeof(FrmSplashScreen));

                Application.ThreadException += Application_ThreadException;
                FormMain = new FormMain
                {
                    StartPosition = FormStartPosition.CenterScreen,
                };
                try
                {
                    Application.Run(FormMain);
                }
                catch (Exception ex)
                {
                    Common.ErrorLog(ex.Message);
                    Common.ErrorLog(ex.StackTrace);
                    Common.ErrorLog(ex.Source);
                }
            }
        }
        private static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs ex)
        {
            Common.ErrorLog(ex.Exception.Message);
            Common.ErrorLog(ex.Exception.StackTrace);
            Common.ErrorLog(ex.Exception.Source);
            string message = $"{ex.Exception.Message}\r\n操作发生错误，您需要退出系统么？";
            if (DialogResult.Yes == MessageBox.Show(message, "系统错误", MessageBoxButtons.YesNo))
            {
                Application.Exit();
            }
        }
    }
}
