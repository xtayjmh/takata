﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DAL;
using DAL.SystemManage;
using DevComponents.DotNetBar;
using Model;

namespace Takata
{
    public partial class FormPlayActions : Office2007Form
    {
        public VlcPlayer vlcPlayer;
        string address;
        private decimal startPoint = 0.0000m;//开始播放的时间点
        private decimal endPoint = 10000.0000m;//播放结束的时间节点
        private string filePath = "";
        private List<t_ActionType> actionTypes = new List<t_ActionType>();
        private List<t_Action> invalidActions;
        private List<t_Action> validActions;
        private int StartIndex = 999;//设置循环时选择开始的行，用于高亮显示这些选择的行
        private int EndIndex = 999;//循环结束的行
        public FormPlayActions()
        {
            InitializeComponent();
            actionTypes = Common.GetList<t_ActionType>(null);
            address = Environment.CurrentDirectory;
            dgv1.AutoGenerateColumns = false;
        }

        private void btnPlay_Click(object sender, System.EventArgs e)
        {
            if (vlcPlayer.IsPlaying()) return;
            vlcPlayer.Play();
            timer1.Start();
        }

        private void btnPause_Click(object sender, System.EventArgs e)
        {
            if (!vlcPlayer.IsPlaying()) return;
            vlcPlayer.Pause();
            timer1.Stop();
        }

        private void btnBackward_Click(object sender, System.EventArgs e)
        {
            vlcPlayer.Pause();
            int time = (int)vlcPlayer.GetPlayTime() - 5;
            if (time > 0)
            {
                vlcPlayer.SetPlayTime(time);
                if (time > trackBar1.Maximum)
                {
                    trackBar1.Value = trackBar1.Maximum;
                }
                else
                {
                    trackBar1.Value = (int)vlcPlayer.GetPlayTime() * 1000;
                }
                vlcPlayer.Play();
            }
            else
            {
                vlcPlayer.SetPlayTime(0);
                trackBar1.Value = 0;
                vlcPlayer.Pause();
            }
            pictureBox1.SendToBack();
        }

        private void btnForward_Click(object sender, System.EventArgs e)
        {
            vlcPlayer.Pause();
            int time = (int)vlcPlayer.GetPlayTime() + 5;
            if (time * 1000 < trackBar1.Maximum)
            {
                vlcPlayer.SetPlayTime(time);
                trackBar1.Value = (int)vlcPlayer.GetPlayTime() * 1000;
                vlcPlayer.Play();
            }
            else
            {
                vlcPlayer.SetPlayTime(vlcPlayer.Duration() - 1);
                trackBar1.Value = trackBar1.Maximum;
            }
            pictureBox1.Hide();
            pictureBox1.Visible = false;
        }

        private void btnCancelLoop_Click(object sender, System.EventArgs e)
        {
            startPoint = 0.0000m;
            endPoint = 10000.0000m;
            lbProcess.Visible = false;
            if (vlcPlayer.IsPlaying()) vlcPlayer.Pause();
            dgv1.ClearSelection();
            for (int i = 0; i < dgv1.RowCount; i++)
            {
                dgv1.Rows[i].DefaultCellStyle.BackColor = Color.White;
            }
        }

        private void btnSetLoopStart_Click(object sender, System.EventArgs e)
        {
            if (vlcPlayer.IsPlaying())
            {
                vlcPlayer.Pause();
                timer1.Stop();
            }
            ClearSelection();
            if (dgv1.SelectedRows.Count == 0) return;
            var startVal = dgv1.SelectedRows[0].Cells["StartTime"].Value;
            startPoint = decimal.Parse(startVal.ToString()) / 1000;
            trackBar1.Value = (int)(startPoint * 1000);
            vlcPlayer.SetPlayTime(decimal.Parse(startVal.ToString()) / 1000);
            if (vlcPlayer.IsPlaying()) vlcPlayer.Pause();
            btnSetLoopEnd.Enabled = true;
            StartIndex = dgv1.SelectedRows[0].Index;
        }

        private void btnSetLoopEnd_Click(object sender, System.EventArgs e)
        {
            vlcPlayer.Pause();
            if (dgv1.SelectedRows.Count == 0) return;
            var endVal = dgv1.SelectedRows[0].Cells["EndTime"].Value;
            endPoint = decimal.Parse(endVal.ToString()) / 1000;
            var pct = endPoint - startPoint;
            lbProcess.Visible = true;
            lbProcess.Width = (int)((float)pct / (float)vlcPlayer.Duration() * 972);
            lbProcess.Location = new Point((int)((float)startPoint / (float)vlcPlayer.Duration() * 972) + 10, lbProcess.Location.Y);
            lbProcess.BringToFront();
            vlcPlayer.SetPlayTime(startPoint);
            trackBar1.Value = (int)(startPoint * 1000);
            vlcPlayer.Play();
            timer1.Start();
            EndIndex = dgv1.SelectedRows[0].Index;
            for (int i = StartIndex; i <= EndIndex; i++)
            {
                dgv1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(255, 0, 120, 215);
            }
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            OpenFile(sender, e);
            var filename = Path.GetFileName(filePath);
            var mode = Common.GetSingleModel<t_Video>(i => i.FileName == filename);
            if (mode == null)
            {
                MessageBox.Show("该视频不包含任何动作标记", "提示");
                return;
            }

            LoadActions(mode.VideoId);
        }

        private void LoadActions(int videoId)
        {
            var list = ActionManage.GetActions(videoId).OrderBy(i => i.EndTime).ToList();
            invalidActions = list.Where(i => i.TypeId != 2).ToList();
            validActions = list.Where(i => i.TypeId == 2).ToList();
            dgv1.DataSource = list;
            //1无效2有效3无关
            for (var i = 0; i < dgv1.Rows.Count; i++)
            {
                var row = dgv1.Rows[i];
                switch ((int)row.Cells["TypeId"].Value)
                {
                    case 1:
                        row.Cells[8].Style.BackColor = Color.Red;
                        row.Cells[8].Value = "×";
                        break;
                    case 2:
                        row.Cells[8].Style.BackColor = Color.GreenYellow;
                        row.Cells[8].Value = "○";
                        break;
                    case 3:
                        row.Cells[8].Style.BackColor = Color.Yellow;
                        row.Cells[8].Value = "△";
                        break;
                }

                if (row.Cells["ActionTypeId"].Value != null && !string.IsNullOrEmpty(row.Cells["ActionTypeId"].Value.ToString()))
                {
                    var actionType = actionTypes.FirstOrDefault(j => j.ActionTypeId == (int)row.Cells["ActionTypeId"].Value);
                    if (actionType != null)
                    {
                        var color = actionType.Color.Split(',');
                        row.Cells[7].Value = actionType.ChineseName;
                        row.Cells[7].Style.BackColor = Color.FromArgb(int.Parse(color[0]), int.Parse(color[1]), int.Parse(color[2]), int.Parse(color[3]));
                    }
                }
                if (new[] { "1", "3" }.Contains(row.Cells["TypeId"].Value.ToString())) row.Cells[5].Value = decimal.Parse("0.00");//无关录像ST为0
            }
        }

        private void FormPlayActions_Load(object sender, EventArgs e)
        {
            if (!File.Exists(address + "\\Menu.ini"))
            {
                FileStream fs = new FileStream(address + "\\Menu.ini", FileMode.Create, FileAccess.Write);
                fs.Close();
            }
            string pluginPath = Environment.CurrentDirectory + "\\plugins\\";
            vlcPlayer = new VlcPlayer(pluginPath);
            IntPtr render_wnd = panel1.Handle;
            vlcPlayer.SetRenderWindow((int)render_wnd);
            vlcPlayer.SetMouseInput();
            tbVideoTime.Text = "00:00:00/00:00:00";
        }
        private void OpenFile(object sender, EventArgs e)
        {
            //bool isSame = false;
            openFileDialog1.FileName = "";
            if (this.openFileDialog1.ShowDialog() == DialogResult.OK)
            {

                StreamWriter s = new StreamWriter(address + "\\Menu.ini", true);
                s.WriteLine(openFileDialog1.FileName);
                s.Flush();
                s.Close();
                string[] text = File.ReadAllLines(address + "\\Menu.ini");
                int row = text.Length;//行
                int rowcount;
                string[] tempdata = new string[] { "", "", "" };
                if (row > 3)// 若记录行数超过3个，则先记录后三个数据，再重新建一个Menu.ini（清除数据）,接着讲记录的后三个数据写入
                {
                    StreamReader sr1 = new StreamReader(address + "\\Menu.ini", true);
                    while (sr1.Peek() > -1)
                    {
                        sr1.ReadLine();//空读，跳过原始的第一个数据，从第二个数据开始读
                        for (rowcount = 0; rowcount < 3; rowcount++)
                        {
                            tempdata[rowcount] = sr1.ReadLine();
                        }
                    }
                    sr1.Close();
                    FileStream fs = new FileStream(address + "\\Menu.ini", FileMode.Create, FileAccess.Write);
                    fs.Close();
                    StreamWriter s1 = new StreamWriter(address + "\\Menu.ini", true);
                    s1.WriteLine(tempdata[0]);
                    s1.WriteLine(tempdata[1]);
                    s1.WriteLine(tempdata[2]);
                    s1.Flush();
                    s1.Close();
                }

                filePath = openFileDialog1.FileName;
                vlcPlayer.PlayFile(openFileDialog1.FileName);
                trackBar1.SetRange(0, (int)vlcPlayer.Duration() * 1000);
                trackBar1.Value = 0;
                timer1.Start();
                if (vlcPlayer.IsPlaying())
                {
                    timer1.Interval = 200;
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!vlcPlayer.IsPlaying() || trackBar1.Value == trackBar1.Maximum)
            {
                vlcPlayer.Pause();
                timer1.Stop();
                tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(vlcPlayer.Duration() * 1000), GetTimeString(vlcPlayer.Duration() * 1000));
                trackBar1.Value = trackBar1.Maximum;
            }
            else
            {
                var skip = false;
                var playTime = vlcPlayer.GetPlayTime();
                if (invalidActions == null) return;
                invalidActions.ForEach(i =>
                {
                    if (playTime * 1000 >= i.StartTime && playTime * 1000 <= i.EndTime)
                    {
                        vlcPlayer.Pause();
                        vlcPlayer.SetPlayTime((i.EndTime / 1000) + 0.01m);
                        vlcPlayer.SetSpeed(1.00f);
                        vlcPlayer.Play();
                        skip = true;
                    }
                });

                if (endPoint != 10000.0000m && playTime >= endPoint)
                {
                    trackBar1.Value = (int)(startPoint * 1000);
                    vlcPlayer.SetPlayTime(startPoint);
                }
                else
                {
                    trackBar1.Value = (int)playTime * 1000;
                    tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(playTime * 1000), GetTimeString(vlcPlayer.Duration() * 1000));
                }
                if (skip) return;
                var action = validActions.Where(i => i.StartTime >= ((decimal)startPoint) * 1000 && i.EndTime <= ((decimal)endPoint) * 1000).FirstOrDefault(a => playTime * 1000 >= a.StartTime && playTime * 1000 <= a.EndTime);
                if (action != null)
                {
                    vlcPlayer.SetSpeed((float)action.Speed);
                    for (int i = 0; i < dgv1.Rows.Count; i++)
                    {
                        DataGridViewRow dgvr = dgv1.Rows[i];
                        if ((int)dgvr.Cells[0].Value == action.ActionId)
                        {
                            if (dgv1.SelectedRows.Count > 0 && dgv1.SelectedRows[0].Index == i) return;
                            dgv1.ClearSelection();
                            //                            if(dgv1.SelectedRows[0].Index==i) return;
                            dgvr.Selected = true;
                        }
                    }
                }
            }
        }
        private string GetTimeString(decimal val)
        {
            val = val / 1000;
            var hour = val / 3600;
            val %= 3600;
            var minute = val / 60;
            var second = val % 60;
            var fps = vlcPlayer.GetFps();
            if (fps == 0)
                fps = 25;
            var allTime = vlcPlayer.Duration();

            var frame = (val % 1) / (decimal)(1 / fps);//帧
            return $"{hour:00}:{minute:00}:{(int)second:00}({(int)frame})";
        }

        private void FormPlayActions_FormClosed(object sender, FormClosedEventArgs e)
        {
            vlcPlayer.Stop();
        }

        private void dgv1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Rectangle rectangle = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, dgv1.RowHeadersWidth - 4, e.RowBounds.Height);
            TextRenderer.DrawText(e.Graphics,
                (e.RowIndex + 1).ToString(),
                dgv1.RowHeadersDefaultCellStyle.Font,
                rectangle,
                dgv1.RowHeadersDefaultCellStyle.ForeColor,
                TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
        }

        private void dgv1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var actionId = dgv1.Rows[e.RowIndex].Cells[0].Value;
            var action = validActions.FirstOrDefault(i => i.ActionId == (int)actionId);
            if (action == null) return;//如果选择的是无效动作则跳出不执行任何操作
            vlcPlayer.SetPlayTime((decimal)action.StartTime / 1000);
            vlcPlayer.SetSpeed((float)action.Speed);
            trackBar1.Value = (int)(action.StartTime);
            if (vlcPlayer.IsPlaying()) vlcPlayer.Pause();
            timer1.Stop();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            vlcPlayer.SetPlayTime(trackBar1.Value / 1000);
            timer1.Start();
        }
        private void ClearSelection()
        {
            for (int i = 0; i < dgv1.RowCount; i++)
            {
                dgv1.Rows[i].DefaultCellStyle.BackColor = Color.White;
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (vlcPlayer.IsPlaying())
            {
                if (vlcPlayer.IsPlaying()) vlcPlayer.Pause();
                timer1.Stop();
            }

            else
            {
                vlcPlayer.Play();
                timer1.Start();
            }
        }

        private void dgv1_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (vlcPlayer.IsPlaying())
            {
                if (vlcPlayer.IsPlaying()) vlcPlayer.Pause();
                timer1.Stop();
            }
        }
    }
}
