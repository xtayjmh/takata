﻿namespace Takata
{
    partial class FormEditSystemInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbxFtpPwd = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.tbxFtpUser = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.tbxFtpAddr = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.tbxDbName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.tbxDbPwd = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.tbxDbUser = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.tbxDbAddr = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.btnCancle = new DevComponents.DotNetBar.ButtonX();
            this.btnOk = new DevComponents.DotNetBar.ButtonX();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbxFtpPwd);
            this.groupBox1.Controls.Add(this.labelX3);
            this.groupBox1.Controls.Add(this.tbxFtpUser);
            this.groupBox1.Controls.Add(this.labelX2);
            this.groupBox1.Controls.Add(this.tbxFtpAddr);
            this.groupBox1.Controls.Add(this.labelX1);
            this.groupBox1.Location = new System.Drawing.Point(25, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(352, 125);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "FTP配置";
            this.groupBox1.Visible = false;
            // 
            // tbxFtpPwd
            // 
            // 
            // 
            // 
            this.tbxFtpPwd.Border.Class = "TextBoxBorder";
            this.tbxFtpPwd.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbxFtpPwd.Location = new System.Drawing.Point(105, 80);
            this.tbxFtpPwd.Name = "tbxFtpPwd";
            this.tbxFtpPwd.PasswordChar = '*';
            this.tbxFtpPwd.PreventEnterBeep = true;
            this.tbxFtpPwd.Size = new System.Drawing.Size(186, 21);
            this.tbxFtpPwd.TabIndex = 5;
            // 
            // labelX3
            // 
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Location = new System.Drawing.Point(38, 80);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(75, 23);
            this.labelX3.TabIndex = 4;
            this.labelX3.Text = "FTP密码：";
            // 
            // tbxFtpUser
            // 
            // 
            // 
            // 
            this.tbxFtpUser.Border.Class = "TextBoxBorder";
            this.tbxFtpUser.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbxFtpUser.Location = new System.Drawing.Point(105, 51);
            this.tbxFtpUser.Name = "tbxFtpUser";
            this.tbxFtpUser.PreventEnterBeep = true;
            this.tbxFtpUser.Size = new System.Drawing.Size(186, 21);
            this.tbxFtpUser.TabIndex = 3;
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Location = new System.Drawing.Point(38, 51);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(75, 23);
            this.labelX2.TabIndex = 2;
            this.labelX2.Text = "FTP账户：";
            // 
            // tbxFtpAddr
            // 
            // 
            // 
            // 
            this.tbxFtpAddr.Border.Class = "TextBoxBorder";
            this.tbxFtpAddr.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbxFtpAddr.Location = new System.Drawing.Point(105, 20);
            this.tbxFtpAddr.Name = "tbxFtpAddr";
            this.tbxFtpAddr.PreventEnterBeep = true;
            this.tbxFtpAddr.Size = new System.Drawing.Size(186, 21);
            this.tbxFtpAddr.TabIndex = 1;
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(38, 20);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 23);
            this.labelX1.TabIndex = 0;
            this.labelX1.Text = "FTP地址：";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonX1);
            this.groupBox2.Controls.Add(this.tbxDbName);
            this.groupBox2.Controls.Add(this.labelX7);
            this.groupBox2.Controls.Add(this.tbxDbPwd);
            this.groupBox2.Controls.Add(this.labelX4);
            this.groupBox2.Controls.Add(this.tbxDbUser);
            this.groupBox2.Controls.Add(this.labelX5);
            this.groupBox2.Controls.Add(this.tbxDbAddr);
            this.groupBox2.Controls.Add(this.labelX6);
            this.groupBox2.Location = new System.Drawing.Point(19, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(352, 137);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "数据库配置";
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Location = new System.Drawing.Point(298, 113);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Size = new System.Drawing.Size(40, 20);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.TabIndex = 1;
            this.buttonX1.Text = "测试";
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // tbxDbName
            // 
            // 
            // 
            // 
            this.tbxDbName.Border.Class = "TextBoxBorder";
            this.tbxDbName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbxDbName.Location = new System.Drawing.Point(106, 85);
            this.tbxDbName.Name = "tbxDbName";
            this.tbxDbName.PreventEnterBeep = true;
            this.tbxDbName.Size = new System.Drawing.Size(186, 21);
            this.tbxDbName.TabIndex = 11;
            // 
            // labelX7
            // 
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Location = new System.Drawing.Point(25, 83);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(90, 23);
            this.labelX7.TabIndex = 10;
            this.labelX7.Text = "数据库名称：";
            // 
            // tbxDbPwd
            // 
            // 
            // 
            // 
            this.tbxDbPwd.Border.Class = "TextBoxBorder";
            this.tbxDbPwd.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbxDbPwd.Location = new System.Drawing.Point(106, 112);
            this.tbxDbPwd.Name = "tbxDbPwd";
            this.tbxDbPwd.PasswordChar = '*';
            this.tbxDbPwd.PreventEnterBeep = true;
            this.tbxDbPwd.Size = new System.Drawing.Size(186, 21);
            this.tbxDbPwd.TabIndex = 11;
            // 
            // labelX4
            // 
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Location = new System.Drawing.Point(60, 110);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(75, 23);
            this.labelX4.TabIndex = 10;
            this.labelX4.Text = "密码：";
            // 
            // tbxDbUser
            // 
            // 
            // 
            // 
            this.tbxDbUser.Border.Class = "TextBoxBorder";
            this.tbxDbUser.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbxDbUser.Location = new System.Drawing.Point(106, 58);
            this.tbxDbUser.Name = "tbxDbUser";
            this.tbxDbUser.PreventEnterBeep = true;
            this.tbxDbUser.Size = new System.Drawing.Size(186, 21);
            this.tbxDbUser.TabIndex = 9;
            // 
            // labelX5
            // 
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Location = new System.Drawing.Point(48, 56);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(61, 23);
            this.labelX5.TabIndex = 8;
            this.labelX5.Text = "用户名：";
            // 
            // tbxDbAddr
            // 
            // 
            // 
            // 
            this.tbxDbAddr.Border.Class = "TextBoxBorder";
            this.tbxDbAddr.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbxDbAddr.Location = new System.Drawing.Point(106, 27);
            this.tbxDbAddr.Name = "tbxDbAddr";
            this.tbxDbAddr.PreventEnterBeep = true;
            this.tbxDbAddr.Size = new System.Drawing.Size(186, 21);
            this.tbxDbAddr.TabIndex = 7;
            // 
            // labelX6
            // 
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Location = new System.Drawing.Point(25, 27);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(89, 23);
            this.labelX6.TabIndex = 6;
            this.labelX6.Text = "数据库地址：";
            // 
            // btnCancle
            // 
            this.btnCancle.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancle.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCancle.Location = new System.Drawing.Point(240, 164);
            this.btnCancle.Name = "btnCancle";
            this.btnCancle.Size = new System.Drawing.Size(75, 23);
            this.btnCancle.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCancle.TabIndex = 0;
            this.btnCancle.Text = "取 消";
            this.btnCancle.Click += new System.EventHandler(this.btnCancle_Click);
            // 
            // btnOk
            // 
            this.btnOk.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnOk.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnOk.Location = new System.Drawing.Point(125, 164);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "确 定";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // FormEditSystemInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 217);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancle);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "FormEditSystemInfo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "系统配置";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevComponents.DotNetBar.ButtonX btnCancle;
        private DevComponents.DotNetBar.ButtonX btnOk;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.TextBoxX tbxFtpAddr;
        private DevComponents.DotNetBar.Controls.TextBoxX tbxFtpPwd;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.TextBoxX tbxFtpUser;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.TextBoxX tbxDbPwd;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.TextBoxX tbxDbUser;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.TextBoxX tbxDbAddr;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.Controls.TextBoxX tbxDbName;
        private DevComponents.DotNetBar.LabelX labelX7;
    }
}