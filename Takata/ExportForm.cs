﻿using System.Windows.Forms;
using DAL.SystemManage;
using DevComponents.DotNetBar;
using Model;
using Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using DAL;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using Microsoft.Office.Interop.Excel;
using ExcelApplication = Microsoft.Office.Interop.Excel.Application;

namespace Takata
{
    public partial class ExportForm : Office2007Form
    {
        public bool IsUpdated;
        private ButtonX btnCancel;
        private ButtonX btnOk;
        private LabelX labelX1;
        private LabelX labelX2;
        private LabelX labelX3;
        private LabelX labelX4;
        private DevComponents.DotNetBar.Controls.TextBoxX tbxWorker;
        private LabelX labelX6;
        private int _ReportType;
        private int _VideoID;
        private t_Video _Video;
        public ExportForm()
        {
            InitializeComponent();
        }

        public ExportForm(int ReportType, int VideoID)
        {
            InitializeComponent();
            _ReportType = ReportType;
            _VideoID = VideoID;
        }

        private void LoadWorkStepData()
        {
            var lists = WorkStepManage.GetWorkSteps(int.Parse(_Video.PartNo));
            cbWorkstep.DataSource = lists;
            cbWorkstep.ValueMember = "WorkStepId";
            cbWorkstep.DisplayMember = "Name";
            if (_Video != null && _Video.WorkStepId.HasValue)
            {
                cbWorkstep.SelectedValue = _Video.WorkStepId;
            }
        }

        private void btnOk_Click(object sender, System.EventArgs e)
        {
            try
            {
                var user = Program.CurrentUser;
                var Workshop = txtWorkShop.Text.Trim();
                var Workline = txtWorLine.Text.Trim();
                var Workstep = cbWorkstep.Text.Trim();
                var ProductName = tbxProductName.Text.Trim();
                var Worker = tbxWorker.Text.Trim();
                var Cartype = txtCartype.Text.Trim();

                if (string.IsNullOrEmpty(Workstep))
                {
                    MessageBox.Show("工序不能为空", "提示");
                }
                else if (string.IsNullOrEmpty(Worker))
                {
                    MessageBox.Show("工作人员不能为空", "提示");
                }
                else
                {
                    btnOk.Enabled = false;//防止多次点击

                    _Video.WorkMan = Worker;
                    _Video.WorkStepId = int.Parse(cbWorkstep.SelectedValue.ToString());
                    _Video.Researcher = user.UserName;
                    Common.AddOrUpdateObject(_Video);

                    var videos = Common.GetList<t_Video>(r => r.PartNo == _Video.PartNo && r.WorkStepId.HasValue);
                    List<string> reportPath = new List<string>();

                    switch (_ReportType)
                    {
                        case 1:
                            var modelList = new List<ImproveReportModel>();
                            foreach (var v in videos)
                            {
                                var prodId = 0;
                                int.TryParse(v.PartNo, out prodId);
                                var prodModel = Common.GetSingleModel<t_Prod>(i => i.ProdId == prodId);
                                if (prodModel == null) continue;
                                var workLineModel = Common.GetSingleModel<t_WorkLine>(i => i.WorkLineId == v.WorkLineId);
                                var cartype = Common.GetSingleModel<t_CarType>(i => i.TypeId == v.CarTypeId);
                                var workShop = Common.GetSingleModel<t_WorkShop>(i => i.WorkShopId == v.WorkShopID);
                                t_Factory factory = null;
                                if (workShop != null)
                                    factory = Common.GetSingleModel<t_Factory>(i => i.FactoryId == workShop.FactoryId);
                                var stepModel = Common.GetSingleModel<t_WorkStep>(r => r.WorkStepId == v.WorkStepId);
                                var listAction = ActionManage.GetActions(v.VideoId).OrderBy(i => i.StartTime).ToList();
                                listAction = listAction.Take(listAction.Count - 1).ToList();//去掉最后一行
                                var strWorkStep = stepModel != null ? stepModel.Name : "";
                                var strWorkDepart = (factory != null ? factory.Name : "") + " " + (workShop != null ? workShop.Name : "");
                                var headerModel = new ImproveReportModel.HeaderInfoModel()
                                {
                                    ActionName = strWorkStep,
                                    CarType = cartype != null ? cartype.TypeName : "",
                                    ProductName = v.ProdName,
                                    WorkDepart = strWorkDepart,
                                    WorkLine = workLineModel != null ? workLineModel.Name : "",
                                    Researcher = v.Researcher,
                                    WorkMan = v.WorkMan,
                                    CreateDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
                                };
                                var dataList = new System.Collections.Generic.List<ImproveReportModel.DataBodyModel>();
                                int index = 1;
                                foreach (var item in listAction)
                                {
                                    if (item.TypeId != 3)
                                    {
                                        var actoinTypeModel = Common.GetSingleModel<t_ActionType>(t => t.ActionTypeId == item.ActionTypeId);
                                        var st = (item.ActionTime == null ? 0 : item.ActionTime / 1000) / (item.Speed == 0 ? 1 : item.Speed);
                                        st = item.AutoStandardTime / 1000;
                                        var databody = new ImproveReportModel.DataBodyModel()
                                        {
                                            Index = index,
                                            Before_ActionDes = item.ActionName,
                                            Before_StandTime = item.ActionTime == null ? 0 : item.ActionTime / 1000,
                                            After_ActionDes = item.ActionName,
                                            After_Time = decimal.Parse(st.Value.ToString(3)),
                                            Anaysis_ActionType = actoinTypeModel == null ? "" : actoinTypeModel.ChineseName,
                                            Anaysis_IsValid = item.TypeId == 2 ? "有效" : "无效",
                                            Anaysis_ECRS = item.TypeId == 1 ? "取消" : (item.AutoStandardTime < item.ActionTime || item.ManualStandardTime < item.ActionTime) ? "简化" : "-"//只要S.T小于动作时间就表示已经手动调整过时间了，而且只会调成一个比较小的时间，这样就显示简化2018-6-7
                                        };
                                        if (item.TypeId == 1 || item.AutoStandardTime < item.ActionTime || item.ManualStandardTime < item.ActionTime)
                                        {
                                            databody.After_Instruction = item.Description;
                                        }
                                        else
                                        {
                                            databody.After_Instruction = "-";
                                        }
                                        if (databody.Anaysis_IsValid == "无效")
                                        {
                                            databody.After_Time = 0;
                                        }
                                        index++;
                                        dataList.Add(databody);
                                    }
                                }

                                var model = new ImproveReportModel
                                {
                                    workStepId = (int)v.WorkStepId,
                                    HeaderModel = headerModel,
                                    SheetName = strWorkStep,
                                    Title = strWorkStep + "动作分析",
                                    DataList = dataList
                                };
                                modelList.Add(model);
                                prodModel.HasImproveReport = 1;
                                Common.AddOrUpdateObject(prodModel);
                            }
                            modelList = modelList.OrderBy(m => m.workStepId).ToList(); //added by Ted for order workstep on 20180620
                            //SaveFileDialog sfd = new SaveFileDialog();
                            //sfd.Filter = @"Excel文件|*.xlsx";
                            //sfd.ShowDialog();

                            var NewFilePath = Environment.CurrentDirectory + "\\Resource\\" + _Video.PartNo + "\\3\\";
                            var newFileName = "改善报表_" + Workline + "_" + Cartype + "_"  + ProductName + ".xlsx";
                            FileHelper.FolderCreate(NewFilePath);
                            reportPath.Add(NewFilePath + newFileName);
                            ExcelHelper.ImproveReport(modelList, NewFilePath + newFileName);
                            AdjustColumnWidth(NewFilePath + newFileName);
                            //Process.Start(NewFilePath + newFileName);
                            break;
                        case 2:
                            var modelListHumman = new List<HumanMatchingModel>();
                            foreach (var v in videos)
                            {
                                var prodId = 0;
                                int.TryParse(v.PartNo, out prodId);
                                var prodModel = Common.GetSingleModel<t_Prod>(i => i.ProdId == prodId);
                                if (prodModel == null) continue;
                                var workLineModel = Common.GetSingleModel<t_WorkLine>(i => i.WorkLineId == v.WorkLineId);
                                var cartype = Common.GetSingleModel<t_CarType>(i => i.TypeId == v.CarTypeId);
                                var workShop = Common.GetSingleModel<t_WorkShop>(i => i.WorkShopId == v.WorkShopID);
                                t_Factory factory = null;
                                if (workShop != null)
                                    factory = Common.GetSingleModel<t_Factory>(i => i.FactoryId == workShop.FactoryId);
                                var stepModel = Common.GetSingleModel<t_WorkStep>(r => r.WorkStepId == v.WorkStepId);
                                var listAction = ActionManage.GetActions(v.VideoId).OrderBy(i => i.StartTime).ToList();
                                listAction = listAction.Take(listAction.Count - 1).ToList();//去掉最后一行
                                var strWorkStep = stepModel != null ? stepModel.Name : "";
                                var strWorkDepart = (factory != null ? factory.Name : "") + " " + (workShop != null ? workShop.Name : "");
                                var headerModel2 = new HumanMatchingModel.HeaderInfoModel()
                                {
                                    ActionName = strWorkStep,
                                    ProductName = v.ProdName,
                                    CarType = cartype != null ? cartype.TypeName : "",
                                    WorkDepart = strWorkDepart,
                                    WorkLine = workLineModel != null ? workLineModel.Name : "",
                                    Researcher = v.Researcher,
                                    WorkMan = v.WorkMan,
                                    CreateDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
                                };
                                var dataList2 = new List<HumanMatchingModel.DataBodyModel>();
                                int index2 = 1;
                                foreach (var item in listAction)
                                {
                                    if (item.TypeId != 3)
                                    {
                                        var actionTypeModel = Common.GetSingleModel<t_ActionType>(i => i.ActionTypeId == item.ActionTypeId);
                                        if (actionTypeModel == null) continue;
                                        var stTime = (item.ActionTime == null ? 0 : item.ActionTime / 1000) / (item.Speed == 0 ? 1 : item.Speed);
                                        stTime = item.AutoStandardTime / 1000;
                                        var databody = new HumanMatchingModel.DataBodyModel()
                                        {
                                            Index = index2,
                                            TypeId = item.TypeId.Value,
                                            Operator_ActionDes = item.ActionName,
                                            Operator_StandTime = decimal.Parse(stTime.Value.ToString(3)),

                                            Matching_Time = item.EquipmentTime ?? 0,
                                            Operator_ActionType = Common.GetSingleModel<t_ActionType>(t => t.ActionTypeId == item.ActionTypeId).ChineseName,
                                            Matching_Status = (item.EquipmentTime == null || item.EquipmentTime == 0) ? HumanMatchingModel.Matching_StatusEnum.Free : HumanMatchingModel.Matching_StatusEnum.Running,
                                            Matching_ActionDes = item.ActionDes
                                        };
                                        if (actionTypeModel.Rank != null) databody.Operator_Status = (HumanMatchingModel.Operator_StatusEnum)actionTypeModel.Rank;
                                        if (item.TypeId == 1)
                                        {
                                            databody.Operator_StandTime = 0;
                                        }

                                        index2++;
                                        dataList2.Add(databody);
                                    }
                                }

                                var model2 = new HumanMatchingModel
                                {
                                    workStepId = (int)v.WorkStepId,
                                    HeaderModel = headerModel2,
                                    SheetName = strWorkStep,
                                    Title = strWorkStep + "人-机分析",
                                    DataList = dataList2
                                };
                                if (dataList2.Count > 0)
                                {
                                    model2.Summary.Operater_FreeTime = dataList2.Where(r => r.Operator_Status == HumanMatchingModel.Operator_StatusEnum.Third).Sum(r => r.Operator_StandTime)?.ToString("F2");
                                    model2.Summary.Operater_ActionTime = dataList2.Where(r => r.Operator_Status != HumanMatchingModel.Operator_StatusEnum.Third && r.TypeId != 1).Sum(r => r.Operator_StandTime)?.ToString("F2");
                                    model2.Summary.Operater_Rate = dataList2.Sum(r => r.Operator_StandTime) == 0 ? "0" : (decimal.Parse(model2.Summary.Operater_ActionTime ?? "0") / dataList2.Sum(r => r.Operator_StandTime))?.ToString("P2");

                                    model2.Summary.Matching_ActionTime = dataList2.Sum(r => r.Matching_Time)?.ToString("F2");
                                    model2.Summary.Matching_FreeTime = (dataList2.Sum(r => r.Operator_StandTime) - dataList2.Sum(r => r.Matching_Time))?.ToString("F2");
                                    model2.Summary.Matching_Rate = dataList2.Sum(r => r.Operator_StandTime) == 0 ? "0" : (dataList2.Sum(r => r.Matching_Time) / dataList2.Sum(r => r.Operator_StandTime))?.ToString("P2");
                                }

                                modelListHumman.Add(model2);

                                prodModel.HasHumanMachineReport = 1;
                                Common.AddOrUpdateObject(prodModel);
                            }
                            modelListHumman = modelListHumman.OrderBy(m => m.workStepId).ToList(); //added by Ted for order workstep on 20180620
                            //SaveFileDialog sfd2 = new SaveFileDialog();
                            //sfd2.Filter = @"Excel文件|*.xlsx";
                            //sfd2.ShowDialog();

                            var filePath = Environment.CurrentDirectory + "\\Resource\\" + _Video.PartNo + "\\2\\";
                            var fileName = "人-机操作分析报表_" + Workline + "_" + Cartype + "_"  + ProductName + ".xlsx";
                            var fileNameAfter = "人-机操作分析报表_" + Workline + "_" + Cartype + "_"  + ProductName + "_" + "改善后.xlsx";
                            FileHelper.FolderCreate(filePath);
                            reportPath.Add(filePath + fileName);
                            reportPath.Add(filePath + fileNameAfter);
                            ExcelHelper.HumanMatchingReport(modelListHumman, filePath + fileName);
                            ExcelHelper.HumanMatchingReport(modelListHumman, filePath + fileNameAfter);
                            //Process.Start(filePath + fileName);
                            //Process.Start(filePath + fileNameAfter);
                            break;
                    }
                    if (Program.CurrentZone != null)
                    {
                        var ip = Program.CurrentZone.FTPAddress;
                        var ftpuser = Program.CurrentZone.FTPUserName;
                        var pwd = Program.CurrentZone.FTPPassword;
                        var port = (int)Program.CurrentZone.FTPPort;
                        FtpHelper ftpop = new FtpHelper(ip, port, ftpuser, pwd);
                        ftpop.UploadExcelFile(reportPath);
                    }
                    foreach (var item in reportPath)
                    {
                        Process.Start(item);
                    }


                    Close();
                }
            }
            catch (Exception exx)
            {
                var st = exx.StackTrace;
                Common.ErrorLog(st);
                MessageBox.Show(exx.Message, "错误提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            Close();
        }


        private void InitializeComponent()
        {
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.btnOk = new DevComponents.DotNetBar.ButtonX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.tbxWorker = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.cbWorkstep = new System.Windows.Forms.ComboBox();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.tbxProductName = new DevComponents.DotNetBar.LabelX();
            this.txtFactory = new DevComponents.DotNetBar.LabelX();
            this.txtWorkShop = new DevComponents.DotNetBar.LabelX();
            this.txtWorLine = new DevComponents.DotNetBar.LabelX();
            this.txtCartype = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCancel.Location = new System.Drawing.Point(289, 232);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "取消";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnOk.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnOk.Location = new System.Drawing.Point(94, 232);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnOk.TabIndex = 8;
            this.btnOk.Text = "确定";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(24, 49);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(64, 23);
            this.labelX1.TabIndex = 11;
            this.labelX1.Text = "生产车间";
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Location = new System.Drawing.Point(36, 78);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(52, 23);
            this.labelX2.TabIndex = 13;
            this.labelX2.Text = "生产线";
            // 
            // labelX3
            // 
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Location = new System.Drawing.Point(28, 135);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(57, 23);
            this.labelX3.TabIndex = 17;
            this.labelX3.Text = "产品名称";
            // 
            // labelX4
            // 
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Location = new System.Drawing.Point(30, 164);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(57, 23);
            this.labelX4.TabIndex = 15;
            this.labelX4.Text = "作业名称";
            // 
            // tbxWorker
            // 
            // 
            // 
            // 
            this.tbxWorker.Border.Class = "TextBoxBorder";
            this.tbxWorker.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbxWorker.Location = new System.Drawing.Point(94, 196);
            this.tbxWorker.Name = "tbxWorker";
            this.tbxWorker.PreventEnterBeep = true;
            this.tbxWorker.Size = new System.Drawing.Size(124, 21);
            this.tbxWorker.TabIndex = 18;
            // 
            // labelX6
            // 
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Location = new System.Drawing.Point(29, 194);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(56, 23);
            this.labelX6.TabIndex = 19;
            this.labelX6.Text = "工作人员";
            // 
            // cbWorkstep
            // 
            this.cbWorkstep.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWorkstep.FormattingEnabled = true;
            this.cbWorkstep.Location = new System.Drawing.Point(94, 167);
            this.cbWorkstep.Name = "cbWorkstep";
            this.cbWorkstep.Size = new System.Drawing.Size(270, 20);
            this.cbWorkstep.TabIndex = 22;
            // 
            // labelX5
            // 
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Location = new System.Drawing.Point(49, 20);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(39, 23);
            this.labelX5.TabIndex = 23;
            this.labelX5.Text = "工厂";
            // 
            // tbxProductName
            // 
            // 
            // 
            // 
            this.tbxProductName.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbxProductName.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbxProductName.FontBold = true;
            this.tbxProductName.Location = new System.Drawing.Point(94, 135);
            this.tbxProductName.Name = "tbxProductName";
            this.tbxProductName.Size = new System.Drawing.Size(270, 23);
            this.tbxProductName.TabIndex = 28;
            // 
            // txtFactory
            // 
            // 
            // 
            // 
            this.txtFactory.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtFactory.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtFactory.FontBold = true;
            this.txtFactory.Location = new System.Drawing.Point(94, 19);
            this.txtFactory.Name = "txtFactory";
            this.txtFactory.Size = new System.Drawing.Size(270, 23);
            this.txtFactory.TabIndex = 29;
            // 
            // txtWorkShop
            // 
            // 
            // 
            // 
            this.txtWorkShop.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtWorkShop.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtWorkShop.FontBold = true;
            this.txtWorkShop.Location = new System.Drawing.Point(94, 49);
            this.txtWorkShop.Name = "txtWorkShop";
            this.txtWorkShop.Size = new System.Drawing.Size(270, 23);
            this.txtWorkShop.TabIndex = 30;
            // 
            // txtWorLine
            // 
            // 
            // 
            // 
            this.txtWorLine.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtWorLine.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtWorLine.FontBold = true;
            this.txtWorLine.Location = new System.Drawing.Point(94, 78);
            this.txtWorLine.Name = "txtWorLine";
            this.txtWorLine.Size = new System.Drawing.Size(270, 23);
            this.txtWorLine.TabIndex = 31;
            // 
            // txtCartype
            // 
            // 
            // 
            // 
            this.txtCartype.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtCartype.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtCartype.FontBold = true;
            this.txtCartype.Location = new System.Drawing.Point(94, 107);
            this.txtCartype.Name = "txtCartype";
            this.txtCartype.Size = new System.Drawing.Size(270, 23);
            this.txtCartype.TabIndex = 33;
            // 
            // labelX8
            // 
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Location = new System.Drawing.Point(50, 107);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(31, 23);
            this.labelX8.TabIndex = 32;
            this.labelX8.Text = "车种";
            // 
            // ExportForm
            // 
            this.ClientSize = new System.Drawing.Size(391, 280);
            this.Controls.Add(this.txtCartype);
            this.Controls.Add(this.labelX8);
            this.Controls.Add(this.txtWorLine);
            this.Controls.Add(this.txtWorkShop);
            this.Controls.Add(this.txtFactory);
            this.Controls.Add(this.tbxProductName);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.cbWorkstep);
            this.Controls.Add(this.tbxWorker);
            this.Controls.Add(this.labelX6);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ExportForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.TitleText = "导出报表";
            this.Load += new System.EventHandler(this.ExportForm_Load);
            this.ResumeLayout(false);

        }

        private void ExportForm_Load(object sender, EventArgs e)
        {

            _Video = Common.GetSingleModel<t_Video>(i => i.VideoId == _VideoID);
            tbxProductName.Text = _Video.ProdName;
            txtCartype.Text = _Video.CarTypeName;
            var workLine = Common.GetSingleModel<t_WorkLine>(i => i.WorkLineId == _Video.WorkLineId);
            var workShop = Common.GetSingleModel<t_WorkShop>(i => i.WorkShopId == _Video.WorkShopID);
            var factory = Common.GetSingleModel<t_Factory>(i => i.FactoryId == workShop.FactoryId);
            txtFactory.Text = factory.Name;
            txtWorkShop.Text = workShop.Name;
            txtWorLine.Text = workLine.Name;
            
            LoadWorkStepData();
        }

        //自动调整列宽
        private void AdjustColumnWidth(string filePath)
        {
            var excel = new ExcelApplication();
            try
            {
                Workbook xBook = excel.Workbooks.Open(filePath, Missing.Value, Missing.Value,
                    Missing.Value, Missing.Value, Missing.Value, Missing.Value
                    , Missing.Value, Missing.Value, Missing.Value, Missing.Value
                    , Missing.Value, Missing.Value, Missing.Value, Missing.Value);


                excel.Visible = false; // 不显示 Excel 文件,如果为 true 则显示 Excel 文件
                var sheet = (Worksheet)xBook.Sheets[xBook.Sheets.Count];//最后一个

                Range r = null;
                r = sheet.get_Range("A1:Z5");
                r.Columns.AutoFit();
                xBook.Save();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                excel.Quit();
                excel = null;
            }
        }
    }
}
