﻿using System.Drawing;

namespace Takata
{
    partial class FormAPlay
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAPlay));
            this.styleManager1 = new DevComponents.DotNetBar.StyleManager(this.components);
            this.styleManager2 = new DevComponents.DotNetBar.StyleManager(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tSBtn_play = new System.Windows.Forms.ToolStripButton();
            this.tsBtnPause = new System.Windows.Forms.ToolStripButton();
            this.tSBtn_stop = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.DDBtnStandardTime = new System.Windows.Forms.ToolStripDropDownButton();
            this.自动标准时间ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.手动标准时间ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tSBtn_openfile = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.tSB_backward = new System.Windows.Forms.ToolStripButton();
            this.tSB_forward = new System.Windows.Forms.ToolStripButton();
            this.tSDDBtnSpeed = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.tSDDBtnStep = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnCancelLoop = new System.Windows.Forms.ToolStripButton();
            this.btnSetLoopStart = new System.Windows.Forms.ToolStripButton();
            this.btnSetLoopEnd = new System.Windows.Forms.ToolStripButton();
            this.tsBtnSign = new System.Windows.Forms.ToolStripButton();
            this.tsBtnSave = new System.Windows.Forms.ToolStripButton();
            this.tsBtnDelete = new System.Windows.Forms.ToolStripButton();
            this.buttonItem4 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem5 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem6 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem7 = new DevComponents.DotNetBar.ButtonItem();
            this.qatCustomizeItem1 = new DevComponents.DotNetBar.QatCustomizeItem();
            this.ribbonTabItem1 = new DevComponents.DotNetBar.RibbonTabItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.clbDFL = new System.Windows.Forms.CheckedListBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnCZLX = new System.Windows.Forms.Button();
            this.btnZHBZ = new System.Windows.Forms.Button();
            this.btnDgbz = new System.Windows.Forms.Button();
            this.tbxSDSJ = new System.Windows.Forms.TextBox();
            this.cbZQ = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cbSD = new System.Windows.Forms.ComboBox();
            this.cbCZMC = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbxDM = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbxDZMC = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbVideoTime = new System.Windows.Forms.TextBox();
            this.tbxBJMS = new System.Windows.Forms.TextBox();
            this.tbxAQZYSX = new System.Windows.Forms.TextBox();
            this.tbxPZZYSX = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActionName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OperationName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cycle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DisplayActionTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DisplayManualStandardTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.速度 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.类型 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TypeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActionTypeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StartTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EndTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TIME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lbProcess = new DevComponents.DotNetBar.LabelX();
            this.timerAutoLoadVideo = new System.Windows.Forms.Timer(this.components);
            this.bgWUploadVideo = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.lblFileName = new System.Windows.Forms.Label();
            this.vlcPlayer = new AxAPlayer3Lib.AxPlayer();
            this.cbLX = new Takata.ColoredComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlcPlayer)).BeginInit();
            this.SuspendLayout();
            // 
            // styleManager1
            // 
            this.styleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2010Blue;
            this.styleManager1.MetroColorParameters = new DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.White, System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154))))));
            // 
            // styleManager2
            // 
            this.styleManager2.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2010Blue;
            this.styleManager2.MetroColorParameters = new DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.White, System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154))))));
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // timer1
            // 
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // trackBar1
            // 
            this.trackBar1.AutoSize = false;
            this.trackBar1.LargeChange = 1;
            this.trackBar1.Location = new System.Drawing.Point(7, 747);
            this.trackBar1.Maximum = 100;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(1069, 25);
            this.trackBar1.TabIndex = 6;
            this.trackBar1.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.CanOverflow = false;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tSBtn_play,
            this.tsBtnPause,
            this.tSBtn_stop,
            this.toolStripButton3,
            this.DDBtnStandardTime,
            this.tSBtn_openfile,
            this.toolStripSeparator3,
            this.toolStripLabel1,
            this.tSB_backward,
            this.tSB_forward,
            this.tSDDBtnSpeed,
            this.toolStripSeparator1,
            this.toolStripLabel2,
            this.toolStripButton1,
            this.toolStripButton2,
            this.tSDDBtnStep,
            this.toolStripSeparator2,
            this.btnCancelLoop,
            this.btnSetLoopStart,
            this.btnSetLoopEnd,
            this.tsBtnSign,
            this.tsBtnSave,
            this.tsBtnDelete});
            this.toolStrip1.Location = new System.Drawing.Point(1, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip1.Size = new System.Drawing.Size(980, 33);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tSBtn_play
            // 
            this.tSBtn_play.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.tSBtn_play.Image = global::Takata.Properties.Resources.tSBtn_play_Image;
            this.tSBtn_play.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSBtn_play.Name = "tSBtn_play";
            this.tSBtn_play.Padding = new System.Windows.Forms.Padding(0, 1, 0, 2);
            this.tSBtn_play.Size = new System.Drawing.Size(57, 30);
            this.tSBtn_play.Text = "播放";
            this.tSBtn_play.Click += new System.EventHandler(this.tSBtn_play_ButtonClick);
            // 
            // tsBtnPause
            // 
            this.tsBtnPause.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.tsBtnPause.Image = global::Takata.Properties.Resources.btnpause;
            this.tsBtnPause.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnPause.Name = "tsBtnPause";
            this.tsBtnPause.Size = new System.Drawing.Size(57, 30);
            this.tsBtnPause.Text = "暂停";
            this.tsBtnPause.Click += new System.EventHandler(this.tsBtnPause_Click);
            // 
            // tSBtn_stop
            // 
            this.tSBtn_stop.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.tSBtn_stop.Image = global::Takata.Properties.Resources.tSBtn_stop_Image;
            this.tSBtn_stop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSBtn_stop.Name = "tSBtn_stop";
            this.tSBtn_stop.Size = new System.Drawing.Size(85, 30);
            this.tSBtn_stop.Text = "回到开始";
            this.tSBtn_stop.Click += new System.EventHandler(this.tSBtn_stop_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.toolStripButton3.Image = global::Takata.Properties.Resources.toolStripButton3_Image;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(85, 30);
            this.toolStripButton3.Text = "跳到最后";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // DDBtnStandardTime
            // 
            this.DDBtnStandardTime.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.DDBtnStandardTime.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.自动标准时间ToolStripMenuItem,
            this.手动标准时间ToolStripMenuItem});
            this.DDBtnStandardTime.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.DDBtnStandardTime.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.DDBtnStandardTime.Name = "DDBtnStandardTime";
            this.DDBtnStandardTime.Size = new System.Drawing.Size(106, 30);
            this.DDBtnStandardTime.Text = "自动标准时间";
            this.DDBtnStandardTime.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStripDropDownButton1_DropDownItemClicked_1);
            // 
            // 自动标准时间ToolStripMenuItem
            // 
            this.自动标准时间ToolStripMenuItem.Name = "自动标准时间ToolStripMenuItem";
            this.自动标准时间ToolStripMenuItem.Size = new System.Drawing.Size(162, 24);
            this.自动标准时间ToolStripMenuItem.Text = "自动标准时间";
            // 
            // 手动标准时间ToolStripMenuItem
            // 
            this.手动标准时间ToolStripMenuItem.Name = "手动标准时间ToolStripMenuItem";
            this.手动标准时间ToolStripMenuItem.Size = new System.Drawing.Size(162, 24);
            this.手动标准时间ToolStripMenuItem.Text = "手动标准时间";
            // 
            // tSBtn_openfile
            // 
            this.tSBtn_openfile.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.tSBtn_openfile.Image = global::Takata.Properties.Resources.tSBtn_openfile_Image;
            this.tSBtn_openfile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSBtn_openfile.Name = "tSBtn_openfile";
            this.tSBtn_openfile.Size = new System.Drawing.Size(57, 30);
            this.tSBtn_openfile.Text = "打开";
            this.tSBtn_openfile.Click += new System.EventHandler(this.tSBtn_openfile_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 33);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(51, 30);
            this.toolStripLabel1.Text = "速度：";
            // 
            // tSB_backward
            // 
            this.tSB_backward.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tSB_backward.Image = global::Takata.Properties.Resources.tSB_backward_Image;
            this.tSB_backward.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSB_backward.Name = "tSB_backward";
            this.tSB_backward.Size = new System.Drawing.Size(23, 30);
            this.tSB_backward.Text = "后退";
            this.tSB_backward.Click += new System.EventHandler(this.tSB_backward_Click);
            // 
            // tSB_forward
            // 
            this.tSB_forward.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tSB_forward.Image = global::Takata.Properties.Resources.tSB_forward_Image;
            this.tSB_forward.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSB_forward.Name = "tSB_forward";
            this.tSB_forward.Size = new System.Drawing.Size(23, 30);
            this.tSB_forward.Text = "快进";
            this.tSB_forward.Click += new System.EventHandler(this.tSB_forward_Click);
            // 
            // tSDDBtnSpeed
            // 
            this.tSDDBtnSpeed.AutoSize = false;
            this.tSDDBtnSpeed.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tSDDBtnSpeed.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.tSDDBtnSpeed.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSDDBtnSpeed.Name = "tSDDBtnSpeed";
            this.tSDDBtnSpeed.Size = new System.Drawing.Size(40, 22);
            this.tSDDBtnSpeed.Text = "1.00";
            this.tSDDBtnSpeed.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStripDropDownButton1_DropDownItemClicked);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 33);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(51, 30);
            this.toolStripLabel2.Text = "步进：";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = global::Takata.Properties.Resources.toolStripButton1_Image;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 30);
            this.toolStripButton1.Text = "回放";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            this.toolStripButton1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.toolStripButton2_MouseDown);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = global::Takata.Properties.Resources.toolStripButton2_Image;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 30);
            this.toolStripButton2.Text = "快放";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // tSDDBtnStep
            // 
            this.tSDDBtnStep.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tSDDBtnStep.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4,
            this.toolStripMenuItem5,
            this.toolStripMenuItem6,
            this.toolStripMenuItem7,
            this.toolStripMenuItem8,
            this.toolStripMenuItem9,
            this.toolStripMenuItem10});
            this.tSDDBtnStep.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.tSDDBtnStep.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSDDBtnStep.Name = "tSDDBtnStep";
            this.tSDDBtnStep.Size = new System.Drawing.Size(30, 30);
            this.tSDDBtnStep.Text = "1";
            this.tSDDBtnStep.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tSDDBtnStep_DropDownItemClicked);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(94, 24);
            this.toolStripMenuItem2.Text = "1";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(94, 24);
            this.toolStripMenuItem3.Text = "2";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(94, 24);
            this.toolStripMenuItem4.Text = "3";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(94, 24);
            this.toolStripMenuItem5.Text = "4";
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(94, 24);
            this.toolStripMenuItem6.Text = "5";
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(94, 24);
            this.toolStripMenuItem7.Text = "10";
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(94, 24);
            this.toolStripMenuItem8.Text = "15";
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(94, 24);
            this.toolStripMenuItem9.Text = "20";
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(94, 24);
            this.toolStripMenuItem10.Text = "25";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 33);
            // 
            // btnCancelLoop
            // 
            this.btnCancelLoop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCancelLoop.Enabled = false;
            this.btnCancelLoop.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.btnCancelLoop.Image = global::Takata.Properties.Resources.btnCancelLoop_Image;
            this.btnCancelLoop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCancelLoop.Name = "btnCancelLoop";
            this.btnCancelLoop.Size = new System.Drawing.Size(23, 30);
            this.btnCancelLoop.Text = "取消循环";
            this.btnCancelLoop.Click += new System.EventHandler(this.btnCancelLoop_Click);
            // 
            // btnSetLoopStart
            // 
            this.btnSetLoopStart.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSetLoopStart.Enabled = false;
            this.btnSetLoopStart.Image = global::Takata.Properties.Resources.btnSetLoopStart_Image;
            this.btnSetLoopStart.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSetLoopStart.Name = "btnSetLoopStart";
            this.btnSetLoopStart.Size = new System.Drawing.Size(23, 30);
            this.btnSetLoopStart.Text = "循环开始点";
            this.btnSetLoopStart.Click += new System.EventHandler(this.btnSetLoopStart_Click);
            // 
            // btnSetLoopEnd
            // 
            this.btnSetLoopEnd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSetLoopEnd.Enabled = false;
            this.btnSetLoopEnd.Image = global::Takata.Properties.Resources.btnSetLoopEnd_Image;
            this.btnSetLoopEnd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSetLoopEnd.Name = "btnSetLoopEnd";
            this.btnSetLoopEnd.Size = new System.Drawing.Size(23, 30);
            this.btnSetLoopEnd.Text = "循环结束点";
            this.btnSetLoopEnd.Click += new System.EventHandler(this.btnSetLoopEnd_Click);
            // 
            // tsBtnSign
            // 
            this.tsBtnSign.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnSign.Enabled = false;
            this.tsBtnSign.Image = global::Takata.Properties.Resources.q4;
            this.tsBtnSign.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnSign.Name = "tsBtnSign";
            this.tsBtnSign.Size = new System.Drawing.Size(23, 30);
            this.tsBtnSign.Text = "动作标记";
            this.tsBtnSign.Click += new System.EventHandler(this.tsBtnSign_Click);
            // 
            // tsBtnSave
            // 
            this.tsBtnSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnSave.Enabled = false;
            this.tsBtnSave.Image = global::Takata.Properties.Resources.q6;
            this.tsBtnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnSave.Name = "tsBtnSave";
            this.tsBtnSave.Size = new System.Drawing.Size(23, 30);
            this.tsBtnSave.Text = "保存标记";
            this.tsBtnSave.Click += new System.EventHandler(this.tsBtnSave_Click);
            // 
            // tsBtnDelete
            // 
            this.tsBtnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnDelete.Enabled = false;
            this.tsBtnDelete.Image = global::Takata.Properties.Resources.q5;
            this.tsBtnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnDelete.Name = "tsBtnDelete";
            this.tsBtnDelete.Size = new System.Drawing.Size(23, 30);
            this.tsBtnDelete.Text = "删除标记";
            this.tsBtnDelete.Click += new System.EventHandler(this.tsBtnDelete_Click);
            // 
            // buttonItem4
            // 
            this.buttonItem4.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem4.Name = "buttonItem4";
            this.buttonItem4.SubItemsExpandWidth = 24;
            this.buttonItem4.Text = "&Save...";
            // 
            // buttonItem5
            // 
            this.buttonItem5.BeginGroup = true;
            this.buttonItem5.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem5.Name = "buttonItem5";
            this.buttonItem5.SubItemsExpandWidth = 24;
            this.buttonItem5.Text = "S&hare...";
            // 
            // buttonItem6
            // 
            this.buttonItem6.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem6.Name = "buttonItem6";
            this.buttonItem6.SubItemsExpandWidth = 24;
            this.buttonItem6.Text = "&Print...";
            // 
            // buttonItem7
            // 
            this.buttonItem7.BeginGroup = true;
            this.buttonItem7.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem7.Name = "buttonItem7";
            this.buttonItem7.SubItemsExpandWidth = 24;
            this.buttonItem7.Text = "&Close";
            // 
            // qatCustomizeItem1
            // 
            this.qatCustomizeItem1.Name = "qatCustomizeItem1";
            // 
            // ribbonTabItem1
            // 
            this.ribbonTabItem1.Checked = true;
            this.ribbonTabItem1.Name = "ribbonTabItem1";
            this.ribbonTabItem1.Text = "系统管理";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.toolStrip1);
            this.panel1.Font = new System.Drawing.Font("SimSun", 10F);
            this.panel1.Location = new System.Drawing.Point(7, -2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1031, 33);
            this.panel1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbLX);
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.btnCZLX);
            this.groupBox1.Controls.Add(this.btnZHBZ);
            this.groupBox1.Controls.Add(this.btnDgbz);
            this.groupBox1.Controls.Add(this.tbxSDSJ);
            this.groupBox1.Controls.Add(this.cbZQ);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.cbSD);
            this.groupBox1.Controls.Add(this.cbCZMC);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.tbxDM);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbxDZMC);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("SimSun", 10F);
            this.groupBox1.Location = new System.Drawing.Point(1082, 437);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(486, 144);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "标记属性";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.clbDFL);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Location = new System.Drawing.Point(230, 52);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(136, 73);
            this.panel2.TabIndex = 40;
            // 
            // clbDFL
            // 
            this.clbDFL.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.clbDFL.CheckOnClick = true;
            this.clbDFL.Font = new System.Drawing.Font("SimSun", 10F);
            this.clbDFL.FormattingEnabled = true;
            this.clbDFL.Items.AddRange(new object[] {
            "无效动作",
            "有效动作",
            "无关录像"});
            this.clbDFL.Location = new System.Drawing.Point(37, 8);
            this.clbDFL.Name = "clbDFL";
            this.clbDFL.Size = new System.Drawing.Size(94, 54);
            this.clbDFL.TabIndex = 19;
            this.clbDFL.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBox1_ItemCheck);
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("SimSun", 10F);
            this.label7.Location = new System.Drawing.Point(10, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(18, 50);
            this.label7.TabIndex = 0;
            this.label7.Text = "大分类";
            // 
            // btnCZLX
            // 
            this.btnCZLX.Enabled = false;
            this.btnCZLX.Font = new System.Drawing.Font("SimSun", 10F);
            this.btnCZLX.Location = new System.Drawing.Point(403, 105);
            this.btnCZLX.Name = "btnCZLX";
            this.btnCZLX.Size = new System.Drawing.Size(75, 23);
            this.btnCZLX.TabIndex = 39;
            this.btnCZLX.Text = "操作类型";
            this.btnCZLX.UseVisualStyleBackColor = true;
            this.btnCZLX.Click += new System.EventHandler(this.btnCZLX_Click);
            // 
            // btnZHBZ
            // 
            this.btnZHBZ.Font = new System.Drawing.Font("SimSun", 10F);
            this.btnZHBZ.Location = new System.Drawing.Point(403, 77);
            this.btnZHBZ.Name = "btnZHBZ";
            this.btnZHBZ.Size = new System.Drawing.Size(75, 23);
            this.btnZHBZ.TabIndex = 38;
            this.btnZHBZ.Text = "组合标准";
            this.btnZHBZ.UseVisualStyleBackColor = true;
            this.btnZHBZ.Click += new System.EventHandler(this.btnZHBZ_Click);
            // 
            // btnDgbz
            // 
            this.btnDgbz.Font = new System.Drawing.Font("SimSun", 10F);
            this.btnDgbz.Location = new System.Drawing.Point(403, 48);
            this.btnDgbz.Name = "btnDgbz";
            this.btnDgbz.Size = new System.Drawing.Size(75, 23);
            this.btnDgbz.TabIndex = 19;
            this.btnDgbz.Text = "单个标准";
            this.btnDgbz.UseVisualStyleBackColor = true;
            this.btnDgbz.Click += new System.EventHandler(this.btnDgbz_Click);
            // 
            // tbxSDSJ
            // 
            this.tbxSDSJ.BackColor = System.Drawing.Color.LightGray;
            this.tbxSDSJ.Enabled = false;
            this.tbxSDSJ.Location = new System.Drawing.Point(413, 21);
            this.tbxSDSJ.Name = "tbxSDSJ";
            this.tbxSDSJ.Size = new System.Drawing.Size(65, 23);
            this.tbxSDSJ.TabIndex = 37;
            this.tbxSDSJ.Text = "0.00";
            // 
            // cbZQ
            // 
            this.cbZQ.Font = new System.Drawing.Font("SimSun", 10F);
            this.cbZQ.FormattingEnabled = true;
            this.cbZQ.Items.AddRange(new object[] {
            "新周期",
            "向上合并",
            "向下合并"});
            this.cbZQ.Location = new System.Drawing.Point(157, 104);
            this.cbZQ.Name = "cbZQ";
            this.cbZQ.Size = new System.Drawing.Size(59, 21);
            this.cbZQ.TabIndex = 36;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("SimSun", 10F);
            this.label8.Location = new System.Drawing.Point(122, 107);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 14);
            this.label8.TabIndex = 35;
            this.label8.Text = "周期";
            // 
            // cbSD
            // 
            this.cbSD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSD.Font = new System.Drawing.Font("SimSun", 10F);
            this.cbSD.FormattingEnabled = true;
            this.cbSD.Location = new System.Drawing.Point(60, 104);
            this.cbSD.Name = "cbSD";
            this.cbSD.Size = new System.Drawing.Size(56, 21);
            this.cbSD.TabIndex = 33;
            // 
            // cbCZMC
            // 
            this.cbCZMC.FormattingEnabled = true;
            this.cbCZMC.Location = new System.Drawing.Point(60, 49);
            this.cbCZMC.Name = "cbCZMC";
            this.cbCZMC.Size = new System.Drawing.Size(156, 21);
            this.cbCZMC.TabIndex = 31;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("SimSun", 10F);
            this.label6.Location = new System.Drawing.Point(350, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 14);
            this.label6.TabIndex = 30;
            this.label6.Text = "手动时间";
            // 
            // tbxDM
            // 
            this.tbxDM.Enabled = false;
            this.tbxDM.Location = new System.Drawing.Point(257, 20);
            this.tbxDM.Name = "tbxDM";
            this.tbxDM.Size = new System.Drawing.Size(90, 23);
            this.tbxDM.TabIndex = 29;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("SimSun", 10F);
            this.label5.Location = new System.Drawing.Point(224, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 14);
            this.label5.TabIndex = 28;
            this.label5.Text = "代码";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("SimSun", 10F);
            this.label4.Location = new System.Drawing.Point(24, 106);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 14);
            this.label4.TabIndex = 26;
            this.label4.Text = "速度";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("SimSun", 10F);
            this.label3.Location = new System.Drawing.Point(24, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 14);
            this.label3.TabIndex = 24;
            this.label3.Text = "类型";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("SimSun", 10F);
            this.label2.Location = new System.Drawing.Point(-2, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 14);
            this.label2.TabIndex = 22;
            this.label2.Text = "操作名称";
            // 
            // tbxDZMC
            // 
            this.tbxDZMC.Location = new System.Drawing.Point(60, 20);
            this.tbxDZMC.Name = "tbxDZMC";
            this.tbxDZMC.Size = new System.Drawing.Size(156, 23);
            this.tbxDZMC.TabIndex = 21;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("SimSun", 10F);
            this.label1.Location = new System.Drawing.Point(-2, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 14);
            this.label1.TabIndex = 20;
            this.label1.Text = "动作名称";
            // 
            // tbVideoTime
            // 
            this.tbVideoTime.Enabled = false;
            this.tbVideoTime.Location = new System.Drawing.Point(815, 32);
            this.tbVideoTime.Name = "tbVideoTime";
            this.tbVideoTime.Size = new System.Drawing.Size(260, 21);
            this.tbVideoTime.TabIndex = 0;
            // 
            // tbxBJMS
            // 
            this.tbxBJMS.Location = new System.Drawing.Point(1172, 585);
            this.tbxBJMS.Multiline = true;
            this.tbxBJMS.Name = "tbxBJMS";
            this.tbxBJMS.Size = new System.Drawing.Size(388, 56);
            this.tbxBJMS.TabIndex = 19;
            // 
            // tbxAQZYSX
            // 
            this.tbxAQZYSX.Location = new System.Drawing.Point(1171, 709);
            this.tbxAQZYSX.Multiline = true;
            this.tbxAQZYSX.Name = "tbxAQZYSX";
            this.tbxAQZYSX.Size = new System.Drawing.Size(389, 56);
            this.tbxAQZYSX.TabIndex = 19;
            // 
            // tbxPZZYSX
            // 
            this.tbxPZZYSX.Location = new System.Drawing.Point(1171, 647);
            this.tbxPZZYSX.Multiline = true;
            this.tbxPZZYSX.Name = "tbxPZZYSX";
            this.tbxPZZYSX.Size = new System.Drawing.Size(389, 56);
            this.tbxPZZYSX.TabIndex = 19;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("SimSun", 10F);
            this.label9.Location = new System.Drawing.Point(1106, 603);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 14);
            this.label9.TabIndex = 20;
            this.label9.Text = "标记描述";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("SimSun", 10F);
            this.label10.Location = new System.Drawing.Point(1080, 727);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 14);
            this.label10.TabIndex = 20;
            this.label10.Text = "安全注意事项";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("SimSun", 10F);
            this.label11.Location = new System.Drawing.Point(1080, 665);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(91, 14);
            this.label11.TabIndex = 20;
            this.label11.Text = "品质注意事项";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dataGridView1);
            this.panel3.Location = new System.Drawing.Point(1082, 54);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(478, 377);
            this.panel3.TabIndex = 24;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.ActionName,
            this.OperationName,
            this.Cycle,
            this.DisplayActionTime,
            this.DisplayManualStandardTime,
            this.速度,
            this.类型,
            this.Type,
            this.Code,
            this.TypeId,
            this.ActionTypeId,
            this.StartTime,
            this.EndTime,
            this.TIME,
            this.ST});
            this.dataGridView1.Location = new System.Drawing.Point(3, 0);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(475, 372);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dataGridView1_CellBeginEdit);
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseDown);
            this.dataGridView1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellValueChanged);
            this.dataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            // 
            // id
            // 
            this.id.DataPropertyName = "ActionId";
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.Visible = false;
            // 
            // ActionName
            // 
            this.ActionName.DataPropertyName = "ActionName";
            this.ActionName.HeaderText = "动作名称";
            this.ActionName.Name = "ActionName";
            this.ActionName.ReadOnly = true;
            this.ActionName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ActionName.Width = 80;
            // 
            // OperationName
            // 
            this.OperationName.DataPropertyName = "OperationName";
            this.OperationName.HeaderText = "操作名称";
            this.OperationName.Name = "OperationName";
            this.OperationName.Width = 80;
            // 
            // Cycle
            // 
            this.Cycle.DataPropertyName = "Cycle";
            this.Cycle.HeaderText = "周期";
            this.Cycle.Name = "Cycle";
            this.Cycle.Width = 40;
            // 
            // DisplayActionTime
            // 
            this.DisplayActionTime.DataPropertyName = "DisplayActionTime";
            dataGridViewCellStyle1.NullValue = "-->";
            this.DisplayActionTime.DefaultCellStyle = dataGridViewCellStyle1;
            this.DisplayActionTime.HeaderText = "TIME";
            this.DisplayActionTime.Name = "DisplayActionTime";
            this.DisplayActionTime.ReadOnly = true;
            this.DisplayActionTime.Width = 60;
            // 
            // DisplayManualStandardTime
            // 
            this.DisplayManualStandardTime.DataPropertyName = "DisplayManualStandardTime";
            dataGridViewCellStyle2.NullValue = "-->";
            this.DisplayManualStandardTime.DefaultCellStyle = dataGridViewCellStyle2;
            this.DisplayManualStandardTime.HeaderText = "S . T";
            this.DisplayManualStandardTime.Name = "DisplayManualStandardTime";
            this.DisplayManualStandardTime.ReadOnly = true;
            this.DisplayManualStandardTime.Width = 60;
            // 
            // 速度
            // 
            this.速度.DataPropertyName = "Speed";
            this.速度.HeaderText = "速度";
            this.速度.Name = "速度";
            this.速度.ReadOnly = true;
            this.速度.Width = 40;
            // 
            // 类型
            // 
            this.类型.HeaderText = "类型";
            this.类型.Name = "类型";
            this.类型.ReadOnly = true;
            this.类型.Width = 55;
            // 
            // Type
            // 
            this.Type.HeaderText = "大分类";
            this.Type.Name = "Type";
            this.Type.ReadOnly = true;
            this.Type.Width = 80;
            // 
            // Code
            // 
            this.Code.DataPropertyName = "ActionCode";
            this.Code.HeaderText = "代码";
            this.Code.Name = "Code";
            this.Code.ReadOnly = true;
            // 
            // TypeId
            // 
            this.TypeId.DataPropertyName = "TypeId";
            this.TypeId.HeaderText = "TypeId";
            this.TypeId.Name = "TypeId";
            this.TypeId.Visible = false;
            // 
            // ActionTypeId
            // 
            this.ActionTypeId.DataPropertyName = "ActionTypeId";
            this.ActionTypeId.HeaderText = "ActionTypeId";
            this.ActionTypeId.Name = "ActionTypeId";
            this.ActionTypeId.Visible = false;
            // 
            // StartTime
            // 
            this.StartTime.DataPropertyName = "StartTime";
            this.StartTime.HeaderText = "StartTime";
            this.StartTime.Name = "StartTime";
            this.StartTime.Visible = false;
            // 
            // EndTime
            // 
            this.EndTime.DataPropertyName = "EndTime";
            this.EndTime.HeaderText = "EndTime";
            this.EndTime.Name = "EndTime";
            this.EndTime.Visible = false;
            // 
            // TIME
            // 
            this.TIME.DataPropertyName = "ActionTime";
            dataGridViewCellStyle3.NullValue = "-->";
            this.TIME.DefaultCellStyle = dataGridViewCellStyle3;
            this.TIME.HeaderText = "TIME";
            this.TIME.Name = "TIME";
            this.TIME.ReadOnly = true;
            this.TIME.Visible = false;
            this.TIME.Width = 60;
            // 
            // ST
            // 
            this.ST.DataPropertyName = "ManualStandardTime";
            dataGridViewCellStyle4.NullValue = "-->";
            this.ST.DefaultCellStyle = dataGridViewCellStyle4;
            this.ST.HeaderText = "S . T";
            this.ST.Name = "ST";
            this.ST.ReadOnly = true;
            this.ST.Visible = false;
            this.ST.Width = 70;
            // 
            // lbProcess
            // 
            this.lbProcess.BackColor = System.Drawing.Color.RoyalBlue;
            // 
            // 
            // 
            this.lbProcess.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbProcess.Location = new System.Drawing.Point(15, 755);
            this.lbProcess.Name = "lbProcess";
            this.lbProcess.Size = new System.Drawing.Size(100, 5);
            this.lbProcess.TabIndex = 28;
            this.lbProcess.WordWrap = true;
            // 
            // timerAutoLoadVideo
            // 
            this.timerAutoLoadVideo.Interval = 500;
            this.timerAutoLoadVideo.Tick += new System.EventHandler(this.timerAutoLoadVideo_Tick);
            // 
            // bgWUploadVideo
            // 
            this.bgWUploadVideo.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWUploadVideo_DoWork);
            // 
            // lblFileName
            // 
            this.lblFileName.AutoSize = true;
            this.lblFileName.Location = new System.Drawing.Point(12, 37);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(0, 12);
            this.lblFileName.TabIndex = 30;
            // 
            // vlcPlayer
            // 
            this.vlcPlayer.Enabled = true;
            this.vlcPlayer.Location = new System.Drawing.Point(7, 54);
            this.vlcPlayer.Name = "vlcPlayer";
            this.vlcPlayer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("vlcPlayer.OcxState")));
            this.vlcPlayer.Size = new System.Drawing.Size(1069, 687);
            this.vlcPlayer.TabIndex = 29;
            this.vlcPlayer.OnMessage += new AxAPlayer3Lib._IPlayerEvents_OnMessageEventHandler(this.vlcPlayer_OnMessage);
            this.vlcPlayer.OnOpenSucceeded += new System.EventHandler(this.vlcPlayer_OnOpenSucceeded);
            // 
            // cbLX
            // 
            this.cbLX.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbLX.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbLX.FormattingEnabled = true;
            this.cbLX.Location = new System.Drawing.Point(60, 76);
            this.cbLX.MyColors = new string[] {
        "Black",
        "Red",
        "Blue",
        "Green"};
            this.cbLX.Name = "cbLX";
            this.cbLX.Size = new System.Drawing.Size(156, 24);
            this.cbLX.TabIndex = 41;
            // 
            // FormAPlay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1564, 784);
            this.ControlBox = false;
            this.Controls.Add(this.lblFileName);
            this.Controls.Add(this.vlcPlayer);
            this.Controls.Add(this.lbProcess);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tbxPZZYSX);
            this.Controls.Add(this.tbxAQZYSX);
            this.Controls.Add(this.tbxBJMS);
            this.Controls.Add(this.tbVideoTime);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.trackBar1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("SimSun", 9F);
            this.Name = "FormAPlay";
            this.Load += new System.EventHandler(this.FormAPlay_Load);
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlcPlayer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevComponents.DotNetBar.StyleManager styleManager1;
        private DevComponents.DotNetBar.StyleManager styleManager2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tSBtn_stop;
        private System.Windows.Forms.ToolStripButton tSBtn_openfile;
        private System.Windows.Forms.ToolStripButton tSB_backward;
        private System.Windows.Forms.ToolStripButton tSB_forward;
        private DevComponents.DotNetBar.ButtonItem buttonItem4;
        private DevComponents.DotNetBar.ButtonItem buttonItem5;
        private DevComponents.DotNetBar.ButtonItem buttonItem6;
        private DevComponents.DotNetBar.ButtonItem buttonItem7;
        private DevComponents.DotNetBar.QatCustomizeItem qatCustomizeItem1;
        private DevComponents.DotNetBar.RibbonTabItem ribbonTabItem1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripDropDownButton tSDDBtnSpeed;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripDropDownButton tSDDBtnStep;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbxDM;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbxDZMC;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbSD;
        private System.Windows.Forms.ComboBox cbZQ;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbxSDSJ;
        private System.Windows.Forms.Button btnDgbz;
        private System.Windows.Forms.Button btnCZLX;
        private System.Windows.Forms.Button btnZHBZ;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.TextBox tbVideoTime;
        private System.Windows.Forms.ToolStripButton tsBtnSign;
        private System.Windows.Forms.ToolStripButton tsBtnSave;
        private System.Windows.Forms.ToolStripButton tsBtnDelete;
        private System.Windows.Forms.CheckedListBox clbDFL;
        private System.Windows.Forms.TextBox tbxBJMS;
        private System.Windows.Forms.TextBox tbxAQZYSX;
        private System.Windows.Forms.TextBox tbxPZZYSX;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox cbCZMC;
        private System.Windows.Forms.ToolStripDropDownButton DDBtnStandardTime;
        private System.Windows.Forms.ToolStripMenuItem 自动标准时间ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 手动标准时间ToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton btnCancelLoop;
        private System.Windows.Forms.ToolStripButton btnSetLoopStart;
        private System.Windows.Forms.ToolStripButton btnSetLoopEnd;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private DevComponents.DotNetBar.LabelX lbProcess;
        private System.Windows.Forms.ToolStripButton tSBtn_play;
        private System.Windows.Forms.ToolStripButton tsBtnPause;
        private System.Windows.Forms.Timer timerAutoLoadVideo;
        private ColoredComboBox cbLX;
        private System.ComponentModel.BackgroundWorker bgWUploadVideo;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private AxAPlayer3Lib.AxPlayer vlcPlayer;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActionName;
        private System.Windows.Forms.DataGridViewTextBoxColumn OperationName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cycle;
        private System.Windows.Forms.DataGridViewTextBoxColumn DisplayActionTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn DisplayManualStandardTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn 速度;
        private System.Windows.Forms.DataGridViewTextBoxColumn 类型;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn Code;
        private System.Windows.Forms.DataGridViewTextBoxColumn TypeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActionTypeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn StartTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn EndTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn TIME;
        private System.Windows.Forms.DataGridViewTextBoxColumn ST;
        private System.Windows.Forms.Label lblFileName;
    }
}

