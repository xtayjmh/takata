﻿namespace Takata
{
    partial class FormDoublePlay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDoublePlay));
            this.styleManager1 = new DevComponents.DotNetBar.StyleManager(this.components);
            this.styleManager2 = new DevComponents.DotNetBar.StyleManager(this.components);
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.buttonItem4 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem5 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem6 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem7 = new DevComponents.DotNetBar.ButtonItem();
            this.qatCustomizeItem1 = new DevComponents.DotNetBar.QatCustomizeItem();
            this.ribbonTabItem1 = new DevComponents.DotNetBar.RibbonTabItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tSBtn_play = new System.Windows.Forms.ToolStripButton();
            this.tsBtnPause = new System.Windows.Forms.ToolStripButton();
            this.tSBtn_stop = new System.Windows.Forms.ToolStripButton();
            this.tSBtn_openfile = new System.Windows.Forms.ToolStripButton();
            this.tSB_backward = new System.Windows.Forms.ToolStripButton();
            this.tSB_forward = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.tSDDBtnSpeed = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.tSDDBtnStep = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.btnCancle1 = new System.Windows.Forms.ToolStripButton();
            this.btnSetStart1 = new System.Windows.Forms.ToolStripButton();
            this.btnSetEnd1 = new System.Windows.Forms.ToolStripButton();
            this.pE2 = new DevComponents.DotNetBar.PanelEx();
            this.trackBar2 = new System.Windows.Forms.TrackBar();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.tSBtn_play2 = new System.Windows.Forms.ToolStripButton();
            this.tsBtnPause2 = new System.Windows.Forms.ToolStripButton();
            this.tSBtn_stop2 = new System.Windows.Forms.ToolStripButton();
            this.btnOpen2 = new System.Windows.Forms.ToolStripButton();
            this.btnBackward2 = new System.Windows.Forms.ToolStripButton();
            this.btnForward2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.tsDDLSpeed2 = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripDropDownButton2 = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripMenuItem88 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem89 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem90 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem91 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem92 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem93 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem94 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem95 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem96 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.btnCancle2 = new System.Windows.Forms.ToolStripButton();
            this.btnContrastStart = new System.Windows.Forms.ToolStripButton();
            this.btnContrastEnd = new System.Windows.Forms.ToolStripButton();
            this.tbVideoTime = new System.Windows.Forms.TextBox();
            this.tbVideoTime2 = new System.Windows.Forms.TextBox();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.dgv1 = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActionName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OperationName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cycle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DisplayActionTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DisplayManualStandardTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.速度 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.类型 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TypeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActionTypeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StartTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EndTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TIME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dgv2 = new System.Windows.Forms.DataGridView();
            this.id2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActionName2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OperationName2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cycle2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.速度2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.类型2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Code2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TypeId2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActionTypeId2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StartTime2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EndTime2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TIME2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ST2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dgv3 = new System.Windows.Forms.DataGridView();
            this.IdA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NameA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NameB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TimeDiff = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblProcess1 = new System.Windows.Forms.Label();
            this.lblProcess2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.panel1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).BeginInit();
            this.toolStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv1)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv2)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv3)).BeginInit();
            this.SuspendLayout();
            // 
            // styleManager1
            // 
            this.styleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2010Blue;
            this.styleManager1.MetroColorParameters = new DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.White, System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154))))));
            // 
            // styleManager2
            // 
            this.styleManager2.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2010Blue;
            this.styleManager2.MetroColorParameters = new DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.White, System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154))))));
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx1.DisabledBackColor = System.Drawing.Color.Empty;
            this.panelEx1.Location = new System.Drawing.Point(7, 60);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(580, 480);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 1;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // timer1
            // 
            this.timer1.Interval = 30;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(6, 557);
            this.trackBar1.Maximum = 100;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(581, 45);
            this.trackBar1.SmallChange = 5;
            this.trackBar1.TabIndex = 6;
            this.trackBar1.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // buttonItem4
            // 
            this.buttonItem4.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem4.Name = "buttonItem4";
            this.buttonItem4.SubItemsExpandWidth = 24;
            this.buttonItem4.Text = "&Save...";
            // 
            // buttonItem5
            // 
            this.buttonItem5.BeginGroup = true;
            this.buttonItem5.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem5.Name = "buttonItem5";
            this.buttonItem5.SubItemsExpandWidth = 24;
            this.buttonItem5.Text = "S&hare...";
            // 
            // buttonItem6
            // 
            this.buttonItem6.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem6.Name = "buttonItem6";
            this.buttonItem6.SubItemsExpandWidth = 24;
            this.buttonItem6.Text = "&Print...";
            // 
            // buttonItem7
            // 
            this.buttonItem7.BeginGroup = true;
            this.buttonItem7.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem7.Name = "buttonItem7";
            this.buttonItem7.SubItemsExpandWidth = 24;
            this.buttonItem7.Text = "&Close";
            // 
            // qatCustomizeItem1
            // 
            this.qatCustomizeItem1.Name = "qatCustomizeItem1";
            // 
            // ribbonTabItem1
            // 
            this.ribbonTabItem1.Checked = true;
            this.ribbonTabItem1.Name = "ribbonTabItem1";
            this.ribbonTabItem1.Text = "系统管理";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.toolStrip1);
            this.panel1.Location = new System.Drawing.Point(7, -2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(631, 33);
            this.panel1.TabIndex = 0;
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tSBtn_play,
            this.tsBtnPause,
            this.tSBtn_stop,
            this.tSBtn_openfile,
            this.tSB_backward,
            this.tSB_forward,
            this.toolStripLabel1,
            this.tSDDBtnSpeed,
            this.toolStripSeparator1,
            this.toolStripLabel2,
            this.tSDDBtnStep,
            this.toolStripButton1,
            this.toolStripButton2,
            this.btnCancle1,
            this.btnSetStart1,
            this.btnSetEnd1});
            this.toolStrip1.Location = new System.Drawing.Point(2, 3);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(640, 27);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tSBtn_play
            // 
            this.tSBtn_play.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.tSBtn_play.Image = ((System.Drawing.Image)(resources.GetObject("tSBtn_play.Image")));
            this.tSBtn_play.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSBtn_play.Name = "tSBtn_play";
            this.tSBtn_play.Padding = new System.Windows.Forms.Padding(0, 1, 0, 2);
            this.tSBtn_play.Size = new System.Drawing.Size(57, 24);
            this.tSBtn_play.Text = "播放";
            this.tSBtn_play.Click += new System.EventHandler(this.tSBtn_play_ButtonClick);
            // 
            // tsBtnPause
            // 
            this.tsBtnPause.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.tsBtnPause.Image = global::Takata.Properties.Resources.btnpause;
            this.tsBtnPause.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnPause.Name = "tsBtnPause";
            this.tsBtnPause.Size = new System.Drawing.Size(57, 24);
            this.tsBtnPause.Text = "暂停";
            this.tsBtnPause.Click += new System.EventHandler(this.tsBtnPause_Click);
            // 
            // tSBtn_stop
            // 
            this.tSBtn_stop.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.tSBtn_stop.Image = ((System.Drawing.Image)(resources.GetObject("tSBtn_stop.Image")));
            this.tSBtn_stop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSBtn_stop.Name = "tSBtn_stop";
            this.tSBtn_stop.Size = new System.Drawing.Size(85, 24);
            this.tSBtn_stop.Text = "回到开始";
            this.tSBtn_stop.Click += new System.EventHandler(this.tSBtn_stop_Click);
            // 
            // tSBtn_openfile
            // 
            this.tSBtn_openfile.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.tSBtn_openfile.Image = ((System.Drawing.Image)(resources.GetObject("tSBtn_openfile.Image")));
            this.tSBtn_openfile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSBtn_openfile.Name = "tSBtn_openfile";
            this.tSBtn_openfile.Size = new System.Drawing.Size(57, 24);
            this.tSBtn_openfile.Text = "打开";
            this.tSBtn_openfile.Click += new System.EventHandler(this.tSBtn_openfile_Click);
            // 
            // tSB_backward
            // 
            this.tSB_backward.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tSB_backward.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.tSB_backward.Image = ((System.Drawing.Image)(resources.GetObject("tSB_backward.Image")));
            this.tSB_backward.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSB_backward.Name = "tSB_backward";
            this.tSB_backward.Size = new System.Drawing.Size(23, 24);
            this.tSB_backward.Text = "后退";
            this.tSB_backward.Click += new System.EventHandler(this.tSB_backward_Click);
            // 
            // tSB_forward
            // 
            this.tSB_forward.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tSB_forward.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.tSB_forward.Image = ((System.Drawing.Image)(resources.GetObject("tSB_forward.Image")));
            this.tSB_forward.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSB_forward.Name = "tSB_forward";
            this.tSB_forward.Size = new System.Drawing.Size(23, 24);
            this.tSB_forward.Text = "快进";
            this.tSB_forward.Click += new System.EventHandler(this.tSB_forward_Click);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(51, 24);
            this.toolStripLabel1.Text = "速度：";
            // 
            // tSDDBtnSpeed
            // 
            this.tSDDBtnSpeed.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tSDDBtnSpeed.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.tSDDBtnSpeed.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSDDBtnSpeed.Name = "tSDDBtnSpeed";
            this.tSDDBtnSpeed.Size = new System.Drawing.Size(49, 24);
            this.tSDDBtnSpeed.Text = "1.00";
            this.tSDDBtnSpeed.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStripDropDownButton1_DropDownItemClicked);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(51, 24);
            this.toolStripLabel2.Text = "步进：";
            // 
            // tSDDBtnStep
            // 
            this.tSDDBtnStep.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tSDDBtnStep.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4,
            this.toolStripMenuItem5,
            this.toolStripMenuItem6,
            this.toolStripMenuItem7,
            this.toolStripMenuItem8,
            this.toolStripMenuItem9,
            this.toolStripMenuItem10});
            this.tSDDBtnStep.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.tSDDBtnStep.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSDDBtnStep.Name = "tSDDBtnStep";
            this.tSDDBtnStep.Size = new System.Drawing.Size(30, 24);
            this.tSDDBtnStep.Text = "1";
            this.tSDDBtnStep.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tSDDBtnStep_DropDownItemClicked);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(94, 24);
            this.toolStripMenuItem2.Text = "1";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(94, 24);
            this.toolStripMenuItem3.Text = "2";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(94, 24);
            this.toolStripMenuItem4.Text = "3";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(94, 24);
            this.toolStripMenuItem5.Text = "4";
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(94, 24);
            this.toolStripMenuItem6.Text = "5";
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(94, 24);
            this.toolStripMenuItem7.Text = "10";
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(94, 24);
            this.toolStripMenuItem8.Text = "15";
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(94, 24);
            this.toolStripMenuItem9.Text = "20";
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(94, 24);
            this.toolStripMenuItem10.Text = "25";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 24);
            this.toolStripButton1.Text = "回放";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 24);
            this.toolStripButton2.Text = "快放";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // btnCancle1
            // 
            this.btnCancle1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCancle1.Image = global::Takata.Properties.Resources.btnCancelLoop;
            this.btnCancle1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCancle1.Name = "btnCancle1";
            this.btnCancle1.Size = new System.Drawing.Size(23, 24);
            this.btnCancle1.Text = "toolStripButton4";
            this.btnCancle1.Click += new System.EventHandler(this.btnCancle1_Click);
            // 
            // btnSetStart1
            // 
            this.btnSetStart1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSetStart1.Image = global::Takata.Properties.Resources.btnSetLoopStart;
            this.btnSetStart1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSetStart1.Name = "btnSetStart1";
            this.btnSetStart1.Size = new System.Drawing.Size(23, 24);
            this.btnSetStart1.Text = "toolStripButton5";
            this.btnSetStart1.Click += new System.EventHandler(this.btnSetStart1_Click);
            // 
            // btnSetEnd1
            // 
            this.btnSetEnd1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSetEnd1.Image = global::Takata.Properties.Resources.btnSetLoopEnd;
            this.btnSetEnd1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSetEnd1.Name = "btnSetEnd1";
            this.btnSetEnd1.Size = new System.Drawing.Size(23, 24);
            this.btnSetEnd1.Text = "toolStripButton6";
            this.btnSetEnd1.Click += new System.EventHandler(this.btnSetEnd1_Click);
            // 
            // pE2
            // 
            this.pE2.CanvasColor = System.Drawing.SystemColors.Control;
            this.pE2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pE2.DisabledBackColor = System.Drawing.Color.Empty;
            this.pE2.Location = new System.Drawing.Point(596, 60);
            this.pE2.Name = "pE2";
            this.pE2.Size = new System.Drawing.Size(580, 480);
            this.pE2.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pE2.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pE2.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pE2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pE2.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pE2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pE2.Style.GradientAngle = 90;
            this.pE2.TabIndex = 5;
            // 
            // trackBar2
            // 
            this.trackBar2.Location = new System.Drawing.Point(590, 557);
            this.trackBar2.Maximum = 100;
            this.trackBar2.Name = "trackBar2";
            this.trackBar2.Size = new System.Drawing.Size(581, 45);
            this.trackBar2.SmallChange = 5;
            this.trackBar2.TabIndex = 10;
            this.trackBar2.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBar2.Scroll += new System.EventHandler(this.tbForPanel2_Scroll);
            // 
            // toolStrip2
            // 
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tSBtn_play2,
            this.tsBtnPause2,
            this.tSBtn_stop2,
            this.btnOpen2,
            this.btnBackward2,
            this.btnForward2,
            this.toolStripLabel3,
            this.tsDDLSpeed2,
            this.toolStripSeparator2,
            this.toolStripLabel4,
            this.toolStripDropDownButton2,
            this.toolStripButton7,
            this.toolStripButton8,
            this.btnCancle2,
            this.btnContrastStart,
            this.btnContrastEnd});
            this.toolStrip2.Location = new System.Drawing.Point(642, 1);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(644, 30);
            this.toolStrip2.TabIndex = 17;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // tSBtn_play2
            // 
            this.tSBtn_play2.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.tSBtn_play2.Image = ((System.Drawing.Image)(resources.GetObject("tSBtn_play2.Image")));
            this.tSBtn_play2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSBtn_play2.Name = "tSBtn_play2";
            this.tSBtn_play2.Padding = new System.Windows.Forms.Padding(0, 1, 0, 2);
            this.tSBtn_play2.Size = new System.Drawing.Size(57, 27);
            this.tSBtn_play2.Text = "播放";
            this.tSBtn_play2.Click += new System.EventHandler(this.tSBtn_play2_ButtonClick);
            // 
            // tsBtnPause2
            // 
            this.tsBtnPause2.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.tsBtnPause2.Image = global::Takata.Properties.Resources.btnpause;
            this.tsBtnPause2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnPause2.Name = "tsBtnPause2";
            this.tsBtnPause2.Size = new System.Drawing.Size(57, 27);
            this.tsBtnPause2.Text = "暂停";
            this.tsBtnPause2.Click += new System.EventHandler(this.tsBtnPause2_Click);
            // 
            // tSBtn_stop2
            // 
            this.tSBtn_stop2.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.tSBtn_stop2.Image = ((System.Drawing.Image)(resources.GetObject("tSBtn_stop2.Image")));
            this.tSBtn_stop2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSBtn_stop2.Name = "tSBtn_stop2";
            this.tSBtn_stop2.Size = new System.Drawing.Size(85, 27);
            this.tSBtn_stop2.Text = "回到开始";
            this.tSBtn_stop2.Click += new System.EventHandler(this.tSBtn_stop2_Click);
            // 
            // btnOpen2
            // 
            this.btnOpen2.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.btnOpen2.Image = ((System.Drawing.Image)(resources.GetObject("btnOpen2.Image")));
            this.btnOpen2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnOpen2.Name = "btnOpen2";
            this.btnOpen2.Size = new System.Drawing.Size(57, 27);
            this.btnOpen2.Text = "打开";
            this.btnOpen2.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // btnBackward2
            // 
            this.btnBackward2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnBackward2.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.btnBackward2.Image = ((System.Drawing.Image)(resources.GetObject("btnBackward2.Image")));
            this.btnBackward2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnBackward2.Name = "btnBackward2";
            this.btnBackward2.Size = new System.Drawing.Size(23, 27);
            this.btnBackward2.Text = "后退";
            this.btnBackward2.Click += new System.EventHandler(this.btnBackward2_Click);
            // 
            // btnForward2
            // 
            this.btnForward2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnForward2.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.btnForward2.Image = ((System.Drawing.Image)(resources.GetObject("btnForward2.Image")));
            this.btnForward2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnForward2.Name = "btnForward2";
            this.btnForward2.Size = new System.Drawing.Size(23, 27);
            this.btnForward2.Text = "快进";
            this.btnForward2.Click += new System.EventHandler(this.btnForward2_Click);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(79, 27);
            this.toolStripLabel3.Text = "播放速度：";
            // 
            // tsDDLSpeed2
            // 
            this.tsDDLSpeed2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsDDLSpeed2.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.tsDDLSpeed2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsDDLSpeed2.Name = "tsDDLSpeed2";
            this.tsDDLSpeed2.Size = new System.Drawing.Size(49, 27);
            this.tsDDLSpeed2.Text = "1.00";
            this.tsDDLSpeed2.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tsDDLSpeed2_DropDownItemClicked);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(51, 27);
            this.toolStripLabel4.Text = "步进：";
            // 
            // toolStripDropDownButton2
            // 
            this.toolStripDropDownButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem88,
            this.toolStripMenuItem89,
            this.toolStripMenuItem90,
            this.toolStripMenuItem91,
            this.toolStripMenuItem92,
            this.toolStripMenuItem93,
            this.toolStripMenuItem94,
            this.toolStripMenuItem95,
            this.toolStripMenuItem96});
            this.toolStripDropDownButton2.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.toolStripDropDownButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton2.Name = "toolStripDropDownButton2";
            this.toolStripDropDownButton2.Size = new System.Drawing.Size(30, 27);
            this.toolStripDropDownButton2.Text = "1";
            this.toolStripDropDownButton2.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStripDropDownButton2_DropDownItemClicked);
            // 
            // toolStripMenuItem88
            // 
            this.toolStripMenuItem88.Name = "toolStripMenuItem88";
            this.toolStripMenuItem88.Size = new System.Drawing.Size(94, 24);
            this.toolStripMenuItem88.Text = "1";
            // 
            // toolStripMenuItem89
            // 
            this.toolStripMenuItem89.Name = "toolStripMenuItem89";
            this.toolStripMenuItem89.Size = new System.Drawing.Size(94, 24);
            this.toolStripMenuItem89.Text = "2";
            // 
            // toolStripMenuItem90
            // 
            this.toolStripMenuItem90.Name = "toolStripMenuItem90";
            this.toolStripMenuItem90.Size = new System.Drawing.Size(94, 24);
            this.toolStripMenuItem90.Text = "3";
            // 
            // toolStripMenuItem91
            // 
            this.toolStripMenuItem91.Name = "toolStripMenuItem91";
            this.toolStripMenuItem91.Size = new System.Drawing.Size(94, 24);
            this.toolStripMenuItem91.Text = "4";
            // 
            // toolStripMenuItem92
            // 
            this.toolStripMenuItem92.Name = "toolStripMenuItem92";
            this.toolStripMenuItem92.Size = new System.Drawing.Size(94, 24);
            this.toolStripMenuItem92.Text = "5";
            // 
            // toolStripMenuItem93
            // 
            this.toolStripMenuItem93.Name = "toolStripMenuItem93";
            this.toolStripMenuItem93.Size = new System.Drawing.Size(94, 24);
            this.toolStripMenuItem93.Text = "10";
            // 
            // toolStripMenuItem94
            // 
            this.toolStripMenuItem94.Name = "toolStripMenuItem94";
            this.toolStripMenuItem94.Size = new System.Drawing.Size(94, 24);
            this.toolStripMenuItem94.Text = "15";
            // 
            // toolStripMenuItem95
            // 
            this.toolStripMenuItem95.Name = "toolStripMenuItem95";
            this.toolStripMenuItem95.Size = new System.Drawing.Size(94, 24);
            this.toolStripMenuItem95.Text = "20";
            // 
            // toolStripMenuItem96
            // 
            this.toolStripMenuItem96.Name = "toolStripMenuItem96";
            this.toolStripMenuItem96.Size = new System.Drawing.Size(94, 24);
            this.toolStripMenuItem96.Text = "25";
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton7.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(23, 27);
            this.toolStripButton7.Text = "回放";
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton8.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.toolStripButton8.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton8.Image")));
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(23, 27);
            this.toolStripButton8.Text = "快放";
            this.toolStripButton8.Click += new System.EventHandler(this.toolStripButton8_Click);
            // 
            // btnCancle2
            // 
            this.btnCancle2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCancle2.Image = global::Takata.Properties.Resources.btnCancelLoop;
            this.btnCancle2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCancle2.Name = "btnCancle2";
            this.btnCancle2.Size = new System.Drawing.Size(23, 27);
            this.btnCancle2.Text = "toolStripButton3";
            this.btnCancle2.Click += new System.EventHandler(this.btnCancle2_Click);
            // 
            // btnContrastStart
            // 
            this.btnContrastStart.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnContrastStart.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.btnContrastStart.Image = ((System.Drawing.Image)(resources.GetObject("btnContrastStart.Image")));
            this.btnContrastStart.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnContrastStart.Name = "btnContrastStart";
            this.btnContrastStart.Size = new System.Drawing.Size(23, 27);
            this.btnContrastStart.Text = "对比起始点";
            this.btnContrastStart.Click += new System.EventHandler(this.btnContrastStart_Click);
            // 
            // btnContrastEnd
            // 
            this.btnContrastEnd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnContrastEnd.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.btnContrastEnd.Image = ((System.Drawing.Image)(resources.GetObject("btnContrastEnd.Image")));
            this.btnContrastEnd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnContrastEnd.Name = "btnContrastEnd";
            this.btnContrastEnd.Size = new System.Drawing.Size(23, 27);
            this.btnContrastEnd.Text = "对比结束点";
            this.btnContrastEnd.Click += new System.EventHandler(this.btnContrastEnd_Click);
            // 
            // tbVideoTime
            // 
            this.tbVideoTime.BackColor = System.Drawing.SystemColors.MenuBar;
            this.tbVideoTime.Enabled = false;
            this.tbVideoTime.Location = new System.Drawing.Point(404, 37);
            this.tbVideoTime.Name = "tbVideoTime";
            this.tbVideoTime.Size = new System.Drawing.Size(181, 21);
            this.tbVideoTime.TabIndex = 18;
            // 
            // tbVideoTime2
            // 
            this.tbVideoTime2.BackColor = System.Drawing.SystemColors.MenuBar;
            this.tbVideoTime2.Enabled = false;
            this.tbVideoTime2.Location = new System.Drawing.Point(996, 37);
            this.tbVideoTime2.Name = "tbVideoTime2";
            this.tbVideoTime2.Size = new System.Drawing.Size(181, 21);
            this.tbVideoTime2.TabIndex = 19;
            // 
            // timer2
            // 
            this.timer2.Interval = 30;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // dgv1
            // 
            this.dgv1.AllowUserToAddRows = false;
            this.dgv1.AllowUserToDeleteRows = false;
            this.dgv1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.ActionName,
            this.OperationName,
            this.Cycle,
            this.DisplayActionTime,
            this.DisplayManualStandardTime,
            this.速度,
            this.类型,
            this.Type,
            this.Code,
            this.TypeId,
            this.ActionTypeId,
            this.StartTime,
            this.EndTime,
            this.ST,
            this.TIME});
            this.dgv1.Location = new System.Drawing.Point(5, 3);
            this.dgv1.MultiSelect = false;
            this.dgv1.Name = "dgv1";
            this.dgv1.ReadOnly = true;
            this.dgv1.RowTemplate.Height = 23;
            this.dgv1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv1.Size = new System.Drawing.Size(570, 170);
            this.dgv1.TabIndex = 0;
            // 
            // id
            // 
            this.id.DataPropertyName = "ActionId";
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            this.id.Width = 10;
            // 
            // ActionName
            // 
            this.ActionName.DataPropertyName = "ActionName";
            this.ActionName.HeaderText = "动作名称";
            this.ActionName.Name = "ActionName";
            this.ActionName.ReadOnly = true;
            this.ActionName.Width = 80;
            // 
            // OperationName
            // 
            this.OperationName.DataPropertyName = "OperationName";
            this.OperationName.HeaderText = "操作名称";
            this.OperationName.Name = "OperationName";
            this.OperationName.ReadOnly = true;
            this.OperationName.Width = 80;
            // 
            // Cycle
            // 
            this.Cycle.DataPropertyName = "Cycle";
            this.Cycle.HeaderText = "周期";
            this.Cycle.Name = "Cycle";
            this.Cycle.ReadOnly = true;
            this.Cycle.Width = 40;
            // 
            // DisplayActionTime
            // 
            this.DisplayActionTime.DataPropertyName = "DisplayActionTime";
            this.DisplayActionTime.HeaderText = "TIME";
            this.DisplayActionTime.Name = "DisplayActionTime";
            this.DisplayActionTime.ReadOnly = true;
            this.DisplayActionTime.Width = 60;
            // 
            // DisplayManualStandardTime
            // 
            this.DisplayManualStandardTime.DataPropertyName = "DisplayManualStandardTime";
            this.DisplayManualStandardTime.HeaderText = "S . T";
            this.DisplayManualStandardTime.Name = "DisplayManualStandardTime";
            this.DisplayManualStandardTime.ReadOnly = true;
            this.DisplayManualStandardTime.Width = 60;
            // 
            // 速度
            // 
            this.速度.DataPropertyName = "Speed";
            this.速度.HeaderText = "速度";
            this.速度.Name = "速度";
            this.速度.ReadOnly = true;
            this.速度.Width = 40;
            // 
            // 类型
            // 
            this.类型.HeaderText = "类型";
            this.类型.Name = "类型";
            this.类型.ReadOnly = true;
            this.类型.Width = 55;
            // 
            // Type
            // 
            this.Type.HeaderText = "大分类";
            this.Type.Name = "Type";
            this.Type.ReadOnly = true;
            this.Type.Width = 80;
            // 
            // Code
            // 
            this.Code.DataPropertyName = "ActionCode";
            this.Code.HeaderText = "代码";
            this.Code.Name = "Code";
            this.Code.ReadOnly = true;
            // 
            // TypeId
            // 
            this.TypeId.DataPropertyName = "TypeId";
            this.TypeId.HeaderText = "TypeId";
            this.TypeId.Name = "TypeId";
            this.TypeId.ReadOnly = true;
            this.TypeId.Visible = false;
            // 
            // ActionTypeId
            // 
            this.ActionTypeId.DataPropertyName = "ActionTypeId";
            this.ActionTypeId.HeaderText = "ActionTypeId";
            this.ActionTypeId.Name = "ActionTypeId";
            this.ActionTypeId.ReadOnly = true;
            this.ActionTypeId.Visible = false;
            // 
            // StartTime
            // 
            this.StartTime.DataPropertyName = "StartTime";
            this.StartTime.HeaderText = "StartTime";
            this.StartTime.Name = "StartTime";
            this.StartTime.ReadOnly = true;
            this.StartTime.Visible = false;
            // 
            // EndTime
            // 
            this.EndTime.DataPropertyName = "EndTime";
            this.EndTime.HeaderText = "EndTime";
            this.EndTime.Name = "EndTime";
            this.EndTime.ReadOnly = true;
            this.EndTime.Visible = false;
            // 
            // ST
            // 
            this.ST.DataPropertyName = "ManualStandardTime";
            this.ST.HeaderText = "S . T";
            this.ST.Name = "ST";
            this.ST.ReadOnly = true;
            this.ST.Visible = false;
            this.ST.Width = 60;
            // 
            // TIME
            // 
            this.TIME.DataPropertyName = "ActionTime";
            this.TIME.HeaderText = "TIME";
            this.TIME.Name = "TIME";
            this.TIME.ReadOnly = true;
            this.TIME.Visible = false;
            this.TIME.Width = 50;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgv1);
            this.panel2.Location = new System.Drawing.Point(9, 587);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(578, 178);
            this.panel2.TabIndex = 26;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dgv2);
            this.panel3.Location = new System.Drawing.Point(598, 587);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(578, 178);
            this.panel3.TabIndex = 27;
            // 
            // dgv2
            // 
            this.dgv2.AllowUserToAddRows = false;
            this.dgv2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id2,
            this.ActionName2,
            this.OperationName2,
            this.Cycle2,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.速度2,
            this.类型2,
            this.Type2,
            this.Code2,
            this.TypeId2,
            this.ActionTypeId2,
            this.StartTime2,
            this.EndTime2,
            this.TIME2,
            this.ST2});
            this.dgv2.Location = new System.Drawing.Point(4, 0);
            this.dgv2.MultiSelect = false;
            this.dgv2.Name = "dgv2";
            this.dgv2.ReadOnly = true;
            this.dgv2.RowTemplate.Height = 23;
            this.dgv2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv2.Size = new System.Drawing.Size(571, 171);
            this.dgv2.TabIndex = 35;
            // 
            // id2
            // 
            this.id2.HeaderText = "id";
            this.id2.Name = "id2";
            this.id2.ReadOnly = true;
            this.id2.Visible = false;
            // 
            // ActionName2
            // 
            this.ActionName2.DataPropertyName = "ActionName";
            this.ActionName2.HeaderText = "动作名称";
            this.ActionName2.Name = "ActionName2";
            this.ActionName2.ReadOnly = true;
            this.ActionName2.Width = 80;
            // 
            // OperationName2
            // 
            this.OperationName2.DataPropertyName = "OperationName";
            this.OperationName2.HeaderText = "操作名称";
            this.OperationName2.Name = "OperationName2";
            this.OperationName2.ReadOnly = true;
            this.OperationName2.Width = 80;
            // 
            // Cycle2
            // 
            this.Cycle2.DataPropertyName = "Cycle";
            this.Cycle2.HeaderText = "周期";
            this.Cycle2.Name = "Cycle2";
            this.Cycle2.ReadOnly = true;
            this.Cycle2.Width = 40;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "DisplayActionTime";
            this.dataGridViewTextBoxColumn1.HeaderText = "TIME";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 60;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "DisplayManualStandardTime";
            this.dataGridViewTextBoxColumn2.HeaderText = "S . T";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 60;
            // 
            // 速度2
            // 
            this.速度2.DataPropertyName = "Speed";
            this.速度2.HeaderText = "速度";
            this.速度2.Name = "速度2";
            this.速度2.ReadOnly = true;
            this.速度2.Width = 40;
            // 
            // 类型2
            // 
            this.类型2.HeaderText = "类型";
            this.类型2.Name = "类型2";
            this.类型2.ReadOnly = true;
            // 
            // Type2
            // 
            this.Type2.HeaderText = "大分类";
            this.Type2.Name = "Type2";
            this.Type2.ReadOnly = true;
            // 
            // Code2
            // 
            this.Code2.DataPropertyName = "ActionCode";
            this.Code2.HeaderText = "代码";
            this.Code2.Name = "Code2";
            this.Code2.ReadOnly = true;
            // 
            // TypeId2
            // 
            this.TypeId2.DataPropertyName = "TypeId";
            this.TypeId2.HeaderText = "TypeId";
            this.TypeId2.Name = "TypeId2";
            this.TypeId2.ReadOnly = true;
            this.TypeId2.Visible = false;
            // 
            // ActionTypeId2
            // 
            this.ActionTypeId2.DataPropertyName = "ActionTypeId";
            this.ActionTypeId2.HeaderText = "ActionTypeId";
            this.ActionTypeId2.Name = "ActionTypeId2";
            this.ActionTypeId2.ReadOnly = true;
            this.ActionTypeId2.Visible = false;
            // 
            // StartTime2
            // 
            this.StartTime2.DataPropertyName = "StartTime";
            this.StartTime2.HeaderText = "StartTime";
            this.StartTime2.Name = "StartTime2";
            this.StartTime2.ReadOnly = true;
            this.StartTime2.Visible = false;
            // 
            // EndTime2
            // 
            this.EndTime2.DataPropertyName = "EndTime";
            this.EndTime2.HeaderText = "EndTime";
            this.EndTime2.Name = "EndTime2";
            this.EndTime2.ReadOnly = true;
            this.EndTime2.Visible = false;
            // 
            // TIME2
            // 
            this.TIME2.DataPropertyName = "ActionTime";
            this.TIME2.HeaderText = "TIME";
            this.TIME2.Name = "TIME2";
            this.TIME2.ReadOnly = true;
            this.TIME2.Visible = false;
            this.TIME2.Width = 50;
            // 
            // ST2
            // 
            this.ST2.DataPropertyName = "ManualStandardTime";
            this.ST2.HeaderText = "S . T";
            this.ST2.Name = "ST2";
            this.ST2.ReadOnly = true;
            this.ST2.Visible = false;
            this.ST2.Width = 60;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.dgv3);
            this.panel4.Location = new System.Drawing.Point(1183, 57);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(381, 712);
            this.panel4.TabIndex = 28;
            // 
            // dgv3
            // 
            this.dgv3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdA,
            this.NameA,
            this.IdB,
            this.NameB,
            this.TimeDiff});
            this.dgv3.Location = new System.Drawing.Point(3, 3);
            this.dgv3.Name = "dgv3";
            this.dgv3.RowTemplate.Height = 23;
            this.dgv3.Size = new System.Drawing.Size(373, 706);
            this.dgv3.TabIndex = 1;
            // 
            // IdA
            // 
            this.IdA.DataPropertyName = "IdA";
            this.IdA.HeaderText = "IdA";
            this.IdA.Name = "IdA";
            this.IdA.Width = 30;
            // 
            // NameA
            // 
            this.NameA.DataPropertyName = "NameA";
            this.NameA.HeaderText = "名称";
            this.NameA.Name = "NameA";
            // 
            // IdB
            // 
            this.IdB.DataPropertyName = "IdB";
            this.IdB.HeaderText = "IdB";
            this.IdB.Name = "IdB";
            this.IdB.Width = 30;
            // 
            // NameB
            // 
            this.NameB.DataPropertyName = "NameB";
            this.NameB.HeaderText = "名称B";
            this.NameB.Name = "NameB";
            // 
            // TimeDiff
            // 
            this.TimeDiff.DataPropertyName = "TimeDiff";
            this.TimeDiff.HeaderText = "动作时间差";
            this.TimeDiff.Name = "TimeDiff";
            // 
            // lblProcess1
            // 
            this.lblProcess1.BackColor = System.Drawing.Color.Blue;
            this.lblProcess1.Location = new System.Drawing.Point(17, 565);
            this.lblProcess1.Name = "lblProcess1";
            this.lblProcess1.Size = new System.Drawing.Size(20, 5);
            this.lblProcess1.TabIndex = 35;
            this.lblProcess1.Visible = false;
            // 
            // lblProcess2
            // 
            this.lblProcess2.BackColor = System.Drawing.Color.Blue;
            this.lblProcess2.Location = new System.Drawing.Point(601, 565);
            this.lblProcess2.Name = "lblProcess2";
            this.lblProcess2.Size = new System.Drawing.Size(20, 5);
            this.lblProcess2.TabIndex = 36;
            this.lblProcess2.Visible = false;
            // 
            // FormDoublePlay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1564, 834);
            this.ControlBox = false;
            this.Controls.Add(this.lblProcess2);
            this.Controls.Add(this.lblProcess1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.tbVideoTime2);
            this.Controls.Add(this.tbVideoTime);
            this.Controls.Add(this.toolStrip2);
            this.Controls.Add(this.trackBar2);
            this.Controls.Add(this.pE2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.panelEx1);
            this.DoubleBuffered = true;
            this.Name = "FormDoublePlay";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv2)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevComponents.DotNetBar.StyleManager styleManager1;
        private DevComponents.DotNetBar.StyleManager styleManager2;
        private DevComponents.DotNetBar.PanelEx panelEx1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        public System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TrackBar trackBar1;
        private DevComponents.DotNetBar.ButtonItem buttonItem4;
        private DevComponents.DotNetBar.ButtonItem buttonItem5;
        private DevComponents.DotNetBar.ButtonItem buttonItem6;
        private DevComponents.DotNetBar.ButtonItem buttonItem7;
        private DevComponents.DotNetBar.QatCustomizeItem qatCustomizeItem1;
        private DevComponents.DotNetBar.RibbonTabItem ribbonTabItem1;
        private System.Windows.Forms.Panel panel1;
        private DevComponents.DotNetBar.PanelEx pE2;
        private System.Windows.Forms.TrackBar trackBar2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tSBtn_stop;
        private System.Windows.Forms.ToolStripButton tSBtn_openfile;
        private System.Windows.Forms.ToolStripButton tSB_backward;
        private System.Windows.Forms.ToolStripButton tSB_forward;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripDropDownButton tSDDBtnSpeed;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripDropDownButton tSDDBtnStep;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton tSBtn_stop2;
        private System.Windows.Forms.ToolStripButton btnOpen2;
        private System.Windows.Forms.ToolStripButton btnBackward2;
        private System.Windows.Forms.ToolStripButton btnForward2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripDropDownButton tsDDLSpeed2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem88;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem89;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem90;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem91;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem92;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem93;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem94;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem95;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem96;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.TextBox tbVideoTime;
        private System.Windows.Forms.TextBox tbVideoTime2;
        public System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.ToolStripButton btnContrastStart;
        private System.Windows.Forms.ToolStripButton btnContrastEnd;
        private System.Windows.Forms.DataGridView dgv1;

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView dgv3;
        private System.Windows.Forms.ToolStripButton tSBtn_play;
        private System.Windows.Forms.ToolStripButton tSBtn_play2;
        private System.Windows.Forms.DataGridView dgv2;
        private System.Windows.Forms.ToolStripButton btnCancle1;
        private System.Windows.Forms.ToolStripButton btnSetStart1;
        private System.Windows.Forms.ToolStripButton btnSetEnd1;
        private System.Windows.Forms.ToolStripButton btnCancle2;
        private System.Windows.Forms.Label lblProcess1;
        private System.Windows.Forms.Label lblProcess2;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdA;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameA;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdB;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameB;
        private System.Windows.Forms.DataGridViewTextBoxColumn TimeDiff;
        private System.Windows.Forms.ToolStripButton tsBtnPause;
        private System.Windows.Forms.ToolStripButton tsBtnPause2;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActionName;
        private System.Windows.Forms.DataGridViewTextBoxColumn OperationName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cycle;
        private System.Windows.Forms.DataGridViewTextBoxColumn DisplayActionTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn DisplayManualStandardTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn 速度;
        private System.Windows.Forms.DataGridViewTextBoxColumn 类型;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn Code;
        private System.Windows.Forms.DataGridViewTextBoxColumn TypeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActionTypeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn StartTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn EndTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ST;
        private System.Windows.Forms.DataGridViewTextBoxColumn TIME;
        private System.Windows.Forms.DataGridViewTextBoxColumn id2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActionName2;
        private System.Windows.Forms.DataGridViewTextBoxColumn OperationName2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cycle2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn 速度2;
        private System.Windows.Forms.DataGridViewTextBoxColumn 类型2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Code2;
        private System.Windows.Forms.DataGridViewTextBoxColumn TypeId2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActionTypeId2;
        private System.Windows.Forms.DataGridViewTextBoxColumn StartTime2;
        private System.Windows.Forms.DataGridViewTextBoxColumn EndTime2;
        private System.Windows.Forms.DataGridViewTextBoxColumn TIME2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ST2;
    }
}