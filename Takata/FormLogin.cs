﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Windows.Forms;
using DAL.SystemManage;
using DevComponents.DotNetBar;
using Microsoft.Win32;
using Model;

namespace Takata
{
    public partial class FormLogin : Office2007Form
    {
        public FormLogin()
        {
            InitializeComponent();
            tbxName.Select();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            Splasher.Show(typeof(FrmSplashScreen));
            var userName = tbxName.Text.Trim();
            var pwd = tbxPwd.Text.Trim();

            try
            {
                var user = new UserManage().Login(new t_User() {LoginName = userName, UserPwd = pwd});
                if (user != null)
                {
                    Program.CurrentUser = user;
                    if (user.ZoneId != null)
                        Program.CurrentZone = new ZoneManage().GetSingleZone((int)user.ZoneId);
                    DialogResult = DialogResult.OK;
                    Splasher.Close();
                }
                else
                {
                    Splasher.Close();
                    DialogResult = DialogResult.None;
                    MessageBox.Show("用户名或密码错误，登录失败", "提示");
                }
            }
            catch (Exception ex)
            {
                var message = "无法连接数据库，请确认数据库配置是否正确";
                if (DialogResult.Yes == MessageBox.Show(message, "数据库错误", MessageBoxButtons.YesNo))
                {
                    var form = new FormEditSystemInfo();
                    form.ShowDialog();
                    Process.Start(Assembly.GetExecutingAssembly().Location);
                }
            }
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            var form = new FormEditSystemInfo();
            form.ShowDialog();
        }
    }
}