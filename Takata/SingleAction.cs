﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DAL.SystemManage;

namespace Takata
{
    public partial class SingleAction : Form
    {
        public string Name = "";
        public string Time = "";
        private delegate void MyInvoke();
        public SingleAction()
        {
            InitializeComponent();
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.AllowUserToAddRows = false;
        }
        public void DoWork()

        {

            MyInvoke mi = LoadData;

            this.BeginInvoke(mi);

        }

        private void SingleAction_Load(object sender, System.EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            dataGridView1.DataSource = SingleActionManage.GetSingleActions();
            dataGridView1.AutoGenerateColumns = false;
        }

        private void dataGridView1_CellEndEdit(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            var n = (string)dataGridView1.Rows[e.RowIndex].Cells[1].Value;
            decimal v = 0;
            try
            {
                v = (decimal)dataGridView1.Rows[e.RowIndex].Cells[2].Value;
            }
            catch { }

            var id = (int)dataGridView1.Rows[e.RowIndex].Cells[0].Value;
            Model.t_SingleAction sa = new Model.t_SingleAction();
            sa.Name = n;
            sa.TimeValue = v;
            if (id == 0)
            {
                SingleActionManage.AddSingleAction(sa);
                Thread thread = new Thread(DoWork);
                thread.Start();
            }
            else
            {
                sa.SingleActionId = id;
                SingleActionManage.EditSingleAction(sa);
            }
        }


        private void tSBtnDelete_Click(object sender, System.EventArgs e)
        {
            var selectedRows = dataGridView1.SelectedRows;
            if (selectedRows.Count != 0)
            {
                foreach (DataGridViewRow selected in selectedRows)
                {
                    var id = (int)selected.Cells[0].Value;
                    SingleActionManage.DeleteSingleAction(id);
                }
            }
            else
            {
                var selectedCells = dataGridView1.SelectedCells;
                if (selectedCells.Count != 0)
                {
                    foreach (DataGridViewCell selected in selectedCells)
                    {
                        var id = (int)selected.OwningRow.Cells[0].Value;
                        SingleActionManage.DeleteSingleAction(id);
                    }
                }
            }
            LoadData();
        }

        private void tSBtnAdd_Click(object sender, System.EventArgs e)
        {
            var SingleActionList = SingleActionManage.GetSingleActions();
            Model.t_SingleAction sa = new Model.t_SingleAction();
            sa.Name = "添加新动作";
            sa.TimeValue = 0;
            SingleActionList.Add(sa);
            dataGridView1.DataSource = SingleActionList;
            dataGridView1.AutoGenerateColumns = false;
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("请输入正确的时间");
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var row = dataGridView1.SelectedRows;
            var selectedCells = dataGridView1.SelectedCells;
            if (row.Count != 0)
            {
                Name = row[0].Cells[1].Value.ToString();
                Time = row[0].Cells[2].Value.ToString();
            }
            else if (selectedCells.Count != 0)
            {
                Name = selectedCells[0].OwningRow.Cells[1].Value.ToString();
                Time = selectedCells[0].OwningRow.Cells[2].Value.ToString();
            }

            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
