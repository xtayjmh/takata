﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using DAL;
using DAL.SystemManage;
using DevComponents.DotNetBar;
using Model;
using Utility;

namespace Takata
{
    public partial class Form2 : Office2007Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string pluginPath = Environment.CurrentDirectory + "\\plugins\\";
            VlcPlayer vlcPlayer = new VlcPlayer(pluginPath);
            //IntPtr render_wnd = this.panelEx1.Handle;
            //vlcPlayer.SetRenderWindow((int)render_wnd);
            vlcPlayer.SetMouseInput();
            vlcPlayer.FrameRender += VlcPlayer_FrameRender;
            vlcPlayer.VideoSetCallBacks(856, 480);
            vlcPlayer.PlayFile("E:\\6.avi");
        }

        private void VlcPlayer_FrameRender(Bitmap obj)
        {
            pictureBox1.BackgroundImage = obj;
        }
    }
}
