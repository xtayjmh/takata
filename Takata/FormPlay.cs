﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using DAL;
using DAL.SystemManage;
using DevComponents.DotNetBar;
using Model;
using Utility;
using WMPLib;

namespace Takata
{
    public partial class FormPlay : Office2007Form
    {
        string address;
        //public VlcPlayer vlcPlayer;
        ComponentResourceManager resources = new ComponentResourceManager(typeof(FormPlay));
        private List<t_ActionType> actionTypes = new List<t_ActionType>();
        private bool is_playinig;
        private bool media_is_open; //标记媒体文件是否打开，若未打开则tsbtn_play读ini打开之前的文件，若打开则跳过这步(避免每次都打开文件造成屏幕跳动)
        private double Speed = 1.00;
        private double rate = 0.04;
        private bool loopPlay;
        private List<t_PlaySpeed> speedList;
        private string OperateType;
        private string ActionDes; //动作叙述

        private double EquipmentTime;
        private string HasWalking;
        private t_Video videoInfo;
        public t_Video CurrentVideo;
        private int CurrentCycle = 0;
        private t_Action tempAction = new t_Action { ActionTime = 0, StartTime = 0, EndTime = 0 }; //临时动作
        public List<t_Action> TempActions = new List<t_Action>();
        public t_Action actionForUpdate = new t_Action();
        private int upCycle = 0;
        private int downCycle = 0;
        private decimal startPoint = 0.0000m; //开始播放的时间点
        private decimal endPoint = 10000.0000m; //播放结束的时间节点
        private bool IsBackToStart = false;
        public Color panelBorderColor = Color.Gray;
        public string filePath = "";
        public int VideoID;
        private int StartIndex = 999; //设置循环时选择开始的行，用于高亮显示这些选择的行
        private int EndIndex = 999; //循环结束的行
        public bool isFromSystemIntegrationForm = false; //added by Ted
        //public MouseHook mouse = new MouseHook();
        private static string oldOperationName = "";
        private static int oldCycle = 0;
        private static bool IsAutoPause = false;
        private static t_Action NewAction = null;
        private static int VScrollIndex = 0;

        private static long lastPlayTime = 0;
        private static long lastPlayTimeGlobal = 0;
        private static bool isAction;
        //private static double correctPlayTime;
        private bool UsedPlayed = false;

        public FormPlay()
        {
            InitializeComponent();
            address = Environment.CurrentDirectory;
            Control.CheckForIllegalCrossThreadCalls = false;
            dataGridView1.AutoGenerateColumns = false;
            tsBtnSave.Enabled = false;
            btnDgbz.Enabled = false;
            btnZHBZ.Enabled = false;
            vlcPlayer.uiMode = "none";
        }

        private void PlayerPlay()
        {
            //double currentTime = vlcPlayer.Ctlcontrols.currentPosition;
            //if(correctPlayTime > currentTime && correctPlayTime != 0)
            //{
            //    setCurrentPostion(correctPlayTime);
            //    //vlcPlayer.Ctlcontrols.currentPosition = correctPlayTime;
            //}

            vlcPlayer.Ctlcontrols.play();
            timer1.Start();
        }

        private void PlayerPause()
        {
            var status = vlcPlayer.playState.ToString();
            if (status  == "wmppsPlaying") vlcPlayer.Ctlcontrols.pause();
            timer1.Stop();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //var role = RoleManage.GetRoleList();//获取权限列表
            //var r = RoleManage.AddNewRole(new t_Role(){RoleName = "一般用户",RoleNote = "一般用户只有一般的权限"});添加权限
            //var r = RoleManage.UpdateRole(new t_Role(){RoleId = 3,RoleName = "一般用户",RoleNote = "一般用户的权限"});
            //var re = RoleManage.DeleteRole(3);
            //            label_media_name.Hide();
            if (!File.Exists(address + "\\Menu.ini"))
            {
                FileStream fs = new FileStream(address + "\\Menu.ini", FileMode.Create, FileAccess.Write);
                fs.Close();
            }

            //string pluginPath = Environment.CurrentDirectory + "\\plugins\\";
            //vlcPlayer = new VlcPlayer(pluginPath);
            ////IntPtr render_wnd = this.panelEx1.Handle;
            //vlcPlayer.SetRenderWindow((int)render_wnd);
            //vlcPlayer.SetMouseInput();
            tbVideoTime.Text = "00:00:00/00:00:00";
            is_playinig = false;
            media_is_open = false;
            this.Size = new Size(800, 800);
            var list = Common.GetList<t_PlaySpeed>(null);
            list.ForEach(i =>
            {
                tSDDBtnSpeed.DropDownItems.Add(i.PlaySpeed.ToString());
                cbSD.Items.Add(i.PlaySpeed);
            });
            cbSD.SelectedText = "1.00";
            speedList = list;
            actionTypes = Common.GetList<t_ActionType>(null); //获取操作类型
            tsBtnSave.Enabled = false;
            //mouse.OnMouseActivity += mouse_OnMouseActivity;
            //mouse.Start();
        }
        //鼠标相应事件-弃用
        //void mouse_OnMouseActivity(object sender, MouseEventArgs e)
        //{
        //    var rect = panelEx1.Bounds;
        //    var point = PointToScreen(panelEx1.Location);

        //    if (e.Button != MouseButtons.Left || e.Clicks != 1 || e.X <= point.X || e.Y <= point.Y + 50 || e.X >= point.X + rect.Width || e.Y >= point.Y + rect.Height) return;
        //    if (vlcPlayer.IsPlaying())
        //    {
        //        vlcPlayer.Pause();
        //        timer1.Stop();
        //    }
        //    else
        //    {
        //        tsBtnSign.Enabled = true;
        //        vlcPlayer.Play();
        //        timer1.Start();
        //    }
        //}

        private void LoadOperationName()
        {
            if (CurrentVideo == null) return;
            var list = ActionManage.GetOperationName(CurrentVideo.VideoId);
            cbCZMC.Items.Clear();
            if (!list.Any()) return;
            list.ForEach(i =>
            {
                if (i != null) cbCZMC.Items.Add(i);
            });
        }

        private void tSBtn_play_ButtonClick(object sender, EventArgs e)
        {
            if (endPoint != 10000.0000m) //如果有结束点
            {
                var status = vlcPlayer.playState.ToString();
                if (status != "wmppsPlaying") PlayHistory();
                setCurrentPostion((double)startPoint);
                //vlcPlayer.Ctlcontrols.currentPosition = (double)startPoint;
                trackBar1.Value = (int)(startPoint * 1000);
                PlayerPlay();
            }
            else if (IsBackToStart) //播放结束回到开始点击播放
            {
                PlayHistory();
                IsBackToStart = false;
            }
            else
            {
                PlayerPlay();
                is_playinig = true;
            }
        }

        private void tSBtn_stop_Click(object sender, EventArgs e)
        {
            var status = vlcPlayer.playState.ToString();
            if (status == "wmppsPlaying")
            {
                vlcPlayer.Ctlcontrols.pause();
                IsBackToStart = false;
            }
            else
            {
                IsBackToStart = true;
            }

            timer1.Stop();
            is_playinig = false;
            media_is_open = true;
            trackBar1.Value = 0;
            vlcPlayer.Ctlcontrols.play();
            setCurrentPostion(0.0001);
            //vlcPlayer.Ctlcontrols.currentPosition = 0.0001;
            tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(trackBar1.Value), GetTimeString(trackBar1.Maximum));
            if (status == "wmppsPlaying") vlcPlayer.Ctlcontrols.pause();
        }

        private void tSBtn_openfile_Click(object sender, EventArgs e)
        {
            if (!OpenFile()) return ;
            //correctPlayTime = 0;
            tsBtnSign.Enabled = true;
//            LoadOperationName();
            lbProcess.Visible = false;
            btnCZLX.Enabled = true;
            timer1.Start();
            var status = vlcPlayer.playState.ToString();
            if (status == "wmppsPlaying")
            {
                timer1.Interval = 1;
            }
        }
        private bool OpenStop = false;
        /// <summary>
        /// 打开ToolStripMenuItem_Click
        /// 打开文件并将文件目录添加到Menu.ini
        /// 若打开相同文件则不添加(这个有Bug,这样的话按tsBtn_play打开的就不是上一个了，因为打开相同的不添加)
        /// 若记录行数超过3个，则先记录后三个数据，再重新建一个Menu.ini（清除数据）,接着讲记录的后三个数据写入
        /// </summary>
        private bool OpenFile()
        {
            var openState = false;
            isFromSystemIntegrationForm = false;
            openFileDialog1.FileName = "";
            OpenStop = false;// true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                
                StreamWriter s = new StreamWriter(address + "\\Menu.ini", true);
                s.WriteLine(openFileDialog1.FileName);
                s.Flush();
                s.Close();
                string[] text = File.ReadAllLines(address + "\\Menu.ini");
                int row = text.Length; //行
                int rowcount;
                string[] tempdata = { "", "", "" };
                if (row > 3) // 若记录行数超过3个，则先记录后三个数据，再重新建一个Menu.ini（清除数据）,接着讲记录的后三个数据写入
                {
                    StreamReader sr1 = new StreamReader(address + "\\Menu.ini", true);
                    while (sr1.Peek() > -1)
                    {
                        sr1.ReadLine(); //空读，跳过原始的第一个数据，从第二个数据开始读
                        for (rowcount = 0; rowcount < 3; rowcount++)
                        {
                            tempdata[rowcount] = sr1.ReadLine();
                        }
                    }

                    sr1.Close();
                    FileStream fs = new FileStream(address + "\\Menu.ini", FileMode.Create, FileAccess.Write);
                    fs.Close();
                    StreamWriter s1 = new StreamWriter(address + "\\Menu.ini", true);
                    s1.WriteLine(tempdata[0]);
                    s1.WriteLine(tempdata[1]);
                    s1.WriteLine(tempdata[2]);
                    s1.Flush();
                    s1.Close();
                }

                filePath = openFileDialog1.FileName;
                var fileName = Path.GetFileName(filePath);
                var mode = Common.GetSingleModel<t_Video>(i => i.FileName == fileName); //先判断选择的视频是否已经在数据库中存在
                if (mode != null) //如果存在
                {
                    CurrentVideo = mode;
                    VideoID = mode.VideoId; // added by Ted on 20180312 for other forms
                }
                else //如果不存在，需要初始化视频基础数据
                {
                    var form = new FormInit();
                    form.ShowDialog();
                    if (form.DialogResult == DialogResult.OK)
                    {
                        var formVideo = form.video;
                        var newFilePath = Environment.CurrentDirectory + "\\Resource\\" + formVideo.WorkLineId + "\\1\\";
                        var FileName = Path.GetFileName(filePath);

                        bgWUploadVideo.RunWorkerAsync(new[] { filePath, newFilePath, fileName });
                        var fileInfo = new FileInfo(filePath);
                        formVideo.FileName = FileName;
                        formVideo.FileSize = (int)(fileInfo.Length / 1024.0);
                        formVideo.IsUploaded = false; // added by Ted on 20180316 for upload
                        formVideo.CreatedBy = Program.CurrentUser.UserId;
                        formVideo.Created = DateTime.Now;
                        formVideo.Updated = DateTime.Now;
                        CurrentVideo = Common.AddNewObjReturnObj(form.video); //获取初始化的视频基础数据之后，保存视频到数据库
                        VideoID = CurrentVideo.VideoId; // added by Ted on 20180312 for other forms
                    }
                    else
                    {
                        return openState;
                    }
                }
                Speed = 1.00;
                tSDDBtnSpeed.Text = "1.00";
                vlcPlayer.settings.rate = Speed; //设置播放速度
                vlcPlayer.URL = filePath;
                
                trackBar1.SetRange(0, (int)vlcPlayer.currentMedia.duration* 1000);
                trackBar1.Value = 0;
                is_playinig = true;
                media_is_open = true;
                Thread.Sleep(200);
                vlcPlayer.Ctlcontrols.play();
                timer1.Start();
                ShowGrid();
                NewAction = null;
                IsAutoPause = false;
                VScrollIndex = 0;
                openState = true;
            }
            return openState;
        }

        private void PlayHistory()
        {
            StreamWriter s = new StreamWriter(address + "\\Menu.ini", true);
            s.WriteLine(openFileDialog1.FileName);
            s.Flush();
            s.Close();
            string[] text = File.ReadAllLines(address + "\\Menu.ini");
            int row = text.Length; //行
            int rowcount;
            string[] tempdata = new string[] { "", "", "" };
            if (row > 3) // 若记录行数超过3个，则先记录后三个数据，再重新建一个Menu.ini（清除数据）,接着讲记录的后三个数据写入
            {
                StreamReader sr1 = new StreamReader(address + "\\Menu.ini", true);
                while (sr1.Peek() > -1)
                {
                    sr1.ReadLine(); //空读，跳过原始的第一个数据，从第二个数据开始读
                    for (rowcount = 0; rowcount < 3; rowcount++)
                    {
                        tempdata[rowcount] = sr1.ReadLine();
                    }
                }

                sr1.Close();
                FileStream fs = new FileStream(address + "\\Menu.ini", FileMode.Create, FileAccess.Write);
                fs.Close();
                StreamWriter s1 = new StreamWriter(address + "\\Menu.ini", true);
                s1.WriteLine(tempdata[0]);
                s1.WriteLine(tempdata[1]);
                s1.WriteLine(tempdata[2]);
                s1.Flush();
                s1.Close();
            }

            vlcPlayer.URL = openFileDialog1.FileName;
            trackBar1.SetRange(0, (int)vlcPlayer.Ctlcontrols.currentItem.duration * 1000);
            trackBar1.Value = 0;
            timer1.Start();
            if (vlcPlayer.playState.ToString()== "wmppsPlaying")
            {
                timer1.Interval = 1;
            }

            is_playinig = true;
            media_is_open = true;

            //            SetVideoInfo(openFileDialog1.FileName);
            LoadOperationName();
        }

        private void tSB_backward_Click(object sender, EventArgs e)
        {
            tsBtnSign.Enabled = true;
            vlcPlayer.Ctlcontrols.pause();
            int time = (int)vlcPlayer.Ctlcontrols.currentPosition - 5;
            if (time > 0)
            {
                setCurrentPostion(time);
                //vlcPlayer.Ctlcontrols.currentPosition=time;
            }
            else
            {
                setCurrentPostion(0);
                //vlcPlayer.Ctlcontrols.currentPosition = 0; 
            }

            vlcPlayer.Ctlcontrols.play();
            trackBar1.Value = (int)vlcPlayer.Ctlcontrols.currentPosition * 1000;
            SetSelectionByTime();
        }

        private void tSB_forward_Click(object sender, EventArgs e)
        {
            tsBtnSign.Enabled = true;
            vlcPlayer.Ctlcontrols.pause();
            int time = (int)vlcPlayer.Ctlcontrols.currentPosition + 5;
            if (time * 1000 < trackBar1.Maximum)
            {
                setCurrentPostion(time);
                //vlcPlayer.Ctlcontrols.currentPosition = time;
                trackBar1.Value = (int)vlcPlayer.Ctlcontrols.currentPosition * 1000;
            }
            else
            {
                setCurrentPostion(trackBar1.Maximum);
                //vlcPlayer.Ctlcontrols.currentPosition = trackBar1.Maximum;
                trackBar1.Value = trackBar1.Maximum;
            }

            vlcPlayer.Ctlcontrols.play();
            SetSelectionByTime();
        }

        /// <summary>
        /// 按帧数获取某一个时间点所在帧的真实开始时间  By Ted
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        private decimal GetRealTime(decimal time,int step=0)
        {
            int fps = vlcPlayer.network.encodedFrameRate;
            int frameCount = (int)(time * (decimal)fps);
            frameCount= frameCount+ step; //add 
            decimal realTime = (decimal)((decimal)frameCount / (decimal)fps);
            return realTime;
        }

        /// <summary>
        /// 获取VLC精确的播放时间 By Ted
        /// </summary>
        /// <returns></returns>
        //public double getCurrentTime()
        //{
        //    long currentTime = vlcPlayer.GetLongPlayTime();
        //    var millisecond = System.DateTime.Now.Millisecond;
        //    millisecond = (int)((float)millisecond * Speed);
        //    Console.WriteLine("0. 获取的当前播放时间:" + currentTime);
        //    Console.WriteLine("0. 上次播放时间:" + lastPlayTime);
        //    Console.WriteLine("0. 当前系统时间毫秒:" + millisecond);
        //    if (lastPlayTime == currentTime && lastPlayTime != 0)
        //    {
        //        if (lastPlayTimeGlobal > millisecond)
        //        {
        //            currentTime += millisecond;
        //        }
        //        else
        //        {
        //            currentTime += (millisecond - lastPlayTimeGlobal);
        //        }
        //        Console.WriteLine("1.上次播放全局时间:" + lastPlayTimeGlobal);
        //        Console.WriteLine("2.当前时间:" + currentTime);
        //    }
        //    else
        //    {
        //        lastPlayTime = currentTime;
        //        lastPlayTimeGlobal = millisecond;
        //        Console.WriteLine("1. 上次播放全局时间:" + lastPlayTimeGlobal);
        //        Console.WriteLine("2. 上次播放时间:" + lastPlayTime);
        //    }

        //    return currentTime * 0.001f ;   //to float
        //}

        private void timer1_Tick(object sender, EventArgs e)
        {
            decimal duration = (decimal)vlcPlayer.Ctlcontrols.currentItem.duration;
            if (duration == 0)
            {
                return;
            }
            trackBar1.SetRange(0, (int)vlcPlayer.currentMedia.duration * 1000);
            if (OpenStop)
            {
                timer1.Stop();
                OpenStop = false;
            }
            //decimal wrongcurrentTime = vlcPlayer.Ctlcontrols.currentPosition();
            double currentTime = vlcPlayer.Ctlcontrols.currentPosition;
            //Console.WriteLine("Rate:" + rate);
          
            //float fps = vlcPlayer.network.encodedFrameRate;

            //int frameCount = (int)((double)tempAction.EndTime * (double)fps);

            //int CurrentframeCount = (int)(currentTime * (double)fps);

            //int varFrameCount = frameCount - 3;

            //Console.WriteLine("3. 动作开始时间:" + tempAction.StartTime);
            //Console.WriteLine("4. 动作结束时间:" + tempAction.EndTime);
            ////Console.WriteLine("5. 错误播放时间:" + wrongcurrentTime);
            //Console.WriteLine("6. 实际播放时间:" + currentTime);
            //Console.WriteLine("计时器间隔:" + timer1.Interval);
            //Console.WriteLine("7. 当前动作总帧数:" + frameCount);
            //Console.WriteLine("8. 当前帧数:" + CurrentframeCount);
            //Console.WriteLine("播放时长:" + duration);


            if (trackBar1.Value == trackBar1.Maximum || currentTime > (double)duration - 0.2)
            {
                //                    vlcPlayer.Stop();
                IsBackToStart = true;
                PlayerPause();
                //                tsBtnSign.Enabled = false;
                if (vlcPlayer.playState.ToString() == "wmppsPlaying")
                    tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(duration * 1000), GetTimeString(duration * 1000));
                trackBar1.Value = trackBar1.Maximum;
            }
            else
            {
                if (endPoint != 10000.0000m && currentTime >= (double)endPoint)
                {
                    trackBar1.Value = (int)(endPoint * 1000);
                    vlcPlayer.Ctlcontrols.currentPosition  = double.Parse(startPoint.ToString());
                    SetSelectionByTime(); //根据播放时间选择右半部分对应的标记
                }
                //else if (IsAutoPause && tempAction != null && currentTime >= (double)tempAction.EndTime && !isAction)//点击标记的话，只播放当前标记，不会继续往后播放
                //{
                //    //PlayerPause();
                //    vlcPlayer.Ctlcontrols.pause();
                //    //setCurrentPostion((double)tempAction.EndTime);
                //}
                else
                {
                    if (vlcPlayer.playState.ToString() == "wmppsPlaying")
                    {
                        tsBtnSign.Enabled = true;
                        //trackBar1.Value = (trackBar1.Value + (int) Speed * 1000) > trackBar1.Maximum ? trackBar1.Maximum : trackBar1.Value + (int) Speed * 1000; //currentTime();//获取播放时间，赋值给trackbar
                        trackBar1.Value = (int)currentTime * 1000;
                        if (vlcPlayer.playState.ToString() == "wmppsPlaying")
                            tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString((decimal)currentTime * 1000), GetTimeString(duration * 1000));
                        SetSelectionByTime(); //根据播放时间选择右半部分对应的标记
                    }
                }
            }

            if (IsAutoPause && tempAction != null && currentTime >= (double)tempAction.EndTime && !isAction)
            {
                IsAutoPause = false;
                return;
            }

      
        }

        private string GetTimeString(decimal val)
        {
            val = val / 1000;
            var hour = val / 3600;
            val %= 3600;
            var minute = val / 60;
            var second = val % 60;
            var fps = vlcPlayer.network.encodedFrameRate;
            var allTime = vlcPlayer.Ctlcontrols.currentItem.duration;

            var frame = (val % 1) / (1 / (decimal)(fps==0?25:fps)); //帧
            return $"{hour:00}:{minute:00}:{(int)second:00}({(int)frame})";
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            //            if (!is_playinig || !vlcPlayer.IsPlaying()) PlayHistory();
            if (vlcPlayer.playState.ToString() == "wmppsPlaying")
            {
                PlayerPause();
            }
            setCurrentPostion(trackBar1.Value / 1000);
            //vlcPlayer.Ctlcontrols.currentPosition = trackBar1.Value / 1000;
            trackBar1.Value = (int)vlcPlayer.Ctlcontrols.currentPosition * 1000;
            SetSelectionByTime();
        }

        private void toolStripDropDownButton1_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            tSDDBtnSpeed.Text = e.ClickedItem.Text;
            Speed = double.Parse(e.ClickedItem.Text);

            //correctPlayTime = vlcPlayer.Ctlcontrols.currentPosition;
            vlcPlayer.settings.rate = Speed; //设置播放速度
 
        }

        private void tSDDBtnStep_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e) //设置步进帧数
        {
            var stepNum = e.ClickedItem.Text;
            tSDDBtnStep.Text = stepNum;
            var fps = vlcPlayer.network.encodedFrameRate; //获取视频的码率29.97003
            rate = 1 * float.Parse(stepNum) / fps;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            tsBtnSign.Enabled = true;
            if (vlcPlayer.playState.ToString() == "wmppsPlaying")
            {
                PlayerPause();
                is_playinig = false;
            }
            var fps = vlcPlayer.network.encodedFrameRate;
            var playingTime = (decimal)vlcPlayer.Ctlcontrols.currentPosition;// vlcPlayer.Ctlcontrols.currentPosition();
            //playingTime = GetRealTime(playingTime,1);  //updated by Ted
            playingTime = playingTime - (decimal)rate;  //updated by Ted
            if ((playingTime) * 1000 > trackBar1.Minimum)
            {
                setCurrentPostion((double)playingTime,true);
                //vlcPlayer.Ctlcontrols.currentPosition = (double)playingTime;
                trackBar1.Value = (int)playingTime * 1000;
                tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(playingTime * 1000), GetTimeString((decimal)vlcPlayer.Ctlcontrols.currentItem.duration * 1000));
            }
            else
            {
                setCurrentPostion(trackBar1.Minimum,true);
                //vlcPlayer.Ctlcontrols.currentPosition = trackBar1.Minimum;
                trackBar1.Value = trackBar1.Minimum;
                tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(playingTime * 1000), GetTimeString((decimal)vlcPlayer.Ctlcontrols.currentItem.duration * 1000));
            }
            //if (vlcPlayer.playState.ToString() == "wmppsPlaying") vlcPlayer.Ctlcontrols.pause();
            is_playinig = false;
            SetSelectionByTime();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            tsBtnSign.Enabled = true;
            if (vlcPlayer.playState.ToString() == "wmppsPlaying")
            {
                PlayerPause();
                is_playinig = false;
            }
            var fps = vlcPlayer.network.encodedFrameRate;
            var playingTime = (decimal)vlcPlayer.Ctlcontrols.currentPosition;// vlcPlayer.Ctlcontrols.currentPosition();
            //playingTime = GetRealTime(playingTime,0);  //updated by Ted
            playingTime = playingTime + (decimal)rate;
            if (playingTime < (decimal)vlcPlayer.Ctlcontrols.currentItem.duration - 0.2m)
            {
                setCurrentPostion((double)playingTime);
                //vlcPlayer.Ctlcontrols.currentPosition  = (double)playingTime;
                trackBar1.Value = (int)playingTime * 1000;
                tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(playingTime * 1000), GetTimeString((decimal)vlcPlayer.Ctlcontrols.currentItem.duration * 1000));
            }
            else
            {
                setCurrentPostion((double)vlcPlayer.Ctlcontrols.currentItem.duration);
                //vlcPlayer.Ctlcontrols.currentPosition  = (double)vlcPlayer.Ctlcontrols.currentItem.duration - 0.1;
                trackBar1.Value = trackBar1.Maximum;
                tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(playingTime * 1000), GetTimeString((decimal)vlcPlayer.Ctlcontrols.currentItem.duration * 1000));
            }
            //if (vlcPlayer.playState.ToString() == "wmppsPlaying") vlcPlayer.Ctlcontrols.pause();
            is_playinig = false;
            SetSelectionByTime();
        }

        private void btnDgbz_Click(object sender, EventArgs e)
        {
            var form = new SingleAction();
            form.ShowDialog();
            if (form.DialogResult == DialogResult.OK)
            {
                tbxDM.Text = "";
//                tbxDZMC.Text = form.Name;
                tbxSDSJ.Text = form.Time;
            }
        }

        private void btnZHBZ_Click(object sender, EventArgs e)
        {
            ModAPTS form = new ModAPTS();
            form.ShowDialog();
            if (form.DialogResult == DialogResult.OK)
            {
                tbxSDSJ.Text = form.ManualTime.ToString();
                tbxDM.Text = form.Code;
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            trackBar1.Value = trackBar1.Maximum;
            vlcPlayer.Ctlcontrols.currentPosition  = trackBar1.Maximum / 1000;
            PlayerPause();
            is_playinig = false;
            tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(trackBar1.Value), GetTimeString(trackBar1.Maximum));
        }

        private void tsBtnDelete_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count <= 0 || MessageBox.Show("您确定删除该动作标记吗？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) != DialogResult.OK) return;
            var selectedRows = dataGridView1.SelectedRows;
            if (selectedRows.Count != 0)
            {
                foreach (DataGridViewRow selected in selectedRows)
                {
                    var actionId = (int)selected.Cells[0].Value;
                    if (selected.Index > 0)
                    {
                        NewAction = dataGridView1.Rows[selected.Index - 1].DataBoundItem as t_Action;
                        actionForUpdate = (t_Action)dataGridView1.Rows[selected.Index - 1].DataBoundItem;
                        tempAction = actionForUpdate;
                    }

                    ActionManage.DeleteAction(actionId);
                }
            }
            CleanActionInfo();
            LoadActions(CurrentVideo.VideoId);
            ReSelectRow();
           
            SetActionInfo(NewAction);
        }

        private void CleanActionInfo()
        {
            tbxSDSJ.Text = "";
            tbxBJMS.Text = "";
            tbxAQZYSX.Text = "";
            tbxPZZYSX.Text = "";
            tbxDZMC.Text = "";
            cbCZMC.Text = "";
            cbLX.Text = "";
            cbSD.Text = "";
            cbZQ.Text = "";
            tbxDM.Text = "";
            //修复新建标记不做操作直接保存，操作类型这里保存的是上一条的数据
            HasWalking = "";
            ActionDes = "";
            OperateType = "";
            EquipmentTime = 0.00;
        }


        private void tsBtnSave_Click(object sender, EventArgs e)
        {
            dataGridView1.EndEdit();
            isAction = true;
            if (actionForUpdate == null) return;
            if (actionForUpdate.ActionTime == null) return; //added by Ted on 20180601
            var bjms = tbxBJMS.Text.Trim(); //标记描述
            var aqzysx = tbxAQZYSX.Text.Trim(); //安全注意事项
            var pzzysx = tbxPZZYSX.Text.Trim(); //品质注意事项
            var dzmc = tbxDZMC.Text.Trim(); //动作名称
            var dm = tbxDM.Text.Trim(); //代码
            decimal sdsj = string.IsNullOrEmpty(tbxSDSJ.Text.Trim()) ? 0 : decimal.Parse(tbxSDSJ.Text); //手动时间
            var czmc = cbCZMC.Text; //操作名称
            var lx = cbLX.Text.Trim(); //类型
            var sd = cbSD.Text; //速度
            var zq = cbZQ.Text; //周期
            if (zq == "向上合并")
            {
                zq = upCycle.ToString();
            }
            else if (zq == "新周期")
            {
                zq = ((actionForUpdate.Cycle ?? 0) + 1).ToString();
            }
            else if (zq == "向下合并")
            {
                zq = downCycle.ToString();
            }

            var dfl = clbDFL.SelectedItem?.ToString();

            var mode = new t_Action
            {
                ActionCode = dm,
                ActionName = dzmc,
                ActionTime = actionForUpdate.ActionTime,
                Description = bjms,
                QualityCaution = pzzysx,
                SafetyCaution = aqzysx,
                OperationName = string.IsNullOrEmpty(czmc) ? null : czmc,
                Speed = decimal.Parse(sd),
                Cycle = int.Parse(zq),
                Created = DateTime.Now,
                VideoId = actionForUpdate.VideoId,
                CreatedBy = Program.CurrentUser.UserId,
                OperationType = OperateType,
                EquipmentTime = (decimal)EquipmentTime,
                Walking = HasWalking,
                TypeId = actionForUpdate.TypeId,
                ActionTypeId = actionTypes.FirstOrDefault(i => i.ChineseName == lx)?.ActionTypeId,
                ActionId = actionForUpdate.ActionId,
                StartTime = actionForUpdate.StartTime,
                EndTime = actionForUpdate.EndTime,  //updated by Ted
                ActionDes = ActionDes
            };
            if (dfl != null)
            {
                mode.TypeId = Common.GetSingleModel<t_Type>(i => i.TypeName == dfl).TypeId;
            }

            if (DDBtnStandardTime.Text == "手动标准时间")
            {
                if (sdsj != 0.00m || mode.TypeId != 2)
                {
                    if (sdsj != 0.00m)
                    {
                        mode.Speed = decimal.Parse((actionForUpdate.ActionTime / sdsj).ToString());
                    }

                    mode.IsManual = true;
                    mode.ManualStandardTime = sdsj;
                    mode.AutoStandardTime = sdsj;
                }
                else
                {
                    MessageBox.Show("手动时间不能为0", "提示");
                    return;
                }
            }
            else
            {
                mode.AutoStandardTime = decimal.Parse((actionForUpdate.ActionTime / decimal.Parse(sd)).ToString());
                mode.ManualStandardTime = mode.AutoStandardTime;
                mode.ActionCode = "";
            }

            ActionManage.AddOrUpdate(mode);
            LoadActions(CurrentVideo.VideoId);

            tsBtnSave.Enabled = false;
            ReSelectRow();

            var rowIndex = TempActions.FindIndex(i => i.ActionId == mode.ActionId);
            if (rowIndex != -1)
               ReSelectNextRow(dataGridView1.Rows[rowIndex + 1]);

            if (!cbCZMC.Items.Contains(czmc)) cbCZMC.Items.Add(czmc);
            IsAutoPause = false;
        }

        private List<int> GetCycle()
        {
            var listCycle = new List<int>();
            if (dataGridView1.Rows.Count == 0)
            {
                listCycle.Add(0);
            }

            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                listCycle.Add((int)(row.Cells[3].Value ?? 0));
            }

            return listCycle;
        }

        private void tsBtnSign_Click(object sender, EventArgs e)
        {
            isAction = true;
            PlayerPause();
            tsBtnSign.Enabled = false;
            IsBackToStart = false;
            if (vlcPlayer.playState.ToString() == "wmppsPlaying")
            {
                PlayerPause();
                is_playinig = false;
            }

            tsBtnSave.Enabled = true;
            var lastEndTime = 0.0000m;
            if (dataGridView1.RowCount > 0 && dataGridView1.Rows[dataGridView1.RowCount - 1].Cells["EndTime"].Value != null)
            {
                lastEndTime = (decimal)dataGridView1.Rows[dataGridView1.RowCount - 1].Cells["EndTime"].Value;
            }

            var playTime = (decimal)vlcPlayer.Ctlcontrols.currentPosition;  //updated by Ted
            playTime = GetRealTime(playTime,1);  //updated by Ted
            //Console.WriteLine("playTimeTemp:" + playTimeTemp);
            //var playTime = decimal.Parse(vlcPlayer.Ctlcontrols.currentPosition().ToString("f2"));
            Console.WriteLine("PlayTime:" + playTime);
            t_Action mode;
            var isSplit = false;
            //如果当前播放时间大于最后一条的结束时间，则新增一个动作，否则就拆分当前动作
            if (playTime > lastEndTime)
            {
                if (dataGridView1.RowCount == 0 && tempAction == null) tempAction = TempActions.Count > 0 ? TempActions.Last() : new t_Action() { StartTime = 0, EndTime = 0 };

                //新增一个动作记录
                mode = new t_Action
                {
                    ActionTime = playTime - tempAction.EndTime, //当前播放时间减去上个动作的
                    StartTime = tempAction.EndTime,
                    EndTime = playTime,
                    VideoId = CurrentVideo.VideoId,
                    Speed = 1.00m,
                    ManualStandardTime = playTime - tempAction.EndTime,
                    AutoStandardTime = playTime - tempAction.EndTime,
                    TypeId = 2,
                    ActionTypeId = 1,
                    Cycle = GetCycle().Max()
                };
            }
            else
            {
                isSplit = true;
                //拆分一个动作
                //被拆分动作的结束时间=视频当前的播放时间
                //新动作的开始时间=视频当前播放时间，新动作结束时间=被拆分动作的结束时间
                //TempActions = Common.GetList<t_Action>(i => i.VideoId == CurrentVideo.VideoId);//重新获取视频的动作数据，虽然有点耗时，但是这个变量的数据会变的不正确，暂时找不到变错的地方，这是一个不完美的解决方案。2018-5-5，Johnon,todo
                var tAction = TempActions.FirstOrDefault(i => playTime > i.StartTime && playTime < i.EndTime);
                if (tAction != null)
                {
                    tempAction = tAction;
                }
                var standardTime = tempAction.EndTime - playTime;

                mode = new t_Action
                {
                    ActionTime = tempAction.EndTime - playTime, //当前播放时间减去上个动作的
                    StartTime = playTime,
                    EndTime = tempAction.EndTime,
                    VideoId = CurrentVideo.VideoId,
                    Speed = 1.00m,
                    ManualStandardTime = standardTime,
                    AutoStandardTime = standardTime,
                    TypeId = 2,
                    ActionTypeId = 1,
                    Cycle = tempAction.Cycle
                };
                tempAction.EndTime = playTime;
                tempAction.AutoStandardTime = (decimal) Math.Round((double) ((playTime - tempAction.StartTime) / (tempAction.Speed == null ? 1.00m : tempAction.Speed)), 2);
                tempAction.ManualStandardTime = (decimal)Math.Round((double)((playTime - tempAction.StartTime) / (tempAction.Speed == null ? 1.00m : tempAction.Speed)), 2);
                tempAction.ActionTime = playTime - tempAction.StartTime;
            }
            if (mode.ActionTime < 0 || tempAction.ActionTime < 0)
            {
                return;//不允许插入负值的时间
            }

            tempAction = isSplit ? ActionManage.SplitAction(tempAction, mode) : ActionManage.AddNewAction(mode, (decimal)vlcPlayer.Ctlcontrols.currentItem.duration); //如果是拆分动作，需要同时更新被拆分动作，新增动作
            NewAction = tempAction;//临时存储新拆分或者新增的标记
            actionForUpdate = tempAction;
            //decimal tempStartTime = actionForUpdate.StartTime;  // added by Ted on 20180530
            //decimal tempEndTime = actionForUpdate.EndTime; // added by Ted on 20180530
            CleanActionInfo();
            if(dataGridView1.RowCount > 0) dataGridView1.Rows[dataGridView1.RowCount - 1].Selected = true;
            SetActionInfo(tempAction);
            if (vlcPlayer.playState.ToString() == "wmppsPlaying") vlcPlayer.Ctlcontrols.pause();
            LoadActions(CurrentVideo.VideoId);
            ReSelectRow();
        }

        private void checkedListBox1_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            for (int i = 0; i < clbDFL.CheckedIndices.Count; i++)
            {
                if (clbDFL.CheckedIndices[i] != e.Index)
                {
                    clbDFL.SetItemChecked(clbDFL.CheckedIndices[i], false);
                }
            }
        }

        private void ReSelectRow()
        {
            if (NewAction == null) return;
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if ((int) row.Cells[0].Value != NewAction.ActionId) continue;
                dataGridView1.ClearSelection();
                if (row.Index == dataGridView1.RowCount - 1)
                {
                    if (row.Index - 1 > 0)
                    {
                        dataGridView1.Rows[row.Index - 1].Selected = true;
                        dataGridView1.CurrentCell = dataGridView1.Rows[row.Index - 1].Cells[1];
                        actionForUpdate = (t_Action) dataGridView1.Rows[row.Index - 1].DataBoundItem;
                        tempAction = actionForUpdate;
                        SetActionInfo(tempAction);
                    }
                }
                else
                {
                    row.Selected = true;
                    dataGridView1.CurrentCell = row.Cells[1];
                    SetActionInfo(row.DataBoundItem as t_Action);
                }
                if (row.Index - VScrollIndex >= 14)
                {
                    VScrollIndex = row.Index - 13;
                }

                if (VScrollIndex < dataGridView1.RowCount)//解决指定的参数已超出有效值的范围2018-5-31
                {
                    dataGridView1.FirstDisplayedScrollingRowIndex = VScrollIndex;
                }

                break;
            }
        }

        private void ReSelectNextRow(DataGridViewRow row)
        {
            var upEndTime = tempAction.EndTime;
            row.Selected = true;
            var id = (int)row.Cells[0].Value;
            actionForUpdate = TempActions.FirstOrDefault(i => i.ActionId == id); //加载动作数据到窗体内对应的控件
            NewAction = actionForUpdate;
            btnSetLoopStart.Enabled = true;
            tempAction = actionForUpdate;
            if (actionForUpdate != null)
            {
                SetActionInfo(actionForUpdate);
                SetPlayProcess(upEndTime == actionForUpdate.StartTime && UsedPlayed == true);
                UsedPlayed = false;
            }

            if (row.Index - VScrollIndex >= 14)
            {
                VScrollIndex = row.Index - 13;
            }

            if (VScrollIndex < dataGridView1.RowCount)//解决指定的参数已超出有效值的范围2018-5-31
            {
                dataGridView1.FirstDisplayedScrollingRowIndex = VScrollIndex;
            }
        }

        //加载动作标记
        public void LoadActions(int videoId)
        {
            var list = ActionManage.GetActions(videoId).OrderBy(i => i.StartTime).ToList();
            if (!list.Any())
            {
                tempAction = new t_Action() {StartTime = 0, EndTime = 0};
                return;
            }

            dataGridView1.DataSource = list;
            tempAction = list.LastOrDefault() ?? new t_Action { ActionTime = 0, StartTime = 0, EndTime = 0 }; //加载视频包含已经分析完成的动作
            TempActions = list;
            //1无效2有效3无关
            for (var i = 0; i < dataGridView1.Rows.Count; i++)
            {
                var row = dataGridView1.Rows[i];
                switch ((int)row.Cells["TypeId"].Value)
                {
                    case 1:
                        row.Cells[8].Style.BackColor = Color.Red;
                        row.Cells[8].Value = "×";
                        break;
                    case 2:
                        row.Cells[8].Style.BackColor = Color.GreenYellow;
                        row.Cells[8].Value = "○";
                        break;
                    case 3:
                        row.Cells[8].Style.BackColor = Color.Yellow;
                        row.Cells[8].Value = "△";
                        break;
                }

                if (row.Cells["ActionTypeId"].Value != null && !string.IsNullOrEmpty(row.Cells["ActionTypeId"].Value.ToString()))
                {
                    var actionType = actionTypes.FirstOrDefault(j => j.ActionTypeId == (int)row.Cells["ActionTypeId"].Value);
                    if (actionType != null)
                    {
                        var color = actionType.Color.Split(',');
                        row.Cells[7].Value = actionType.ChineseName;
                        row.Cells[7].Style.BackColor = Color.FromArgb(int.Parse(color[0]), int.Parse(color[1]), int.Parse(color[2]), int.Parse(color[3]));
                    }
                }

                if (new[] { "1", "3" }.Contains(row.Cells["TypeId"].Value.ToString())) row.Cells[5].Value = decimal.Parse("0.00"); //无关录像ST为0
            }

            if (dataGridView1.RowCount > 0)
            {
                var dgr = dataGridView1.Rows[dataGridView1.RowCount - 1];
                dgr.Cells[4].Value = null;
                dgr.Cells[5].Value = null;
                dgr.Cells[6].Value = 1.00m;
                dgr.Cells[7].Value = null;
                dgr.Cells[7].Style.BackColor = Color.White;
                dgr.Cells[8].Value = null;
                dgr.Cells[8].Style.BackColor = Color.White;
                dgr.Cells[9].Value = null;
            }
        }

        public void LoadActionsFromSystemIntegration(int videoId)
        {
            var list = ActionManage.GetActions(videoId).OrderBy(i => i.StartTime).ToList();
            if (!list.Any()) { list.Add(new t_Action{StartTime = 0,EndTime = (decimal)vlcPlayer.Ctlcontrols.currentItem.duration});}
            dataGridView1.DataSource = list;
            tempAction = list.LastOrDefault() ?? new t_Action { ActionTime = 0, StartTime = 0, EndTime = 0 }; //加载视频包含已经分析完成的动作
            TempActions = list;
            //1无效2有效3无关
            for (var i = 0; i < dataGridView1.Rows.Count; i++)
            {
                var row = dataGridView1.Rows[i];
                if (row.Cells["TypeId"].Value != null)
                {
                    switch ((int) row.Cells["TypeId"].Value)
                    {
                        case 1:
                            row.Cells[8].Style.BackColor = Color.Red;
                            row.Cells[8].Value = "×";
                            break;
                        case 2:
                            row.Cells[8].Style.BackColor = Color.GreenYellow;
                            row.Cells[8].Value = "○";
                            break;
                        case 3:
                            row.Cells[8].Style.BackColor = Color.Yellow;
                            row.Cells[8].Value = "△";
                            break;
                    }
                }

                if (row.Cells["ActionTypeId"].Value != null && !string.IsNullOrEmpty(row.Cells["ActionTypeId"].Value.ToString()))
                {
                    var actionType = actionTypes.FirstOrDefault(j => j.ActionTypeId == (int)row.Cells["ActionTypeId"].Value);
                    if (actionType != null)
                    {
                        var color = actionType.Color.Split(',');
                        row.Cells[7].Value = actionType.ChineseName;
                        row.Cells[7].Style.BackColor = Color.FromArgb(int.Parse(color[0]), int.Parse(color[1]), int.Parse(color[2]), int.Parse(color[3]));
                    }
                }

                var value = row.Cells["TypeId"].Value;
                if (value != null && new[] { "1", "3" }.Contains(value.ToString())) row.Cells[5].Value = decimal.Parse("0.00"); //无关录像ST为0
            }

            if (dataGridView1.RowCount > 0)
            {
                var dgr = dataGridView1.Rows[dataGridView1.RowCount - 1];
                dgr.Cells[4].Value = null;
                dgr.Cells[5].Value = null;
                dgr.Cells[6].Value = 1.00m;
                dgr.Cells[7].Value = null;
                dgr.Cells[7].Style.BackColor = Color.White;
                dgr.Cells[8].Value = null;
                dgr.Cells[8].Style.BackColor = Color.White;
                dgr.Cells[9].Value = null;
            }

            dataGridView1.ClearSelection();
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Rectangle rectangle = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, dataGridView1.RowHeadersWidth - 4, e.RowBounds.Height);
            TextRenderer.DrawText(e.Graphics,
                (e.RowIndex + 1).ToString(),
                dataGridView1.RowHeadersDefaultCellStyle.Font,
                rectangle,
                dataGridView1.RowHeadersDefaultCellStyle.ForeColor,
                TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
        }

        private void btnCZLX_Click(object sender, EventArgs e)
        {
            if (actionForUpdate != null)
            {
                OperateType = actionForUpdate.OperationType;
                EquipmentTime = (double)(actionForUpdate.EquipmentTime ?? 0);
                HasWalking = actionForUpdate.Walking;
                ActionDes = actionForUpdate.ActionDes;
            }

            var form = new OperateStandard(OperateType, EquipmentTime, HasWalking, ActionDes);
            form.ShowDialog();
            OperateType = form.OperateType;
            EquipmentTime = double.Parse(form.EquipmentTime);
            HasWalking = form.HasWalking;
            ActionDes = form.ActionDes;
            if (actionForUpdate == null) return;
            actionForUpdate.OperationType = OperateType;
            actionForUpdate.EquipmentTime = (decimal?)EquipmentTime;
            actionForUpdate.Walking = HasWalking;
            actionForUpdate.ActionDes = ActionDes;
        }


        //点击右侧动作列表，视频自动调到对应的开始时间，暂停状态
        private void SetPlayProcess(bool skip)
        {
            isAction = false;
            //if (vlcPlayer.playState.ToString() == "wmppsPlaying") vlcPlayer.Ctlcontrols.pause();
            if (dataGridView1.SelectedRows.Count == 0) return;
            var startVal = dataGridView1.SelectedRows[0].Cells["StartTime"].Value;

            var currentStartTime = double.Parse(startVal.ToString());
            Console.WriteLine("0. 设置开始时间" + currentStartTime);
            if (!skip)
            {
                setCurrentPostion(currentStartTime);
            }
            vlcPlayer.Ctlenabled = true;
            trackBar1.Value = double.Parse(startVal.ToString()) * 1000 < trackBar1.Maximum ? (int) (double.Parse(startVal.ToString()) + 0.01) * 1000 : 0;
            tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(trackBar1.Value), GetTimeString(trackBar1.Maximum));
            PlayerPause();
        }


        private void SetSelectionByTime()
        {
            var playTime = (decimal)vlcPlayer.Ctlcontrols.currentPosition; //vlcPlayer.Ctlcontrols.currentPosition(); //getCurrentTime();//
            Console.WriteLine("9. 获取实际设置时间" + playTime);
            var action = TempActions.FirstOrDefault(a => playTime >=a.StartTime && playTime <= a.EndTime);
            if (action != null) tempAction = action;
            else
            {
                tempAction = TempActions.Count > 0 ? TempActions.Last() : new t_Action(){StartTime = 0,EndTime = 0};
            }
            Console.WriteLine("10. 当前action ID" + tempAction.ActionId);
            if (action == null) return;
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                DataGridViewRow dgvr = dataGridView1.Rows[i];
                if ((int)dgvr.Cells[0].Value == action.ActionId)
                {
                    if (dataGridView1.SelectedRows.Count > 0 && dataGridView1.SelectedRows[0].Index == i) return;
                    dataGridView1.ClearSelection();
                    dgvr.Selected = true;

                    // added by Ted
                    if (dgvr.Index - VScrollIndex >= 14)
                    {
                        VScrollIndex = dgvr.Index - 13;
                    }

                    if (VScrollIndex < dataGridView1.RowCount)//解决指定的参数已超出有效值的范围2018-5-31
                    {
                        dataGridView1.FirstDisplayedScrollingRowIndex = VScrollIndex;
                    }
                }
            }

        }

        private void SetOperateType(string operateType, decimal? equipmentTime, string hasWalking)
        {
            OperateType = operateType;
            if (equipmentTime != null) EquipmentTime = (double)equipmentTime;
            else
            {
                EquipmentTime = 0d;
            }

            HasWalking = hasWalking;
        }

        /// <summary>
        /// 新增一个新标记，自动选中，点击保存时更新的就是这条新标记
        /// </summary>
        /// <param name="action"></param>
        private void SetActionInfo(t_Action action)
        {
            if(action == null) return;
            tbxBJMS.Text = action.Description;
            tbxAQZYSX.Text = action.SafetyCaution;
            tbxPZZYSX.Text = action.QualityCaution;
            tbxDZMC.Text = action.ActionName;
            tbxDM.Text = action.ActionCode;
            tbxSDSJ.Text = action.ManualStandardTime.ToString();
            cbCZMC.Text = action.OperationName;
            if (action.ActionTypeId != null)
            {
                cbLX.Text = actionTypes.FirstOrDefault(i => i.ActionTypeId == action.ActionTypeId)?.ChineseName;
            }

            cbSD.Text = action.Speed.ToString();
            SetCycle(action.Cycle.ToString());
            cbZQ.Text = action.Cycle.ToString();
            if (action.TypeId != null)
            {
                clbDFL.SetItemChecked((int)(action.TypeId - 1), true);
                clbDFL.SelectedItem = action.TypeId == 2 ? "有效动作" :
                    action.TypeId == 1 ? "无效动作" : "无关录像";
            }

            tsBtnDelete.Enabled = true;
            tsBtnSave.Enabled = true;
            SetOperateType(action.OperationType, action.EquipmentTime, action.Walking);
        }

        //设置周期下拉框的数据
        private void SetCycle(string cycle)
        {
            cbZQ.Items.Clear();
            cbZQ.Items.Add(cycle);
            cbZQ.Items.Add("新周期");
            if(dataGridView1.SelectedRows.Count == 0) return;
            if (dataGridView1.SelectedRows[0].Index != 0) cbZQ.Items.Add("向上合并"); //第一行的周期没有向上合并

            if (dataGridView1.SelectedRows[0].Index < dataGridView1.RowCount - 1) cbZQ.Items.Add("向下合并"); //最后一行没有向下合并
        }

        private void toolStripDropDownButton1_DropDownItemClicked_1(object sender, ToolStripItemClickedEventArgs e) //切换手动/自动标准时间
        {
            var text = e.ClickedItem.Text;
            if (text == "手动标准时间")
            {
                btnDgbz.Enabled = true;
                btnZHBZ.Enabled = true;
                cbSD.Enabled = false;
                dataGridView1.Columns[5].HeaderText = "ST手";
            }
            else
            {
                btnDgbz.Enabled = false;
                btnZHBZ.Enabled = false;
                cbSD.Enabled = true;
                dataGridView1.Columns[5].HeaderText = "S.T";
            }

            DDBtnStandardTime.Text = text;
            var hasSelected = false;
            if (CurrentVideo == null) return;
            var selectedRows = dataGridView1.SelectedRows;
            if (selectedRows.Count == 0) return;
            if (dataGridView1.SelectedRows[0].Index > 0)
            {
                NewAction = dataGridView1.SelectedRows[0].DataBoundItem as t_Action;
                hasSelected = true;
            }

            LoadActions(CurrentVideo.VideoId);
            if (hasSelected) ReSelectRow();

        }

        private void btnCancelLoop_Click(object sender, EventArgs e)
        {
            startPoint = 0.0000m;
            endPoint = 10000.0000m;
            lbProcess.SendToBack();
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.White;
            }

            //            trackBar1.Value = (int) (vlcPlayer.Ctlcontrols.currentPosition() * 1000);
            PlayerPause();
            btnCancelLoop.Enabled = false;
            btnSetLoopStart.Enabled = false;
            btnSetLoopEnd.Enabled = false;
        }

        private void btnSetLoopStart_Click(object sender, EventArgs e)
        {
            PlayerPause();
            ClearSelection();
            if (dataGridView1.SelectedRows.Count == 0) return;
            var startVal = double.Parse(dataGridView1.SelectedRows[0].Cells["StartTime"].Value.ToString()) + 0.01;
            StartIndex = dataGridView1.SelectedRows[0].Index;
            startPoint = decimal.Parse(startVal.ToString());
            trackBar1.Value = (int)(startPoint * 1000);
            //vlcPlayer.Ctlcontrols.currentPosition = double.Parse(startVal.ToString());

            setCurrentPostion(double.Parse(startVal.ToString()));

            if (vlcPlayer.playState.ToString() == "wmppsPlaying") vlcPlayer.Ctlcontrols.pause();
            btnSetLoopEnd.Enabled = true;
            btnCancelLoop.Enabled = true;
        }

        private void btnSetLoopEnd_Click(object sender, EventArgs e)
        {
            IsAutoPause = false;
            if (vlcPlayer.playState.ToString() == "wmppsPlaying") vlcPlayer.Ctlcontrols.pause();
            if (dataGridView1.SelectedRows.Count == 0) return;
            var endVal = dataGridView1.SelectedRows[0].Cells["EndTime"].Value;
            EndIndex = dataGridView1.SelectedRows[0].Index;
            endPoint = decimal.Parse(endVal.ToString());
            var pct = endPoint - startPoint;
            lbProcess.Width = (int)((pct / (decimal)vlcPlayer.Ctlcontrols.currentItem.duration) * 673);
            lbProcess.Location = new Point((int)(startPoint / (decimal)vlcPlayer.Ctlcontrols.currentItem.duration * 673) + 18, trackBar1.Location.Y + 8);
            lbProcess.Visible = true;
            lbProcess.BringToFront();
            is_playinig = false;
            vlcPlayer.Ctlcontrols.currentPosition  = (double)startPoint;
            trackBar1.Value = (int)(startPoint * 1000);
            PlayerPlay();
            for (int i = StartIndex; i <= EndIndex; i++)
            {
                dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(255, 0, 120, 215);
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
                return;
            if (vlcPlayer.playState.ToString() == "wmppsPlaying") vlcPlayer.Ctlcontrols.pause();
            VScrollIndex = dataGridView1.FirstDisplayedScrollingRowIndex;//获取当前显示的第一条的索引值，用于重新刷新数据的时候控制滚动条
            Console.WriteLine("滚动条位置：" + VScrollIndex);
            IsAutoPause = true;
            timer1.Stop();
            tsBtnSign.Enabled = false;
            var selectedRows = dataGridView1.SelectedRows;
            if (selectedRows.Count == 0) return;
            if (dataGridView1.SelectedRows[0].Index > 0)
            {
                DataGridViewRow upRow = dataGridView1.Rows[dataGridView1.SelectedRows[0].Index - 1];
                upCycle = (int)(upRow.Cells["Cycle"].Value ?? 0); //上一个动作的周期
            }

            if (dataGridView1.SelectedRows[0].Index < dataGridView1.RowCount - 1)
            {
                DataGridViewRow downRow = dataGridView1.Rows[dataGridView1.SelectedRows[0].Index + 1];
                downCycle = (int)(downRow.Cells["Cycle"].Value ?? 0); //下一个动作的周期
            }

            var id = (int)selectedRows[0].Cells[0].Value;
            actionForUpdate = TempActions.FirstOrDefault(i => i.ActionId == id); //加载动作数据到窗体内对应的控件
            NewAction = actionForUpdate;
            btnSetLoopStart.Enabled = true;
            var upEndTime = tempAction.EndTime;
            tempAction = actionForUpdate;
            if (actionForUpdate != null)
            {
                SetActionInfo(actionForUpdate);
                SetPlayProcess(upEndTime == actionForUpdate.StartTime && UsedPlayed == true);
                UsedPlayed = false;
            }
            else
            {
                tsBtnDelete.Enabled = false;
                tsBtnSave.Enabled = false;
                btnSetLoopStart.Enabled = false;
            }

            btnSetLoopStart.Enabled = true;
        }

        private void tsBtnPause_Click(object sender, EventArgs e)
        {
            PlayerPause();
        }

        private void timerAutoLoadVideo_Tick(object sender, EventArgs e)
        {
            if (!isFromSystemIntegrationForm)
            {
                timerAutoLoadVideo.Stop();
                return;
            }

            if (!File.Exists(filePath)) return;
            vlcPlayer.URL = filePath;
            trackBar1.SetRange(0, (int)vlcPlayer.currentMedia.duration * 1000);
            trackBar1.Value = 0;
            timer1.Start();
            if (vlcPlayer.playState.ToString() == "wmppsPlaying")
            {
                timer1.Interval = 1;
            }

            is_playinig = true;
            media_is_open = true;
            LoadOperationName(); //加载当前视频包含的所有操作名称
            isFromSystemIntegrationForm = false;
            timerAutoLoadVideo.Stop();
            Program.FormMain.SetLoadStatus(false);
            var name = Path.GetFileName(filePath);
            var mode = Common.GetSingleModel<t_Video>(i => i.FileName == name); //先判断选择的视频是否已经在数据库中存在
            if (mode != null) //如果存在
            {
                CurrentVideo = mode;
                VideoID = mode.VideoId; // added by Ted on 20180312 for other forms
                btnCZLX.Enabled = true;
            }
        }

        public void SetVideoPath(string VideoPath)
        {
            isFromSystemIntegrationForm = true;
            timerAutoLoadVideo.Start();
            filePath = VideoPath;
        }

        private void ClearSelection()
        {
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.White;
            }
        }

        private void bgWUploadVideo_DoWork(object sender, DoWorkEventArgs e)
        {
            var args = e.Argument as string[];
            FileHelper.FolderCreate(args[1]);
            FileHelper.FileCopy(args[0], args[1] + args[2]);
            var ip = ConfigurationManager.AppSettings["ftpaddr"];
            var user = ConfigurationManager.AppSettings["ftpuser"];
            var pwd = ConfigurationManager.AppSettings["ftppwd"];
            FtpHelper ftpop = new FtpHelper(ip, user, pwd);
            Thread.Sleep(2000);
            ftpop.Upload();
        }
        //延时加载视频动作，防止视频等动作加载完了才显示
        private void ShowGrid()
        {
            var list = ActionManage.GetActions(CurrentVideo.VideoId).OrderBy(i => i.StartTime).ToList();
            if (!list.Any())
            {
                list.Add(new t_Action { StartTime = 0.00m, EndTime = 0.00m });
            }
            dataGridView1.DataSource = list;
            tempAction = list.LastOrDefault() ?? new t_Action { ActionTime = 0, StartTime = 0, EndTime = 0 }; //加载视频包含已经分析完成的动作
            TempActions = list;
            //1无效2有效3无关
            for (var i = 0; i < dataGridView1.RowCount; i++)
            {
                var row = dataGridView1.Rows[i];
                if (row.Cells["TypeId"].Value != null)
                {
                    switch ((int)row.Cells["TypeId"].Value)
                    {
                        case 1:
                            row.Cells[8].Style.BackColor = Color.Red;
                            row.Cells[8].Value = "×";
                            break;
                        case 2:
                            row.Cells[8].Style.BackColor = Color.GreenYellow;
                            row.Cells[8].Value = "○";
                            break;
                        case 3:
                            row.Cells[8].Style.BackColor = Color.Yellow;
                            row.Cells[8].Value = "△";
                            break;
                    }
                }

                if (row.Cells["ActionTypeId"].Value != null && !string.IsNullOrEmpty(row.Cells["ActionTypeId"].Value.ToString()))
                {
                    var actionType = actionTypes.FirstOrDefault(j => j.ActionTypeId == (int)row.Cells["ActionTypeId"].Value);
                    if (actionType != null)
                    {
                        var color = actionType.Color.Split(',');
                        row.Cells[7].Value = actionType.ChineseName;
                        row.Cells[7].Style.BackColor = Color.FromArgb(int.Parse(color[0]), int.Parse(color[1]), int.Parse(color[2]), int.Parse(color[3]));
                    }
                }

                var value = row.Cells["TypeId"].Value;
                if (value != null && new[] { "1", "3" }.Contains(value.ToString())) row.Cells[5].Value = decimal.Parse("0.00"); //无关录像ST为0
            }
            if (dataGridView1.RowCount > 0)
            {
                var dgr = dataGridView1.Rows[dataGridView1.RowCount - 1];
                dgr.Cells[4].Value = null;
                dgr.Cells[5].Value = null;
                dgr.Cells[6].Value = 1.00m;
                dgr.Cells[7].Value = null;
                dgr.Cells[7].Style.BackColor = Color.White;
                dgr.Cells[8].Value = null;
                dgr.Cells[8].Style.BackColor = Color.White;
                dgr.Cells[9].Value = null;
            }
            LoadOperationName(); //加载当前视频包含的所有操作名称
  
            var fps = vlcPlayer.network.encodedFrameRate == 0 ? 25 : vlcPlayer.network.encodedFrameRate; //获取视频的码率29.97003
            rate = 1 / (double)fps; //初始化默认帧数为1
            CurrentVideo.Duration = (decimal)vlcPlayer.currentMedia.duration;
            CurrentVideo.FrameCount = (int)vlcPlayer.currentMedia.duration * fps;

            Common.AddOrUpdateObject(CurrentVideo);
            //视频加载完了之后更新视频的帧数和时长
        }

        private void RePaintDgv()
        {
            //1无效2有效3无关
            for (var i = 0; i < dataGridView1.Rows.Count; i++)
            {
                var row = dataGridView1.Rows[i];
                if (row.Cells["TypeId"].Value != null)
                {
                    switch ((int) row.Cells["TypeId"].Value)
                    {
                        case 1:
                            row.Cells[8].Style.BackColor = Color.Red;
                            row.Cells[8].Value = "×";
                            break;
                        case 2:
                            row.Cells[8].Style.BackColor = Color.GreenYellow;
                            row.Cells[8].Value = "○";
                            break;
                        case 3:
                            row.Cells[8].Style.BackColor = Color.Yellow;
                            row.Cells[8].Value = "△";
                            break;
                    }
                }

                if (row.Cells["ActionTypeId"].Value != null && !string.IsNullOrEmpty(row.Cells["ActionTypeId"].Value.ToString()))
                {
                    var actionType = actionTypes.FirstOrDefault(j => j.ActionTypeId == (int)row.Cells["ActionTypeId"].Value);
                    if (actionType != null)
                    {
                        var color = actionType.Color.Split(',');
                        row.Cells[7].Value = actionType.ChineseName;
                        row.Cells[7].Style.BackColor = Color.FromArgb(int.Parse(color[0]), int.Parse(color[1]), int.Parse(color[2]), int.Parse(color[3]));
                    }
                }

                var value = row.Cells["TypeId"].Value;
                if (value != null && new[] { "1", "3" }.Contains(value.ToString())) row.Cells[5].Value = decimal.Parse("0.00"); //无关录像ST为0
            }

            dataGridView1.ClearSelection();
        }

        private void bWDeleteAction_DoWork(object sender, DoWorkEventArgs e)
        {
            var actionId = (int)e.Argument;
            ActionManage.DeleteAction(actionId);
        }

        //异步执行新增、拆分动作，减少数据库操作的耗时带来前台的卡顿
        private void bWNewAction_DoWork(object sender, DoWorkEventArgs e)
        {
            if (e.Argument is object[])
            {
                var args = (object[])e.Argument;
                var mode = (t_Action)args[0];
                var isSplit = (bool)args[1];
                //本阶段暂时不需要截图功能，所以暂时注销了新增标记时自动截取图片2018-5-10，Johnson
                //                var directory = Environment.CurrentDirectory + "\\SnapShots\\";
                //                if (!Directory.Exists(directory))
                //                {
                //                    Directory.CreateDirectory(directory);
                //                }
                //
                //                var name = DateTime.Now.ToFileTime() + ".png";
                //                var path = directory + name;
                //                var picturePath = Helper.GetFolderPath();
                //                var realPath = $"{picturePath}\\{name}";
                //                if (vlcPlayer.TakeSnapShot(realPath))
                //                {
                //                    FileHelper.FileCopy(realPath, path);
                //                    FileHelper.FileDel(realPath);
                //                    mode.ActionImage = Helper.GetPictureData(path);
                if (mode.ActionTime < 0 || tempAction.ActionTime < 0)
                {
                    return;//不允许插入负值的时间
                }
                tempAction = isSplit ? ActionManage.SplitAction(tempAction, mode) : ActionManage.AddNewAction(mode, (decimal)vlcPlayer.currentMedia.duration); //如果是拆分动作，需要同时更新被拆分动作，新增动作
                actionForUpdate = tempAction;
                CleanActionInfo();
                dataGridView1.Rows[dataGridView1.RowCount - 1].Selected = true;
                SetActionInfo(tempAction);
                if (vlcPlayer.playState.ToString() == "wmppsPlaying") vlcPlayer.Ctlcontrols.pause();
                //                }
                //                else
                //                {
                //                    tempAction = isSplit ? ActionManage.SplitAction(tempAction, mode) : ActionManage.AddNewAction(mode, (decimal)vlcPlayer.currentMedia.duration());
                //                    actionForUpdate = tempAction;
                //                    CleanActionInfo();
                //                    dataGridView1.Rows[dataGridView1.RowCount - 1].Selected = true;
                //                    SetActionInfo(tempAction);
                //                }
            }
            //
            //            var ta = TempActions.FirstOrDefault(i => i.ActionId == 0);
            //            if (ta != null) ta.ActionId = tempAction.ActionId;
            //            RePaintDgv();
        }

        private void dataGridView1_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                oldOperationName = (string)dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
            }
            else if (e.ColumnIndex == 3)
            {
                oldCycle = (int)dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (vlcPlayer.playState.ToString() == "wmppsPlaying")
            {
                PlayerPause();
            }

            else
            {
                PlayerPlay();
            }
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1) return;
            var updateId = dataGridView1.Rows[e.RowIndex].Cells[0].Value;
            var value = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
            var list = (List<t_Action>)dataGridView1.DataSource;
            var m = list.FirstOrDefault(i => i.ActionId == (int)updateId);
            if (m == null) return;
            if (e.ColumnIndex == 2)//批量修改操作名称
            {
                cbCZMC.Text = value.ToString();
                ActionManage.BatchUpdateOperationName(m, oldOperationName);
                LoadActions(CurrentVideo.VideoId);
            }
            else if (e.ColumnIndex == 3)//批量修改周期
            {
                cbZQ.Text = value.ToString();
                ActionManage.BatchUpdateCycle(m, oldCycle);
                LoadActions(CurrentVideo.VideoId);
            }

            ReSelectRow();
        }

        //private void dataGridView1_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        //{
        //    if (e.RowIndex == -1)
        //        return;
        //    timer1.Stop();
        //    correctPlayTime = 0;
        //    if (vlcPlayer.playState.ToString() == "wmppsPlaying") vlcPlayer.Ctlcontrols.pause();
        //}

        private void vlcPlayer_ClickEvent(object sender, AxWMPLib._WMPOCXEvents_ClickEvent e)
        {
            UsedPlayed = true;
            if (vlcPlayer.fullScreen == true)
            { vlcPlayer.fullScreen = false; }
            if (vlcPlayer.playState.ToString() == "wmppsPlaying")
            {
                PlayerPause();
            }
            else if(vlcPlayer.playState.ToString() == "wmppsUndefined")
            {
                return;
            }
            else
            {
                PlayerPlay();
            }
        }

        private void setCurrentPostion(double varPlayTime, bool isBack=false)
        {
            timer1.Stop();
            vlcPlayer.Ctlcontrols.currentPosition = varPlayTime;
 

            //vlcPlayer.settings.rate = 0.001;
            //vlcPlayer.Ctlcontrols.play();
            //var var = decimal.Round((decimal)(2 * rate * Speed),4);
            //if (isBack)
            //    vlcPlayer.Ctlcontrols.currentPosition = Math.Round((varPlayTime -(double)var), 4);
            //else
            //    vlcPlayer.Ctlcontrols.currentPosition = Math.Round((varPlayTime +(double)var),4);
            //vlcPlayer.Ctlcontrols.pause();
            //vlcPlayer.settings.rate = Speed;
        }

        private void dataGridView1_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex == -1)
                return;
            timer1.Stop();
            if (vlcPlayer.playState.ToString() == "wmppsPlaying") vlcPlayer.Ctlcontrols.pause();
        }

        private void vlcPlayer_PositionChange(object sender, AxWMPLib._WMPOCXEvents_PositionChangeEvent e)
        {
            vlcPlayer.Ctlcontrols.pause();
        }

        private void vlcPlayer_DoubleClickEvent(object sender, AxWMPLib._WMPOCXEvents_DoubleClickEvent e)
        {
            if (vlcPlayer.fullScreen == true)
            { vlcPlayer.fullScreen = false; }
        }
    }
}