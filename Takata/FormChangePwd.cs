﻿using DAL;
using DAL.SystemManage;
using DevComponents.DotNetBar;

namespace Takata
{
    public partial class FormChangePwd : Office2007Form
    {
        public FormChangePwd()
        {
            InitializeComponent();
            tbxOldPwd.Select();//设置焦点
        }

        private void buttonX1_Click(object sender, System.EventArgs e)
        {
            var oldPwd = tbxOldPwd.Text.Trim();
            var newPwd = tbxNewPwd.Text.Trim();
            var confirmPwd = tbxConfirmPwd.Text.Trim();
            var user = Program.CurrentUser;
            if (user.UserPwd != Common.EncryptMd5(oldPwd))
            {
                MessageBoxEx.Show("原密码不正确，无法修改密码！", "提示");
            }
            else if(string.IsNullOrEmpty(tbxNewPwd.Text.Trim()) || tbxNewPwd.Text.Length < 6)
            {
                MessageBoxEx.Show("密码长度必须大于6位！", "提示");
            }
            else if (newPwd != confirmPwd)
            {
                MessageBoxEx.Show("两次输入的密码不一致，请确认！", "提示");
            }

            else
            {
                if (new UserManage().UpdateUserPwd(user, newPwd))
                {
                    MessageBoxEx.Show("密码修改成功。", "提示");
                    Close();
                }
            }
        }

        private void buttonX2_Click(object sender, System.EventArgs e)
        {
            Close();
        }
    }
}
