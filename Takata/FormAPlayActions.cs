﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DAL;
using DAL.SystemManage;
using DevComponents.DotNetBar;
using Model;

namespace Takata
{
    public partial class FormAPlayActions : Office2007Form
    {
        string address;
        private int startPoint = 0;//开始播放的时间点
        private int endPoint = 10000000;//播放结束的时间节点
        private string filePath = "";
        private List<t_ActionType> actionTypes = new List<t_ActionType>();
        private List<t_Action> invalidActions;
        private List<t_Action> validActions;
        private List<t_Action> actions = new List<t_Action>();
        private int StartIndex = 999;//设置循环时选择开始的行，用于高亮显示这些选择的行
        private int EndIndex = 999;//循环结束的行
        private bool is_playing;
        private static int VScrollIndex = 0;
        public FormAPlayActions()
        {
            InitializeComponent();
            actionTypes = Common.GetList<t_ActionType>(null);
            address = Environment.CurrentDirectory;
            dgv1.AutoGenerateColumns = false;
        }

        private void btnPlay_Click(object sender, System.EventArgs e)
        {
            if (vlcPlayer.GetState() == 0)
                return;
            if (vlcPlayer.GetState()==5) return;
            vlcPlayer.Play();
            timer1.Start();
            is_playing = true;
        }

        private void btnPause_Click(object sender, System.EventArgs e)
        {
            if (vlcPlayer.GetState() == 0)
                return;
            if (vlcPlayer.GetState() != 5) return;
            vlcPlayer.Pause();
            timer1.Stop();
            is_playing = false;
        }

        private void btnBackward_Click(object sender, System.EventArgs e)
        {
            if (vlcPlayer.GetState() == 0)
                return;
            vlcPlayer.Pause();
            int time = (int)vlcPlayer.GetPosition() - 5000;
            if (time > 0)
            {
                vlcPlayer.SetPosition(time);
            }
            else
            {
                vlcPlayer.SetPosition(0);
            }

            vlcPlayer.Play();
            trackBar1.Value = (int)vlcPlayer.GetPosition();
            timer1.Start();
        }

        private void btnForward_Click(object sender, System.EventArgs e)
        {
            if (vlcPlayer.GetState() == 0)
                return;
            vlcPlayer.Pause();
            int time = (int)vlcPlayer.GetPosition() + 5000;
            if (time < trackBar1.Maximum)
            {
                vlcPlayer.SetPosition(time);
                trackBar1.Value = (int)vlcPlayer.GetPosition();
                vlcPlayer.Play();
                timer1.Start();
            }
            else
            {
                timer1.Stop();
                var duration = vlcPlayer.GetDuration();
                vlcPlayer.SetPosition(duration - 1);
                trackBar1.Value = trackBar1.Maximum;
                SetSelectRow();
            }
        }

        private void btnCancelLoop_Click(object sender, System.EventArgs e)
        {
            if (vlcPlayer.GetState() == 0)
                return;
            startPoint = 0;
            endPoint = 10000000;
            lbProcess.Visible = false;
            if (vlcPlayer.GetState() == 5) vlcPlayer.Pause();
            dgv1.ClearSelection();
            for (int i = 0; i < dgv1.RowCount; i++)
            {
                dgv1.Rows[i].DefaultCellStyle.BackColor = Color.White;
            }
        }

        private void btnSetLoopStart_Click(object sender, System.EventArgs e)
        {
            if (vlcPlayer.GetState() == 0)
                return;
            if (vlcPlayer.GetState() == 5)
            {
                vlcPlayer.Pause();
                timer1.Stop();
            }
            ClearSelection();
            if (dgv1.SelectedRows.Count == 0) return;
            var startVal = (int)(decimal.Parse(dgv1.SelectedRows[0].Cells["StartTime"].Value.ToString()));
            startVal++;
            startPoint = startVal;
            trackBar1.Value = (int)startPoint;
            vlcPlayer.SetPosition((int)startPoint);
            if (vlcPlayer.GetState()==5) vlcPlayer.Pause();
            btnSetLoopEnd.Enabled = true;
            StartIndex = dgv1.SelectedRows[0].Index;
        }

        private void btnSetLoopEnd_Click(object sender, System.EventArgs e)
        {
            if (vlcPlayer.GetState() == 0)
                return;
            if (vlcPlayer.GetState() == 5) vlcPlayer.Pause();
            timer1.Stop();
            if (dgv1.SelectedRows.Count == 0) return;
            var endVal = (int)decimal.Parse(dgv1.SelectedRows[0].Cells["EndTime"].Value.ToString());
            endPoint =  endVal;
            var pct = endPoint - startPoint;
            var duration = vlcPlayer.GetDuration();
            lbProcess.Visible = true;
            lbProcess.Width = (int)((float)pct / (float)duration * 1073);
            lbProcess.Location = new Point((int)((float)startPoint / (float)duration * 1073 + 25), trackBar1.Location.Y + 8);
            lbProcess.BringToFront();
            vlcPlayer.SetPosition(startPoint);
            trackBar1.Value = (int)startPoint;
            EndIndex = dgv1.SelectedRows[0].Index;
            for (int i = StartIndex; i <= EndIndex; i++)
            {
                dgv1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(255, 0, 120, 215);
            }
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            OpenFile(sender, e);
            lbProcess.Visible = false;
            var filename = Path.GetFileName(filePath);
            var mode = Common.GetSingleModel<t_Video>(i => i.FileName == filename);
            if (mode == null)
            {
                MessageBox.Show("该视频不包含任何动作标记", "提示");
                dgv1.DataSource = null;
                return;
            }

            LoadActions(mode.VideoId);
        }

        private void LoadActions(int videoId)
        {
            var list = ActionManage.GetActions(videoId).OrderBy(i => i.EndTime).ToList();
            actions = list;
            invalidActions = list.Where(i => i.TypeId != 2).ToList();
            validActions = list.Where(i => i.TypeId == 2).ToList();
            dgv1.DataSource = list;
            //1无效2有效3无关
            for (var i = 0; i < dgv1.Rows.Count; i++)
            {
                var row = dgv1.Rows[i];
                switch ((int)row.Cells["TypeId"].Value)
                {
                    case 1:
                        row.Cells[8].Style.BackColor = Color.Red;
                        row.Cells[8].Value = "×";
                        break;
                    case 2:
                        row.Cells[8].Style.BackColor = Color.GreenYellow;
                        row.Cells[8].Value = "○";
                        break;
                    case 3:
                        row.Cells[8].Style.BackColor = Color.Yellow;
                        row.Cells[8].Value = "△";
                        break;
                }

                if (row.Cells["ActionTypeId"].Value != null && !string.IsNullOrEmpty(row.Cells["ActionTypeId"].Value.ToString()))
                {
                    var actionType = actionTypes.FirstOrDefault(j => j.ActionTypeId == (int)row.Cells["ActionTypeId"].Value);
                    if (actionType != null)
                    {
                        var color = actionType.Color.Split(',');
                        row.Cells[7].Value = actionType.ChineseName;
                        row.Cells[7].Style.BackColor = Color.FromArgb(int.Parse(color[0]), int.Parse(color[1]), int.Parse(color[2]), int.Parse(color[3]));
                    }
                }
                if (new[] { "1", "3" }.Contains(row.Cells["TypeId"].Value.ToString())) row.Cells[5].Value = decimal.Parse("0.00");//无关录像ST为0
            }
        }

        private void FormAPlayActions_Load(object sender, EventArgs e)
        {
            if (!File.Exists(address + "\\Menu.ini"))
            {
                FileStream fs = new FileStream(address + "\\Menu.ini", FileMode.Create, FileAccess.Write);
                fs.Close();
            }
            vlcPlayer.SetConfig(105, "0");
            vlcPlayer.SetConfig(120, "1");
            vlcPlayer.SetConfig(37, "b:\\aaaaa1.jpg");
            vlcPlayer.SetConfig(209, "1");
            vlcPlayer.SetConfig(707, "3");
            vlcPlayer.SetConfig(8, "0");
            is_playing = false;
            tbVideoTime.Text = "00:00:00/00:00:00";
        }
        private void OpenFile(object sender, EventArgs e)
        {
            //bool isSame = false;
            openFileDialog1.FileName = "";
            if (this.openFileDialog1.ShowDialog() == DialogResult.OK)
            {

                StreamWriter s = new StreamWriter(address + "\\Menu.ini", true);
                s.WriteLine(openFileDialog1.FileName);
                s.Flush();
                s.Close();
                string[] text = File.ReadAllLines(address + "\\Menu.ini");
                int row = text.Length;//行
                int rowcount;
                string[] tempdata = new string[] { "", "", "" };
                if (row > 3)// 若记录行数超过3个，则先记录后三个数据，再重新建一个Menu.ini（清除数据）,接着讲记录的后三个数据写入
                {
                    StreamReader sr1 = new StreamReader(address + "\\Menu.ini", true);
                    while (sr1.Peek() > -1)
                    {
                        sr1.ReadLine();//空读，跳过原始的第一个数据，从第二个数据开始读
                        for (rowcount = 0; rowcount < 3; rowcount++)
                        {
                            tempdata[rowcount] = sr1.ReadLine();
                        }
                    }
                    sr1.Close();
                    FileStream fs = new FileStream(address + "\\Menu.ini", FileMode.Create, FileAccess.Write);
                    fs.Close();
                    StreamWriter s1 = new StreamWriter(address + "\\Menu.ini", true);
                    s1.WriteLine(tempdata[0]);
                    s1.WriteLine(tempdata[1]);
                    s1.WriteLine(tempdata[2]);
                    s1.Flush();
                    s1.Close();
                }

                filePath = openFileDialog1.FileName;
                vlcPlayer.Open(filePath);
                var fileName = Path.GetFileName(filePath);
                lblFileName.Text = filePath;
                VScrollIndex = 0;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!is_playing) return;
            var duration = vlcPlayer.GetDuration();
            var playTime = vlcPlayer.GetPosition();
            if (trackBar1.Value == trackBar1.Maximum || playTime >= trackBar1.Maximum)
            {
                vlcPlayer.Pause();
                timer1.Stop();
                tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(duration), GetTimeString(duration));
                trackBar1.Value = trackBar1.Maximum;
            }
            else
            {
                var skip = false;
                if (invalidActions == null) return;
                invalidActions.ForEach(i =>
                {
                    if (playTime >= i.StartTime && playTime <= i.EndTime)
                    {
                        vlcPlayer.Pause();
                        vlcPlayer.SetPosition((int)i.EndTime + 10);
                        vlcPlayer.SetConfig(104, "100");
                        vlcPlayer.Play();
                        skip = true;
                    }
                });

                if (endPoint != 10000000 && playTime >= endPoint)
                {
                    //timer1.Stop();
                    trackBar1.Value = (int)(startPoint);
                    vlcPlayer.SetPosition(startPoint);
                    //vlcPlayer.Pause();
                }
                else
                {
                    trackBar1.Value = (int)playTime;
                    tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(playTime), GetTimeString(duration));
                }
                if (skip) return;
                SetSelectRow();
            }
        }

        private void SetSelectRow()
        {
            var playTime = vlcPlayer.GetPosition();
            //var action = validActions.Where(i => i.StartTime >= ((decimal)startPoint) && i.EndTime <= ((decimal)endPoint)).FirstOrDefault(a => playTime >= a.StartTime && playTime <= a.EndTime);
            var action = actions.FirstOrDefault(a => playTime >= a.StartTime && playTime < a.EndTime);
            if (action != null)
            {
                vlcPlayer.SetConfig(104, ((int)((decimal)action.Speed * 100)).ToString());
                for (int i = 0; i < dgv1.Rows.Count; i++)
                {
                    DataGridViewRow dgvr = dgv1.Rows[i];
                    if ((int)dgvr.Cells[0].Value == action.ActionId)
                    {
                        if (dgv1.SelectedRows.Count > 0 && dgv1.SelectedRows[0].Index == i) return;
                        dgv1.ClearSelection();
                        //                            if(dgv1.SelectedRows[0].Index==i) return;
                        dgvr.Selected = true;

                        if (dgvr.Index - VScrollIndex >= 29)
                        {
                            VScrollIndex = dgvr.Index - 28;
                        }

                        if (VScrollIndex < dgv1.RowCount)//解决指定的参数已超出有效值的范围2018-5-31
                        {
                            dgv1.FirstDisplayedScrollingRowIndex = VScrollIndex;
                        }
                    }
                }
            }
        }

        private string GetTimeString(int val)
        {
            var currentMS = val;
            val = val / 1000;
            var hour = val / 3600;
            val %= 3600;
            var minute = val / 60;
            var second = val % 60;

            int frameInterval = 40;
            int.TryParse(vlcPlayer.GetConfig(117), out frameInterval);

            var allTime = vlcPlayer.GetDuration();
            if (frameInterval == 0)
                frameInterval = 40;
            var frame = (int)(currentMS % 1000 / frameInterval); //帧
            return $"{hour:00}:{minute:00}:{(int)second:00}({(int)frame})";
        }


        private void dgv1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Rectangle rectangle = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, dgv1.RowHeadersWidth - 4, e.RowBounds.Height);
            TextRenderer.DrawText(e.Graphics,
                (e.RowIndex + 1).ToString(),
                dgv1.RowHeadersDefaultCellStyle.Font,
                rectangle,
                dgv1.RowHeadersDefaultCellStyle.ForeColor,
                TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
        }

        private void dgv1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                var actionId = dgv1.Rows[e.RowIndex].Cells[0].Value;
                var action = actions.FirstOrDefault(i => i.ActionId == (int)actionId);
                if (action == null) return;//如果选择的是无效动作则跳出不执行任何操作
                var startVal =  (int)decimal.Parse(action.StartTime.ToString());
                vlcPlayer.SetPosition(startVal);
                vlcPlayer.SetConfig(104, ((int)((decimal)action.Speed * 100)).ToString());
                trackBar1.Value = (int)decimal.Parse(startVal.ToString()) < trackBar1.Maximum ? (int)decimal.Parse(startVal.ToString()) : 0;
                tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(trackBar1.Value), GetTimeString(trackBar1.Maximum));
                if (vlcPlayer.GetState() == 5) vlcPlayer.Pause();
                timer1.Stop();
            }
            catch { }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            if (vlcPlayer.GetState() == 0)
                return;
            if (is_playing)
            {
                vlcPlayer.Pause();
                timer1.Stop();
            }
            var duration = vlcPlayer.GetDuration();
            vlcPlayer.SetPosition(trackBar1.Value >= duration ? duration- 180 : trackBar1.Value);
            trackBar1.Value = (int)vlcPlayer.GetPosition();
            SetSelectRow();
        }
        private void ClearSelection()
        {
            for (int i = 0; i < dgv1.RowCount; i++)
            {
                dgv1.Rows[i].DefaultCellStyle.BackColor = Color.White;
            }
        }

        private void vlcPlayer_OnMessage(object sender, AxAPlayer3Lib._IPlayerEvents_OnMessageEvent e)
        {
            if (e.nMessage == 0x0201)
            {

                if (vlcPlayer.GetState() == 5)
                {
                    is_playing = false;
                    vlcPlayer.Pause();
                    timer1.Stop();
                }
                else
                {
                    is_playing = true;
                    vlcPlayer.Play();
                    timer1.Start();
                }
            }
        }

        private void BacktoStart()
        {
            PlayerPause();

            trackBar1.Value = 0;
            is_playing = false;
            vlcPlayer.SetPosition(0);
            SetSelectRow();
            tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(trackBar1.Value), GetTimeString(trackBar1.Maximum));
            if (vlcPlayer.GetState() == 5) vlcPlayer.Pause();
        }

        private void vlcPlayer_OnOpenSucceeded(object sender, EventArgs e)
        {
            BacktoStart();
         
            tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(trackBar1.Value), GetTimeString(trackBar1.Maximum));
            if (vlcPlayer.GetState() == 5) vlcPlayer.Pause();
            var tbMax = vlcPlayer.GetDuration();
            vlcPlayer.SetConfig(103, tbMax.ToString());
            trackBar1.SetRange(0, tbMax);
            trackBar1.Value = 0;
        }

        private void dgv1_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            PlayerPause();
        }

        private void FormAPlayActions_FormClosing(object sender, FormClosingEventArgs e)
        {
            PlayerPause();
        }
        public void PlayerPause()
        {
            if (vlcPlayer.GetState() == 5)
            {
                vlcPlayer.Pause();
                timer1.Stop();
            }
        }
    }
}
