﻿namespace Takata
{
    partial class FormAPlayActions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAPlayActions));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnPlay = new System.Windows.Forms.ToolStripButton();
            this.btnPause = new System.Windows.Forms.ToolStripButton();
            this.btnBackward = new System.Windows.Forms.ToolStripButton();
            this.btnForward = new System.Windows.Forms.ToolStripButton();
            this.btnCancelLoop = new System.Windows.Forms.ToolStripButton();
            this.btnSetLoopStart = new System.Windows.Forms.ToolStripButton();
            this.btnSetLoopEnd = new System.Windows.Forms.ToolStripButton();
            this.btnOpen = new System.Windows.Forms.ToolStripButton();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.dgv1 = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActionName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OperationName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cycle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DisplayActionTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DisplayManualStandardTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.速度 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.类型 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TypeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActionTypeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StartTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EndTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TIME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tbVideoTime = new System.Windows.Forms.TextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.lbProcess = new System.Windows.Forms.Label();
            this.lblFileName = new System.Windows.Forms.Label();
            this.vlcPlayer = new AxAPlayer3Lib.AxPlayer();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vlcPlayer)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnPlay,
            this.btnPause,
            this.btnBackward,
            this.btnForward,
            this.btnCancelLoop,
            this.btnSetLoopStart,
            this.btnSetLoopEnd,
            this.btnOpen});
            this.toolStrip1.Location = new System.Drawing.Point(15, 9);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(496, 27);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnPlay
            // 
            this.btnPlay.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.btnPlay.Image = global::Takata.Properties.Resources.btnplay;
            this.btnPlay.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(57, 24);
            this.btnPlay.Text = "播放";
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // btnPause
            // 
            this.btnPause.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.btnPause.Image = global::Takata.Properties.Resources.btnpause;
            this.btnPause.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(57, 24);
            this.btnPause.Text = "暂停";
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // btnBackward
            // 
            this.btnBackward.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.btnBackward.Image = global::Takata.Properties.Resources.btnbackward;
            this.btnBackward.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnBackward.Name = "btnBackward";
            this.btnBackward.Size = new System.Drawing.Size(57, 24);
            this.btnBackward.Text = "后退";
            this.btnBackward.Click += new System.EventHandler(this.btnBackward_Click);
            // 
            // btnForward
            // 
            this.btnForward.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.btnForward.Image = global::Takata.Properties.Resources.btnforward;
            this.btnForward.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnForward.Name = "btnForward";
            this.btnForward.Size = new System.Drawing.Size(57, 24);
            this.btnForward.Text = "快进";
            this.btnForward.Click += new System.EventHandler(this.btnForward_Click);
            // 
            // btnCancelLoop
            // 
            this.btnCancelLoop.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.btnCancelLoop.Image = global::Takata.Properties.Resources.btnCancelLoop;
            this.btnCancelLoop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCancelLoop.Name = "btnCancelLoop";
            this.btnCancelLoop.Size = new System.Drawing.Size(57, 24);
            this.btnCancelLoop.Text = "取消";
            this.btnCancelLoop.Click += new System.EventHandler(this.btnCancelLoop_Click);
            // 
            // btnSetLoopStart
            // 
            this.btnSetLoopStart.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.btnSetLoopStart.Image = global::Takata.Properties.Resources.btnSetLoopStart;
            this.btnSetLoopStart.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSetLoopStart.Name = "btnSetLoopStart";
            this.btnSetLoopStart.Size = new System.Drawing.Size(71, 24);
            this.btnSetLoopStart.Text = "开始点";
            this.btnSetLoopStart.Click += new System.EventHandler(this.btnSetLoopStart_Click);
            // 
            // btnSetLoopEnd
            // 
            this.btnSetLoopEnd.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.btnSetLoopEnd.Image = global::Takata.Properties.Resources.btnSetLoopEnd;
            this.btnSetLoopEnd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSetLoopEnd.Name = "btnSetLoopEnd";
            this.btnSetLoopEnd.Size = new System.Drawing.Size(71, 24);
            this.btnSetLoopEnd.Text = "结束点";
            this.btnSetLoopEnd.Click += new System.EventHandler(this.btnSetLoopEnd_Click);
            // 
            // btnOpen
            // 
            this.btnOpen.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F);
            this.btnOpen.Image = global::Takata.Properties.Resources.tSBtn_openfile;
            this.btnOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(57, 24);
            this.btnOpen.Text = "打开";
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(13, 755);
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(1073, 45);
            this.trackBar1.TabIndex = 0;
            this.trackBar1.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // dgv1
            // 
            this.dgv1.AllowUserToAddRows = false;
            this.dgv1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.ActionName,
            this.OperationName,
            this.Cycle,
            this.DisplayActionTime,
            this.DisplayManualStandardTime,
            this.速度,
            this.类型,
            this.Type,
            this.Code,
            this.TypeId,
            this.ActionTypeId,
            this.StartTime,
            this.EndTime,
            this.TIME,
            this.ST});
            this.dgv1.Location = new System.Drawing.Point(3, 0);
            this.dgv1.MultiSelect = false;
            this.dgv1.Name = "dgv1";
            this.dgv1.RowTemplate.Height = 23;
            this.dgv1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv1.Size = new System.Drawing.Size(478, 715);
            this.dgv1.TabIndex = 2;
            this.dgv1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv1_CellClick);
            this.dgv1.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgv1_CellMouseDown);
            this.dgv1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgv1_RowPostPaint);
            // 
            // id
            // 
            this.id.DataPropertyName = "ActionId";
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            // 
            // ActionName
            // 
            this.ActionName.DataPropertyName = "ActionName";
            this.ActionName.HeaderText = "动作名称";
            this.ActionName.Name = "ActionName";
            this.ActionName.ReadOnly = true;
            this.ActionName.Width = 80;
            // 
            // OperationName
            // 
            this.OperationName.DataPropertyName = "OperationName";
            this.OperationName.HeaderText = "操作名称";
            this.OperationName.Name = "OperationName";
            this.OperationName.ReadOnly = true;
            this.OperationName.Width = 80;
            // 
            // Cycle
            // 
            this.Cycle.DataPropertyName = "Cycle";
            this.Cycle.HeaderText = "周期";
            this.Cycle.Name = "Cycle";
            this.Cycle.ReadOnly = true;
            this.Cycle.Width = 40;
            // 
            // DisplayActionTime
            // 
            this.DisplayActionTime.DataPropertyName = "DisplayActionTime";
            this.DisplayActionTime.HeaderText = "TIME";
            this.DisplayActionTime.Name = "DisplayActionTime";
            this.DisplayActionTime.ReadOnly = true;
            this.DisplayActionTime.Width = 60;
            // 
            // DisplayManualStandardTime
            // 
            this.DisplayManualStandardTime.DataPropertyName = "DisplayManualStandardTime";
            this.DisplayManualStandardTime.HeaderText = "S . T";
            this.DisplayManualStandardTime.Name = "DisplayManualStandardTime";
            this.DisplayManualStandardTime.ReadOnly = true;
            this.DisplayManualStandardTime.Width = 60;
            // 
            // 速度
            // 
            this.速度.DataPropertyName = "Speed";
            this.速度.HeaderText = "速度";
            this.速度.Name = "速度";
            this.速度.ReadOnly = true;
            this.速度.Width = 40;
            // 
            // 类型
            // 
            this.类型.HeaderText = "类型";
            this.类型.Name = "类型";
            this.类型.ReadOnly = true;
            this.类型.Width = 55;
            // 
            // Type
            // 
            this.Type.HeaderText = "大分类";
            this.Type.Name = "Type";
            this.Type.ReadOnly = true;
            this.Type.Width = 80;
            // 
            // Code
            // 
            this.Code.DataPropertyName = "ActionCode";
            this.Code.HeaderText = "代码";
            this.Code.Name = "Code";
            this.Code.ReadOnly = true;
            // 
            // TypeId
            // 
            this.TypeId.DataPropertyName = "TypeId";
            this.TypeId.HeaderText = "TypeId";
            this.TypeId.Name = "TypeId";
            this.TypeId.ReadOnly = true;
            this.TypeId.Visible = false;
            // 
            // ActionTypeId
            // 
            this.ActionTypeId.DataPropertyName = "ActionTypeId";
            this.ActionTypeId.HeaderText = "ActionTypeId";
            this.ActionTypeId.Name = "ActionTypeId";
            this.ActionTypeId.ReadOnly = true;
            this.ActionTypeId.Visible = false;
            // 
            // StartTime
            // 
            this.StartTime.DataPropertyName = "StartTime";
            this.StartTime.HeaderText = "StartTime";
            this.StartTime.Name = "StartTime";
            this.StartTime.ReadOnly = true;
            this.StartTime.Visible = false;
            // 
            // EndTime
            // 
            this.EndTime.DataPropertyName = "EndTime";
            this.EndTime.HeaderText = "EndTime";
            this.EndTime.Name = "EndTime";
            this.EndTime.ReadOnly = true;
            this.EndTime.Visible = false;
            // 
            // TIME
            // 
            this.TIME.DataPropertyName = "ActionTime";
            this.TIME.HeaderText = "TIME";
            this.TIME.Name = "TIME";
            this.TIME.ReadOnly = true;
            this.TIME.Visible = false;
            this.TIME.Width = 60;
            // 
            // ST
            // 
            this.ST.DataPropertyName = "ManualStandardTime";
            this.ST.HeaderText = "S . T";
            this.ST.Name = "ST";
            this.ST.ReadOnly = true;
            this.ST.Visible = false;
            this.ST.Width = 70;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgv1);
            this.panel2.Location = new System.Drawing.Point(1092, 63);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(480, 718);
            this.panel2.TabIndex = 3;
            // 
            // tbVideoTime
            // 
            this.tbVideoTime.Enabled = false;
            this.tbVideoTime.Location = new System.Drawing.Point(781, 40);
            this.tbVideoTime.Name = "tbVideoTime";
            this.tbVideoTime.Size = new System.Drawing.Size(305, 21);
            this.tbVideoTime.TabIndex = 0;
            // 
            // timer1
            // 
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // lbProcess
            // 
            this.lbProcess.BackColor = System.Drawing.Color.RoyalBlue;
            this.lbProcess.Location = new System.Drawing.Point(21, 762);
            this.lbProcess.Name = "lbProcess";
            this.lbProcess.Size = new System.Drawing.Size(100, 5);
            this.lbProcess.TabIndex = 4;
            this.lbProcess.Visible = false;
            // 
            // lblFileName
            // 
            this.lblFileName.AutoSize = true;
            this.lblFileName.Location = new System.Drawing.Point(17, 47);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(0, 12);
            this.lblFileName.TabIndex = 52;
            // 
            // vlcPlayer
            // 
            this.vlcPlayer.Enabled = true;
            this.vlcPlayer.Location = new System.Drawing.Point(15, 63);
            this.vlcPlayer.Name = "vlcPlayer";
            this.vlcPlayer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("vlcPlayer.OcxState")));
            this.vlcPlayer.Size = new System.Drawing.Size(1071, 687);
            this.vlcPlayer.TabIndex = 53;
            this.vlcPlayer.OnMessage += new AxAPlayer3Lib._IPlayerEvents_OnMessageEventHandler(this.vlcPlayer_OnMessage);
            this.vlcPlayer.OnOpenSucceeded += new System.EventHandler(this.vlcPlayer_OnOpenSucceeded);
            // 
            // FormAPlayActions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1580, 800);
            this.Controls.Add(this.vlcPlayer);
            this.Controls.Add(this.lblFileName);
            this.Controls.Add(this.lbProcess);
            this.Controls.Add(this.tbVideoTime);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.toolStrip1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormAPlayActions";
            this.Text = "标记播放";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormAPlayActions_FormClosing);
            this.Load += new System.EventHandler(this.FormAPlayActions_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv1)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vlcPlayer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnPlay;
        private System.Windows.Forms.ToolStripButton btnPause;
        private System.Windows.Forms.ToolStripButton btnBackward;
        private System.Windows.Forms.ToolStripButton btnForward;
        private System.Windows.Forms.ToolStripButton btnCancelLoop;
        private System.Windows.Forms.ToolStripButton btnSetLoopStart;
        private System.Windows.Forms.ToolStripButton btnSetLoopEnd;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.DataGridView dgv1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStripButton btnOpen;
        private System.Windows.Forms.TextBox tbVideoTime;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label lbProcess;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActionName;
        private System.Windows.Forms.DataGridViewTextBoxColumn OperationName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cycle;
        private System.Windows.Forms.DataGridViewTextBoxColumn DisplayActionTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn DisplayManualStandardTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn 速度;
        private System.Windows.Forms.DataGridViewTextBoxColumn 类型;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn Code;
        private System.Windows.Forms.DataGridViewTextBoxColumn TypeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActionTypeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn StartTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn EndTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn TIME;
        private System.Windows.Forms.DataGridViewTextBoxColumn ST;
        private System.Windows.Forms.Label lblFileName;
        private AxAPlayer3Lib.AxPlayer vlcPlayer;
    }
}