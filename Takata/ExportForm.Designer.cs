﻿namespace Takata
{
    partial class ExportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>

        #endregion
        private System.Windows.Forms.ComboBox cbWorkstep;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX tbxProductName;
        private DevComponents.DotNetBar.LabelX txtFactory;
        private DevComponents.DotNetBar.LabelX txtWorkShop;
        private DevComponents.DotNetBar.LabelX txtWorLine;
        private DevComponents.DotNetBar.LabelX txtCartype;
        private DevComponents.DotNetBar.LabelX labelX8;
    }
}