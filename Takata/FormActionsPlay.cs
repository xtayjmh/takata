﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DAL;
using Model;

namespace Takata
{
    public partial class FormActionsPlay : Form
    {
        private VlcPlayer vlcPlayer;
        private List<t_Action> actions;
        private List<t_Action> validActions;
        Boolean m_IsFullScreen = false;//标记是否全屏
        public FormActionsPlay()
        {
            InitializeComponent();
           
        }

        public FormActionsPlay(t_Video video, string filePath)
        {
            InitializeComponent();
            var list = Common.GetList<t_Action>(i => i.VideoId == video.VideoId);
            actions = list.Where(i => i.TypeId != 2).OrderBy(i=>i.VideoId).ToList();//无效动作
            validActions = list.Where(i => i.TypeId == 2).OrderBy(i=>i.VideoId).ToList();//有效动作
            string pluginPath = System.Environment.CurrentDirectory + "\\plugins\\";
            vlcPlayer = new VlcPlayer(pluginPath);
            IntPtr render_wnd = this.panelEx1.Handle;
            vlcPlayer.SetRenderWindow((int)render_wnd);
            vlcPlayer.SetMouseInput();
            vlcPlayer.PlayFile(filePath);
            timer1.Interval = 10;
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            var skip = false;
            var playTime = vlcPlayer.GetPlayTime();
            actions.ForEach(i =>
            {
                if (playTime >= i.StartTime/1000 && playTime <= i.EndTime/1000)
                {
                    vlcPlayer.Pause();
                    vlcPlayer.SetPlayTime((i.EndTime/1000) + 0.01m);
                    vlcPlayer.SetSpeed(1.00f);
                    vlcPlayer.Play();
                    skip = true;
                }
            });
            if (skip) return;
            var action = validActions.FirstOrDefault(a => playTime >= a.StartTime/1000 && playTime <= a.EndTime/1000);
            if (action != null)
            {
                vlcPlayer.SetSpeed((float)action.Speed);
            }
        }
        //窗体相应键盘事件
        private void FormActionsPlay_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F11:
                    FormBorderStyle = FormBorderStyle.None;
                    WindowState = FormWindowState.Maximized;
                    break;
                case Keys.Escape:
                    if (FormBorderStyle == FormBorderStyle.FixedDialog)//窗口时按ESC关闭
                    {
                        vlcPlayer.Stop();
                        Close();
                    }
                    FormBorderStyle = FormBorderStyle.FixedDialog;
                    WindowState = FormWindowState.Normal;
                    break;
                case Keys.F12:
                    vlcPlayer.Stop();
                    Close();
                    break;
                case Keys.Space:
                    vlcPlayer.Pause();
                    break;
            }
        }
        //关闭窗口时，停止vlcPlayer
        private void FormActionsPlay_FormClosed(object sender, FormClosedEventArgs e)
        {
            timer1.Stop();
            vlcPlayer.Stop();
        }
    }
}
