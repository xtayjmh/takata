﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DAL;
using DAL.SystemManage;
using Model;

namespace Takata
{
    public partial class FormInit : Form
    {
        public t_Video video;
        public FormInit()
        {
            InitializeComponent();           
        }

        private void LoadWorkLine()
        {
            cbSCX.DataSource = null;
            cbCarType.DataSource = null;
            cbProductName.DataSource = null;
            if (cbLB.SelectedItem == null) return;
            var selected = ((ComboBoxItem)cbLB.SelectedItem);
            var swId = selected.Value;
            var workLines = WorkLineManage.GetWorkLines(swId);
            var list = new List<ComboBoxItem>();
            workLines.ForEach(i =>
            {
                list.Add(new ComboBoxItem(i.Name, i.WorkLineId));
            });
            cbSCX.DataSource = list;
            cbSCX.DisplayMember = "Text";
            if (cbSCX.Items.Count > 0)
                cbSCX.SelectedIndex = 0;
        }

        private void LoadProductName()
        {
            cbProductName.DataSource = null;
            if (cbCarType.SelectedItem == null) return;
            var selected = ((ComboBoxItem)cbCarType.SelectedItem);
            var swId = selected.Value;
            var prods = WorkShopManage.GetProds(swId);
            var list = new List<ComboBoxItem>();
            prods.ForEach(i =>
            {
                list.Add(new ComboBoxItem(i.ProdName, i.ProdId));
            });
            cbProductName.DataSource = list;
            cbProductName.DisplayMember = "Text";

        }

        private void LoadCarType()
        {
            cbCarType.SelectedItem = null;
            cbProductName.DataSource = null;
            if (cbSCX.SelectedItem == null) return;
            var selected = ((ComboBoxItem)cbSCX.SelectedItem);
            var swId = selected.Value;
            var carTypes = WorkLineManage.GetCarTypes(swId);
            var list = new List<ComboBoxItem>();
            carTypes.ForEach(i =>
            {
                list.Add(new ComboBoxItem(i.TypeName, i.TypeId));
            });
            cbCarType.DataSource = list;
            cbCarType.DisplayMember = "Text";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var lb = ((ComboBoxItem)cbLB.Items[cbLB.SelectedIndex]).Value; //类别
            if (cbSCX.SelectedIndex < 0)
            {
                DialogResult = DialogResult.None;
                MessageBox.Show("请选择生产线", "提示");
            }
            else if (cbCarType.SelectedIndex < 0)
            {
                DialogResult = DialogResult.None;
                MessageBox.Show("请选择车种", "提示");
            }
            else if (cbProductName.SelectedIndex < 0)
            {
                DialogResult = DialogResult.None;
                MessageBox.Show("请选择品号", "提示");
            }
            else
            {
                var scx = ((ComboBoxItem)cbSCX.Items[cbSCX.SelectedIndex]); //生产线
                var cz = ((ComboBoxItem)cbCarType.Items[cbCarType.SelectedIndex]);
                var ph = ((ComboBoxItem)cbProductName.Items[cbProductName.SelectedIndex]);
                var zx = tbxZX.Text; //坐席
                var cplx = tbxCPLX.Text; //产品类型
   


                DialogResult = DialogResult.None;
                if (string.IsNullOrEmpty(lb.ToString()))
                {
                    MessageBox.Show("请选择类别", "提示");
                }

                else if (string.IsNullOrEmpty(zx))
                {
                    MessageBox.Show("请填写坐席", "提示");
                }
                else if (string.IsNullOrEmpty(cplx))
                {
                    MessageBox.Show("请填写产品类型", "提示");
                }
                else
                {
                    video = new t_Video
                    {
                        WorkLineId = scx.Value,
                        WorkType = lb,
                        SeatNo = zx,
                        ProdType = cplx,
                        PartNo = ph.Value.ToString(),
                        ProdName = ph.Text,
                        CarTypeId = cz.Value,
                        CarTypeName = cz.Text,
                        WorkShopID = lb,
                        ZoneId = Program.CurrentUser.ZoneId
                    };
                    DialogResult = DialogResult.OK;
                }
            }
        }

        private void cbLB_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadWorkLine();
        }

        private void cbCarType_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadProductName();
        }
        private void cbSCX_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCarType();
        }
        
        private void FormInit_Load(object sender, EventArgs e)
        {
            var prodTypes = WorkShopManage.GetWorkShops();
            var prodTypeList = new List<ComboBoxItem>();
            prodTypes.ForEach(i => { prodTypeList.Add(new ComboBoxItem(i.Name, i.WorkShopId)); });
            cbLB.DataSource = prodTypeList;
            cbLB.DisplayMember = "Text";
        }
    }
}
