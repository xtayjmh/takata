﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DAL;
using DevComponents.DotNetBar;
using DevComponents.DotNetBar.Controls;
using Model;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using Utility;
using System.Threading.Tasks;
using DAL.SystemManage;
using System.Threading;

namespace Takata
{
    public partial class FormSystemIntegration : Office2007Form
    {
        private List<t_Video> videos;
        private string _excelPath = "";
        private CancellationTokenSource cancelTokenSource = new CancellationTokenSource();
        public FormSystemIntegration()
        {
            InitializeComponent();
        }
        private void LoadProdType()
        {
            var prodTypes = WorkShopManage.GetWorkShops();
            var prodTypeList = new List<Model.ComboBoxItem>();
            prodTypeList.Add(new Model.ComboBoxItem("全部", 0));
            prodTypes.ForEach(i => { prodTypeList.Add(new Model.ComboBoxItem(i.Name, i.WorkShopId)); });
            cbLB.DataSource = prodTypeList;
            cbLB.DisplayMember = "Text";
        }
        private void LoadWorkLine()
        {
            cbSCX.DataSource = null;
            cbCarType.DataSource = null;
            cbProductName.DataSource = null;
            if (cbLB.SelectedItem == null) return;
            var selected = ((Model.ComboBoxItem)cbLB.SelectedItem);
            var swId = selected.Value;
            var workLines = WorkLineManage.GetWorkLines(swId);
            var list = new List<Model.ComboBoxItem>();
            list.Add(new Model.ComboBoxItem("全部", 0));
            workLines.ForEach(i =>
            {
                list.Add(new Model.ComboBoxItem(i.Name, i.WorkLineId));
            });
            cbSCX.DataSource = list;
            cbSCX.DisplayMember = "Text";
            if (cbSCX.Items.Count > 0)
                cbSCX.SelectedIndex = 0;
        }

        private void LoadProductName()
        {
            cbProductName.DataSource = null;
            if (cbCarType.SelectedItem == null) return;
            var selected = ((Model.ComboBoxItem)cbCarType.SelectedItem);
            var swId = selected.Value;
            var prods = WorkShopManage.GetProds(swId);
            var list = new List<Model.ComboBoxItem>();
            list.Add(new Model.ComboBoxItem("全部", 0));
            prods.ForEach(i =>
            {
                list.Add(new Model.ComboBoxItem(i.ProdName, i.ProdId));
            });
            cbProductName.DataSource = list;
            cbProductName.DisplayMember = "Text";

        }

        private void LoadCarType()
        {
            cbCarType.SelectedItem = null;
            cbProductName.DataSource = null;
            if (cbSCX.SelectedItem == null) return;
            var selected = ((Model.ComboBoxItem)cbSCX.SelectedItem);
            var swId = selected.Value;
            var carTypes = WorkLineManage.GetCarTypes(swId);
            var list = new List<Model.ComboBoxItem>();
            list.Add(new Model.ComboBoxItem("全部", 0));
            carTypes.ForEach(i =>
            {
                list.Add(new Model.ComboBoxItem(i.TypeName, i.TypeId));
            });
            cbCarType.DataSource = list;
            cbCarType.DisplayMember = "Text";
        }

        private void LoadData()
        {
            this.dgv1.DataSource = null;
            var workLines = Common.GetList<t_WorkLine>();
            var prods = WorkShopManage.GetProds();
            if (cbProductName.SelectedItem != null && cbProductName.SelectedIndex != 0)
            {
                var selected = ((Model.ComboBoxItem)cbProductName.SelectedItem);
                var id = selected.Value;
                prods = prods.Where(p => p.ProdId == id).ToList();
            }
            else if (cbCarType.SelectedItem != null && cbCarType.SelectedIndex != 0)
            {
                var selected = ((Model.ComboBoxItem)cbCarType.SelectedItem);
                var id = selected.Value;

                prods = prods.Where(p => p.CarTypeId == id).ToList();

            }
            else if (cbSCX.SelectedItem != null && cbSCX.SelectedIndex != 0)
            {
                var selected = ((Model.ComboBoxItem)cbSCX.SelectedItem);
                var id = selected.Value;

                var tempCarTypes = Common.GetList<t_CarType>().Where(c => c.WorkLineId == id);
                var tempProds = new List<t_Prod>();
                foreach (var c in tempCarTypes)
                {
                    tempProds.AddRange(prods.Where(p => p.CarTypeId == c.TypeId).ToList());
                }
                prods = tempProds;

            }
            else if (cbLB.SelectedItem != null && cbLB.SelectedIndex != 0)
            {
                var selected = ((Model.ComboBoxItem)cbLB.SelectedItem);
                var id = selected.Value;

                var tempWorkLines = Common.GetList<t_WorkLine>().Where(w => w.WorkShopId == id);
                var tempCarTypes = new List<t_CarType>();
                foreach (var wl in tempWorkLines)
                {
                    tempCarTypes.AddRange(Common.GetList<t_CarType>().Where(c => c.WorkLineId == wl.WorkLineId).ToList());
                }
                var tempProds = new List<t_Prod>();
                foreach (var c in tempCarTypes)
                {
                    tempProds.AddRange(prods.Where(p => p.CarTypeId == c.TypeId).ToList());
                }
                prods = tempProds;

            }

            var workShops = Common.GetList<t_WorkShop>();
            var carTypes = Common.GetList<t_CarType>();
            var Zones = Common.GetList<t_Zone>();
            videos = Common.GetList<t_Video>();
            if (Program.CurrentUser.RoleId != 1)
            {
                if (Program.CurrentUser.ZoneId == null)
                {
                    MessageBox.Show("该用户没有对应区域，请设置后再查询");
                    return;
                }
                videos = videos.Where(v => v.ZoneId == Program.CurrentUser.ZoneId).ToList();
            }
            var list = new List<SysItnModel>();
            prods.ForEach(i =>
            {
                var vdos = videos.Where(v => v.PartNo == i.ProdId.ToString()).OrderBy(v => v.Created).ToList();
                var fv = videos.Where(v => v.PartNo == i.ProdId.ToString()).OrderByDescending(v => v.Created).FirstOrDefault();
                if (fv != null)
                {
                    var worklineEntity = Common.GetSingleModel<t_WorkLine>(l => l.WorkLineId == fv.WorkLineId);
                    if (fv != null)
                        list.Add(new SysItnModel()
                        {
                            Videos = vdos,
                            ProdName = fv.ProdName,
                            ProdType = fv.ProdType,
                            CarType = carTypes.FirstOrDefault(c => c.TypeId == fv.CarTypeId)?.TypeName,
                            FileName = fv.FileName,
                            PartNo = fv.PartNo,
                            SeatNo = fv.SeatNo,
                            WorkLineId = fv.WorkLineId,
                            WorkType = workShops.FirstOrDefault(p => p.WorkShopId == fv.WorkType)?.Name,
                            Date = fv.Created.ToString("yyyy/MM/dd"),
                            WorkLine = worklineEntity.Name,
                            HasImproveReport = i.HasImproveReport == 1,
                            HasHumanMachineReport = i.HasHumanMachineReport == 1,
                            HasImproveReportEdit = i.HasImProveReportEdit == 1,
                            ZoneId = fv.ZoneId,
                            ZoneName = Zones.FirstOrDefault(z=>z.ZoneId == fv.ZoneId)?.ZoneName
                        });
                }
            });
            var videosMaxCout = 0;
            if (list.Count > 0)
                videosMaxCout = list.ToList().Select(i => i.Videos.Count).Max();

            var dt = new DataTable();
            dt.Columns.Add("序号");
            dt.Columns.Add("日期");
            dt.Columns.Add("区域");
            dt.Columns.Add("类别");
            dt.Columns.Add("生产线");
            dt.Columns.Add("车种");
            dt.Columns.Add("品号");
            //            dt.Columns.Add("产品名称");
            dt.Columns.Add("座席");
            dt.Columns.Add("产品类型");

            for (int i = 0; i < videosMaxCout; i++)
            {
                dt.Columns.Add($"分析视频{i + 1}");
            }

            dt.Columns.Add("人-机操作分析报表");
            dt.Columns.Add("改善报表");
            dt.Columns.Add("人-机操作分析报表1");
            dt.Columns.Add("改善报表-改");
            dt.Columns.Add("下载改善报表-改");
            dt.Columns.Add("WorkLineId");
            list.ForEach(i =>
            {
                var paras = new Object[15 + videosMaxCout];
                paras.SetValue(list.IndexOf(i) + 1, 0);
                paras.SetValue(i.Date, 1);
                paras.SetValue(i.ZoneName, 2);
                paras.SetValue(i.WorkType, 3);
                paras.SetValue(i.WorkLine, 4);
                paras.SetValue(i.CarType, 5);
                paras.SetValue(i.ProdName, 6);
                //                paras.SetValue(i.ProdName,4);
                paras.SetValue(i.SeatNo, 7);
                paras.SetValue(i.ProdType, 8);


                i.Videos.ForEach(j => { paras.SetValue(j.FileName, 8 + i.Videos.IndexOf(j) + 1); });
                if (i.HasImproveReport) paras.SetValue("改善报表", 14 + videosMaxCout - 4);
                paras.SetValue("双击上传", 14 + videosMaxCout - 2);
                if (i.HasImproveReportEdit) paras.SetValue("双击查看", 14 + videosMaxCout - 1);
                if (i.HasHumanMachineReport)
                {
                    paras.SetValue("人-机操作分析报表", 14 + videosMaxCout - 5);
                    paras.SetValue("人-机操作分析报表", 14 + videosMaxCout - 3);
                    //paras.SetValue("人-机操作分析报表", 11 + videosMaxCout - 3);
                }

                paras.SetValue(i.PartNo, 14 + videosMaxCout);
                dt.Rows.Add(paras);
            });

            this.dgv1.DataSource = dt;
            this.dgv1.ColumnHeadersHeight = 60;
            this.dgv1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv1.MergeColumnNames.Add("Videos");
            this.dgv1.AddSpanHeader(9, videosMaxCout + 2, "改善前");
            this.dgv1.AddSpanHeader(9 + videosMaxCout + 2, 3, "改善后");
            dgv1.Columns[0].Width = 40;
            dgv1.Columns[dgv1.ColumnCount - 6].Width = 120;
            dgv1.Columns[dgv1.ColumnCount - 4].Width = 120;
            dgv1.Columns[0].Width = 40;
            dgv1.Columns[dgv1.ColumnCount - 1].Visible = false;
            dgv1.Font = new Font("宋体", 9);
        }

        private void FormSystemIntegration_Load(object sender, System.EventArgs e)
        {
            LoadProdType();
        }

        private void dgv1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            FormMain form = Program.FormMain;
            var ci = e.ColumnIndex;
            var ri = e.RowIndex;
            if (ri < 0) return;
            var v = dgv1.Rows[ri].Cells[ci].Value;
            var zoneName = dgv1.Rows[ri].Cells[2].Value;
            if (zoneName == null)
            {
                MessageBox.Show("视频没有对应区域");
                return;
            }
            var currentZone = new ZoneManage().GetZoneList().FirstOrDefault(i => i.ZoneName == zoneName.ToString());
            if(currentZone == null)
            {
                MessageBox.Show("视频没有对应区域");
                return;
            }
            var PartNo = dgv1.Rows[ri].Cells[dgv1.ColumnCount - 1].Value;

            var worklineName = dgv1.Rows[ri].Cells[4].Value;
            var Cartype = dgv1.Rows[ri].Cells[5].Value;
            var ProductName = dgv1.Rows[ri].Cells[6].Value;

            var columnCount = dgv1.ColumnCount;
            var ip = currentZone.FTPAddress;
            var user = currentZone.FTPUserName;
            var pwd = currentZone.FTPPassword;
            var port = (int)currentZone.FTPPort;
            FtpHelper ftpop = new FtpHelper(ip, port, user, pwd);
            if (ci > 8 && ci < columnCount - 6) //点击的是分析视频
            {
                form.SetLoadStatusDirectly(true);
                if (!string.IsNullOrEmpty(v.ToString()))
                {
                    bool isExit = true;

                    var FilePath = "Resource\\" + PartNo + "\\1\\" + v;

                    if (File.Exists(Environment.CurrentDirectory + "\\" + FilePath))
                    {
                        form.SetVideoPath(Environment.CurrentDirectory + "\\" + FilePath);
                    }
                    else
                    {
                        var result = ftpop.Download(FilePath);
                        if (result != "")
                        {
                            MessageBox.Show(result);
                            isExit = false;
                        }
                        else
                        {
                            form.SetVideoPath(Environment.CurrentDirectory + "\\" + FilePath);
                        }
                    }
                    form.SetLoadStatusDirectly(false);
                    if (isExit)
                    {
                        form.SetTabShow("视频播放", "Form1");
                        var videoid = videos.FirstOrDefault(i => i.FileName == v.ToString()).VideoId;
                        form.LoadActions(videoid);
                    }
                }
                form.SetLoadStatusDirectly(false);
            }
            else
            {
                var filePath = "";
                var fileName = "";
                try
                {
                    if (ci == columnCount - 6) //人机分析报表
                    {
                        form.SetLoadStatusDirectly(true, "报表");
                        fileName = "人-机操作分析报表_" + worklineName + "_" + Cartype + "_" + ProductName + ".xlsx";
                        filePath = "Resource\\" + PartNo + "\\2\\" + fileName; //1是视频，2是人-机分析报表，3是改善报表
                    }

                    else if (ci == columnCount - 4) //人机分析报表1
                    {
                        form.SetLoadStatusDirectly(true, "报表");
                        fileName = "人-机操作分析报表_" + worklineName + "_" + Cartype + "_" + ProductName + "_" + "改善后.xlsx";
                        filePath = "Resource\\" + PartNo + "\\2\\" + fileName; //1是视频，2是人-机分析报表，3是改善报表
                    }

                    else if (ci == columnCount - 5) //改善报表
                    {
                        form.SetLoadStatusDirectly(true, "报表");
                        fileName = "改善报表_" + worklineName + "_" + Cartype + "_" + ProductName + ".xlsx";
                        filePath = "Resource\\" + PartNo + "\\3\\" + fileName; //1是视频，2是人-机分析报表，3是改善报表
                    }

                    else if (ci == columnCount - 2)
                    {
                        form.SetLoadStatusDirectly(true, "报表");
                        fileName = "改善报表_" + worklineName + "_" + Cartype + "_" + ProductName + "_改.xlsx";
                        filePath = "Resource\\" + PartNo + "\\3\\" + fileName; //1是视频，2是人-机分析报表，3是改善报表
                    }
                    else if (ci == columnCount - 3) //改善报表
                    {
                        if (!Directory.Exists("Resource\\" + PartNo + "\\3\\"))
                        {
                            MessageBox.Show("请先导出改善报表后再进行修改上传");
                            return;
                        }

                        fileName = "改善报表_" + worklineName + "_" + Cartype + "_" + ProductName + "_改.xlsx";
                        filePath = "Resource\\" + PartNo + "\\3\\" + fileName;

                        openFileDialog1.FileName = "";
                        if (openFileDialog1.ShowDialog() == DialogResult.OK)
                        {
                            var file = new FileInfo(openFileDialog1.FileName);
                            var ext = file.Extension;
                            var errorMsg = "";
                            if (!new string[] { ".xls", ".xlsx" }.Contains(ext)) errorMsg = "文件格式不正确，";
                            var fileLength = file.Length;
                            if (fileLength > 10000000) errorMsg += "文件过大，不允许上传";
                            if (!string.IsNullOrEmpty(errorMsg))
                            {
                                MessageBox.Show(errorMsg);
                                return;
                            }
                            File.Copy(openFileDialog1.FileName, filePath, true);
                            ftpop.UpLoadEditReport(openFileDialog1.FileName, PartNo.ToString(), fileName);
                            var wId = int.Parse(PartNo.ToString());
                            var prod = Common.GetSingleModel<t_Prod>(i => i.ProdId == wId);
                            prod.HasImProveReportEdit = 1;
                            Common.AddOrUpdateObject(prod);//上传完成之后更新产线的数据
                            dgv1.Rows[ri].Cells[ci + 1].Value = "双击查看";
                            MessageBox.Show("上传成功");
                            return;
                        }
                        else
                        {
                            return;
                        }
                    }
                    else
                    {
                        return;
                    }

                    if (string.IsNullOrEmpty(filePath))
                    {
                        MessageBox.Show("找不到报表路径：" + filePath);
                        form.SetLoadStatusDirectly(false);
                        return;
                    }

                    if (File.Exists(Environment.CurrentDirectory + "\\" + filePath))
                    {
                        Process.Start(filePath);
                        form.SetLoadStatusDirectly(false);
                        return;
                    }

                    if (!File.Exists(Environment.CurrentDirectory + "\\SysLog.txt")) File.Create(Environment.CurrentDirectory + "\\SysLog.txt");
                    using (var sw = new StreamWriter(Environment.CurrentDirectory + "\\SysLog.txt", true))
                    {
                        sw.WriteLine($"开始下载{fileName}，报表路径：{filePath}...{DateTime.Now}...");
                        sw.Flush();
                    }

                    var result = ftpop.Download(filePath);
                    if (result != "")
                    {
                        MessageBox.Show(result);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("下载报表出错，错误信息：" + ex.Message);
                }

                if (File.Exists(Environment.CurrentDirectory + "\\" + filePath))
                {
                    Process.Start(filePath);
                    form.SetLoadStatusDirectly(false);
                }
                else
                {
                    MessageBox.Show("下载报表出错");
                    form.SetLoadStatusDirectly(false);
                }
            }
        }

        private void cbLB_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadWorkLine();
        }

        private void cbSCX_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCarType();
        }

        private void cbCarType_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadProductName();
        }

        private void cbProductName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            cbLB.SelectedIndex = 0;
            cbSCX.DataSource = null;
            cbCarType.DataSource = null;
            cbProductName.DataSource = null;
            this.dgv1.DataSource = null;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            LoadData();
        }
    }
}