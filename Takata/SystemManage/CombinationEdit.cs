﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DAL.SystemManage;
using DevComponents.DotNetBar;
using Model;

namespace Takata
{
    public partial class CombinationEdit : Office2007Form
    {
        public bool IsUpdated;
        private ButtonX btnCancel;
        private ButtonX btnOk;
        private DevComponents.DotNetBar.Controls.TextBoxX tbxCategoryLevel1;
        private LabelX labelX1;
        private DevComponents.DotNetBar.Controls.TextBoxX tbxCategoryLevel2;
        private LabelX labelX2;
        private DevComponents.DotNetBar.Controls.TextBoxX tbxTimeValue;
        private LabelX labelX5;
        private DevComponents.DotNetBar.Controls.TextBoxX tbxCode;
        private LabelX labelX6;
        private int ModAPTSId;
        public CombinationEdit()
        {
            InitializeComponent();
//            cpBtnColor.SelectedColor = Color.FromArgb(255, 155, 187, 89);
            Text = "添加";
        }

        public CombinationEdit(t_ModAPTS ModAPTS)
        {
            InitializeComponent();
            tbxCategoryLevel1.Text = ModAPTS.CategoryLevel1;
            tbxCategoryLevel2.Text = ModAPTS.CategoryLevel2;
            tbxCode.Text = ModAPTS.Code;
            tbxTimeValue.Text = ModAPTS.TimeVale.ToString();
            Text = "修改";
            ModAPTSId = ModAPTS.ModAPTSId;
        }

        private void btnOk_Click(object sender, System.EventArgs e)
        {
            var CategoryLevel1 = tbxCategoryLevel1.Text.Trim();
            var CategoryLevel2 = tbxCategoryLevel2.Text.Trim();
            var Code = tbxCode.Text.Trim();
            var TimeValue = tbxTimeValue.Text.Trim();
            decimal dTimeValue = 0;
            decimal.TryParse(TimeValue, out dTimeValue);

            if (string.IsNullOrEmpty(CategoryLevel1))
            {
                MessageBox.Show("大类别不能为空", "提示");
            }
            else if (string.IsNullOrEmpty(CategoryLevel2))
            {
                MessageBox.Show("小类别不能为空", "提示");
            }
            else if (string.IsNullOrEmpty(Code))
            {
                MessageBox.Show("代码不能为空", "提示");
            }
            else if (dTimeValue < 0)
            {
                MessageBox.Show("时间值必须大于等于0", "提示");
            }
            else
            {
                var mode = new t_ModAPTS
                {
                    CategoryLevel1 = CategoryLevel1,
                    CategoryLevel2 = CategoryLevel2,
                    Code = Code,
                    TimeVale = Math.Round(dTimeValue, 2)
                };
                switch (Text)
                {
                    case "添加":
                        if (CombinationManage.AddCombination(mode))
                        {
                            IsUpdated = true;
                            DialogResult = DialogResult.OK;
                        }
                        else
                        {
                            MessageBox.Show("添加失败！", "提示");
                            DialogResult = DialogResult.Cancel;
                        }

                        break;
                    case "修改":
                        mode.ModAPTSId = ModAPTSId;
                        if (CombinationManage.EditCombination(mode))
                        {
                            IsUpdated = true;
                            DialogResult = DialogResult.OK;
                        }
                        else
                        {
                            MessageBox.Show("修改失败！", "提示");
                            DialogResult = DialogResult.Cancel;
                        }

                        break;
                }

            }
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            Close();
        }


        private void InitializeComponent()
        {
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.btnOk = new DevComponents.DotNetBar.ButtonX();
            this.tbxCategoryLevel1 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.tbxCategoryLevel2 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.tbxTimeValue = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.tbxCode = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCancel.Location = new System.Drawing.Point(236, 114);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "取消";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnOk.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnOk.Location = new System.Drawing.Point(93, 114);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnOk.TabIndex = 8;
            this.btnOk.Text = "确定";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // tbxCategoryLevel1
            // 
            // 
            // 
            // 
            this.tbxCategoryLevel1.Border.Class = "TextBoxBorder";
            this.tbxCategoryLevel1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbxCategoryLevel1.Location = new System.Drawing.Point(64, 12);
            this.tbxCategoryLevel1.Name = "tbxCategoryLevel1";
            this.tbxCategoryLevel1.PreventEnterBeep = true;
            this.tbxCategoryLevel1.Size = new System.Drawing.Size(124, 21);
            this.tbxCategoryLevel1.TabIndex = 10;
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(7, 12);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 23);
            this.labelX1.TabIndex = 11;
            this.labelX1.Text = " 大类别";
            // 
            // tbxCategoryLevel2
            // 
            // 
            // 
            // 
            this.tbxCategoryLevel2.Border.Class = "TextBoxBorder";
            this.tbxCategoryLevel2.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbxCategoryLevel2.Location = new System.Drawing.Point(272, 12);
            this.tbxCategoryLevel2.Name = "tbxCategoryLevel2";
            this.tbxCategoryLevel2.PreventEnterBeep = true;
            this.tbxCategoryLevel2.Size = new System.Drawing.Size(124, 21);
            this.tbxCategoryLevel2.TabIndex = 12;
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Location = new System.Drawing.Point(215, 12);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(75, 23);
            this.labelX2.TabIndex = 13;
            this.labelX2.Text = "小类别";
            // 
            // tbxTimeValue
            // 
            // 
            // 
            // 
            this.tbxTimeValue.Border.Class = "TextBoxBorder";
            this.tbxTimeValue.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbxTimeValue.Location = new System.Drawing.Point(272, 43);
            this.tbxTimeValue.Name = "tbxTimeValue";
            this.tbxTimeValue.PreventEnterBeep = true;
            this.tbxTimeValue.Size = new System.Drawing.Size(124, 21);
            this.tbxTimeValue.TabIndex = 20;
            // 
            // labelX5
            // 
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Location = new System.Drawing.Point(215, 43);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(75, 23);
            this.labelX5.TabIndex = 21;
            this.labelX5.Text = "时间值";
            // 
            // tbxCode
            // 
            // 
            // 
            // 
            this.tbxCode.Border.Class = "TextBoxBorder";
            this.tbxCode.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbxCode.Location = new System.Drawing.Point(64, 43);
            this.tbxCode.Name = "tbxCode";
            this.tbxCode.PreventEnterBeep = true;
            this.tbxCode.Size = new System.Drawing.Size(124, 21);
            this.tbxCode.TabIndex = 18;
            // 
            // labelX6
            // 
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Location = new System.Drawing.Point(24, 43);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(56, 23);
            this.labelX6.TabIndex = 19;
            this.labelX6.Text = "代码";
            // 
            // labelX3
            // 
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Red;
            this.labelX3.Location = new System.Drawing.Point(215, 72);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(164, 23);
            this.labelX3.TabIndex = 24;
            this.labelX3.Text = "时间值只保留小数点后两位";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(396, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 12);
            this.label1.TabIndex = 25;
            this.label1.Text = "s";
            // 
            // CombinationEdit
            // 
            this.ClientSize = new System.Drawing.Size(412, 160);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.tbxTimeValue);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.tbxCode);
            this.Controls.Add(this.labelX6);
            this.Controls.Add(this.tbxCategoryLevel2);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.tbxCategoryLevel1);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CombinationEdit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }
}
