﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DAL;
using DAL.SystemManage;
using DevComponents.DotNetBar;
using Model;
using Utility;

namespace Takata
{
    public partial class FormZoneEdit : Office2007Form
    {
        public bool m_bUpdate;
         private DevComponents.DotNetBar.LabelX lblUserName;
        private DevComponents.DotNetBar.Controls.TextBoxX tbxZoneName;
        private DevComponents.DotNetBar.Controls.TextBoxX tbxFTPPassword;
        private DevComponents.DotNetBar.LabelX lblAddress;
        private DevComponents.DotNetBar.Controls.TextBoxX tbxFTPUserName;
        private DevComponents.DotNetBar.LabelX lblPhone;
        private DevComponents.DotNetBar.Controls.TextBoxX tbxFTPAddress;
        private DevComponents.DotNetBar.LabelX lblLoginName;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.Controls.TextBoxX tbxFTPPort;
        private DevComponents.DotNetBar.LabelX labelX1;
        private int ZoneId;
        public FormZoneEdit()
        {
            InitializeComponent();
            Text = "添加";
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            var zm = new ZoneManage();

            if (string.IsNullOrEmpty(tbxZoneName.Text))
            {
                MessageBox.Show("区域名称不能为空！", "提示");
            }
            else if (string.IsNullOrEmpty(tbxFTPAddress.Text))
            {
                MessageBox.Show("FTP地址不能为空！", "提示");
            }
            else if (string.IsNullOrEmpty(tbxFTPPort.Text))
            {
                MessageBox.Show("FTP端口不能为空！", "提示");
            }

            else if (string.IsNullOrEmpty(tbxFTPUserName.Text))
            {
                MessageBox.Show("FTP用户名不能为空！", "提示");
            }
            else if (string.IsNullOrEmpty(tbxFTPPassword.Text))
            {
                MessageBox.Show("FTP密码不能为空！", "提示");
            }
            else
            {
                var newZone = new t_Zone
                {
                    IsActive = true,
                    FTPAddress = tbxFTPAddress.Text.Trim(),
                    FTPUserName = tbxFTPUserName.Text.Trim(),
                    FTPPassword = tbxFTPPassword.Text.Trim(),
                    FTPPort = int.Parse(tbxFTPPort.Text.Trim()),
                    ZoneName = tbxZoneName.Text.Trim()
                };

                var zoneList = zm.GetZoneList();
                switch (Text)
                {
                    case "添加":

                        if (zoneList.Where(z => z.ZoneName == tbxZoneName.Text.Trim()).ToList().Count > 0)
                        {
                            MessageBox.Show("该区域名称已存在！", "提示");
                        }

                        if (zoneList.Where(z => z.FTPAddress == newZone.FTPAddress && z.FTPPort == newZone.FTPPort).ToList().Count > 0)
                        {
                            MessageBox.Show("相同FTP信息的区域已存在，请修改后重试！", "提示");
                        }
                        else
                        {
                            if (zm.AddZone(newZone))
                            {
                                MessageBox.Show("添加成功！", "提示");
                                m_bUpdate = true;
                                DialogResult = DialogResult.OK;
                            }
                            else
                            {
                                MessageBox.Show("添加失败！", "提示");
                                DialogResult = DialogResult.Cancel;
                            }
                        }

                        break;
                    case "修改":
                        bool bValid = true;
                        if (tbxZoneNameShadow.Text.Trim() != tbxZoneName.Text.Trim())
                        {
                            if (zoneList.Where(z => z.ZoneName == tbxZoneName.Text.Trim()).ToList().Count > 0)
                            {
                                MessageBox.Show("该区域名称已存在！", "提示");
                                bValid = false;
                            }
                        }
                        if (bValid)
                        {
                            newZone.ZoneId = ZoneId;
                            if (zm.UpdateZone(newZone))
                            {
                                MessageBox.Show("修改成功！", "提示");
                                m_bUpdate = true;
                                DialogResult = DialogResult.OK;
                            }
                            else
                            {
                                MessageBox.Show("修改失败！", "提示");
                                DialogResult = DialogResult.Cancel;
                            }
                        }
                        break;
                }
            }
        }

        public FormZoneEdit(t_Zone zone)
        {
            InitializeComponent();
            tbxFTPPassword.Text = zone.FTPPassword;
            tbxZoneNameShadow.Text = tbxZoneName.Text = zone.ZoneName;
            tbxFTPAddress.Text = zone.FTPAddress;
            tbxFTPPort.Text = zone.FTPPort.ToString();
            tbxFTPUserName.Text = zone.FTPUserName;
            Text = "修改";
            ZoneId = zone.ZoneId;
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Dispose();
        }

        private void InitializeComponent()
        {
            this.lblUserName = new DevComponents.DotNetBar.LabelX();
            this.tbxZoneName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tbxFTPPassword = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblAddress = new DevComponents.DotNetBar.LabelX();
            this.tbxFTPUserName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblPhone = new DevComponents.DotNetBar.LabelX();
            this.tbxFTPAddress = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblLoginName = new DevComponents.DotNetBar.LabelX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.tbxFTPPort = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.tbxZoneNameShadow = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.SuspendLayout();
            // 
            // lblUserName
            // 
            // 
            // 
            // 
            this.lblUserName.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblUserName.Location = new System.Drawing.Point(39, 25);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(67, 23);
            this.lblUserName.TabIndex = 0;
            this.lblUserName.Text = "区域名称：";
            // 
            // tbxZoneName
            // 
            // 
            // 
            // 
            this.tbxZoneName.Border.Class = "TextBoxBorder";
            this.tbxZoneName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbxZoneName.Location = new System.Drawing.Point(115, 27);
            this.tbxZoneName.Name = "tbxZoneName";
            this.tbxZoneName.PreventEnterBeep = true;
            this.tbxZoneName.Size = new System.Drawing.Size(150, 21);
            this.tbxZoneName.TabIndex = 1;
            // 
            // tbxFTPPassword
            // 
            // 
            // 
            // 
            this.tbxFTPPassword.Border.Class = "TextBoxBorder";
            this.tbxFTPPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbxFTPPassword.Location = new System.Drawing.Point(115, 136);
            this.tbxFTPPassword.Name = "tbxFTPPassword";
            this.tbxFTPPassword.PreventEnterBeep = true;
            this.tbxFTPPassword.Size = new System.Drawing.Size(150, 21);
            this.tbxFTPPassword.TabIndex = 6;
            // 
            // lblAddress
            // 
            // 
            // 
            // 
            this.lblAddress.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAddress.Location = new System.Drawing.Point(44, 136);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(75, 23);
            this.lblAddress.TabIndex = 2;
            this.lblAddress.Text = "FTP密码：";
            // 
            // tbxFTPUserName
            // 
            // 
            // 
            // 
            this.tbxFTPUserName.Border.Class = "TextBoxBorder";
            this.tbxFTPUserName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbxFTPUserName.Location = new System.Drawing.Point(115, 109);
            this.tbxFTPUserName.Name = "tbxFTPUserName";
            this.tbxFTPUserName.PreventEnterBeep = true;
            this.tbxFTPUserName.Size = new System.Drawing.Size(150, 21);
            this.tbxFTPUserName.TabIndex = 3;
            // 
            // lblPhone
            // 
            // 
            // 
            // 
            this.lblPhone.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblPhone.Location = new System.Drawing.Point(32, 109);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(75, 23);
            this.lblPhone.TabIndex = 6;
            this.lblPhone.Text = "FTP用户名：";
            // 
            // tbxFTPAddress
            // 
            // 
            // 
            // 
            this.tbxFTPAddress.Border.Class = "TextBoxBorder";
            this.tbxFTPAddress.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbxFTPAddress.Location = new System.Drawing.Point(115, 54);
            this.tbxFTPAddress.Name = "tbxFTPAddress";
            this.tbxFTPAddress.PreventEnterBeep = true;
            this.tbxFTPAddress.Size = new System.Drawing.Size(247, 21);
            this.tbxFTPAddress.TabIndex = 2;
            // 
            // lblLoginName
            // 
            // 
            // 
            // 
            this.lblLoginName.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblLoginName.Location = new System.Drawing.Point(44, 54);
            this.lblLoginName.Name = "lblLoginName";
            this.lblLoginName.Size = new System.Drawing.Size(60, 23);
            this.lblLoginName.TabIndex = 8;
            this.lblLoginName.Text = "FTP地址：";
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Location = new System.Drawing.Point(72, 180);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Size = new System.Drawing.Size(65, 23);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.TabIndex = 8;
            this.buttonX1.Text = "确定";
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Location = new System.Drawing.Point(162, 180);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Size = new System.Drawing.Size(66, 23);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.TabIndex = 11;
            this.buttonX2.Text = "取消";
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // tbxFTPPort
            // 
            // 
            // 
            // 
            this.tbxFTPPort.Border.Class = "TextBoxBorder";
            this.tbxFTPPort.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbxFTPPort.Location = new System.Drawing.Point(115, 82);
            this.tbxFTPPort.Name = "tbxFTPPort";
            this.tbxFTPPort.PreventEnterBeep = true;
            this.tbxFTPPort.Size = new System.Drawing.Size(150, 21);
            this.tbxFTPPort.TabIndex = 12;
            this.tbxFTPPort.Text = "21";
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(44, 82);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(60, 23);
            this.labelX1.TabIndex = 13;
            this.labelX1.Text = "FTP端口：";
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX3.Location = new System.Drawing.Point(297, 136);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.Size = new System.Drawing.Size(65, 23);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX3.TabIndex = 14;
            this.buttonX3.Text = "测试FTP";
            this.buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // tbxZoneNameShadow
            // 
            // 
            // 
            // 
            this.tbxZoneNameShadow.Border.Class = "TextBoxBorder";
            this.tbxZoneNameShadow.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbxZoneNameShadow.Location = new System.Drawing.Point(271, 27);
            this.tbxZoneNameShadow.Name = "tbxZoneNameShadow";
            this.tbxZoneNameShadow.PreventEnterBeep = true;
            this.tbxZoneNameShadow.Size = new System.Drawing.Size(39, 21);
            this.tbxZoneNameShadow.TabIndex = 15;
            this.tbxZoneNameShadow.Visible = false;
            // 
            // FormZoneEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(386, 211);
            this.Controls.Add(this.tbxZoneNameShadow);
            this.Controls.Add(this.buttonX3);
            this.Controls.Add(this.tbxFTPPort);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.buttonX2);
            this.Controls.Add(this.buttonX1);
            this.Controls.Add(this.tbxFTPAddress);
            this.Controls.Add(this.lblLoginName);
            this.Controls.Add(this.tbxFTPUserName);
            this.Controls.Add(this.lblPhone);
            this.Controls.Add(this.tbxFTPPassword);
            this.Controls.Add(this.lblAddress);
            this.Controls.Add(this.tbxZoneName);
            this.Controls.Add(this.lblUserName);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormZoneEdit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.ResumeLayout(false);

        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            var ip = tbxFTPAddress.Text.Trim();
            var ftpuser = tbxFTPUserName.Text.Trim();
            var pwd = tbxFTPPassword.Text.Trim();
            var port = int.Parse(tbxFTPPort.Text.Trim());
            FtpHelper ftpop = new FtpHelper(ip, port, ftpuser, pwd);
            if (ftpop.TestFTP())
            {
                MessageBoxEx.Show("连接成功");
            }
            else
            {
                MessageBoxEx.Show("连接失败，请重新输入FTP信息");
            }
        }
    }
}
