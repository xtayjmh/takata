﻿namespace Takata
{
    partial class WorkShopEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>

        #endregion

        private DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn WorkShopId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActionName;
    }
}