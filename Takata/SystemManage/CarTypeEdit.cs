﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DAL;
using DAL.SystemManage;
using DevComponents.DotNetBar;
using Model;
using System.Threading;

namespace Takata
{
    public partial class CarTypeEdit : Office2007Form
    {
        public CarTypeEdit()
        {
            InitializeComponent();
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.AllowUserToAddRows = false;
        }

        private void tSBtnAdd_Click(object sender, EventArgs e)
        {
            if (cbWorkLine.SelectedItem == null)
            {
                MessageBox.Show("请选择车间");
                return;
            }

            var selected = cbWorkLine.SelectedItem.ToString();
            var swid = selected.Substring(0, selected.IndexOf("-"));
            int wid = int.Parse(swid);
            var WorkLineList = WorkLineManage.GetCarTypes(wid);
            var sa = new t_CarType { TypeName = "添加新车种" };
            WorkLineList.Add(sa);
            dataGridView1.DataSource = WorkLineList;
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.ClearSelection();
            if (dataGridView1.RowCount > 0)
            {
                dataGridView1.Rows[dataGridView1.RowCount - 1].Selected = true;
            }
        }

        private void tSBtnDelete_Click(object sender, EventArgs e)
        {
            var selectedRows = dataGridView1.SelectedRows;
            if ((selectedRows.Count == 0 && dataGridView1.SelectedCells.Count == 0) || MessageBox.Show("车种包含的产品信息也会一起被删除", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) != DialogResult.OK) return;
            if (selectedRows.Count != 0)
            {
                foreach (DataGridViewRow selected in selectedRows)
                {
                    var id = (int)selected.Cells[0].Value;
                    WorkLineManage.DeleteCarType(id);
                }
            }
            else
            {
                var selectedCells = dataGridView1.SelectedCells;
                if (selectedCells.Count != 0)
                {
                    foreach (DataGridViewCell selected in selectedCells)
                    {
                        var id = (int)selected.OwningRow.Cells[0].Value;
                        WorkLineManage.DeleteCarType(id);
                    }
                }
            }
            LoadCarType();
        }

        private void cbFactroy_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            LoadWorkShopData();
        }
        private void LoadWorkShopData()
        {
            cbWorkShop.SelectedItem = null;
            cbWorkShop.Items.Clear();
            if (cbFactroy.SelectedItem == null)
                return;

            var selected = cbFactroy.SelectedItem.ToString();
            var sfid = selected.Substring(0, selected.IndexOf("-"));
            int fid = int.Parse(sfid);
            var lists = WorkShopManage.GetWorkShops(fid);
            foreach (var item in lists)
            {
                cbWorkShop.Items.Add(item.WorkShopId + "--" + item.Name);
            }
        }
        private void LoadWorkLineData()
        {
            cbWorkLine.SelectedItem = null;
            cbWorkLine.Items.Clear();
            if (cbWorkShop.SelectedItem == null)
                return;
            var selected = cbWorkShop.SelectedItem.ToString();
            var swid = selected.Substring(0, selected.IndexOf("-"));
            int wid = int.Parse(swid);
            var lists = WorkLineManage.GetWorkLines(wid);
            foreach (var item in lists)
            {
                cbWorkLine.Items.Add(item.WorkLineId + "--" + item.Name);
            }
        }
        private void cbWorkShop_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            LoadWorkLineData();
        }
        private void cbWorkLine_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            LoadCarType();
        }

        private void LoadCarType()
        {
            dataGridView1.DataSource = null;
            if (cbWorkLine.SelectedItem == null) return;
            var selected = cbWorkLine.SelectedItem.ToString();
            var swId = selected.Substring(0, selected.IndexOf("-"));
            int wid = int.Parse(swId);
            var list = WorkLineManage.GetCarTypes(wid);
            dataGridView1.DataSource = list;
            dataGridView1.AutoGenerateColumns = false;
        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (cbWorkLine.SelectedItem == null)
                return;
            var selected = cbWorkLine.SelectedItem.ToString();
            var swid = selected.Substring(0, selected.IndexOf("-"));
            int wid = int.Parse(swid);
            var n = (string)dataGridView1.Rows[e.RowIndex].Cells[1].Value;
            var id = (int)dataGridView1.Rows[e.RowIndex].Cells[0].Value;
            t_CarType sa = new t_CarType()
            {
                TypeName = n,
                WorkLineId = wid
            };
            if (id == 0)
            {
                Common.AddNewObjReturnObj(sa);
                Thread thread = new Thread(DoWork);
                thread.Start();
            }
            else
            {
                sa.TypeId = id;
                Common.AddOrUpdateObject(sa);
            }

        }

        public delegate void MyInvoke();

        public void DoWork()
        {
            MyInvoke mi = new MyInvoke(LoadCarType);
            this.BeginInvoke(mi);
        }

        private void dataGridView1_RowEnter(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void CarTypeEdit_Load(object sender, EventArgs e)
        {
            var lists = FactoryManage.GetFactorys();
            foreach (var item in lists)
            {
                cbFactroy.Items.Add(item.FactoryId + "--" + item.Name);
            }
        }
    }
}
