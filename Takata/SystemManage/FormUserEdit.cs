﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DAL;
using DAL.SystemManage;
using DevComponents.DotNetBar;
using Model;

namespace Takata
{
    public partial class FormUserEdit : Office2007Form
    {
        public bool m_bUpdate;
        private int UserId;
        public FormUserEdit()
        {
            InitializeComponent();
            GetRoleList();
            GetZoneList();
            comboBoxEx1.SelectedIndex = 0;
            comboBoxEx2.SelectedIndex = 0;
            Text = "添加";
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbxLoginName.Text))
            {
                MessageBox.Show("登录名不能为空！", "提示");
            }
            else if (string.IsNullOrEmpty(tbxUserName.Text))
            {
                MessageBox.Show("姓名不能为空！", "提示");
            }
            else if (string.IsNullOrEmpty(tbxPhone.Text))
            {
                MessageBox.Show("联系方式不能为空！", "提示");
            }
            else if (string.IsNullOrEmpty(comboBoxEx1.Text))
            {
                MessageBox.Show("权限不能为空！", "提示");
            }
            else
            {
                var newUser = new t_User
                {
                    IsActive = 1,
                    LoginName = tbxLoginName.Text.Trim(),
                    Phone = tbxPhone.Text.Trim(),
                    Sex = rBtnMan.Checked ? 1 : 0,
                    UserAddress = tbxAddress.Text.Trim(),
                    UserName = tbxUserName.Text.Trim(),
                    UserPwd = Common.EncryptMd5("123456"),
                    RoleId = (int)comboBoxEx1.SelectedValue,
                    ZoneId = ((int)comboBoxEx2.SelectedValue)
                };
                var um = new UserManage();
                var loginNameList = um.GetLoginNameList();
                switch (Text)
                {
                    case "添加":
                        if (loginNameList.Contains(newUser.LoginName))
                        {
                            MessageBox.Show("登录名已存在，请修改后重试！", "提示");
                        }
                        else
                        {
                            if (um.AddUser(newUser))
                            {
                                m_bUpdate = true;
                                DialogResult = DialogResult.OK;
                            }
                            else
                            {
                                MessageBox.Show("添加失败！", "提示");
                                DialogResult = DialogResult.Cancel;
                            }
                        }

                        break;
                    case "修改":
                        newUser.UserId = UserId;
                        if (um.UpdateUser(newUser))
                        {
                            m_bUpdate = true;
                            DialogResult = DialogResult.OK;
                        }
                        else
                        {
                            MessageBox.Show("修改失败！", "提示");
                            DialogResult = DialogResult.Cancel;
                        }

                        break;
                }
            }
        }

        public FormUserEdit(t_User user)
        {
            InitializeComponent();
            tbxAddress.Text = user.UserAddress;
            tbxUserName.Text = user.UserName;
            tbxLoginName.Text = user.LoginName;
            //            tbxSex.Text = user.SexStr;
            tbxPhone.Text = user.Phone;
            Text = "修改";
            UserId = user.UserId;
            GetRoleList();
            if (user.RoleId !=null)
            {
                comboBoxEx1.SelectedValue = user.RoleId;
            }
            GetZoneList();
            if (user.ZoneId != null)
            {
                comboBoxEx2.SelectedValue = user.ZoneId;
            }

            if (user.Sex == 0)
            {
                rBtnFmale.Select();
            }
            else
            {
                rBtnMan.Select();
            }
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Dispose();
        }

        private void GetRoleList()
        {
            var list = RoleManage.GetRoleList();
            comboBoxEx1.DataSource = list;
            comboBoxEx1.DisplayMember = "RoleName";
            comboBoxEx1.ValueMember = "RoleId";
        }
        private void GetZoneList()
        {
            var list = new ZoneManage().GetZoneList();
            comboBoxEx2.DataSource = list;
            comboBoxEx2.DisplayMember = "ZoneName";
            comboBoxEx2.ValueMember = "ZoneId";
        }
    }
}
