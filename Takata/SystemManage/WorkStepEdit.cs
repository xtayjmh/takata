﻿using System.Data.SqlClient;
using System.Threading;
using System.Windows.Forms;
using DAL;
using DAL.SystemManage;
using DevComponents.DotNetBar;
using Model;

namespace Takata
{
    public partial class WorkStepEdit : Office2007Form
    {
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tSBtnDelete;
        private ToolStripButton tSBtnAdd;
        private ToolStripComboBox cbFactroy;
        private ToolStripComboBox cbWorkShop;
        private ToolStripComboBox cbWorkLine;
        private System.Windows.Forms.DataGridView dataGridView1;
        private delegate void MyInvoke();

        public WorkStepEdit()
        {
            InitializeComponent();
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.AllowUserToAddRows = false;
        }

        public void DoWork()

        {

            MyInvoke mi = new MyInvoke(LoadData);

            this.BeginInvoke(mi);

        }

        private void WorkStepEdit_Load(object sender, System.EventArgs e)
        {
            var lists = FactoryManage.GetFactorys();
            foreach (var item in lists)
            {
                cbFactroy.Items.Add(item.FactoryId + "--" + item.Name);
            }
        }
        private void LoadWorkShopData()
        {
            cbWorkShop.SelectedItem = null;
            cbWorkShop.Items.Clear();
            cbWorkLine.SelectedItem = null;
            cbWorkLine.Items.Clear();
            cbCarType.SelectedItem = null;
            cbCarType.Items.Clear();
            cbProd.SelectedItem = null;
            cbProd.Items.Clear();
            if (cbFactroy.SelectedItem == null)
                return;
            var selected = cbFactroy.SelectedItem.ToString();
            var sfid = selected.Substring(0, selected.IndexOf("-"));
            int fid = int.Parse(sfid);
            var lists = WorkShopManage.GetWorkShops(fid);
            foreach (var item in lists)
            {
                cbWorkShop.Items.Add(item.WorkShopId + "--" + item.Name);
            }
        }
        private void LoadWorkLineData()
        {
            cbWorkLine.SelectedItem = null;
            cbWorkLine.Items.Clear();
            if (cbWorkShop.SelectedItem == null)
                return;
            var selected = cbWorkShop.SelectedItem.ToString();
            var swid = selected.Substring(0, selected.IndexOf("-"));
            int wid = int.Parse(swid);
            var lists = WorkLineManage.GetWorkLines(wid);
            foreach (var item in lists)
            {
                cbWorkLine.Items.Add(item.WorkLineId + "--" + item.Name);
            }
        }

        private void LoadData()
        {
            if (cbProd.SelectedItem == null)
                return;
            var selected = cbProd.SelectedItem.ToString();
            var swid = selected.Substring(0, selected.IndexOf("-"));
            int wid = int.Parse(swid);
            dataGridView1.DataSource = WorkStepManage.GetWorkSteps(wid);
            dataGridView1.AutoGenerateColumns = false;
        }

        private void dataGridView1_CellEndEdit(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            if (cbProd.SelectedItem == null)
                return;
            var selected = cbProd.SelectedItem.ToString();
            var swid = selected.Substring(0, selected.IndexOf("-"));
            int wid = int.Parse(swid);
            var n = (string)dataGridView1.Rows[e.RowIndex].Cells[1].Value;
            var id = (int)dataGridView1.Rows[e.RowIndex].Cells[0].Value;
            Model.t_WorkStep sa = new Model.t_WorkStep();
            sa.Name = n;
            sa.ProdId = wid;
            if (id == 0)
            {
                WorkStepManage.AddWorkStep(sa);
                Thread thread = new Thread(DoWork);
                thread.Start();
            }
            else
            {
                sa.WorkStepId = id;
                WorkStepManage.EditWorkStep(sa);
            }
        }

        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tSBtnAdd = new System.Windows.Forms.ToolStripButton();
            this.tSBtnDelete = new System.Windows.Forms.ToolStripButton();
            this.cbFactroy = new System.Windows.Forms.ToolStripComboBox();
            this.cbWorkShop = new System.Windows.Forms.ToolStripComboBox();
            this.cbWorkLine = new System.Windows.Forms.ToolStripComboBox();
            this.cbCarType = new System.Windows.Forms.ToolStripComboBox();
            this.cbProd = new System.Windows.Forms.ToolStripComboBox();
            this.WorkStepId = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.ActionName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.WorkStepId,
            this.ActionName});
            this.dataGridView1.Location = new System.Drawing.Point(0, 25);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1564, 784);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEndEdit);
            this.dataGridView1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView1_DataError);
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tSBtnAdd,
            this.tSBtnDelete,
            this.cbFactroy,
            this.cbWorkShop,
            this.cbWorkLine,
            this.cbCarType,
            this.cbProd});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(2, 0, 1, 0);
            this.toolStrip1.Size = new System.Drawing.Size(1386, 25);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "添加";
            // 
            // tSBtnAdd
            // 
            this.tSBtnAdd.Image = global::Takata.Properties.Resources.add;
            this.tSBtnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSBtnAdd.Name = "tSBtnAdd";
            this.tSBtnAdd.Size = new System.Drawing.Size(52, 22);
            this.tSBtnAdd.Text = "添加";
            this.tSBtnAdd.Click += new System.EventHandler(this.tSBtnAdd_Click);
            // 
            // tSBtnDelete
            // 
            this.tSBtnDelete.Image = global::Takata.Properties.Resources.delete;
            this.tSBtnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSBtnDelete.Name = "tSBtnDelete";
            this.tSBtnDelete.Size = new System.Drawing.Size(52, 22);
            this.tSBtnDelete.Text = "删除";
            this.tSBtnDelete.Click += new System.EventHandler(this.tSBtnDelete_Click);
            // 
            // cbFactroy
            // 
            this.cbFactroy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFactroy.Name = "cbFactroy";
            this.cbFactroy.Size = new System.Drawing.Size(200, 25);
            this.cbFactroy.SelectedIndexChanged += new System.EventHandler(this.cbFactroy_SelectedIndexChanged);
            // 
            // cbWorkShop
            // 
            this.cbWorkShop.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWorkShop.Name = "cbWorkShop";
            this.cbWorkShop.Size = new System.Drawing.Size(200, 25);
            this.cbWorkShop.SelectedIndexChanged += new System.EventHandler(this.cbWorkShop_SelectedIndexChanged);
            // 
            // cbWorkLine
            // 
            this.cbWorkLine.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWorkLine.Name = "cbWorkLine";
            this.cbWorkLine.Size = new System.Drawing.Size(200, 25);
            this.cbWorkLine.SelectedIndexChanged += new System.EventHandler(this.cbWorkLine_SelectedIndexChanged);
            // 
            // cbCarType
            // 
            this.cbCarType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCarType.Name = "cbCarType";
            this.cbCarType.Size = new System.Drawing.Size(200, 25);
            this.cbCarType.SelectedIndexChanged += new System.EventHandler(this.cbCarType_SelectedIndexChanged);
            // 
            // cbProd
            // 
            this.cbProd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbProd.Name = "cbProd";
            this.cbProd.Size = new System.Drawing.Size(200, 25);
            this.cbProd.SelectedIndexChanged += new System.EventHandler(this.cbProd_SelectedIndexChanged);
            // 
            // WorkStepId
            // 
            this.WorkStepId.DataPropertyName = "WorkStepId";
            this.WorkStepId.FillWeight = 50F;
            this.WorkStepId.HeaderText = "ID";
            this.WorkStepId.Name = "WorkStepId";
            this.WorkStepId.ReadOnly = true;
            this.WorkStepId.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.WorkStepId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.WorkStepId.Visible = false;
            this.WorkStepId.Width = 40;
            // 
            // ActionName
            // 
            this.ActionName.DataPropertyName = "Name";
            this.ActionName.HeaderText = "工序名称";
            this.ActionName.Name = "ActionName";
            this.ActionName.Width = 340;
            // 
            // WorkStepEdit
            // 
            this.ClientSize = new System.Drawing.Size(1386, 784);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.dataGridView1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "WorkStepEdit";
            this.Load += new System.EventHandler(this.WorkStepEdit_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        private void tSBtnDelete_Click(object sender, System.EventArgs e)
        {
            var selectedRows = dataGridView1.SelectedRows;
            if ((selectedRows.Count == 0 && dataGridView1.SelectedCells.Count == 0) || MessageBox.Show("该工序信息将被删除", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) != DialogResult.OK) return;
            if (selectedRows.Count != 0)
            {
                foreach (DataGridViewRow selected in selectedRows)
                {
                    var id = (int)selected.Cells[0].Value;
                    WorkStepManage.DeleteWorkStep(id);
                }
            }
            else
            {
                var selectedCells = dataGridView1.SelectedCells;
                if (selectedCells.Count != 0)
                {
                    foreach (DataGridViewCell selected in selectedCells)
                    {
                        var id = (int)selected.OwningRow.Cells[0].Value;
                        WorkStepManage.DeleteWorkStep(id);
                    }
                }
            }
            LoadData();
        }

        private void tSBtnAdd_Click(object sender, System.EventArgs e)
        {
            if (cbProd.SelectedItem == null)
            {
                MessageBox.Show("请选择产品");
                return;
            }
            var selected = cbProd.SelectedItem.ToString();
            var swid = selected.Substring(0, selected.IndexOf("-"));
            int wid = int.Parse(swid);
            var WorkStepList = WorkStepManage.GetWorkSteps(wid);
            Model.t_WorkStep sa = new Model.t_WorkStep();
            sa.Name = "添加新工序";
            WorkStepList.Add(sa);
            dataGridView1.DataSource = WorkStepList;
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.ClearSelection();
            dataGridView1.Rows[dataGridView1.RowCount - 1].Selected = true;
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("请输入正确的数据格式");
        }

        private void cbFactroy_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            dataGridView1.DataSource = null;
            LoadWorkShopData();
        }

        private void cbWorkShop_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            dataGridView1.DataSource = null;
            LoadWorkLineData();
        }
        private void LoadCarType()
        {
            cbCarType.SelectedItem = null;
            cbCarType.Items.Clear();
            if (cbWorkLine.SelectedItem == null) return;
            var selected = cbWorkLine.SelectedItem.ToString();
            var swId = selected.Substring(0, selected.IndexOf("-"));
            int wid = int.Parse(swId);
            var list = WorkLineManage.GetCarTypes(wid);
            foreach (var item in list)
            {
                cbCarType.Items.Add($"{item.TypeId}--{item.TypeName}");
            }
        }

        private void LoadProd()
        {
            cbProd.SelectedItem = null;
            cbProd.Items.Clear();
            if (cbCarType.SelectedItem == null) return;
            var selected = cbCarType.SelectedItem.ToString();
            var swId = selected.Substring(0, selected.IndexOf("-"));
            int wid = int.Parse(swId);
            var list = WorkShopManage.GetProds(wid);
            foreach (var item in list)
            {
                cbProd.Items.Add($"{item.ProdId}--{item.ProdName}");
            }
        }

        private void cbWorkLine_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            dataGridView1.DataSource = null;
             LoadCarType();
        }

        private void cbCarType_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            dataGridView1.DataSource = null;
            LoadProd();
        }

        private void cbProd_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            dataGridView1.DataSource = null;
            LoadData();
        }
    }
}
