﻿using System.Data.SqlClient;
using System.Threading;
using System.Windows.Forms;
using DAL;
using DAL.SystemManage;
using DevComponents.DotNetBar;
using Model;

namespace Takata
{
    public partial class WorkShopEdit : Office2007Form
    {
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tSBtnDelete;
        private ToolStripButton tSBtnAdd;
        private ToolStripComboBox cbFactroy;
        private System.Windows.Forms.DataGridView dataGridView1;
        private delegate void MyInvoke();

        public WorkShopEdit()
        {
            InitializeComponent();
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.AllowUserToAddRows = false;
        }

        public void DoWork()

        {

            MyInvoke mi = new MyInvoke(LoadData);

            this.BeginInvoke(mi);

        }

        private void WorkShopEdit_Load(object sender, System.EventArgs e)
        {
            var lists = FactoryManage.GetFactorys();
            foreach (var item in lists)
            {
                cbFactroy.Items.Add(item.FactoryId + "--" + item.Name);
            }
        }

        private void LoadData()
        {
            if (cbFactroy.SelectedItem  == null)
                return;
            var selected = cbFactroy.SelectedItem.ToString();
            var sfid = selected.Substring(0, selected.IndexOf("-"));
            int fid = int.Parse(sfid);
            dataGridView1.DataSource = WorkShopManage.GetWorkShops(fid);
            dataGridView1.AutoGenerateColumns = false;
           
        }

        private void dataGridView1_CellEndEdit(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            if (cbFactroy.SelectedItem == null)
                return;
            var selected = cbFactroy.SelectedItem.ToString();
            var sfid = selected.Substring(0, selected.IndexOf("-"));
            int fid = int.Parse(sfid);
            var n = (string)dataGridView1.Rows[e.RowIndex].Cells[1].Value;
            var id = (int)dataGridView1.Rows[e.RowIndex].Cells[0].Value;
            Model.t_WorkShop sa = new Model.t_WorkShop();
            sa.Name = n;
            sa.FactoryId = fid;
            if (id == 0)
            {
                WorkShopManage.AddWorkShop(sa);
                Thread thread = new Thread(DoWork);
                thread.Start();
            }
            else
            {
                sa.WorkShopId = id;
                WorkShopManage.EditWorkShop(sa);
            }
        }

        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tSBtnAdd = new System.Windows.Forms.ToolStripButton();
            this.tSBtnDelete = new System.Windows.Forms.ToolStripButton();
            this.cbFactroy = new System.Windows.Forms.ToolStripComboBox();
            this.WorkShopId = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.ActionName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.WorkShopId,
            this.ActionName});
            this.dataGridView1.Location = new System.Drawing.Point(0, 25);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1564, 784);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEndEdit);
            this.dataGridView1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView1_DataError);
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tSBtnAdd,
            this.tSBtnDelete,
            this.cbFactroy});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(2, 0, 1, 0);
            this.toolStrip1.Size = new System.Drawing.Size(1386, 25);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "添加";
            // 
            // tSBtnAdd
            // 
            this.tSBtnAdd.Image = global::Takata.Properties.Resources.add;
            this.tSBtnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSBtnAdd.Name = "tSBtnAdd";
            this.tSBtnAdd.Size = new System.Drawing.Size(52, 22);
            this.tSBtnAdd.Text = "添加";
            this.tSBtnAdd.Click += new System.EventHandler(this.tSBtnAdd_Click);
            // 
            // tSBtnDelete
            // 
            this.tSBtnDelete.Image = global::Takata.Properties.Resources.delete;
            this.tSBtnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSBtnDelete.Name = "tSBtnDelete";
            this.tSBtnDelete.Size = new System.Drawing.Size(52, 22);
            this.tSBtnDelete.Text = "删除";
            this.tSBtnDelete.Click += new System.EventHandler(this.tSBtnDelete_Click);
            // 
            // cbFactroy
            // 
            this.cbFactroy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFactroy.Name = "cbFactroy";
            this.cbFactroy.Size = new System.Drawing.Size(200, 25);
            this.cbFactroy.SelectedIndexChanged += new System.EventHandler(this.cbFactroy_SelectedIndexChanged);
            // 
            // WorkShopId
            // 
            this.WorkShopId.DataPropertyName = "WorkShopId";
            this.WorkShopId.FillWeight = 50F;
            this.WorkShopId.HeaderText = "ID";
            this.WorkShopId.Name = "WorkShopId";
            this.WorkShopId.ReadOnly = true;
            this.WorkShopId.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.WorkShopId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.WorkShopId.Visible = false;
            this.WorkShopId.Width = 40;
            // 
            // ActionName
            // 
            this.ActionName.DataPropertyName = "Name";
            this.ActionName.HeaderText = "车间名称";
            this.ActionName.Name = "ActionName";
            this.ActionName.Width = 340;
            // 
            // WorkShopEdit
            // 
            this.ClientSize = new System.Drawing.Size(1386, 784);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.dataGridView1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "WorkShopEdit";
            this.Load += new System.EventHandler(this.WorkShopEdit_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        private void tSBtnDelete_Click(object sender, System.EventArgs e)
        {
            var selectedRows = dataGridView1.SelectedRows;
            if ((selectedRows.Count == 0 && dataGridView1.SelectedCells.Count == 0) || MessageBox.Show("车间包含的产线以及产线包含的工序数据都会一起被删除", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) != DialogResult.OK) return;
            if (selectedRows.Count != 0)
            {
                foreach (DataGridViewRow selected in selectedRows)
                {
                    var id = (int)selected.Cells[0].Value;
                    WorkShopManage.DeleteWorkShop(id);
                }
            }
            else
            {
                var selectedCells = dataGridView1.SelectedCells;
                if (selectedCells.Count != 0)
                {
                    foreach (DataGridViewCell selected in selectedCells)
                    {
                        var id = (int)selected.OwningRow.Cells[0].Value;
                        WorkShopManage.DeleteWorkShop(id);
                    }
                }
            }
            LoadData();
        }

        private void tSBtnAdd_Click(object sender, System.EventArgs e)
        {
            if (cbFactroy.SelectedItem == null)
            {
                MessageBox.Show("请选择工厂");
                return;
            }
            var selected = cbFactroy.SelectedItem.ToString();
            var sfid = selected.Substring(0, selected.IndexOf("-"));
            int fid = int.Parse(sfid);
            var WorkShopList = WorkShopManage.GetWorkShops(fid);
            Model.t_WorkShop sa = new Model.t_WorkShop();
            sa.Name = "添加新车间";
            WorkShopList.Add(sa);
            dataGridView1.DataSource = WorkShopList;
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.ClearSelection();
            dataGridView1.Rows[dataGridView1.RowCount - 1].Selected = true;
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("请输入正确的数据格式"); 
        }

        private void cbFactroy_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            dataGridView1.DataSource = null;
            LoadData();
        }
    }
}
