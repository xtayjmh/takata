﻿using System.Data.SqlClient;
using System.Threading;
using System.Windows.Forms;
using DAL;
using DAL.SystemManage;
using DevComponents.DotNetBar;
using Model;

namespace Takata
{
    public partial class WorkLineEdit : Office2007Form
    {
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tSBtnDelete;
        private ToolStripButton tSBtnAdd;
        private ToolStripComboBox cbFactroy;
        private ToolStripComboBox cbWorkShop;
        private System.Windows.Forms.DataGridView dataGridView1;
        private delegate void MyInvoke();

        public WorkLineEdit()
        {
            InitializeComponent();
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.AllowUserToAddRows = false;
        }

        public void DoWork()

        {

            MyInvoke mi = new MyInvoke(LoadData);

            this.BeginInvoke(mi);

        }

        private void WorkLineEdit_Load(object sender, System.EventArgs e)
        {
            var lists = FactoryManage.GetFactorys();
            foreach (var item in lists)
            {
                cbFactroy.Items.Add(item.FactoryId + "--" + item.Name);
            }
        }
        private void LoadWorkShopData()
        {
            cbWorkShop.SelectedItem = null;
            cbWorkShop.Items.Clear();
            if (cbFactroy.SelectedItem == null)
                return;
          
            var selected = cbFactroy.SelectedItem.ToString();
            var sfid = selected.Substring(0, selected.IndexOf("-"));
            int fid = int.Parse(sfid);
            var lists = WorkShopManage.GetWorkShops(fid);
            foreach (var item in lists)
            {
                cbWorkShop.Items.Add(item.WorkShopId + "--" + item.Name);
            }
        }


        private void LoadData()
        {
            if (cbWorkShop.SelectedItem  == null)
                return;
            var selected = cbWorkShop.SelectedItem.ToString();
            var swid = selected.Substring(0, selected.IndexOf("-"));
            int wid = int.Parse(swid);
            dataGridView1.DataSource = WorkLineManage.GetWorkLines(wid);
            dataGridView1.AutoGenerateColumns = false;
        }

        private void dataGridView1_CellEndEdit(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            if (cbWorkShop.SelectedItem == null)
                return;
            var selected = cbWorkShop.SelectedItem.ToString();
            var swid = selected.Substring(0, selected.IndexOf("-"));
            int wid = int.Parse(swid);
            var n = (string)dataGridView1.Rows[e.RowIndex].Cells[1].Value;
            var id = (int)dataGridView1.Rows[e.RowIndex].Cells[0].Value;
            t_WorkLine sa = new t_WorkLine
            {
                Name = n,
                WorkShopId = wid
            };
            if (id == 0)
            {
                WorkLineManage.AddWorkLine(sa);
                Thread thread = new Thread(DoWork);
                thread.Start();
            }
            else
            {
                sa.WorkLineId = id;
                WorkLineManage.EditWorkLine(sa);
            }
        }

        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tSBtnAdd = new System.Windows.Forms.ToolStripButton();
            this.tSBtnDelete = new System.Windows.Forms.ToolStripButton();
            this.cbFactroy = new System.Windows.Forms.ToolStripComboBox();
            this.cbWorkShop = new System.Windows.Forms.ToolStripComboBox();
            this.WorkLineId = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.ActionName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.WorkLineId,
            this.ActionName});
            this.dataGridView1.Location = new System.Drawing.Point(0, 25);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1564, 784);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEndEdit);
            this.dataGridView1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView1_DataError);
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tSBtnAdd,
            this.tSBtnDelete,
            this.cbFactroy,
            this.cbWorkShop});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(2, 0, 1, 0);
            this.toolStrip1.Size = new System.Drawing.Size(1386, 25);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "添加";
            // 
            // tSBtnAdd
            // 
            this.tSBtnAdd.Image = global::Takata.Properties.Resources.add;
            this.tSBtnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSBtnAdd.Name = "tSBtnAdd";
            this.tSBtnAdd.Size = new System.Drawing.Size(52, 22);
            this.tSBtnAdd.Text = "添加";
            this.tSBtnAdd.Click += new System.EventHandler(this.tSBtnAdd_Click);
            // 
            // tSBtnDelete
            // 
            this.tSBtnDelete.Image = global::Takata.Properties.Resources.delete;
            this.tSBtnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSBtnDelete.Name = "tSBtnDelete";
            this.tSBtnDelete.Size = new System.Drawing.Size(52, 22);
            this.tSBtnDelete.Text = "删除";
            this.tSBtnDelete.Click += new System.EventHandler(this.tSBtnDelete_Click);
            // 
            // cbFactroy
            // 
            this.cbFactroy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFactroy.Name = "cbFactroy";
            this.cbFactroy.Size = new System.Drawing.Size(200, 25);
            this.cbFactroy.SelectedIndexChanged += new System.EventHandler(this.cbFactroy_SelectedIndexChanged);
            // 
            // cbWorkShop
            // 
            this.cbWorkShop.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWorkShop.Name = "cbWorkShop";
            this.cbWorkShop.Size = new System.Drawing.Size(200, 25);
            this.cbWorkShop.SelectedIndexChanged += new System.EventHandler(this.cbWorkShop_SelectedIndexChanged);
            // 
            // WorkLineId
            // 
            this.WorkLineId.DataPropertyName = "WorkLineId";
            this.WorkLineId.FillWeight = 50F;
            this.WorkLineId.HeaderText = "ID";
            this.WorkLineId.Name = "WorkLineId";
            this.WorkLineId.ReadOnly = true;
            this.WorkLineId.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.WorkLineId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.WorkLineId.Visible = false;
            this.WorkLineId.Width = 40;
            // 
            // ActionName
            // 
            this.ActionName.DataPropertyName = "Name";
            this.ActionName.HeaderText = "生产线名称";
            this.ActionName.Name = "ActionName";
            this.ActionName.Width = 340;
            // 
            // WorkLineEdit
            // 
            this.ClientSize = new System.Drawing.Size(1386, 784);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.dataGridView1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "WorkLineEdit";
            this.Load += new System.EventHandler(this.WorkLineEdit_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        private void tSBtnDelete_Click(object sender, System.EventArgs e)
        {
            var selectedRows = dataGridView1.SelectedRows;
            if ((selectedRows.Count == 0 && dataGridView1.SelectedCells.Count == 0) || MessageBox.Show("产线包含的车种信息也会一起被删除", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) != DialogResult.OK) return;
            if (selectedRows.Count != 0)
            {
                foreach (DataGridViewRow selected in selectedRows)
                {
                    var id = (int)selected.Cells[0].Value;
                    WorkLineManage.DeleteWorkLine(id);
                }
            }
            else
            {
                var selectedCells = dataGridView1.SelectedCells;
                if (selectedCells.Count != 0)
                {
                    foreach (DataGridViewCell selected in selectedCells)
                    {
                        var id = (int)selected.OwningRow.Cells[0].Value;
                        WorkLineManage.DeleteWorkLine(id);
                    }
                }
            }
            LoadData();
        }

        private void tSBtnAdd_Click(object sender, System.EventArgs e)
        {
            if (cbWorkShop.SelectedItem == null)
            {
                MessageBox.Show("请选择车间");
                return;
            }
                
            var selected = cbWorkShop.SelectedItem.ToString();
            var swid = selected.Substring(0, selected.IndexOf("-"));
            int wid = int.Parse(swid);
            var WorkLineList = WorkLineManage.GetWorkLines(wid);
            var sa = new t_WorkLine {Name = "添加新生产线"};
            WorkLineList.Add(sa);
            dataGridView1.DataSource = WorkLineList;
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.ClearSelection();
            dataGridView1.Rows[dataGridView1.RowCount - 1].Selected = true;//选中最后一行新添加的数据
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("请输入正确的数据格式"); 
        }

        private void cbFactroy_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            dataGridView1.DataSource = null;
            LoadWorkShopData();
        }

        private void cbWorkShop_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            dataGridView1.DataSource = null;
            LoadData();
        }
    }
}
