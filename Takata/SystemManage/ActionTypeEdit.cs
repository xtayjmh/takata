﻿using System.Drawing;
using System.Windows.Forms;
using DAL.SystemManage;
using DevComponents.DotNetBar;
using Model;

namespace Takata
{
    public partial class ActionTypeEdit : Office2007Form
    {
        public bool IsUpdated;
        private int actionTypeId;
        public ActionTypeEdit()
        {
            InitializeComponent();
//            cpBtnColor.SelectedColor = Color.FromArgb(255, 155, 187, 89);
            Text = "添加";
            SetRankItem();
            tbxChineseName.Select();
        }

        public ActionTypeEdit(t_ActionType actionType)
        {
            InitializeComponent();
            SetRankItem();
            var arr = actionType.Color.Split(',');
            tbxShortName.Text = actionType.ShortName;
            tbxChineseName.Text = actionType.ChineseName;
            tbxEnglishName.Text = actionType.EnglishName;
            tbxDescription.Text = actionType.Description;
            cpBtnColor.SelectedColor = Color.FromArgb(int.Parse(arr[0]), int.Parse(arr[1]), int.Parse(arr[2]), int.Parse(arr[3]));
            cbRank.SelectedIndex = (actionType.Rank??0)-1;
            Text = "修改";
            actionTypeId = actionType.ActionTypeId;
        }

        private void btnOk_Click(object sender, System.EventArgs e)
        {
            var sc = cpBtnColor.SelectedColor;
            var chineseName = tbxChineseName.Text.Trim();
            var englishName = tbxEnglishName.Text.Trim();
            var description = tbxDescription.Text.Trim();
            var shortName = tbxShortName.Text.Trim();
            var color = $"{sc.A},{sc.R},{sc.G},{sc.B}";
            if (string.IsNullOrEmpty(chineseName))
            {
                MessageBox.Show("中文名不能为空", "提示");
            }
            else if (string.IsNullOrEmpty(englishName))
            {
                MessageBox.Show("英文名不能为空", "提示");
            }
            else if (string.IsNullOrEmpty(description))
            {
                MessageBox.Show("描述内容不能为空", "提示");
            }
            else if (string.IsNullOrEmpty(shortName))
            {
                MessageBox.Show("简称不能为空", "提示");
            }
            else if (sc.Name=="0")
            {
                MessageBox.Show("颜色不能为空", "提示");
            }
            else
            {
                var mode = new t_ActionType
                {
                    ChineseName = chineseName,
                    Color = color,
                    Description = description,
                    EnglishName = englishName,
                    Rank = (cbRank.SelectedItem as RankItem)?.Value,
                    ShortName = shortName
                };
                switch (Text)
                {
                    case "添加":
                        if (ActionTypeManage.AddActionType(mode))
                        {
                            IsUpdated = true;
                            DialogResult = DialogResult.OK;
                        }
                        else
                        {
                            MessageBox.Show("添加失败！", "提示");
                            DialogResult = DialogResult.Cancel;
                        }

                        break;
                    case "修改":
                        mode.ActionTypeId = actionTypeId;
                        if (ActionTypeManage.EditActionType(mode))
                        {
                            IsUpdated= true;
                            DialogResult = DialogResult.OK;
                        }
                        else
                        {
                            MessageBox.Show("修改失败！", "提示");
                            DialogResult = DialogResult.Cancel;
                        }

                        break;
                }
               
            }
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void SetRankItem()
        {
            cbRank.Items.Add(new RankItem("第一项",1));
            cbRank.Items.Add(new RankItem("第二项",2));
            cbRank.Items.Add(new RankItem("第三项",3));
            cbRank.SelectedIndex = 0;
        }
        
    }
}
