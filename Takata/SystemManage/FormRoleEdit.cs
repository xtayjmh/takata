﻿using System.Windows.Forms;
using DAL.SystemManage;
using DevComponents.DotNetBar;
using Model;

namespace Takata
{
    public partial class FormRoleEdit : Office2007Form
    {
        private int RoleId;
        public bool m_bUpdate;
        public FormRoleEdit()
        {
            InitializeComponent();
            Text = "添加权限";
        }

        public FormRoleEdit(t_Role role)
        {
            InitializeComponent();
            tbxRoleName.Text = role.RoleName;
            tbxRoleNote.Text = role.RoleNote;
            Text = "编辑权限";
            RoleId = role.RoleId;
        }
        private void buttonX1_Click(object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(tbxRoleName.Text))
            {
                MessageBox.Show("权限名称不能为空！", "提示");
            }
            else
            {
                var role = new t_Role()
                {
                    RoleNote = tbxRoleNote.Text.Trim(),
                    RoleName = tbxRoleName.Text.Trim()
                };
                switch (Text)
                {
                    case "添加权限":
                        if (RoleManage.AddNewRole(role))
                        {
                            m_bUpdate = true;
                            DialogResult = DialogResult.OK;
                        }
                        else
                        {
                            MessageBox.Show("添加失败！", "提示");
                            DialogResult = DialogResult.Cancel;
                        }

                        break;
                    case "编辑权限":
                        role.RoleId = RoleId;
                        if (RoleManage.UpdateRole(role))
                        {
                            m_bUpdate = true;
                            DialogResult = DialogResult.OK;
                        }
                        else
                        {
                            MessageBox.Show("修改失败！", "提示");
                            DialogResult = DialogResult.Cancel;
                        }

                        break;
                }
            }
        }

        private void buttonX2_Click(object sender, System.EventArgs e)
        {
            Close();
        }
    }
}
