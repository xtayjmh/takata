﻿using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using DAL;
using DevComponents.DotNetBar;

namespace Takata
{
    public partial class SpeedEdit : Office2007Form
    {
        public SpeedEdit()
        {
            InitializeComponent();
            dataGridView1.AutoGenerateColumns = false;
        }

        private void SpeedEdit_Load(object sender, System.EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“takataDataSet.t_PlaySpeed”中。您可以根据需要移动或删除它。
            this.t_PlaySpeedTableAdapter.Fill(this.takataDataSet.t_PlaySpeed);

        }


        private void dataGridView1_CellEndEdit(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            var v = dataGridView1.Rows[e.RowIndex].Cells[1].Value;
            var id= dataGridView1.Rows[e.RowIndex].Cells[0].Value;
            var sql = $"update t_PlaySpeed set PlaySpeed={v} where PlaySpeedId={id}";
            var r=Common.ExecuteSql(sql);
        }

        private void dataGridView1_RowPostPaint(object sender, System.Windows.Forms.DataGridViewRowPostPaintEventArgs e)
        {
            Rectangle rectangle = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, dataGridView1.RowHeadersWidth - 4, e.RowBounds.Height);
            TextRenderer.DrawText(e.Graphics,
                (e.RowIndex + 1).ToString(),
                dataGridView1.RowHeadersDefaultCellStyle.Font,
                rectangle,
                dataGridView1.RowHeadersDefaultCellStyle.ForeColor,
                TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
        }
    }
}
