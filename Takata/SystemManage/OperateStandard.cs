﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using DevComponents.DotNetBar.Controls;

namespace Takata
{
    public partial class OperateStandard : Office2007Form
    {
        public string OperateType { get; set; }
        public string EquipmentTime { get; set; }
        public string HasWalking { get; set; }
        public string ActionDes { get; set; }//动作叙述
        public OperateStandard()
        {
            InitializeComponent();
            cbOperate.SelectedIndex = 2;
            cbWalking.SelectedIndex = 0;
        }

        public OperateStandard(string operateType, double equipmentTime, string hasWalking, string actionDes)
        {
            InitializeComponent();
            cbOperate.SelectedItem = operateType??"无";
            tbxEquipment.Text = equipmentTime.ToString("f2");
            cbWalking.SelectedItem = hasWalking??"无";
            tbxActionDes.Text = actionDes;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            OperateType = cbOperate.SelectedItem.ToString();
            EquipmentTime = tbxEquipment.Text.Trim();
            HasWalking = cbWalking.SelectedItem.ToString();
            ActionDes = tbxActionDes.Text.Trim();
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
