﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DAL.SystemManage;
using DevComponents.DotNetBar;
using Model;

namespace Takata
{
    public partial class CombinationList : Office2007Form
    {
        private ToolStrip toolStrip1;
        private ToolStripButton tSBtnAdd;
        private ToolStripButton tSBtnEdit;
        private ToolStripButton tSBtnDelete;
        private DataGridView dataGridView1;

        public CombinationList()
        {
            InitializeComponent();
            dataGridView1.AutoGenerateColumns = false;
        }

        private void LoadData()
        {
            dataGridView1.DataSource = CombinationManage.GetCombinations();
            dataGridView1.AutoGenerateColumns = false;
        }

        private void tSBtnAdd_Click(object sender, System.EventArgs e)
        {
            var form = new CombinationEdit();
            if (form.ShowDialog() == DialogResult.OK || form.IsUpdated)
            {
                LoadData();
            }
        }

        private void tSBtnEdit_Click(object sender, System.EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count <= 0)
            {
                MessageBox.Show("请选择要修改的记录", "提示");
                return;
            }
            var selectedRow = dataGridView1.SelectedRows[0];
            var model = new t_ModAPTS
            {
                ModAPTSId = (int)selectedRow.Cells["ModAPTSId"].Value,
                CategoryLevel1 = selectedRow.Cells["CategoryLevel1"].Value.ToString(),
                CategoryLevel2 = selectedRow.Cells["CategoryLevel2"].Value.ToString(),
                Code = selectedRow.Cells["Code"].Value.ToString(),
                TimeVale = (decimal)selectedRow.Cells["TimeVale"].Value
            };
            CombinationEdit edit = new CombinationEdit(model);
            if (edit.ShowDialog() == DialogResult.OK)
            {
                LoadData();
            }
        }

        private void tSBtnDelete_Click(object sender, System.EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count <= 0 || MessageBox.Show("您确定删除所选项吗？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) != DialogResult.OK) return;
            var id = (int)dataGridView1.SelectedRows[0].Cells["ModAPTSId"].Value;
            if (CombinationManage.DeleteCombination(id))
            {
                LoadData();
            }
            else
            {
                MessageBox.Show("删除失败！", "提示");
            }
        }

        private void CombinationList_Load(object sender, System.EventArgs e)
        {
            LoadData();
        }

        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ModAPTSId = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.CategoryLevel1 = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.CategoryLevel2 = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.Code = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.TimeVale = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tSBtnAdd = new System.Windows.Forms.ToolStripButton();
            this.tSBtnEdit = new System.Windows.Forms.ToolStripButton();
            this.tSBtnDelete = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ModAPTSId,
            this.CategoryLevel1,
            this.CategoryLevel2,
            this.Code,
            this.TimeVale});
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.Location = new System.Drawing.Point(0, 25);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1564, 784);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            // 
            // ModAPTSId
            // 
            this.ModAPTSId.DataPropertyName = "ModAPTSId";
            this.ModAPTSId.HeaderText = "ID";
            this.ModAPTSId.Name = "ModAPTSId";
            this.ModAPTSId.ReadOnly = true;
            this.ModAPTSId.Visible = false;
            // 
            // CategoryLevel1
            // 
            this.CategoryLevel1.DataPropertyName = "CategoryLevel1";
            this.CategoryLevel1.HeaderText = "大类别";
            this.CategoryLevel1.Name = "CategoryLevel1";
            this.CategoryLevel1.ReadOnly = true;
            // 
            // CategoryLevel2
            // 
            this.CategoryLevel2.DataPropertyName = "CategoryLevel2";
            this.CategoryLevel2.HeaderText = "小类别";
            this.CategoryLevel2.Name = "CategoryLevel2";
            this.CategoryLevel2.ReadOnly = true;
            // 
            // Code
            // 
            this.Code.DataPropertyName = "Code";
            this.Code.HeaderText = "代码";
            this.Code.Name = "Code";
            this.Code.ReadOnly = true;
            // 
            // TimeVale
            // 
            this.TimeVale.DataPropertyName = "TimeVale";
            this.TimeVale.HeaderText = "时间值";
            this.TimeVale.Name = "TimeVale";
            this.TimeVale.ReadOnly = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tSBtnAdd,
            this.tSBtnEdit,
            this.tSBtnDelete});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(2, 0, 1, 0);
            this.toolStrip1.Size = new System.Drawing.Size(1564, 25);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "添加";
            // 
            // tSBtnAdd
            // 
            this.tSBtnAdd.Image = global::Takata.Properties.Resources.add;
            this.tSBtnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSBtnAdd.Name = "tSBtnAdd";
            this.tSBtnAdd.Size = new System.Drawing.Size(52, 22);
            this.tSBtnAdd.Text = "添加";
            this.tSBtnAdd.Click += new System.EventHandler(this.tSBtnAdd_Click);
            // 
            // tSBtnEdit
            // 
            this.tSBtnEdit.Image = global::Takata.Properties.Resources.modify;
            this.tSBtnEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSBtnEdit.Name = "tSBtnEdit";
            this.tSBtnEdit.Size = new System.Drawing.Size(52, 22);
            this.tSBtnEdit.Text = "修改";
            this.tSBtnEdit.Click += new System.EventHandler(this.tSBtnEdit_Click);
            // 
            // tSBtnDelete
            // 
            this.tSBtnDelete.Image = global::Takata.Properties.Resources.delete;
            this.tSBtnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSBtnDelete.Name = "tSBtnDelete";
            this.tSBtnDelete.Size = new System.Drawing.Size(52, 22);
            this.tSBtnDelete.Text = "删除";
            this.tSBtnDelete.Click += new System.EventHandler(this.tSBtnDelete_Click);
            // 
            // CombinationList
            // 
            this.ClientSize = new System.Drawing.Size(1564, 784);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.dataGridView1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "CombinationList";
            this.Load += new System.EventHandler(this.CombinationList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Rectangle rectangle = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, dataGridView1.RowHeadersWidth - 4, e.RowBounds.Height);
            TextRenderer.DrawText(e.Graphics,
                (e.RowIndex + 1).ToString(),
                dataGridView1.RowHeadersDefaultCellStyle.Font,
                rectangle,
                dataGridView1.RowHeadersDefaultCellStyle.ForeColor,
                TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
        }
    }
}
