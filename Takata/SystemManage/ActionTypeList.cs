﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DAL.SystemManage;
using DevComponents.DotNetBar;
using Model;

namespace Takata
{
    public partial class ActionTypeList : Office2007Form
    {
        public ActionTypeList()
        {
            InitializeComponent();
        }

        private void LoadData()
        {
            dataGridViewX1.DataSource = ActionTypeManage.GetActionTypes();
            dataGridViewX1.AutoGenerateColumns = false;
            for (int i = 0; i < dataGridViewX1.RowCount; i++)
            {
                var v = dataGridViewX1.Rows[i].Cells["Color"].Value.ToString().Split(',');
                dataGridViewX1.Rows[i].Cells["Color"].Value = "类型颜色";
                dataGridViewX1.Rows[i].Cells["Color"].Style.BackColor = System.Drawing.Color.FromArgb(int.Parse(v[0]), int.Parse(v[1]), int.Parse(v[2]), int.Parse(v[3]));
                
            }
        }

        private void tSBtnAdd_Click(object sender, System.EventArgs e)
        {
            var form = new ActionTypeEdit();
            if (form.ShowDialog() == DialogResult.OK || form.IsUpdated)
            {
                LoadData();
            }
        }

        private void tSBtnEdit_Click(object sender, System.EventArgs e)
        {
            if (dataGridViewX1.SelectedRows.Count <= 0) return;
            var selectedRow = dataGridViewX1.SelectedRows[0];
            var sc = selectedRow.Cells["Color"].Style.BackColor;
            var model = new t_ActionType
            {
                ActionTypeId= (int)selectedRow.Cells["ActionTypeId"].Value,
                ChineseName = selectedRow.Cells["ChineseName"].Value.ToString(),
                EnglishName = selectedRow.Cells["EnglishName"].Value.ToString(),
                ShortName = selectedRow.Cells["ShortName"].Value.ToString(),
                Description = selectedRow.Cells["Description"].Value.ToString(),
                Color = $"{sc.A},{sc.R},{sc.G},{sc.B}",
                Rank = (int)selectedRow.Cells["Rank"].Value
            };
            ActionTypeEdit edit = new ActionTypeEdit(model);
            if (edit.ShowDialog() == DialogResult.OK)
            {
                LoadData();
            }
        }

        private void tSBtnDelete_Click(object sender, System.EventArgs e)
        {
            if (dataGridViewX1.SelectedRows.Count <= 0 || MessageBox.Show("您确定删除所选项吗？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) != DialogResult.OK) return;
            var id = (int)dataGridViewX1.SelectedRows[0].Cells["ActionTypeId"].Value;
            if (ActionTypeManage.DeleteActionType(id)) LoadData();
            else
            {
                MessageBox.Show("删除失败！", "提示");
            }
        }

        private void ActionTypeList_Load(object sender, System.EventArgs e)
        {
            LoadData();
        }

        private void dataGridViewX1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Rectangle rectangle = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, dataGridViewX1.RowHeadersWidth - 4, e.RowBounds.Height);
            TextRenderer.DrawText(e.Graphics,
                (e.RowIndex + 1).ToString(),
                dataGridViewX1.RowHeadersDefaultCellStyle.Font,
                rectangle,
                dataGridViewX1.RowHeadersDefaultCellStyle.ForeColor,
                TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
        }

    }
}
