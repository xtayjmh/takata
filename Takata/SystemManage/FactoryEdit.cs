﻿using System.Data.SqlClient;
using System.Threading;
using System.Windows.Forms;
using DAL;
using DAL.SystemManage;
using DevComponents.DotNetBar;

namespace Takata
{
    public partial class FactoryEdit : Office2007Form
    {
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tSBtnDelete;
        private ToolStripButton tSBtnAdd;
        private System.Windows.Forms.DataGridView dataGridView1;
        private delegate void MyInvoke();

        public FactoryEdit()
        {
            InitializeComponent();
            dataGridView1.AutoGenerateColumns = false;
        }

        public void DoWork()

        {

            MyInvoke mi = new MyInvoke(LoadData);

            this.BeginInvoke(mi);

        }

        private void FactoryEdit_Load(object sender, System.EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            dataGridView1.DataSource = FactoryManage.GetFactorys();
            dataGridView1.AutoGenerateColumns = false;
        }

        private void dataGridView1_CellEndEdit(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            var n = (string)dataGridView1.Rows[e.RowIndex].Cells[1].Value;

            var id = (int)dataGridView1.Rows[e.RowIndex].Cells[0].Value;
            Model.t_Factory sa = new Model.t_Factory();
            sa.Name = n;
            if (id == 0)
            {
                FactoryManage.AddFactory(sa);
                Thread thread = new Thread(DoWork);
                thread.Start();
            }
            else
            {
                sa.FactoryId = id;
                FactoryManage.EditFactory(sa);
            }
        }

        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tSBtnAdd = new System.Windows.Forms.ToolStripButton();
            this.tSBtnDelete = new System.Windows.Forms.ToolStripButton();
            this.FactoryId = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.ActionName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FactoryId,
            this.ActionName});
            this.dataGridView1.Location = new System.Drawing.Point(0, 25);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1564, 784);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEndEdit);
            this.dataGridView1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView1_DataError);
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tSBtnAdd,
            this.tSBtnDelete});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(2, 0, 1, 0);
            this.toolStrip1.Size = new System.Drawing.Size(1386, 25);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "添加";
            // 
            // tSBtnAdd
            // 
            this.tSBtnAdd.Image = global::Takata.Properties.Resources.add;
            this.tSBtnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSBtnAdd.Name = "tSBtnAdd";
            this.tSBtnAdd.Size = new System.Drawing.Size(52, 22);
            this.tSBtnAdd.Text = "添加";
            this.tSBtnAdd.Click += new System.EventHandler(this.tSBtnAdd_Click);
            // 
            // tSBtnDelete
            // 
            this.tSBtnDelete.Image = global::Takata.Properties.Resources.delete;
            this.tSBtnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSBtnDelete.Name = "tSBtnDelete";
            this.tSBtnDelete.Size = new System.Drawing.Size(52, 22);
            this.tSBtnDelete.Text = "删除";
            this.tSBtnDelete.Click += new System.EventHandler(this.tSBtnDelete_Click);
            // 
            // FactoryId
            // 
            this.FactoryId.DataPropertyName = "FactoryId";
            this.FactoryId.FillWeight = 50F;
            this.FactoryId.HeaderText = "ID";
            this.FactoryId.Name = "FactoryId";
            this.FactoryId.ReadOnly = true;
            this.FactoryId.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.FactoryId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.FactoryId.Visible = false;
            this.FactoryId.Width = 40;
            // 
            // ActionName
            // 
            this.ActionName.DataPropertyName = "Name";
            this.ActionName.HeaderText = "工厂名称";
            this.ActionName.Name = "ActionName";
            this.ActionName.Width = 340;
            // 
            // FactoryEdit
            // 
            this.ClientSize = new System.Drawing.Size(1386, 784);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.dataGridView1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FactoryEdit";
            this.Load += new System.EventHandler(this.FactoryEdit_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        private void tSBtnDelete_Click(object sender, System.EventArgs e)
        {
            var selectedRows = dataGridView1.SelectedRows;
            if ((selectedRows.Count == 0 && dataGridView1.SelectedCells.Count ==0) || MessageBox.Show("工厂包含的车间以及车间包含的产线数据都会一起被删除", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) != DialogResult.OK) return;
            if (selectedRows.Count != 0)
            {
                foreach (DataGridViewRow selected in selectedRows)
                {
                    var id = (int)selected.Cells[0].Value;
                    FactoryManage.DeleteFactory(id);
                }
            }
            else
            {
                var selectedCells = dataGridView1.SelectedCells;
                if (selectedCells.Count != 0)
                {
                    foreach (DataGridViewCell selected in selectedCells)
                    {
                        var id = (int)selected.OwningRow.Cells[0].Value;
                        FactoryManage.DeleteFactory(id);
                    }
                }
            }
            LoadData();
        }

        private void tSBtnAdd_Click(object sender, System.EventArgs e)
        {
            var FactoryList = FactoryManage.GetFactorys();
            Model.t_Factory sa = new Model.t_Factory();
            sa.Name = "添加新工厂";
            FactoryList.Add(sa);
            dataGridView1.DataSource = FactoryList;
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.ClearSelection();
            dataGridView1.Rows[dataGridView1.RowCount - 1].Selected = true;
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("请输入正确的数据格式"); 
        }
    }
}
