﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DAL;
using DevComponents.DotNetBar;
using Model;

namespace Takata
{
    public partial class ModAPTS : Office2007Form
    {
        public double ManualTime { get; set; }
        public string Code { get; set; }
        public List<t_ModAPTS> _modApts { get; set; }
        private List<string> CodeList=new List<string>();
        private List<double> tempMultiplier { get; set; }
        public ModAPTS()
        {
            InitializeComponent();
            LoadModApts();
        }

        private void btnX_Click(object sender, System.EventArgs e)
        {
            var multiplier = decimal.Parse(tbxMultiplier.Text).ToString("#0.000");
            tbxBDS.Text = tbxBDS.Text + "*" + multiplier;
            CalTime();
        }

        private void btnClean_Click(object sender, System.EventArgs e)
        {
            tbxBDS.Text = "";
            tbxDWMS.Text = "";
            tbxJSZ.Text = "";
            tbxDWZ.Text = "";
            CodeList = new List<string>();
            CalTime();
            btnX.Enabled = false;
        }

        private void btnBack_Click(object sender, System.EventArgs e)
        {
            var text = tbxBDS.Text;
            text = text.Substring(0, (text.LastIndexOf('+') == -1 ? text.LastIndexOf('*') : text.LastIndexOf('+')) == -1 ? 0 :
                text.LastIndexOf('+') == -1 ? text.LastIndexOf('*') : text.LastIndexOf('+'));
            tbxBDS.Text = text;
            CodeList.Remove(CodeList.LastOrDefault());
            CalTime();
            if (string.IsNullOrEmpty(text))
            {
                btnX.Enabled = false;
            }
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void btnOk_Click(object sender, System.EventArgs e)
        {
            ManualTime = double.Parse(tbxJSZ.Text);
            Code = tbxBDS.Text;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void LoadModApts()
        {
            _modApts = Common.GetList<t_ModAPTS>(null);
            var firstType = _modApts.Select(i => i.CategoryLevel1).Distinct().ToList();
            firstType.ForEach(i => { listBox1.Items.Add(i.ToString()); });
        }

        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (listBox1.SelectedItem == null) return;
            var selectedText = listBox1.SelectedItem.ToString();
            var selectedCode = _modApts.Where(i => i.CategoryLevel1 == selectedText).ToList();
            listBox2.Items.Clear();
            listBox5.Items.Clear();

            selectedCode.ForEach(i => { listBox2.Items.Add(i.Code); });
        }

        private void listBox2_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (listBox2.SelectedItem == null) return;
            var selectedCode = listBox2.SelectedItem.ToString();
            listBox5.Items.Clear();
            listBox5.Items.Add(selectedCode);
        }

        private void listBox5_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            btnX.Enabled = true;
            if (listBox5.SelectedItem == null) return;
            var selectedCode= listBox5.SelectedItem.ToString();//最后选取的编码，比如P2
            CodeList.Add(selectedCode);
            tbxDWZ.Text = _modApts.FirstOrDefault(i => i.Code == selectedCode).TimeVale.ToString();
            tbxDWMS.Text = selectedCode;
            var text = tbxBDS.Text.Trim();

            tbxBDS.Text = tbxBDS.Text.Trim() == "" ? selectedCode : text + "+" + selectedCode;
            CalTime();
        }

        private void CalTime()
        {
            decimal sdsj = 0;
            if (string.IsNullOrEmpty(tbxBDS.Text))
            {
                tbxJSZ.Text = "";
            }
            else
            {
                var arr = tbxBDS.Text.Split('+').ToList();
                arr.ForEach(i =>
                {
                    var temSplit = i.Split('*');
                    if (temSplit.Length > 1) //有乘数
                    {
                        var timeval = _modApts.FirstOrDefault(j => j.Code == temSplit[0].ToString()).TimeVale * (int) Math.Round(decimal.Parse(temSplit[1]), 0);
                        sdsj += timeval??0;
                    }
                    else
                    {
                        sdsj += _modApts.FirstOrDefault(j => j.Code == temSplit[0].ToString()).TimeVale??0;
                    }
                });
                
                tbxJSZ.Text = sdsj.ToString("f2");
            }
        }
    }
}
