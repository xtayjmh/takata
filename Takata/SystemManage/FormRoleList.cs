﻿using System.Linq;
using System.Windows.Forms;
using DAL.SystemManage;
using DevComponents.DotNetBar;
using Model;

namespace Takata
{
    public partial class FormRoleList : Office2007Form
    {
        public FormRoleList()
        {
            InitializeComponent();
            dataGridViewX1.AutoGenerateColumns = false;
            LoadRoleList();
        }

        private void tSBtnAdd_Click(object sender, System.EventArgs e)
        {
            FormRoleEdit fre = new FormRoleEdit();
            if(fre.ShowDialog() == DialogResult.OK) LoadRoleList();
        }

        private void tSBtnEdit_Click(object sender, System.EventArgs e)
        {
            var selectedRow = dataGridViewX1.SelectedRows[0];
            var role = new t_Role()
            {
                RoleId = (int)selectedRow.Cells["RoleId"].Value,
                RoleName = selectedRow.Cells["RoleName"].Value.ToString(),
                RoleNote = selectedRow.Cells["RoleNote"].Value.ToString()
            };
            FormRoleEdit fre=new FormRoleEdit(role);
            if (fre.ShowDialog() == DialogResult.OK) LoadRoleList();
        }

        private void tSBtnDelete_Click(object sender, System.EventArgs e)
        {
            var usedRoleIdList = RoleManage.GetUsedRoleIdList();
            var roleId = dataGridViewX1.SelectedRows[0].Cells["RoleId"].Value;
            if (usedRoleIdList.Contains((int)roleId))
            {
                MessageBox.Show("有用户指定了该角色，无法删除！", "提示");
            }
            else
            {
                if (dataGridViewX1.SelectedRows.Count <= 0 || MessageBox.Show("您确定删除所选项吗？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) != DialogResult.OK) return;
                if (RoleManage.DeleteRole((int) roleId)) LoadRoleList();
                else
                {
                    MessageBox.Show("删除失败", "提示");
                }
            }
        }

        private void LoadRoleList()
        {
            var list = RoleManage.GetRoleList();
            dataGridViewX1.DataSource = list;
        }
    }
}
