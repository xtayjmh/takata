﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DAL.SystemManage;
using DevComponents.DotNetBar;
using Model;

namespace Takata
{
    public partial class FormZone : Office2007Form
    {
        public FormZone()
        {
            InitializeComponent();
            LoadZone();
        }

        private void tSBtnAdd_Click(object sender, EventArgs e)
        {
            FormZoneEdit edit = new FormZoneEdit();
            if (edit.ShowDialog() == DialogResult.OK)
            {
                LoadZone();
            }
            else if (edit.m_bUpdate)
            {
                LoadZone();
            }
        }

        private void LoadZone()
        {
            var list = new ZoneManage().GetZoneList();
            dataGridViewX1.AutoGenerateColumns = false;
            dataGridViewX1.DataSource = list;
        }

        private void tSBtnEdit_Click(object sender, EventArgs e)
        {
            if (dataGridViewX1.SelectedRows.Count <= 0) return;
            var selectedRow = dataGridViewX1.SelectedRows[0];

            var ZoneId = (int)selectedRow.Cells["ZoneId"].Value;
            var IsActive = true;
            var ZoneName = selectedRow.Cells["ZoneName"].Value.ToString();
            var FTPAddress = selectedRow.Cells["FTPAddress"].Value.ToString();
            var FTPUserName = selectedRow.Cells["FTPUserName"].Value.ToString();
            var FTPPassword = selectedRow.Cells["FTPPassword"].Value.ToString();
            var FTPPort = (int)selectedRow.Cells["FTPPort"].Value;

            var zone = new t_Zone
            {
                ZoneId = ZoneId,
                IsActive = IsActive,
                ZoneName = ZoneName,
                FTPAddress = FTPAddress,
                FTPUserName = FTPUserName,
                FTPPassword = FTPPassword,
                FTPPort = FTPPort
            };
            FormZoneEdit edit = new FormZoneEdit(zone);
            if (edit.ShowDialog() == DialogResult.OK)
            {
                LoadZone();
            }
        }

        private void tSBtnDelete_Click(object sender, EventArgs e)
        {
            if (dataGridViewX1.SelectedRows.Count <= 0 || MessageBox.Show("您确定删除所选项吗？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) != DialogResult.OK) return;
            var id = (int)dataGridViewX1.SelectedRows[0].Cells["ZoneId"].Value;
            if (new ZoneManage().DeleteZone(id)) LoadZone();
            else
            {
                MessageBox.Show("删除失败！", "提示");
            }
        }
 
    }
}
