﻿namespace Takata
{
    partial class FormUserEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblUserName = new DevComponents.DotNetBar.LabelX();
            this.tbxUserName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tbxAddress = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblAddress = new DevComponents.DotNetBar.LabelX();
            this.lblSex = new DevComponents.DotNetBar.LabelX();
            this.tbxPhone = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblPhone = new DevComponents.DotNetBar.LabelX();
            this.tbxLoginName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblLoginName = new DevComponents.DotNetBar.LabelX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.lblRole = new DevComponents.DotNetBar.LabelX();
            this.rBtnFmale = new System.Windows.Forms.RadioButton();
            this.rBtnMan = new System.Windows.Forms.RadioButton();
            this.comboBoxEx1 = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboBoxEx2 = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.SuspendLayout();
            // 
            // lblUserName
            // 
            // 
            // 
            // 
            this.lblUserName.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblUserName.Location = new System.Drawing.Point(56, 27);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(48, 23);
            this.lblUserName.TabIndex = 0;
            this.lblUserName.Text = "姓名：";
            // 
            // tbxUserName
            // 
            // 
            // 
            // 
            this.tbxUserName.Border.Class = "TextBoxBorder";
            this.tbxUserName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbxUserName.Location = new System.Drawing.Point(115, 27);
            this.tbxUserName.Name = "tbxUserName";
            this.tbxUserName.PreventEnterBeep = true;
            this.tbxUserName.Size = new System.Drawing.Size(150, 21);
            this.tbxUserName.TabIndex = 1;
            // 
            // tbxAddress
            // 
            // 
            // 
            // 
            this.tbxAddress.Border.Class = "TextBoxBorder";
            this.tbxAddress.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbxAddress.Location = new System.Drawing.Point(115, 135);
            this.tbxAddress.Name = "tbxAddress";
            this.tbxAddress.PreventEnterBeep = true;
            this.tbxAddress.Size = new System.Drawing.Size(150, 21);
            this.tbxAddress.TabIndex = 6;
            // 
            // lblAddress
            // 
            // 
            // 
            // 
            this.lblAddress.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAddress.Location = new System.Drawing.Point(56, 135);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(75, 23);
            this.lblAddress.TabIndex = 2;
            this.lblAddress.Text = "住址：";
            // 
            // lblSex
            // 
            // 
            // 
            // 
            this.lblSex.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSex.Location = new System.Drawing.Point(56, 108);
            this.lblSex.Name = "lblSex";
            this.lblSex.Size = new System.Drawing.Size(48, 23);
            this.lblSex.TabIndex = 4;
            this.lblSex.Text = "性别：";
            // 
            // tbxPhone
            // 
            // 
            // 
            // 
            this.tbxPhone.Border.Class = "TextBoxBorder";
            this.tbxPhone.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbxPhone.Location = new System.Drawing.Point(115, 81);
            this.tbxPhone.Name = "tbxPhone";
            this.tbxPhone.PreventEnterBeep = true;
            this.tbxPhone.Size = new System.Drawing.Size(150, 21);
            this.tbxPhone.TabIndex = 3;
            // 
            // lblPhone
            // 
            // 
            // 
            // 
            this.lblPhone.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblPhone.Location = new System.Drawing.Point(32, 81);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(75, 23);
            this.lblPhone.TabIndex = 6;
            this.lblPhone.Text = "联系方式：";
            // 
            // tbxLoginName
            // 
            // 
            // 
            // 
            this.tbxLoginName.Border.Class = "TextBoxBorder";
            this.tbxLoginName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbxLoginName.Location = new System.Drawing.Point(115, 54);
            this.tbxLoginName.Name = "tbxLoginName";
            this.tbxLoginName.PreventEnterBeep = true;
            this.tbxLoginName.Size = new System.Drawing.Size(150, 21);
            this.tbxLoginName.TabIndex = 2;
            // 
            // lblLoginName
            // 
            // 
            // 
            // 
            this.lblLoginName.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblLoginName.Location = new System.Drawing.Point(44, 54);
            this.lblLoginName.Name = "lblLoginName";
            this.lblLoginName.Size = new System.Drawing.Size(60, 23);
            this.lblLoginName.TabIndex = 8;
            this.lblLoginName.Text = "登录名：";
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Location = new System.Drawing.Point(59, 234);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Size = new System.Drawing.Size(75, 23);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.TabIndex = 8;
            this.buttonX1.Text = "确定";
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Location = new System.Drawing.Point(202, 234);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Size = new System.Drawing.Size(75, 23);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.TabIndex = 11;
            this.buttonX2.Text = "取消";
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // lblRole
            // 
            // 
            // 
            // 
            this.lblRole.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblRole.Location = new System.Drawing.Point(56, 164);
            this.lblRole.Name = "lblRole";
            this.lblRole.Size = new System.Drawing.Size(75, 23);
            this.lblRole.TabIndex = 12;
            this.lblRole.Text = "角色：";
            // 
            // rBtnFmale
            // 
            this.rBtnFmale.AutoSize = true;
            this.rBtnFmale.Location = new System.Drawing.Point(156, 112);
            this.rBtnFmale.Name = "rBtnFmale";
            this.rBtnFmale.Size = new System.Drawing.Size(35, 16);
            this.rBtnFmale.TabIndex = 5;
            this.rBtnFmale.TabStop = true;
            this.rBtnFmale.Text = "女";
            this.rBtnFmale.UseVisualStyleBackColor = true;
            // 
            // rBtnMan
            // 
            this.rBtnMan.AutoSize = true;
            this.rBtnMan.Checked = true;
            this.rBtnMan.Location = new System.Drawing.Point(115, 112);
            this.rBtnMan.Name = "rBtnMan";
            this.rBtnMan.Size = new System.Drawing.Size(35, 16);
            this.rBtnMan.TabIndex = 4;
            this.rBtnMan.TabStop = true;
            this.rBtnMan.Text = "男";
            this.rBtnMan.UseVisualStyleBackColor = true;
            // 
            // comboBoxEx1
            // 
            this.comboBoxEx1.DisplayMember = "Text";
            this.comboBoxEx1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.comboBoxEx1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxEx1.FormattingEnabled = true;
            this.comboBoxEx1.ItemHeight = 16;
            this.comboBoxEx1.Location = new System.Drawing.Point(115, 164);
            this.comboBoxEx1.Name = "comboBoxEx1";
            this.comboBoxEx1.Size = new System.Drawing.Size(121, 22);
            this.comboBoxEx1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.comboBoxEx1.TabIndex = 7;
            // 
            // comboBoxEx2
            // 
            this.comboBoxEx2.DisplayMember = "Text";
            this.comboBoxEx2.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.comboBoxEx2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxEx2.FormattingEnabled = true;
            this.comboBoxEx2.ItemHeight = 16;
            this.comboBoxEx2.Location = new System.Drawing.Point(115, 193);
            this.comboBoxEx2.Name = "comboBoxEx2";
            this.comboBoxEx2.Size = new System.Drawing.Size(150, 22);
            this.comboBoxEx2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.comboBoxEx2.TabIndex = 13;
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(34, 193);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 23);
            this.labelX1.TabIndex = 14;
            this.labelX1.Text = "所属区域：";
            // 
            // FormUserEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(337, 290);
            this.Controls.Add(this.comboBoxEx2);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.comboBoxEx1);
            this.Controls.Add(this.rBtnFmale);
            this.Controls.Add(this.rBtnMan);
            this.Controls.Add(this.lblRole);
            this.Controls.Add(this.buttonX2);
            this.Controls.Add(this.buttonX1);
            this.Controls.Add(this.tbxLoginName);
            this.Controls.Add(this.lblLoginName);
            this.Controls.Add(this.tbxPhone);
            this.Controls.Add(this.lblPhone);
            this.Controls.Add(this.lblSex);
            this.Controls.Add(this.tbxAddress);
            this.Controls.Add(this.lblAddress);
            this.Controls.Add(this.tbxUserName);
            this.Controls.Add(this.lblUserName);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormUserEdit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevComponents.DotNetBar.LabelX lblUserName;
        private DevComponents.DotNetBar.Controls.TextBoxX tbxUserName;
        private DevComponents.DotNetBar.Controls.TextBoxX tbxAddress;
        private DevComponents.DotNetBar.LabelX lblAddress;
        private DevComponents.DotNetBar.LabelX lblSex;
        private DevComponents.DotNetBar.Controls.TextBoxX tbxPhone;
        private DevComponents.DotNetBar.LabelX lblPhone;
        private DevComponents.DotNetBar.Controls.TextBoxX tbxLoginName;
        private DevComponents.DotNetBar.LabelX lblLoginName;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.LabelX lblRole;
        private System.Windows.Forms.RadioButton rBtnFmale;
        private System.Windows.Forms.RadioButton rBtnMan;
        private DevComponents.DotNetBar.Controls.ComboBoxEx comboBoxEx1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx comboBoxEx2;
        private DevComponents.DotNetBar.LabelX labelX1;
    }
}