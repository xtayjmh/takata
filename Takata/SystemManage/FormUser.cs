﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DAL.SystemManage;
using DevComponents.DotNetBar;
using Model;

namespace Takata
{
    public partial class FormUser : Office2007Form
    {
        public FormUser()
        {
            InitializeComponent();
            LoadUser();
        }

        private void tSBtnAdd_Click(object sender, EventArgs e)
        {
            FormUserEdit edit = new FormUserEdit();
            if (edit.ShowDialog()==DialogResult.OK)
            {
                LoadUser();
            }
            else if (edit.m_bUpdate)
            {
                LoadUser();
            }
        }

        private void LoadUser()
        {
            var list = new UserManage().GetUserList();
            dataGridViewX1.AutoGenerateColumns = false;
            dataGridViewX1.DataSource = list;
        }

        private void tSBtnEdit_Click(object sender, EventArgs e)
        {
            if (dataGridViewX1.SelectedRows.Count <= 0) return;
            var selectedRow = dataGridViewX1.SelectedRows[0];
            var user = new t_User
            {
                UserId = (int)selectedRow.Cells["UserId"].Value,
                IsActive = 1,
                LoginName = selectedRow.Cells["LoginName"].Value.ToString(),
                Phone = selectedRow.Cells["Phone"].Value.ToString(),
                Sex = selectedRow.Cells["SexStr"].Value.ToString() == "男" ? 1 : 0,
                UserAddress = selectedRow.Cells["UserAddress"].Value.ToString(),
                UserName = selectedRow.Cells["UserName"].Value.ToString(),
                RoleId = (int)selectedRow.Cells["RoleId"].Value,
                RoleName = selectedRow.Cells["RoleName"].Value.ToString(),
                ZoneId =  selectedRow.Cells["ZoneId"].Value==null?0: (int)selectedRow.Cells["ZoneId"].Value,
                ZoneName = selectedRow.Cells["ZoneName"].Value == null ? "":  selectedRow.Cells["ZoneName"].Value.ToString() 
 
            };
            FormUserEdit edit = new FormUserEdit(user);
            if (edit.ShowDialog() == DialogResult.OK)
            {
                LoadUser();
            }
        }

        private void tSBtnDelete_Click(object sender, EventArgs e)
        {
            if (dataGridViewX1.SelectedRows.Count <= 0 || MessageBox.Show("您确定删除所选项吗？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) != DialogResult.OK) return;
            var id = (int) dataGridViewX1.SelectedRows[0].Cells["UserId"].Value;
            if (new UserManage().DeleteUser(id)) LoadUser();
            else
            {
                MessageBox.Show("删除失败！", "提示");
            }
        }
        /// <summary>
        /// 超级管理员角色的用户不允许删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewX1_SelectionChanged(object sender, EventArgs e)
        {
            
            if (dataGridViewX1.SelectedRows.Count <= 0) return;
            if (dataGridViewX1.SelectedRows[0].Cells["RoleId"].Value.ToString() == "1")
            {
                tSBtnDelete.Enabled = false;
            }
            else
            {
                tSBtnDelete.Enabled = true;
            }
        }
    }
}
