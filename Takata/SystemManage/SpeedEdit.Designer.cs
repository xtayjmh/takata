﻿namespace Takata
{
    partial class SpeedEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tPlaySpeedBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.takataDataSet = new Takata.TakataDataSet();
            this.t_PlaySpeedTableAdapter = new Takata.TakataDataSetTableAdapters.t_PlaySpeedTableAdapter();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.playSpeedIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.playSpeedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.tPlaySpeedBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.takataDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // tPlaySpeedBindingSource
            // 
            this.tPlaySpeedBindingSource.DataMember = "t_PlaySpeed";
            this.tPlaySpeedBindingSource.DataSource = this.takataDataSet;
            // 
            // takataDataSet
            // 
            this.takataDataSet.DataSetName = "TakataDataSet";
            this.takataDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // t_PlaySpeedTableAdapter
            // 
            this.t_PlaySpeedTableAdapter.ClearBeforeFill = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.playSpeedIdDataGridViewTextBoxColumn,
            this.playSpeedDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.tPlaySpeedBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(254, 610);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEndEdit);
            this.dataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            // 
            // playSpeedIdDataGridViewTextBoxColumn
            // 
            this.playSpeedIdDataGridViewTextBoxColumn.DataPropertyName = "PlaySpeedId";
            this.playSpeedIdDataGridViewTextBoxColumn.HeaderText = "PlaySpeedId";
            this.playSpeedIdDataGridViewTextBoxColumn.Name = "playSpeedIdDataGridViewTextBoxColumn";
            this.playSpeedIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.playSpeedIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // playSpeedDataGridViewTextBoxColumn
            // 
            this.playSpeedDataGridViewTextBoxColumn.DataPropertyName = "PlaySpeed";
            this.playSpeedDataGridViewTextBoxColumn.HeaderText = "速度";
            this.playSpeedDataGridViewTextBoxColumn.Name = "playSpeedDataGridViewTextBoxColumn";
            // 
            // SpeedEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(284, 585);
            this.Controls.Add(this.dataGridView1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SpeedEdit";
            this.Text = "SpeedEdit";
            this.Load += new System.EventHandler(this.SpeedEdit_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tPlaySpeedBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.takataDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private TakataDataSet takataDataSet;
        private System.Windows.Forms.BindingSource tPlaySpeedBindingSource;
        private TakataDataSetTableAdapters.t_PlaySpeedTableAdapter t_PlaySpeedTableAdapter;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn playSpeedIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn playSpeedDataGridViewTextBoxColumn;
    }
}