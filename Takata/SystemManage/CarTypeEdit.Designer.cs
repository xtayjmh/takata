﻿namespace Takata
{
    partial class CarTypeEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tSBtnAdd = new System.Windows.Forms.ToolStripButton();
            this.tSBtnDelete = new System.Windows.Forms.ToolStripButton();
            this.cbFactroy = new System.Windows.Forms.ToolStripComboBox();
            this.cbWorkShop = new System.Windows.Forms.ToolStripComboBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.cbWorkLine = new System.Windows.Forms.ToolStripComboBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TypeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // tSBtnAdd
            // 
            this.tSBtnAdd.Image = global::Takata.Properties.Resources.add;
            this.tSBtnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSBtnAdd.Name = "tSBtnAdd";
            this.tSBtnAdd.Size = new System.Drawing.Size(52, 22);
            this.tSBtnAdd.Text = "添加";
            this.tSBtnAdd.Click += new System.EventHandler(this.tSBtnAdd_Click);
            // 
            // tSBtnDelete
            // 
            this.tSBtnDelete.Image = global::Takata.Properties.Resources.delete;
            this.tSBtnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSBtnDelete.Name = "tSBtnDelete";
            this.tSBtnDelete.Size = new System.Drawing.Size(52, 22);
            this.tSBtnDelete.Text = "删除";
            this.tSBtnDelete.Click += new System.EventHandler(this.tSBtnDelete_Click);
            // 
            // cbFactroy
            // 
            this.cbFactroy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFactroy.Name = "cbFactroy";
            this.cbFactroy.Size = new System.Drawing.Size(200, 25);
            this.cbFactroy.SelectedIndexChanged += new System.EventHandler(this.cbFactroy_SelectedIndexChanged);
            // 
            // cbWorkShop
            // 
            this.cbWorkShop.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWorkShop.Name = "cbWorkShop";
            this.cbWorkShop.Size = new System.Drawing.Size(200, 25);
            this.cbWorkShop.SelectedIndexChanged += new System.EventHandler(this.cbWorkShop_SelectedIndexChanged);
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tSBtnAdd,
            this.tSBtnDelete,
            this.cbFactroy,
            this.cbWorkShop,
            this.cbWorkLine});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(2, 0, 1, 0);
            this.toolStrip1.Size = new System.Drawing.Size(1386, 25);
            this.toolStrip1.TabIndex = 4;
            this.toolStrip1.Text = "添加";
            // 
            // cbWorkLine
            // 
            this.cbWorkLine.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWorkLine.Name = "cbWorkLine";
            this.cbWorkLine.Size = new System.Drawing.Size(200, 25);
            this.cbWorkLine.SelectedIndexChanged += new System.EventHandler(this.cbWorkLine_SelectedIndexChanged);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.TypeName});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 25);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1386, 759);
            this.dataGridView1.TabIndex = 5;
            this.dataGridView1.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dataGridView1_CellBeginEdit);
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEndEdit);
            this.dataGridView1.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_RowEnter);
            // 
            // ID
            // 
            this.ID.DataPropertyName = "TypeId";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // TypeName
            // 
            this.TypeName.DataPropertyName = "TypeName";
            this.TypeName.HeaderText = "车种名称";
            this.TypeName.Name = "TypeName";
            this.TypeName.Width = 340;
            // 
            // CarTypeEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1386, 784);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.toolStrip1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "CarTypeEdit";
            this.Text = "CarTypeEdit";
            this.Load += new System.EventHandler(this.CarTypeEdit_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripButton tSBtnAdd;
        private System.Windows.Forms.ToolStripButton tSBtnDelete;
        private System.Windows.Forms.ToolStripComboBox cbFactroy;
        private System.Windows.Forms.ToolStripComboBox cbWorkShop;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ToolStripComboBox cbWorkLine;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn TypeName;
    }
}