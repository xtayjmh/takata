﻿namespace Takata
{
    partial class ActionTypeEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ActionTypeEdit));
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.btnOk = new DevComponents.DotNetBar.ButtonX();
            this.tbxChineseName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.tbxEnglishName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tbxShortName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tbxDescription = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.cpBtnColor = new DevComponents.DotNetBar.ColorPickerButton();
            this.cbRank = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(46, 17);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 23);
            this.labelX1.TabIndex = 0;
            this.labelX1.Text = "中文名";
            // 
            // btnOk
            // 
            this.btnOk.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnOk.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnOk.Location = new System.Drawing.Point(36, 205);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnOk.TabIndex = 6;
            this.btnOk.Text = "确定";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // tbxChineseName
            // 
            // 
            // 
            // 
            this.tbxChineseName.Border.Class = "TextBoxBorder";
            this.tbxChineseName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbxChineseName.Location = new System.Drawing.Point(103, 17);
            this.tbxChineseName.Name = "tbxChineseName";
            this.tbxChineseName.PreventEnterBeep = true;
            this.tbxChineseName.Size = new System.Drawing.Size(124, 21);
            this.tbxChineseName.TabIndex = 0;
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Location = new System.Drawing.Point(46, 42);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(75, 23);
            this.labelX2.TabIndex = 2;
            this.labelX2.Text = "英文名";
            // 
            // labelX3
            // 
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Location = new System.Drawing.Point(57, 70);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(75, 23);
            this.labelX3.TabIndex = 3;
            this.labelX3.Text = "简称";
            // 
            // labelX4
            // 
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Location = new System.Drawing.Point(57, 97);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(75, 23);
            this.labelX4.TabIndex = 4;
            this.labelX4.Text = "描述";
            // 
            // labelX5
            // 
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Location = new System.Drawing.Point(57, 140);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(75, 23);
            this.labelX5.TabIndex = 5;
            this.labelX5.Text = "颜色";
            // 
            // labelX6
            // 
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Location = new System.Drawing.Point(57, 168);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(75, 23);
            this.labelX6.TabIndex = 6;
            this.labelX6.Text = "分类";
            // 
            // tbxEnglishName
            // 
            // 
            // 
            // 
            this.tbxEnglishName.Border.Class = "TextBoxBorder";
            this.tbxEnglishName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbxEnglishName.Location = new System.Drawing.Point(103, 44);
            this.tbxEnglishName.Name = "tbxEnglishName";
            this.tbxEnglishName.PreventEnterBeep = true;
            this.tbxEnglishName.Size = new System.Drawing.Size(124, 21);
            this.tbxEnglishName.TabIndex = 1;
            // 
            // tbxShortName
            // 
            // 
            // 
            // 
            this.tbxShortName.Border.Class = "TextBoxBorder";
            this.tbxShortName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbxShortName.Location = new System.Drawing.Point(103, 71);
            this.tbxShortName.Name = "tbxShortName";
            this.tbxShortName.PreventEnterBeep = true;
            this.tbxShortName.Size = new System.Drawing.Size(124, 21);
            this.tbxShortName.TabIndex = 2;
            // 
            // tbxDescription
            // 
            // 
            // 
            // 
            this.tbxDescription.Border.Class = "TextBoxBorder";
            this.tbxDescription.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbxDescription.Location = new System.Drawing.Point(103, 98);
            this.tbxDescription.Multiline = true;
            this.tbxDescription.Name = "tbxDescription";
            this.tbxDescription.PreventEnterBeep = true;
            this.tbxDescription.Size = new System.Drawing.Size(124, 36);
            this.tbxDescription.TabIndex = 3;
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCancel.Location = new System.Drawing.Point(161, 205);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "取消";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // cpBtnColor
            // 
            this.cpBtnColor.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.cpBtnColor.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.cpBtnColor.Image = ((System.Drawing.Image)(resources.GetObject("cpBtnColor.Image")));
            this.cpBtnColor.Location = new System.Drawing.Point(103, 140);
            this.cpBtnColor.Name = "cpBtnColor";
            this.cpBtnColor.SelectedColorImageRectangle = new System.Drawing.Rectangle(2, 2, 12, 12);
            this.cpBtnColor.Size = new System.Drawing.Size(50, 23);
            this.cpBtnColor.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cpBtnColor.TabIndex = 4;
            // 
            // cbRank
            // 
            this.cbRank.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRank.FormattingEnabled = true;
            this.cbRank.Location = new System.Drawing.Point(103, 168);
            this.cbRank.Name = "cbRank";
            this.cbRank.Size = new System.Drawing.Size(121, 20);
            this.cbRank.TabIndex = 5;
            // 
            // ActionTypeEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.cbRank);
            this.Controls.Add(this.cpBtnColor);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.tbxDescription);
            this.Controls.Add(this.tbxShortName);
            this.Controls.Add(this.tbxEnglishName);
            this.Controls.Add(this.labelX6);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.tbxChineseName);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.labelX1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ActionTypeEdit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "动作类型编辑";
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.ButtonX btnOk;
        private DevComponents.DotNetBar.Controls.TextBoxX tbxChineseName;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.Controls.TextBoxX tbxEnglishName;
        private DevComponents.DotNetBar.Controls.TextBoxX tbxShortName;
        private DevComponents.DotNetBar.Controls.TextBoxX tbxDescription;
        private DevComponents.DotNetBar.ButtonX btnCancel;
        private DevComponents.DotNetBar.ColorPickerButton cpBtnColor;
        private System.Windows.Forms.ComboBox cbRank;
    }
}