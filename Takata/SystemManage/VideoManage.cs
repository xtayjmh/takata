﻿using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Threading;
using System.Windows.Forms;
using DAL;
using DAL.SystemManage;
using DevComponents.DotNetBar;
using Model;

namespace Takata
{
    public partial class VideoManage : Office2007Form
    {
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tSBtnDelete;
        private System.Windows.Forms.DataGridView dataGridView1;
        private delegate void MyInvoke();

        public VideoManage()
        {
            InitializeComponent();
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.AllowUserToAddRows = false;
        }

        public void DoWork()

        {

            MyInvoke mi = new MyInvoke(LoadData);

            this.BeginInvoke(mi);

        }

        private void VideoManage_Load(object sender, System.EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            List<t_Video> videoList = Common.GetList<t_Video>();
            if (Program.CurrentUser.RoleId != 1)
            {
                if (Program.CurrentUser.ZoneId == null)
                {
                    MessageBox.Show("该用户没有对应区域，请设置后再查询");
                    return;
                }
                videoList = videoList.Where(v => v.ZoneId == Program.CurrentUser.ZoneId).ToList();
            }
            dataGridView1.DataSource = videoList;
            dataGridView1.AutoGenerateColumns = false;
        }


        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.SingleActionId = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.FileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FrameCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SizeShadow = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Duration = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tSBtnDelete = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SingleActionId,
            this.FileName,
            this.FrameCount,
            this.SizeShadow,
            this.FileSize,
            this.Duration});
            this.dataGridView1.Location = new System.Drawing.Point(0, 25);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1560, 750);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dataGridView1_DataBindingComplete);
            // 
            // SingleActionId
            // 
            this.SingleActionId.DataPropertyName = "VideoId";
            this.SingleActionId.FillWeight = 50F;
            this.SingleActionId.HeaderText = "ID";
            this.SingleActionId.Name = "SingleActionId";
            this.SingleActionId.ReadOnly = true;
            this.SingleActionId.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.SingleActionId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.SingleActionId.Visible = false;
            this.SingleActionId.Width = 40;
            // 
            // FileName
            // 
            this.FileName.DataPropertyName = "FileName";
            this.FileName.HeaderText = "视频名称";
            this.FileName.Name = "FileName";
            this.FileName.ReadOnly = true;
            this.FileName.Width = 240;
            // 
            // FrameCount
            // 
            this.FrameCount.DataPropertyName = "FrameCount";
            this.FrameCount.HeaderText = "帧数";
            this.FrameCount.Name = "FrameCount";
            this.FrameCount.ReadOnly = true;
            // 
            // SizeShadow
            // 
            this.SizeShadow.DataPropertyName = "FileSize";
            this.SizeShadow.HeaderText = "SizeShadow";
            this.SizeShadow.Name = "SizeShadow";
            this.SizeShadow.ReadOnly = true;
            this.SizeShadow.Visible = false;
            // 
            // FileSize
            // 
            this.FileSize.HeaderText = "大小";
            this.FileSize.Name = "FileSize";
            this.FileSize.ReadOnly = true;
            // 
            // Duration
            // 
            this.Duration.DataPropertyName = "Duration";
            this.Duration.HeaderText = "时长";
            this.Duration.Name = "Duration";
            this.Duration.ReadOnly = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tSBtnDelete});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(2, 0, 1, 0);
            this.toolStrip1.Size = new System.Drawing.Size(1564, 25);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "添加";
            // 
            // tSBtnDelete
            // 
            this.tSBtnDelete.Image = global::Takata.Properties.Resources.delete;
            this.tSBtnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSBtnDelete.Name = "tSBtnDelete";
            this.tSBtnDelete.Size = new System.Drawing.Size(52, 22);
            this.tSBtnDelete.Text = "删除";
            this.tSBtnDelete.Click += new System.EventHandler(this.tSBtnDelete_Click);
            // 
            // VideoManage
            // 
            this.ClientSize = new System.Drawing.Size(1564, 900);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.dataGridView1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "VideoManage";
            this.Load += new System.EventHandler(this.VideoManage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        private void tSBtnDelete_Click(object sender, System.EventArgs e)
        {
            MessageBoxButtons messButton = MessageBoxButtons.OKCancel;
            DialogResult dr = MessageBox.Show("确定要删除吗?", "删除录像数据", messButton);
            if (dr == DialogResult.OK)

            {
                var selectedRows = dataGridView1.SelectedRows;
                if (selectedRows.Count != 0)
                {
                    foreach (DataGridViewRow selected in selectedRows)
                    {
                        var id = (int)selected.Cells[0].Value;
                        if (ActionManage.DeleteActionByVideo(id))
                        {
                            ActionManage.DeleteVideo(id);
                        }
                    }
                }
                else
                {
                    var selectedCells = dataGridView1.SelectedCells;
                    if (selectedCells.Count != 0)
                    {
                        foreach (DataGridViewCell selected in selectedCells)
                        {
                            var id = (int)selected.OwningRow.Cells[0].Value;
                            if (ActionManage.DeleteActionByVideo(id))
                            {
                                ActionManage.DeleteVideo(id);
                            }
                        }
                    }
                }
                LoadData();
            }
        }

        private void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            foreach (DataGridViewRow rows in dataGridView1.Rows)
            {
  
                decimal size = 0;
                decimal.TryParse(rows.Cells[3].Value.ToString(), out size);

                if (size < 1024)
                {
                    rows.Cells[4].Value = size + "KB";
                }
                else if (size > 1024)
                {
                    size = size / 1024;
                    size = decimal.Round(size, 2);
                    rows.Cells[4].Value = size + "MB";
                }
                else
                {
                    size = size / 1048576;
                    size = decimal.Round(size, 2);
                    rows.Cells[4].Value = size + "GB";
                }

            }
        }
    }
}
