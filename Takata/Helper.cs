﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Takata
{
    public class Helper
    {
        public static byte[] GetPictureData(string imagepath)
        {
            /**/////根据图片文件的路径使用文件流打开，并保存为byte[] 
            try
            {
                FileStream fs = new FileStream(imagepath, FileMode.Open); //可以是其他重载方法 
                byte[] byData = new byte[fs.Length];
                fs.Read(byData, 0, byData.Length);
                fs.Close();
                return byData;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// 获取系统图片文件夹路径
        /// </summary>
        /// <returns></returns>
        public static string GetFolderPath()
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
            return path;
        }
    }
}
