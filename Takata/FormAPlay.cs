﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using DAL;
using DAL.SystemManage;
using DevComponents.DotNetBar;
using Model;
using Utility;

namespace Takata
{
    public partial class FormAPlay : Office2007Form
    {
        string address;
        ComponentResourceManager resources = new ComponentResourceManager(typeof(FormAPlay));
        private List<t_ActionType> actionTypes = new List<t_ActionType>();
        private bool is_playinig;
        private bool media_is_open; //标记媒体文件是否打开，若未打开则tsbtn_play读ini打开之前的文件，若打开则跳过这步(避免每次都打开文件造成屏幕跳动)
        private decimal Speed = 1;
        private decimal rate = 1;
        private int step = 1;
        private int TotalFrameAmont = 0;
        private bool loopPlay;
        private List<t_PlaySpeed> speedList;
        private string OperateType;
        private string ActionDes; //动作叙述

        private double EquipmentTime;
        private string HasWalking;
        private t_Video videoInfo;
        public t_Video CurrentVideo;
        private int CurrentCycle = 0;
        private t_Action tempAction = new t_Action { ActionTime = 0, StartTime = 0, EndTime = 0 }; //临时动作
        public List<t_Action> TempActions = new List<t_Action>();
        public t_Action actionForUpdate = new t_Action();
        private int upCycle = 0;
        private int downCycle = 0;
        private int startPoint = 0; //开始播放的时间点
        private int endPoint = 10000000; //播放结束的时间节点
        private bool IsBackToStart = false;
        public Color panelBorderColor = Color.Gray;
        public string filePath = "";
        public int VideoID;
        private int StartIndex = 999; //设置循环时选择开始的行，用于高亮显示这些选择的行
        private int EndIndex = 999; //循环结束的行
        public bool isFromSystemIntegrationForm = false; //added by Ted
 
        private static string oldOperationName = "";
        private static int oldCycle = 0;
        private static bool IsAutoPause = false;
        private static t_Action NewAction = null;
        private static int VScrollIndex = 0;

        private static bool isAction;

        private bool UsedPlayed = false;
        private int intFactor = 40;

        public FormAPlay()
        {
            InitializeComponent();
            address = Environment.CurrentDirectory;
            Control.CheckForIllegalCrossThreadCalls = false;
            dataGridView1.AutoGenerateColumns = false;
            tsBtnSave.Enabled = false;
            btnDgbz.Enabled = false;
            btnZHBZ.Enabled = false;
        }

        private void StartTimer()
        {
            timer1.Start();
        }

        private void StopTimer()
        {
            timer1.Stop();
        }

        private void PlayerPlay()
        {
            if (CurrentVideo == null)
                return;
            tsBtnSign.Enabled = true;
            UsedPlayed = true;
            vlcPlayer.Play();
            StartTimer();
            int tempSpeed = (int)(Speed * 100);
            vlcPlayer.SetConfig(104, tempSpeed.ToString()); //设置播放速度
        }

        public void PlayerPause()
        {
            if (vlcPlayer.GetState() == 5)
            {
                StopTimer();
 
                if (intFactor != 0)
                {
                    Console.WriteLine("2. intFactor:" + intFactor);
                    vlcPlayer.SetPosition(vlcPlayer.GetPosition() - intFactor);
                }
                vlcPlayer.Pause();
            }
        }

        private void FormAPlay_Load(object sender, EventArgs e)
        {
            if (!File.Exists(address + "\\Menu.ini"))
            {
                FileStream fs = new FileStream(address + "\\Menu.ini", FileMode.Create, FileAccess.Write);
                fs.Close();
            }
            vlcPlayer.SetConfig(105, "0");
            vlcPlayer.SetConfig(120, "1");
            vlcPlayer.SetConfig(37, "b:\\aaaaa1.jpg");
            vlcPlayer.SetConfig(209, "1");
            vlcPlayer.SetConfig(707, "3");
            vlcPlayer.SetConfig(8, "0");
            tbVideoTime.Text = "00:00:00/00:00:00";
            is_playinig = false;
            media_is_open = false;
            this.Size = new Size(800, 800);
            var list = Common.GetList<t_PlaySpeed>(null);
            list.ForEach(i =>
            {
                tSDDBtnSpeed.DropDownItems.Add(i.PlaySpeed.ToString());
                cbSD.Items.Add(i.PlaySpeed);
            });
            cbSD.SelectedText = "1.00";
            speedList = list;
            actionTypes = Common.GetList<t_ActionType>(null); //获取操作类型
            tsBtnSave.Enabled = false;
            this.FormClosing += FormAPlay_FormClosing;
            lbProcess.SendToBack();
        }
        private void FormAPlay_FormClosing(object sender, FormClosingEventArgs e)
        {
            PlayerPause();
        }

        private void LoadOperationName()
        {
            if (CurrentVideo == null) return;
            var list = ActionManage.GetOperationName(CurrentVideo.VideoId);
            cbCZMC.Items.Clear();
            if (!list.Any()) return;
            list.ForEach(i =>
            {
                if (i != null) cbCZMC.Items.Add(i);
            });
        }

        private void tSBtn_play_ButtonClick(object sender, EventArgs e)
        {
            if (endPoint != 10000000) //如果有结束点
            {
                vlcPlayer.SetPosition(startPoint);
                trackBar1.Value = startPoint;
                PlayerPlay();
            }
            else if (IsBackToStart) //播放结束回到开始点击播放
            {
                PlayHistory();
                IsBackToStart = false;
            }
            else
            {
                PlayerPlay();
                is_playinig = true;
            }
        }

        private void tSBtn_stop_Click(object sender, EventArgs e)
        {
            if (CurrentVideo == null)
                return;
            BacktoStart(true);
        }

        private void BacktoStart(bool isOpened)
        {
            if (vlcPlayer.GetState() == 5)
            {
                vlcPlayer.Pause();
                IsBackToStart = false;
            }
            else
            {
                IsBackToStart = isOpened;
                vlcPlayer.Pause();
            }

            StopTimer();
            is_playinig = false;
            media_is_open = true;
            trackBar1.Value = 0;
            vlcPlayer.SetPosition(0);
            SetSelectionByTime();
            tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(trackBar1.Value), GetTimeString(trackBar1.Maximum));
            if (vlcPlayer.GetState() == 5) vlcPlayer.Pause();
        }
        private void tSBtn_openfile_Click(object sender, EventArgs e)
        {
            OpenFile();

            lbProcess.Visible = false;
            btnCZLX.Enabled = true;

            if (vlcPlayer.GetState() == 5)
            {
                timer1.Interval = 1;
            }
        }


        private bool OpenStop = false;
        /// <summary>
        /// 打开ToolStripMenuItem_Click
        /// 打开文件并将文件目录添加到Menu.ini
        /// 若打开相同文件则不添加(这个有Bug,这样的话按tsBtn_play打开的就不是上一个了，因为打开相同的不添加)
        /// 若记录行数超过3个，则先记录后三个数据，再重新建一个Menu.ini（清除数据）,接着讲记录的后三个数据写入
        /// </summary>
        private void OpenFile()
        {

            var tempFilePath = filePath;
            isFromSystemIntegrationForm = false;
            openFileDialog1.FileName = "";
            OpenStop = false;
            IsBackToStart = false;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                var TempFileName = openFileDialog1.FileName;
                filePath = TempFileName;
                if (TempFileName.ToLower().IndexOf(".mts") > 0)
                {
                    var preFileName = Path.GetFileNameWithoutExtension(TempFileName);
                    var prePath = Path.GetDirectoryName(TempFileName);

                    if (System.Text.Encoding.UTF8.GetBytes(prePath).Length > prePath.Length)
                    {
                        MessageBox.Show("请将mts视频文件放置在英文路径下");
                        return;
                    }

                    var TempFileNameAVI = prePath + "\\" + preFileName + ".avi";

                    if (File.Exists(TempFileNameAVI))
                    {
                        MessageBox.Show("该mts视频文件已经转换");
                        filePath = TempFileNameAVI;
                    }
                    else
                    {
                        MessageBox.Show("转换MTS文件为AVI文件");
                        var newName = DateTime.Now.Ticks.ToString();
                        var afterPath = prePath + "\\" + newName;
                        var afterMTSName = afterPath + ".mts";
                        var afterAVIName = afterPath + ".avi";
                        FileInfo fi = new FileInfo(TempFileName);
                        fi.MoveTo(Path.Combine(afterMTSName));

                        if (SwichFormat(afterMTSName, afterAVIName))
                        {
                            FileInfo nfi = new FileInfo(afterMTSName);
                            nfi.MoveTo(Path.Combine(TempFileName));

                            FileInfo afi = new FileInfo(afterAVIName);
                            afi.MoveTo(Path.Combine(TempFileNameAVI));

                            filePath = TempFileNameAVI;
                        }
                        MessageBox.Show("转换完毕");
                    }
                }
                StreamWriter s = new StreamWriter(address + "\\Menu.ini", true);
                s.WriteLine(filePath);
                s.Flush();
                s.Close();
                string[] text = File.ReadAllLines(address + "\\Menu.ini");
                int row = text.Length; //行
                int rowcount;
                string[] tempdata = { "", "", "" };
                if (row > 3) // 若记录行数超过3个，则先记录后三个数据，再重新建一个Menu.ini（清除数据）,接着讲记录的后三个数据写入
                {
                    StreamReader sr1 = new StreamReader(address + "\\Menu.ini", true);
                    while (sr1.Peek() > -1)
                    {
                        sr1.ReadLine(); //空读，跳过原始的第一个数据，从第二个数据开始读
                        for (rowcount = 0; rowcount < 3; rowcount++)
                        {
                            tempdata[rowcount] = sr1.ReadLine();
                        }
                    }

                    sr1.Close();
                    FileStream fs = new FileStream(address + "\\Menu.ini", FileMode.Create, FileAccess.Write);
                    fs.Close();
                    StreamWriter s1 = new StreamWriter(address + "\\Menu.ini", true);
                    s1.WriteLine(tempdata[0]);
                    s1.WriteLine(tempdata[1]);
                    s1.WriteLine(tempdata[2]);
                    s1.Flush();
                    s1.Close();
                }

                var fileName = Path.GetFileName(filePath);
                var mode = Common.GetSingleModel<t_Video>(i => i.FileName == fileName); //先判断选择的视频是否已经在数据库中存在
                if (mode != null) //如果存在
                {
                    CurrentVideo = mode;
                    VideoID = mode.VideoId; // added by Ted on 20180312 for other forms
                }
                else //如果不存在，需要初始化视频基础数据
                {
                    if (bgWUploadVideo.IsBusy)
                    {
                        MessageBox.Show( "您分析视频太快，前一个视频还在处理中。请稍后打开新视频");
                        return;
                    }
                    var form = new FormInit();
                    form.ShowDialog();
                    if (form.DialogResult == DialogResult.OK)
                    {
                        var formVideo = form.video;
                        var newFilePath = Environment.CurrentDirectory + "\\Resource\\" + formVideo.PartNo + "\\1\\";
                        var FileName = Path.GetFileName(filePath);
                        try
                        {
                            bgWUploadVideo.RunWorkerAsync(new[] { filePath, newFilePath, fileName });
                        }
                        catch  
                        { 
                        }
                        var fileInfo = new FileInfo(filePath);
                        formVideo.FileName = FileName;
                        formVideo.FileSize = (int)(fileInfo.Length / 1024.0);
                        formVideo.IsUploaded = false; // added by Ted on 20180316 for upload
                        formVideo.CreatedBy = Program.CurrentUser.UserId;
                        formVideo.Created = DateTime.Now;
                        formVideo.Updated = DateTime.Now;
                        CurrentVideo = Common.AddNewObjReturnObj(form.video); //获取初始化的视频基础数据之后，保存视频到数据库
                        VideoID = CurrentVideo.VideoId; // added by Ted on 20180312 for other forms
                    }
                    else
                    {
                        if (tempFilePath != "")
                            filePath = tempFilePath;
                        return;
                    }
                }
                Speed = 1;
                tSDDBtnSpeed.Text = "1.00";
                vlcPlayer.SetConfig(104, (Speed * 100).ToString()); //设置播放速度
                vlcPlayer.Open(filePath);
                lblFileName.Text = filePath;
                StopTimer();
                is_playinig = true;
                media_is_open = true;
                Thread.Sleep(200);
                //StartTimer();
                startPoint = 0; //开始播放的时间点
                endPoint = 10000000; //播放结束的时间节点
                ShowGrid();
                NewAction = null;
                IsAutoPause = false;
                VScrollIndex = 0;

            }

        }

        public bool SwichFormat(string OriginFile, string TargetFile)
        {
            if (File.Exists(TargetFile))
            {
                try
                {
                    File.Delete(TargetFile);
                }
                catch { return false; }
            }
            string strCmd = " -i " + OriginFile + " -q:v 10 -g 1 -vf scale=856:480 " + TargetFile;
     
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            p.StartInfo.FileName = "ffmpeg.exe";//要执行的程序名称  
            p.StartInfo.Arguments = " " + strCmd;
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardInput = false;//可能接受来自调用程序的输入信息  
            p.StartInfo.RedirectStandardOutput = false;//由调用程序获取输出信息   
            p.StartInfo.RedirectStandardError = false;//重定向标准错误输出
            p.StartInfo.CreateNoWindow = false;//不显示程序窗口   

            p.Start();//启动程序   


            p.WaitForExit();//等待程序执行完退出进程

            return true;
        }

        private void PlayHistory()
        {
            StreamWriter s = new StreamWriter(address + "\\Menu.ini", true);
            s.WriteLine(openFileDialog1.FileName);
            s.Flush();
            s.Close();
            string[] text = File.ReadAllLines(address + "\\Menu.ini");
            int row = text.Length; //行
            int rowcount;
            string[] tempdata = new string[] { "", "", "" };
            if (row > 3) // 若记录行数超过3个，则先记录后三个数据，再重新建一个Menu.ini（清除数据）,接着讲记录的后三个数据写入
            {
                StreamReader sr1 = new StreamReader(address + "\\Menu.ini", true);
                while (sr1.Peek() > -1)
                {
                    sr1.ReadLine(); //空读，跳过原始的第一个数据，从第二个数据开始读
                    for (rowcount = 0; rowcount < 3; rowcount++)
                    {
                        tempdata[rowcount] = sr1.ReadLine();
                    }
                }

                sr1.Close();
                FileStream fs = new FileStream(address + "\\Menu.ini", FileMode.Create, FileAccess.Write);
                fs.Close();
                StreamWriter s1 = new StreamWriter(address + "\\Menu.ini", true);
                s1.WriteLine(tempdata[0]);
                s1.WriteLine(tempdata[1]);
                s1.WriteLine(tempdata[2]);
                s1.Flush();
                s1.Close();
            }
            vlcPlayer.Open(openFileDialog1.FileName);
            trackBar1.SetRange(0, (int)vlcPlayer.GetDuration());
            trackBar1.Value = 0;
            StartTimer();

            is_playinig = true;
            media_is_open = true;
            startPoint = 0; //开始播放的时间点
            endPoint = 10000000; //播放结束的时间节点
            //            SetVideoInfo(openFileDialog1.FileName);
            LoadOperationName();
        }

        private void tSB_backward_Click(object sender, EventArgs e)
        {
            if (CurrentVideo == null)
                return;
            tsBtnSign.Enabled = true;
            vlcPlayer.Pause();
            int time = (int)vlcPlayer.GetPosition() - 5000;
            if (time > 0)
            {
                vlcPlayer.SetPosition(time);
            }
            else
            {
                vlcPlayer.SetPosition(0);
            }

            vlcPlayer.Play();
            trackBar1.Value = (int)vlcPlayer.GetPosition();
            StartTimer();
        }

        private void tSB_forward_Click(object sender, EventArgs e)
        {
            if (CurrentVideo == null)
                return;
            tsBtnSign.Enabled = true;
            vlcPlayer.Pause();
            int time = (int)vlcPlayer.GetPosition() + 5000;
            if (time < trackBar1.Maximum)
            {
                vlcPlayer.SetPosition(time);
                trackBar1.Value = (int)vlcPlayer.GetPosition();
                vlcPlayer.Play();
                StartTimer();
            }
            else
            {
                StopTimer();
                var duration = vlcPlayer.GetDuration();
                vlcPlayer.SetPosition(duration - intFactor);
                trackBar1.Value = trackBar1.Maximum;
                SetSelectionByTime();
            }

        }
         
        private void timer1_Tick(object sender, EventArgs e)
        {

            if (OpenStop)
            {
                StopTimer();
                OpenStop = false;
            }
            int currentTime = vlcPlayer.GetPosition();

            int duration = vlcPlayer.GetDuration();

            int endTimeM = (int)(tempAction.EndTime);

            if ((trackBar1.Value >= trackBar1.Maximum || currentTime >= (double)duration)&& currentTime!=0)
            {
                IsBackToStart = true;
                StopTimer();

                tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(duration), GetTimeString(duration));
                trackBar1.Value = trackBar1.Maximum;
            }
            else
            {
                if (endPoint != 10000000 && currentTime >= (double)endPoint)
                {
                    trackBar1.Value = startPoint;
                    vlcPlayer.SetPosition(startPoint);
                }
                else
                {
                    tsBtnSign.Enabled = true;
                    if (currentTime < 0) currentTime = 0;
                    trackBar1.Value = (int)currentTime;
                    if (vlcPlayer.GetState() == 5)
                        tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(currentTime), GetTimeString(duration));
                    SetSelectionByTime(currentTime); //根据播放时间选择右半部分对应的标记
                }
            }

            if (IsAutoPause && tempAction != null && currentTime >= endTimeM && !isAction)
            {
                IsAutoPause = false;
                return;
            }


        }

        private string GetTimeString(int val)
        {
            var currentMS = val;
            val = val / 1000;
            var hour = val / 3600;
            val %= 3600;
            var minute = val / 60;
            var second = val % 60;

            int frameInterval = 40;
            int.TryParse(vlcPlayer.GetConfig(117), out frameInterval);

            var allTime = vlcPlayer.GetDuration();
            if(frameInterval==0)
                frameInterval = 40;
            var frame = (int)(currentMS % 1000 / frameInterval); //帧
            return $"{hour:00}:{minute:00}:{(int)second:00}({(int)frame})";
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            if (vlcPlayer.GetState() == 0)
                return;
            if (CurrentVideo == null)
                return;
            if (vlcPlayer.GetState() == 5)
            {
                PlayerPause();
            }
            var duration = vlcPlayer.GetDuration();
            vlcPlayer.SetPosition(trackBar1.Value >= duration ? duration - 180 : trackBar1.Value);
            trackBar1.Value = (int)vlcPlayer.GetPosition();
            SetSelectionByTime();
        }

        private void toolStripDropDownButton1_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            int frameInterval = 40;
            int.TryParse(vlcPlayer.GetConfig(117), out frameInterval);
            int factor = frameInterval / 20;  //40是因为数据库中速度的分为40等分
            tSDDBtnSpeed.Text = e.ClickedItem.Text;
            Speed = decimal.Parse(e.ClickedItem.Text);
            intFactor = (2 * frameInterval) - ((int)(Speed * 20) - 1) * factor;
            PlayerPause();
        }
        private void tSDDBtnStep_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e) //设置步进帧数
        {
            var stepNum = e.ClickedItem.Text;
            tSDDBtnStep.Text = stepNum;
            step = int.Parse(stepNum);
        }

        private void toolStripButton1_MouseDown(object sender, EventArgs e)
        {
            if (CurrentVideo == null)
                return;
            if (vlcPlayer.GetState() == 5)
            {
                StopTimer();
                vlcPlayer.Pause();
                is_playinig = false;
            }
        }
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (CurrentVideo == null)
                return;
            tsBtnSign.Enabled = true;
            var playingTime = 0;

            if (vlcPlayer.GetConfig(111) == "1")
            {
                vlcPlayer.SetConfig(113, "-" + step.ToString());
            }
            else
            {
                int frameInterval = 40;
                int.TryParse(vlcPlayer.GetConfig(117), out frameInterval);
                playingTime = vlcPlayer.GetPosition();// vlcPlayer.GetPlayTime();
                playingTime = playingTime - step * frameInterval; //updated by Ted
                if (playingTime > trackBar1.Minimum)
                {
                    vlcPlayer.SetPosition(playingTime);
                }
                else
                {
                    vlcPlayer.SetPosition(trackBar1.Minimum);
                }
            }
            playingTime = vlcPlayer.GetPosition();
            if (playingTime > trackBar1.Minimum)
            {
                trackBar1.Value = playingTime;
                tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(playingTime), GetTimeString(vlcPlayer.GetDuration()));
            }
            else
            {
                trackBar1.Value = trackBar1.Minimum;
                tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(playingTime), GetTimeString(vlcPlayer.GetDuration()));
            }

            if (vlcPlayer.GetState() == 5) vlcPlayer.Pause();
            is_playinig = false;
            SetSelectionByTime();
        }

        private void toolStripButton2_MouseDown(object sender, EventArgs e)
        {
            if (CurrentVideo == null)
                return;
            if (vlcPlayer.GetState() == 5)
            {
                StopTimer();
                vlcPlayer.Pause();
                is_playinig = false;
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (CurrentVideo == null)
                return;
            tsBtnSign.Enabled = true;
            var playingTime = 0;

            if (vlcPlayer.GetConfig(109) == "1")
            {
                vlcPlayer.SetConfig(113, step.ToString());
            }

            else
            {
                int frameInterval = 40;
                int.TryParse(vlcPlayer.GetConfig(117), out frameInterval);
                playingTime = vlcPlayer.GetPosition();// vlcPlayer.GetPlayTime();
                playingTime = playingTime + step * frameInterval; //updated by Ted
                if (playingTime < vlcPlayer.GetDuration() - 200)
                {
                    vlcPlayer.SetPosition(playingTime);
                }
                else
                {
                    vlcPlayer.SetPosition(vlcPlayer.GetDuration() - 100);
                }
            }
            playingTime = vlcPlayer.GetPosition();

            if (playingTime < vlcPlayer.GetDuration() - 200)
            {
                trackBar1.Value = playingTime;
                tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(playingTime), GetTimeString(vlcPlayer.GetDuration()));
            }
            else
            {
                trackBar1.Value = trackBar1.Maximum;
                tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(playingTime), GetTimeString(vlcPlayer.GetDuration()));
            }

            if (vlcPlayer.GetState() == 5) vlcPlayer.Pause();
            is_playinig = false;
            SetSelectionByTime();
        }

        private void btnDgbz_Click(object sender, EventArgs e)
        {
            var form = new SingleAction();
            form.ShowDialog();
            if (form.DialogResult == DialogResult.OK)
            {
                tbxDM.Text = "";
                tbxSDSJ.Text = form.Time;
            }
        }

        private void btnZHBZ_Click(object sender, EventArgs e)
        {
            ModAPTS form = new ModAPTS();
            form.ShowDialog();
            if (form.DialogResult == DialogResult.OK)
            {
                tbxSDSJ.Text = form.ManualTime.ToString();
                tbxDM.Text = form.Code;
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if (CurrentVideo == null)
                return;
            tsBtnSign.Enabled = true;
            trackBar1.Value = trackBar1.Maximum;
            var duration = vlcPlayer.GetDuration() - 180;
            vlcPlayer.SetPosition(duration);
            SetSelectionByTime();
            vlcPlayer.Play();
            tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(trackBar1.Value), GetTimeString(trackBar1.Maximum));
        }

        private void tsBtnDelete_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count <= 0 || MessageBox.Show("您确定删除该动作标记吗？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) != DialogResult.OK) return;
            var selectedRows = dataGridView1.SelectedRows;
            if (selectedRows.Count != 0)
            {
                foreach (DataGridViewRow selected in selectedRows)
                {
                    var actionId = (int)selected.Cells[0].Value;
                    if (selected.Index > 0)
                    {
                        NewAction = dataGridView1.Rows[selected.Index - 1].DataBoundItem as t_Action;
                        actionForUpdate = (t_Action)dataGridView1.Rows[selected.Index - 1].DataBoundItem;
                        tempAction = actionForUpdate;
                        ActionManage.DeleteAction(actionId);
                    }
                    else//删除第一条标记，时间向下合并
                    {
                        if (dataGridView1.Rows.Count > 1)
                        {
                            NewAction = dataGridView1.Rows[selected.Index + 1].DataBoundItem as t_Action;
                            actionForUpdate = (t_Action)dataGridView1.Rows[selected.Index + 1].DataBoundItem;
                            tempAction = actionForUpdate;
                        }
                        ActionManage.DeleteFirstAction(actionId);
                        vlcPlayer.SetPosition(0);
                        trackBar1.Value = 0;
                    }
                }
            }
            CleanActionInfo();
            LoadActions(CurrentVideo.VideoId);
            ReSelectRow();

            SetActionInfo(NewAction);
            tsBtnSave.Enabled = false;
        }

        private void CleanActionInfo()
        {
            tbxSDSJ.Text = "";
            tbxBJMS.Text = "";
            tbxAQZYSX.Text = "";
            tbxPZZYSX.Text = "";
            tbxDZMC.Text = "";
            cbCZMC.Text = "";
            cbLX.Text = "";
            cbSD.Text = "";
            cbZQ.Text = "";
            tbxDM.Text = "";
            //修复新建标记不做操作直接保存，操作类型这里保存的是上一条的数据
            HasWalking = "";
            ActionDes = "";
            OperateType = "";
            EquipmentTime = 0.00;
        }


        private void tsBtnSave_Click(object sender, EventArgs e)
        {
            dataGridView1.EndEdit();
            isAction = true;
            if (actionForUpdate == null) return;
            if (actionForUpdate.ActionTime == null) return; //added by Ted on 20180601
            var bjms = tbxBJMS.Text.Trim(); //标记描述
            var aqzysx = tbxAQZYSX.Text.Trim(); //安全注意事项
            var pzzysx = tbxPZZYSX.Text.Trim(); //品质注意事项
            var dzmc = tbxDZMC.Text.Trim(); //动作名称
            var dm = tbxDM.Text.Trim(); //代码
            decimal sdsj = string.IsNullOrEmpty(tbxSDSJ.Text.Trim()) ? 0 : decimal.Parse(tbxSDSJ.Text); //手动时间
            var czmc = cbCZMC.Text; //操作名称
            var lx = cbLX.Text.Trim(); //类型
            var sd = cbSD.Text; //速度
            var zq = cbZQ.Text; //周期
            if (zq == "向上合并")
            {
                zq = upCycle.ToString();
            }
            else if (zq == "新周期")
            {
                zq = ((actionForUpdate.Cycle ?? 0) + 1).ToString();
            }
            else if (zq == "向下合并")
            {
                zq = downCycle.ToString();
            }

            var dfl = clbDFL.SelectedItem?.ToString();

            var mode = new t_Action
            {
                ActionCode = dm,
                ActionName = dzmc,
                ActionTime = actionForUpdate.ActionTime,
                Description = bjms,
                QualityCaution = pzzysx,
                SafetyCaution = aqzysx,
                OperationName = string.IsNullOrEmpty(czmc) ? null : czmc,
                Speed = decimal.Parse(sd==""?"1": sd),
                Cycle = int.Parse(zq == "" ? "0" : zq),
                Created = DateTime.Now,
                VideoId = actionForUpdate.VideoId,
                CreatedBy = Program.CurrentUser.UserId,
                OperationType = OperateType,
                EquipmentTime = (decimal)EquipmentTime,
                Walking = HasWalking,
                TypeId = actionForUpdate.TypeId,
                ActionTypeId = actionTypes.FirstOrDefault(i => i.ChineseName == lx)?.ActionTypeId,
                ActionId = actionForUpdate.ActionId,
                StartTime = actionForUpdate.StartTime,
                EndTime = actionForUpdate.EndTime,  //updated by Ted
                ActionDes = ActionDes
            };
            if (dfl != null)
            {
                mode.TypeId = Common.GetSingleModel<t_Type>(i => i.TypeName == dfl).TypeId;
            }

            if (DDBtnStandardTime.Text == "手动标准时间")
            {
                if (sdsj != 0.00m || mode.TypeId != 2)
                {
                    if (sdsj != 0.00m)
                    {
                        mode.Speed = decimal.Parse((actionForUpdate.ActionTime / sdsj / 1000).ToString());
                    }

                    mode.IsManual = true;
                    mode.ManualStandardTime = sdsj * 1000;
                    mode.AutoStandardTime = sdsj * 1000;
                }
                else
                {
                    MessageBox.Show("手动时间不能为0", "提示");
                    return;
                }
            }
            else
            {
                mode.AutoStandardTime = (int)((((decimal)(actionForUpdate.ActionTime / 1000)) / decimal.Parse(sd)) * 1000);
                mode.ManualStandardTime = mode.AutoStandardTime;
                mode.ActionCode = "";
            }

            ActionManage.AddOrUpdate(mode);
            LoadActions(CurrentVideo.VideoId);

            tsBtnSave.Enabled = false;
            ReSelectRow();

            var rowIndex = TempActions.FindIndex(i => i.ActionId == mode.ActionId);
            if (rowIndex != -1)
            {
                ReSelectNextRow(dataGridView1.Rows[rowIndex + 1]);
            }
            if (!cbCZMC.Items.Contains(czmc)) cbCZMC.Items.Add(czmc);
            IsAutoPause = false;
        }

        private List<int> GetCycle()
        {
            var listCycle = new List<int>();
            if (dataGridView1.Rows.Count == 0)
            {
                listCycle.Add(0);
            }

            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                listCycle.Add((int)(row.Cells[3].Value ?? 0));
            }

            return listCycle;
        }

        private void tsBtnSign_Click(object sender, EventArgs e)
        {
            isAction = true;
            PlayerPause();
            tsBtnSign.Enabled = false;
            IsBackToStart = false;
            if (vlcPlayer.GetState() == 5)
            {
                PlayerPause();
                is_playinig = false;
            }

            tsBtnSave.Enabled = true;
            var lastEndTime = 0.0000m;
            if (dataGridView1.RowCount > 0 && dataGridView1.Rows[dataGridView1.RowCount - 1].Cells["EndTime"].Value != null)
            {
                lastEndTime = (decimal)dataGridView1.Rows[dataGridView1.RowCount - 1].Cells["EndTime"].Value;
            }

            var playTime = vlcPlayer.GetPosition();  //updated by Ted
            t_Action mode;
            var isSplit = false;
            //如果当前播放时间大于最后一条的结束时间，则新增一个动作，否则就拆分当前动作
            if (playTime > lastEndTime)
            {
                if (dataGridView1.RowCount == 0 && tempAction == null) tempAction = TempActions.Count > 0 ? TempActions.Last() : new t_Action() { StartTime = 0, EndTime = 0 };

                //新增一个动作记录
                mode = new t_Action
                {
                    ActionTime = playTime - tempAction.EndTime, //当前播放时间减去上个动作的
                    StartTime = tempAction.EndTime,
                    EndTime = playTime,
                    VideoId = CurrentVideo.VideoId,
                    Speed = 1.00m,
                    ManualStandardTime = playTime - tempAction.EndTime,
                    AutoStandardTime = playTime - tempAction.EndTime,
                    TypeId = 2,
                    ActionTypeId = 1,
                    Cycle = GetCycle().Max()
                };
            }
            else
            {
                isSplit = true;
                //拆分一个动作
                //被拆分动作的结束时间=视频当前的播放时间
                //新动作的开始时间=视频当前播放时间，新动作结束时间=被拆分动作的结束时间
                //TempActions = Common.GetList<t_Action>(i => i.VideoId == CurrentVideo.VideoId);//重新获取视频的动作数据，虽然有点耗时，但是这个变量的数据会变的不正确，暂时找不到变错的地方，这是一个不完美的解决方案。2018-5-5，Johnon,todo
                var tAction = TempActions.FirstOrDefault(i => playTime > i.StartTime && playTime < i.EndTime);
                if (tAction != null)
                {
                    tempAction = tAction;
                }
                var standardTime = tempAction.EndTime - playTime;

                mode = new t_Action
                {
                    ActionTime = tempAction.EndTime - playTime, //当前播放时间减去上个动作的
                    StartTime = playTime,
                    EndTime = tempAction.EndTime,
                    VideoId = CurrentVideo.VideoId,
                    Speed = 1.00m,
                    ManualStandardTime = standardTime,
                    AutoStandardTime = standardTime,
                    TypeId = 2,
                    ActionTypeId = 1,
                    Cycle = tempAction.Cycle
                };
                tempAction.EndTime = playTime;
                tempAction.AutoStandardTime = (decimal)Math.Round((double)((playTime - tempAction.StartTime) / (tempAction.Speed == null ? 1.00m : tempAction.Speed)), 2);
                tempAction.ManualStandardTime = (decimal)Math.Round((double)((playTime - tempAction.StartTime) / (tempAction.Speed == null ? 1.00m : tempAction.Speed)), 2);
                tempAction.ActionTime = playTime - tempAction.StartTime;
            }
            if (mode.ActionTime < 0 || tempAction.ActionTime < 0)
            {
                return;//不允许插入负值的时间
            }

            tempAction = isSplit ? ActionManage.SplitAction(tempAction, mode) : ActionManage.AddNewAction(mode, (decimal)vlcPlayer.GetDuration()); //如果是拆分动作，需要同时更新被拆分动作，新增动作
            NewAction = tempAction;//临时存储新拆分或者新增的标记
            actionForUpdate = tempAction;
            CleanActionInfo();
            if (dataGridView1.RowCount > 0) dataGridView1.Rows[dataGridView1.RowCount - 1].Selected = true;
            SetActionInfo(tempAction);
            if (vlcPlayer.GetState() == 5) vlcPlayer.Pause();
            LoadActions(CurrentVideo.VideoId);
            ReSelectRow();
        }

        private void checkedListBox1_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            for (int i = 0; i < clbDFL.CheckedIndices.Count; i++)
            {
                if (clbDFL.CheckedIndices[i] != e.Index)
                {
                    clbDFL.SetItemChecked(clbDFL.CheckedIndices[i], false);
                }
            }
        }

        private void ReSelectRow()
        {
            if (NewAction == null) return;
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if ((int)row.Cells[0].Value != NewAction.ActionId) continue;
                dataGridView1.ClearSelection();
                if (row.Index == dataGridView1.RowCount - 1)
                {
                    if (row.Index - 1 >= 0)
                    {
                        dataGridView1.Rows[row.Index - 1].Selected = true;
                        dataGridView1.CurrentCell = dataGridView1.Rows[row.Index - 1].Cells[1];
                        actionForUpdate = (t_Action)dataGridView1.Rows[row.Index - 1].DataBoundItem;
                        tempAction = actionForUpdate;
                        SetActionInfo(tempAction);
                    }
                }
                else
                {
                    row.Selected = true;
                    dataGridView1.CurrentCell = row.Cells[1];
                    SetActionInfo(row.DataBoundItem as t_Action);
                }
                if (row.Index - VScrollIndex >= 14)
                {
                    VScrollIndex = row.Index - 13;
                }

                if (VScrollIndex < dataGridView1.RowCount)//解决指定的参数已超出有效值的范围2018-5-31
                {
                    dataGridView1.FirstDisplayedScrollingRowIndex = VScrollIndex;
                }

                break;
            }
        }

        private void ReSelectNextRow(DataGridViewRow row)
        {
            var upEndTime = tempAction.EndTime;
            row.Selected = true;
            var id = (int)row.Cells[0].Value;
            actionForUpdate = TempActions.FirstOrDefault(i => i.ActionId == id); //加载动作数据到窗体内对应的控件
            NewAction = actionForUpdate;
            btnSetLoopStart.Enabled = true;
            tempAction = actionForUpdate;
            if (actionForUpdate != null)
            {
                SetActionInfo(actionForUpdate);
                SetPlayProcess(upEndTime == actionForUpdate.StartTime && UsedPlayed == true);
                UsedPlayed = false;
            }

            if (row.Index - VScrollIndex >= 14)
            {
                VScrollIndex = row.Index - 13;
            }

            if (VScrollIndex < dataGridView1.RowCount)//解决指定的参数已超出有效值的范围2018-5-31
            {
                dataGridView1.FirstDisplayedScrollingRowIndex = VScrollIndex;
            }
        }

        //加载动作标记
        public void LoadActions(int videoId)
        {
            var list = ActionManage.GetActions(videoId).OrderBy(i => i.StartTime).ToList();
            if (!list.Any())
            {
                tempAction = new t_Action() { StartTime = 0, EndTime = 0 };
                return;
            }

            dataGridView1.DataSource = list;
            tempAction = list.LastOrDefault() ?? new t_Action { ActionTime = 0, StartTime = 0, EndTime = 0 }; //加载视频包含已经分析完成的动作
            TempActions = list;
            //1无效2有效3无关
            for (var i = 0; i < dataGridView1.Rows.Count; i++)
            {
                var row = dataGridView1.Rows[i];
                switch ((int)row.Cells["TypeId"].Value)
                {
                    case 1:
                        row.Cells[8].Style.BackColor = Color.Red;
                        row.Cells[8].Value = "×";
                        break;
                    case 2:
                        row.Cells[8].Style.BackColor = Color.GreenYellow;
                        row.Cells[8].Value = "○";
                        break;
                    case 3:
                        row.Cells[8].Style.BackColor = Color.Yellow;
                        row.Cells[8].Value = "△";
                        break;
                }

                if (row.Cells["ActionTypeId"].Value != null && !string.IsNullOrEmpty(row.Cells["ActionTypeId"].Value.ToString()))
                {
                    var actionType = actionTypes.FirstOrDefault(j => j.ActionTypeId == (int)row.Cells["ActionTypeId"].Value);
                    if (actionType != null)
                    {
                        var color = actionType.Color.Split(',');
                        row.Cells[7].Value = actionType.ChineseName;
                        row.Cells[7].Style.BackColor = Color.FromArgb(int.Parse(color[0]), int.Parse(color[1]), int.Parse(color[2]), int.Parse(color[3]));
                    }
                }

                if (new[] { "1", "3" }.Contains(row.Cells["TypeId"].Value.ToString())) row.Cells[5].Value = decimal.Parse("0.00"); //无关录像ST为0
            }

            if (dataGridView1.RowCount > 0)
            {
                var dgr = dataGridView1.Rows[dataGridView1.RowCount - 1];
                dgr.Cells[4].Value = null;
                dgr.Cells[5].Value = null;
                dgr.Cells[6].Value = 1.00m;
                dgr.Cells[7].Value = null;
                dgr.Cells[7].Style.BackColor = Color.White;
                dgr.Cells[8].Value = null;
                dgr.Cells[8].Style.BackColor = Color.White;
                dgr.Cells[9].Value = null;
            }
        }

        public void LoadActionsFromSystemIntegration(int videoId)
        {
            var list = ActionManage.GetActions(videoId).OrderBy(i => i.StartTime).ToList();
            if (!list.Any()) { list.Add(new t_Action { StartTime = 0, EndTime = vlcPlayer.GetDuration() }); }
            dataGridView1.DataSource = list;
            tempAction = list.LastOrDefault() ?? new t_Action { ActionTime = 0, StartTime = 0, EndTime = 0 }; //加载视频包含已经分析完成的动作
            TempActions = list;
            //1无效2有效3无关
            for (var i = 0; i < dataGridView1.Rows.Count; i++)
            {
                var row = dataGridView1.Rows[i];
                if (row.Cells["TypeId"].Value != null)
                {
                    switch ((int)row.Cells["TypeId"].Value)
                    {
                        case 1:
                            row.Cells[8].Style.BackColor = Color.Red;
                            row.Cells[8].Value = "×";
                            break;
                        case 2:
                            row.Cells[8].Style.BackColor = Color.GreenYellow;
                            row.Cells[8].Value = "○";
                            break;
                        case 3:
                            row.Cells[8].Style.BackColor = Color.Yellow;
                            row.Cells[8].Value = "△";
                            break;
                    }
                }

                if (row.Cells["ActionTypeId"].Value != null && !string.IsNullOrEmpty(row.Cells["ActionTypeId"].Value.ToString()))
                {
                    var actionType = actionTypes.FirstOrDefault(j => j.ActionTypeId == (int)row.Cells["ActionTypeId"].Value);
                    if (actionType != null)
                    {
                        var color = actionType.Color.Split(',');
                        row.Cells[7].Value = actionType.ChineseName;
                        row.Cells[7].Style.BackColor = Color.FromArgb(int.Parse(color[0]), int.Parse(color[1]), int.Parse(color[2]), int.Parse(color[3]));
                    }
                }

                var value = row.Cells["TypeId"].Value;
                if (value != null && new[] { "1", "3" }.Contains(value.ToString())) row.Cells[5].Value = decimal.Parse("0.00"); //无关录像ST为0
            }

            if (dataGridView1.RowCount > 0)
            {
                var dgr = dataGridView1.Rows[dataGridView1.RowCount - 1];
                dgr.Cells[4].Value = null;
                dgr.Cells[5].Value = null;
                dgr.Cells[6].Value = 1.00m;
                dgr.Cells[7].Value = null;
                dgr.Cells[7].Style.BackColor = Color.White;
                dgr.Cells[8].Value = null;
                dgr.Cells[8].Style.BackColor = Color.White;
                dgr.Cells[9].Value = null;
            }

            dataGridView1.ClearSelection();
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Rectangle rectangle = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, dataGridView1.RowHeadersWidth - 4, e.RowBounds.Height);
            TextRenderer.DrawText(e.Graphics,
                (e.RowIndex + 1).ToString(),
                dataGridView1.RowHeadersDefaultCellStyle.Font,
                rectangle,
                dataGridView1.RowHeadersDefaultCellStyle.ForeColor,
                TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
        }

        private void btnCZLX_Click(object sender, EventArgs e)
        {
            if (actionForUpdate != null)
            {
                OperateType = actionForUpdate.OperationType;
                EquipmentTime = (double)(actionForUpdate.EquipmentTime ?? 0);
                HasWalking = actionForUpdate.Walking;
                ActionDes = actionForUpdate.ActionDes;
            }
            try
            {
                var form = new OperateStandard(OperateType, EquipmentTime, HasWalking, ActionDes);
                form.ShowDialog();
                OperateType = form.OperateType;
                EquipmentTime = double.Parse(form.EquipmentTime);
                HasWalking = form.HasWalking;
                ActionDes = form.ActionDes;
                if (actionForUpdate == null) return;
                actionForUpdate.OperationType = OperateType;
                actionForUpdate.EquipmentTime = (decimal?)EquipmentTime;
                actionForUpdate.Walking = HasWalking;
                actionForUpdate.ActionDes = ActionDes;
            }
            catch { }
        }


        //点击右侧动作列表，视频自动调到对应的开始时间，暂停状态
        private void SetPlayProcess(bool skip)
        {
            isAction = false;
            if (vlcPlayer.GetState() == 5) vlcPlayer.Pause();
            if (dataGridView1.SelectedRows.Count == 0) return;
            var startVal = dataGridView1.SelectedRows[0].Cells["StartTime"].Value;

            var currentStartTime = (int)decimal.Parse(startVal.ToString());

            vlcPlayer.SetPosition(currentStartTime);

            trackBar1.Value = (int)decimal.Parse(startVal.ToString()) < trackBar1.Maximum ? (int)decimal.Parse(startVal.ToString()) : 0;
            tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(trackBar1.Value), GetTimeString(trackBar1.Maximum));
            PlayerPause();
        }

        private void SetSelectionByTime()
        {
            var playTime = vlcPlayer.GetPosition(); 

            var action = TempActions.FirstOrDefault(a => playTime >= a.StartTime && playTime < a.EndTime);
            if (action != null) tempAction = action;
            else
            {
                tempAction = TempActions.Count > 0 ? TempActions.Last() : new t_Action() { StartTime = 0, EndTime = 0 };
            }
            if (action == null) return;
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                DataGridViewRow dgvr = dataGridView1.Rows[i];
                if ((int)dgvr.Cells[0].Value == action.ActionId)
                {

                    if (dataGridView1.SelectedRows.Count > 0 && dataGridView1.SelectedRows[0].Index == i)
                        return;
                    dataGridView1.ClearSelection();

                    dgvr.Selected = true;

                    // added by Ted
                    if (dgvr.Index - VScrollIndex >= 14)
                    {
                        VScrollIndex = dgvr.Index - 13;
                    }

                    if (VScrollIndex < dataGridView1.RowCount)//解决指定的参数已超出有效值的范围2018-5-31
                    {
                        dataGridView1.FirstDisplayedScrollingRowIndex = VScrollIndex;
                    }
                }
            }

        }
        private void SetSelectionByTime(int playTime)
        {
            var action = TempActions.FirstOrDefault(a => playTime >= a.StartTime && playTime < a.EndTime);
            if (action != null) tempAction = action;
            else
            {
                tempAction = TempActions.Count > 0 ? TempActions.Last() : new t_Action() { StartTime = 0, EndTime = 0 };
            }
            if (action == null) return;
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                DataGridViewRow dgvr = dataGridView1.Rows[i];
                if ((int)dgvr.Cells[0].Value == action.ActionId)
                {
                    if (dataGridView1.SelectedRows.Count > 0 && dataGridView1.SelectedRows[0].Index == i)
                        return;
                    dataGridView1.ClearSelection();
                    dgvr.Selected = true;

                    // added by Ted
                    if (dgvr.Index - VScrollIndex >= 14)
                    {
                        VScrollIndex = dgvr.Index - 13;
                    }

                    if (VScrollIndex < dataGridView1.RowCount)//解决指定的参数已超出有效值的范围2018-5-31
                    {
                        dataGridView1.FirstDisplayedScrollingRowIndex = VScrollIndex;
                    }
                }
    
            }

        }

        private void SetOperateType(string operateType, decimal? equipmentTime, string hasWalking)
        {
            OperateType = operateType;
            if (equipmentTime != null) EquipmentTime = (double)equipmentTime;
            else
            {
                EquipmentTime = 0d;
            }

            HasWalking = hasWalking;
        }

        /// <summary>
        /// 新增一个新标记，自动选中，点击保存时更新的就是这条新标记
        /// </summary>
        /// <param name="action"></param>
        private void SetActionInfo(t_Action action)
        {
            if (action == null) return;
            tbxBJMS.Text = action.Description;
            tbxAQZYSX.Text = action.SafetyCaution;
            tbxPZZYSX.Text = action.QualityCaution;
            tbxDZMC.Text = action.ActionName;
            tbxDM.Text = action.ActionCode;
            tbxSDSJ.Text = (action.ManualStandardTime/1000).ToString();
            cbCZMC.Text = action.OperationName;
            if (action.ActionTypeId != null)
            {
                cbLX.Text = actionTypes.FirstOrDefault(i => i.ActionTypeId == action.ActionTypeId)?.ChineseName;
            }

            cbSD.Text = action.Speed.ToString();
            SetCycle(action.Cycle.ToString());
            cbZQ.Text = action.Cycle.ToString();
            if (action.TypeId != null)
            {
                clbDFL.SetItemChecked((int)(action.TypeId - 1), true);
                clbDFL.SelectedItem = action.TypeId == 2 ? "有效动作" :
                    action.TypeId == 1 ? "无效动作" : "无关录像";
            }

            tsBtnDelete.Enabled = true;
            tsBtnSave.Enabled = true;
            SetOperateType(action.OperationType, action.EquipmentTime, action.Walking);
        }

        //设置周期下拉框的数据
        private void SetCycle(string cycle)
        {
            cbZQ.Items.Clear();
            cbZQ.Items.Add(cycle);
            cbZQ.Items.Add("新周期");
            if (dataGridView1.SelectedRows.Count == 0) return;
            if (dataGridView1.SelectedRows[0].Index != 0) cbZQ.Items.Add("向上合并"); //第一行的周期没有向上合并

            if (dataGridView1.SelectedRows[0].Index < dataGridView1.RowCount - 1) cbZQ.Items.Add("向下合并"); //最后一行没有向下合并
        }

        private void toolStripDropDownButton1_DropDownItemClicked_1(object sender, ToolStripItemClickedEventArgs e) //切换手动/自动标准时间
        {
            var text = e.ClickedItem.Text;
            if (text == "手动标准时间")
            {
                btnDgbz.Enabled = true;
                btnZHBZ.Enabled = true;
                cbSD.Enabled = false;
                dataGridView1.Columns[5].HeaderText = "ST手";
            }
            else
            {
                btnDgbz.Enabled = false;
                btnZHBZ.Enabled = false;
                cbSD.Enabled = true;
                dataGridView1.Columns[5].HeaderText = "S.T";
            }

            DDBtnStandardTime.Text = text;
            var hasSelected = false;
            if (CurrentVideo == null) return;
            var selectedRows = dataGridView1.SelectedRows;
            if (selectedRows.Count == 0) return;
            if (dataGridView1.SelectedRows[0].Index > 0)
            {
                NewAction = dataGridView1.SelectedRows[0].DataBoundItem as t_Action;
                hasSelected = true;
            }

            LoadActions(CurrentVideo.VideoId);
            if (hasSelected) ReSelectRow();

        }

        private void btnCancelLoop_Click(object sender, EventArgs e)
        {
            startPoint = 0;
            endPoint = 10000000;
            lbProcess.SendToBack();
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.White;
            }

            PlayerPause();
            btnCancelLoop.Enabled = false;
            btnSetLoopStart.Enabled = false;
            btnSetLoopEnd.Enabled = false;
        }

        private void btnSetLoopStart_Click(object sender, EventArgs e)
        {
            PlayerPause();
            ClearSelection();
            if (dataGridView1.SelectedRows.Count == 0) return;
            var startVal = (int)(decimal.Parse(dataGridView1.SelectedRows[0].Cells["StartTime"].Value.ToString()));
            startVal++;
            StartIndex = dataGridView1.SelectedRows[0].Index;
            startPoint = startVal;
            trackBar1.Value = startPoint;
            vlcPlayer.SetPosition(startVal);
            if (vlcPlayer.GetState() == 5) vlcPlayer.Pause();
            btnSetLoopEnd.Enabled = true;
            btnCancelLoop.Enabled = true;
        }

        private void btnSetLoopEnd_Click(object sender, EventArgs e)
        {
            IsAutoPause = false;
            if (vlcPlayer.GetState() == 5) vlcPlayer.Pause();
            timer1.Stop();
            if (dataGridView1.SelectedRows.Count == 0) return;
            var endVal = (int)decimal.Parse(dataGridView1.SelectedRows[0].Cells["EndTime"].Value.ToString());
            EndIndex = dataGridView1.SelectedRows[0].Index;
            endPoint = endVal;
            var pct = endPoint - startPoint;
            var duration = vlcPlayer.GetDuration();
            lbProcess.Width = (int)((float)pct / (float)duration * 1069);
            lbProcess.Location = new Point((int)((float)startPoint / (float)duration * 1069 + 18), trackBar1.Location.Y + 8);
            lbProcess.Visible = true;
            lbProcess.BringToFront();
            is_playinig = false;
            vlcPlayer.SetPosition(startPoint);
            trackBar1.Value = startPoint;
            for (int i = StartIndex; i <= EndIndex; i++)
            {
                dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(255, 0, 120, 215);
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
                return;
            if (vlcPlayer.GetState() == 5)
                vlcPlayer.Pause();
            VScrollIndex = dataGridView1.FirstDisplayedScrollingRowIndex;//获取当前显示的第一条的索引值，用于重新刷新数据的时候控制滚动条
            //Console.WriteLine("滚动条位置：" + VScrollIndex);
            IsAutoPause = true;
            StopTimer();
            tsBtnSign.Enabled = false;
            var selectedRows = dataGridView1.SelectedRows;
            if (selectedRows.Count == 0) return;
            if (selectedRows[0].Cells[0].Value == null) return;
            if (dataGridView1.SelectedRows[0].Index > 0)
            {
                DataGridViewRow upRow = dataGridView1.Rows[dataGridView1.SelectedRows[0].Index - 1];
                upCycle = (int)(upRow.Cells["Cycle"].Value ?? 0); //上一个动作的周期
            }

            if (dataGridView1.SelectedRows[0].Index < dataGridView1.RowCount - 1)
            {
                DataGridViewRow downRow = dataGridView1.Rows[dataGridView1.SelectedRows[0].Index + 1];
                downCycle = (int)(downRow.Cells["Cycle"].Value ?? 0); //下一个动作的周期
            }

            var id = (int)selectedRows[0].Cells[0].Value;
            actionForUpdate = TempActions.FirstOrDefault(i => i.ActionId == id); //加载动作数据到窗体内对应的控件
            NewAction = actionForUpdate;
            btnSetLoopStart.Enabled = true;
            var upEndTime = tempAction.EndTime;
            tempAction = actionForUpdate;

            if (actionForUpdate != null)
            {
                SetActionInfo(actionForUpdate);
                SetPlayProcess(upEndTime == actionForUpdate.StartTime && UsedPlayed == true);
                UsedPlayed = false;
            }
            else
            {
                tsBtnDelete.Enabled = false;
                tsBtnSave.Enabled = false;
                btnSetLoopStart.Enabled = false;
            }

            btnSetLoopStart.Enabled = true;
        }

        private void tsBtnPause_Click(object sender, EventArgs e)
        {
            PlayerPause();
        }

        private void timerAutoLoadVideo_Tick(object sender, EventArgs e)
        {
            if (!isFromSystemIntegrationForm)
            {
                timerAutoLoadVideo.Stop();
                return;
            }

            if (!File.Exists(filePath)) return;
            vlcPlayer.Open(filePath);
            trackBar1.SetRange(0, (int)vlcPlayer.GetDuration());
            trackBar1.Value = 0;
            StartTimer();
            if (vlcPlayer.GetState() == 5)
            {
                timer1.Interval = 1;
            }

            is_playinig = true;
            media_is_open = true;
            LoadOperationName(); //加载当前视频包含的所有操作名称
            isFromSystemIntegrationForm = false;
            timerAutoLoadVideo.Stop();
            Program.FormMain.SetLoadStatus(false);
            var name = Path.GetFileName(filePath);
            var mode = Common.GetSingleModel<t_Video>(i => i.FileName == name); //先判断选择的视频是否已经在数据库中存在
            if (mode != null) //如果存在
            {
                CurrentVideo = mode;
                VideoID = mode.VideoId; // added by Ted on 20180312 for other forms
                btnCZLX.Enabled = true;
            }
        }

        public void SetVideoPath(string VideoPath)
        {
            isFromSystemIntegrationForm = true;
            timerAutoLoadVideo.Start();
            filePath = VideoPath;
            lblFileName.Text = filePath;
        }

        private void ClearSelection()
        {
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.White;
            }
        }

        private void bgWUploadVideo_DoWork(object sender, DoWorkEventArgs e)
        {
            var args = e.Argument as string[];
            FileHelper.FolderCreate(args[1]);
            FileHelper.FileCopy(args[0], args[1] + args[2]);
            if (Program.CurrentZone != null)
            {
                var ip = Program.CurrentZone.FTPAddress;
                var user = Program.CurrentZone.FTPUserName;
                var pwd = Program.CurrentZone.FTPPassword;
                var port = (int)Program.CurrentZone.FTPPort;

                FtpHelper ftpop = new FtpHelper(ip, port, user, pwd);

                Thread.Sleep(2000);
                ftpop.Upload();
            }
        }
        //延时加载视频动作，防止视频等动作加载完了才显示
        private void ShowGrid()
        {
            var list = ActionManage.GetActions(CurrentVideo.VideoId).OrderBy(i => i.StartTime).ToList();
            if (!list.Any())
            {
                list.Add(new t_Action { StartTime = 0.00m, EndTime = 0.00m });
            }
            dataGridView1.DataSource = list;
            tempAction = list.LastOrDefault() ?? new t_Action { ActionTime = 0, StartTime = 0, EndTime = 0 }; //加载视频包含已经分析完成的动作
            
            TempActions = list;
            //1无效2有效3无关
            for (var i = 0; i < dataGridView1.RowCount; i++)
            {
                var row = dataGridView1.Rows[i];
                if (row.Cells["TypeId"].Value != null)
                {
                    switch ((int)row.Cells["TypeId"].Value)
                    {
                        case 1:
                            row.Cells[8].Style.BackColor = Color.Red;
                            row.Cells[8].Value = "×";
                            break;
                        case 2:
                            row.Cells[8].Style.BackColor = Color.GreenYellow;
                            row.Cells[8].Value = "○";
                            break;
                        case 3:
                            row.Cells[8].Style.BackColor = Color.Yellow;
                            row.Cells[8].Value = "△";
                            break;
                    }
                }

                if (row.Cells["ActionTypeId"].Value != null && !string.IsNullOrEmpty(row.Cells["ActionTypeId"].Value.ToString()))
                {
                    var actionType = actionTypes.FirstOrDefault(j => j.ActionTypeId == (int)row.Cells["ActionTypeId"].Value);
                    if (actionType != null)
                    {
                        var color = actionType.Color.Split(',');
                        row.Cells[7].Value = actionType.ChineseName;
                        row.Cells[7].Style.BackColor = Color.FromArgb(int.Parse(color[0]), int.Parse(color[1]), int.Parse(color[2]), int.Parse(color[3]));
                    }
                }

                var value = row.Cells["TypeId"].Value;
                if (value != null && new[] { "1", "3" }.Contains(value.ToString())) row.Cells[5].Value = decimal.Parse("0.00"); //无关录像ST为0
            }
            if (dataGridView1.RowCount > 0)
            {
                var dgr = dataGridView1.Rows[dataGridView1.RowCount - 1];
                dgr.Cells[4].Value = null;
                dgr.Cells[5].Value = null;
                dgr.Cells[6].Value = 1.00m;
                dgr.Cells[7].Value = null;
                dgr.Cells[7].Style.BackColor = Color.White;
                dgr.Cells[8].Value = null;
                dgr.Cells[8].Style.BackColor = Color.White;
                dgr.Cells[9].Value = null;
            }
            LoadOperationName(); //加载当前视频包含的所有操作名称


            //视频加载完了之后更新视频的帧数和时长
        }

        private void RePaintDgv()
        {
            //1无效2有效3无关
            for (var i = 0; i < dataGridView1.Rows.Count; i++)
            {
                var row = dataGridView1.Rows[i];
                if (row.Cells["TypeId"].Value != null)
                {
                    switch ((int)row.Cells["TypeId"].Value)
                    {
                        case 1:
                            row.Cells[8].Style.BackColor = Color.Red;
                            row.Cells[8].Value = "×";
                            break;
                        case 2:
                            row.Cells[8].Style.BackColor = Color.GreenYellow;
                            row.Cells[8].Value = "○";
                            break;
                        case 3:
                            row.Cells[8].Style.BackColor = Color.Yellow;
                            row.Cells[8].Value = "△";
                            break;
                    }
                }

                if (row.Cells["ActionTypeId"].Value != null && !string.IsNullOrEmpty(row.Cells["ActionTypeId"].Value.ToString()))
                {
                    var actionType = actionTypes.FirstOrDefault(j => j.ActionTypeId == (int)row.Cells["ActionTypeId"].Value);
                    if (actionType != null)
                    {
                        var color = actionType.Color.Split(',');
                        row.Cells[7].Value = actionType.ChineseName;
                        row.Cells[7].Style.BackColor = Color.FromArgb(int.Parse(color[0]), int.Parse(color[1]), int.Parse(color[2]), int.Parse(color[3]));
                    }
                }

                var value = row.Cells["TypeId"].Value;
                if (value != null && new[] { "1", "3" }.Contains(value.ToString())) row.Cells[5].Value = decimal.Parse("0.00"); //无关录像ST为0
            }

            dataGridView1.ClearSelection();
        }

        private void bWDeleteAction_DoWork(object sender, DoWorkEventArgs e)
        {
            var actionId = (int)e.Argument;
            ActionManage.DeleteAction(actionId);
        }

        //异步执行新增、拆分动作，减少数据库操作的耗时带来前台的卡顿
        private void bWNewAction_DoWork(object sender, DoWorkEventArgs e)
        {
            if (e.Argument is object[])
            {
                var args = (object[])e.Argument;
                var mode = (t_Action)args[0];
                var isSplit = (bool)args[1];

                if (mode.ActionTime < 0 || tempAction.ActionTime < 0)
                {
                    return;//不允许插入负值的时间
                }
                tempAction = isSplit ? ActionManage.SplitAction(tempAction, mode) : ActionManage.AddNewAction(mode, (decimal)vlcPlayer.GetDuration()); //如果是拆分动作，需要同时更新被拆分动作，新增动作
                actionForUpdate = tempAction;
                CleanActionInfo();
                dataGridView1.Rows[dataGridView1.RowCount - 1].Selected = true;
                SetActionInfo(tempAction);
                if (vlcPlayer.GetState() == 5) vlcPlayer.Pause();
            }
        }

        private void dataGridView1_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                oldOperationName = (string)dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
            }
            else if (e.ColumnIndex == 3)
            {
                oldCycle = (int)dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
            }
        }


        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1) return;
            var updateId = dataGridView1.Rows[e.RowIndex].Cells[0].Value;
            var value = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
            var list = (List<t_Action>)dataGridView1.DataSource;
            var m = list.FirstOrDefault(i => i.ActionId == (int)updateId);
            if (m == null) return;
            if (e.ColumnIndex == 2)//批量修改操作名称
            {
                cbCZMC.Text = value.ToString();
                ActionManage.BatchUpdateOperationName(m, oldOperationName);
                LoadActions(CurrentVideo.VideoId);
            }
            else if (e.ColumnIndex == 3)//批量修改周期
            {
                cbZQ.Text = value.ToString();
                ActionManage.BatchUpdateCycle(m, oldCycle);
                LoadActions(CurrentVideo.VideoId);
            }

            ReSelectRow();
        }

        private void dataGridView1_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex == -1)
                return;
            StopTimer();
            if (vlcPlayer.GetState() == 5) vlcPlayer.Pause();
        }

        private void vlcPlayer_OnMessage(object sender, AxAPlayer3Lib._IPlayerEvents_OnMessageEvent e)
        {
            if (e.nMessage == 0x0201)
            {
                
                if (vlcPlayer.GetState() == 5)
                {
                    StopTimer();
                    if (intFactor != 0)
                    {
                        vlcPlayer.SetPosition(vlcPlayer.GetPosition() - intFactor);
                    }

                    vlcPlayer.Pause();
                }
                else
                {
                    PlayerPlay();
                }
            }
        }

        private void vlcPlayer_OnOpenSucceeded(object sender, EventArgs e)
        {
            BacktoStart(false);
            var tbMax = vlcPlayer.GetDuration();
            vlcPlayer.SetConfig(103, tbMax.ToString());
            trackBar1.SetRange(0, tbMax);
            trackBar1.Value = 0;
            int frameInterval = 40;
            int.TryParse(vlcPlayer.GetConfig(117), out frameInterval);
            TotalFrameAmont = tbMax / frameInterval;
            intFactor = frameInterval;
            CurrentVideo.FrameCount = TotalFrameAmont;
            CurrentVideo.Duration = (decimal)((decimal)tbMax / 1000);
            Common.AddOrUpdateObject(CurrentVideo);
            tsBtnSign.Enabled = false;
            tsBtnDelete.Enabled = false;
            tsBtnSave.Enabled = false;
            CleanActionInfo();
            var p1 = vlcPlayer.GetConfig(109);
            var p2 = vlcPlayer.GetConfig(110);
            if (p1 == "0" || p2 == "0")
            {
                toolStripButton2.Enabled = false;
            }
            else
            {
                toolStripButton2.Enabled = true;
            }

            var p3 = vlcPlayer.GetConfig(111);
            var p4 = vlcPlayer.GetConfig(112);
            if (p3 == "0" || p4 == "0")
            {
                toolStripButton1.Enabled = false;
            }
            else
            {
                toolStripButton1.Enabled = true;
            }
        }

        public void SaveSnapshot(string imagePath)
        {
            var dd = vlcPlayer.GetConfig(701);
            vlcPlayer.SetConfig(702, imagePath);
        }

    }
}