﻿using System;
using System.Reflection;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.ComponentModel;
using Utility;
using System.Configuration;

namespace Takata
{
    public partial class FormMain : Office2007RibbonForm
    {
        public delegate void re();
//        public event re refresh;
        //private Form1 privlcForm;
        private FormAPlay priForm;
        private FormADoublePlay dblForm;
        private FormAPlayActions paForm;
        public Label _lblUploadImage;
        public Label _lblUploading;

        public FormMain()
        {
            InitializeComponent();
 

            if (Program.CurrentUser.RoleId != 1)
            {
                buttonItem13.Visible = false;
            }
            if (Program.CurrentUser.RoleId != 1 && Program.CurrentUser.RoleId != 2)
            {
                ribbonBar3.Hide();
                //                ribbonBar2.Hide();
            }
            SetTabShow("视频播放", "FormAPlay");
            Splasher.Close();
            Text = $"均胜安全{Assembly.GetExecutingAssembly().GetName().Version.ToString()}";
            _lblUploadImage = this.lblUploadImage;
            _lblUploading = this.lblUploading;
        }

        public void SetTabShow(string tabName, string sfrmName)
        {
            bool isOpen = false;
            foreach (SuperTabItem item in superTabControl1.Tabs)
            {
                //已打开
                if (item.Name == tabName)
                {
                    superTabControl1.SelectedTab = item;
                    isOpen = true;
                    break;
                }
            }
            if (!isOpen)
            {
                //反射取得子窗体对象。
                object obj = Assembly.GetExecutingAssembly().CreateInstance("Takata." + sfrmName, false);
                //需要强转
                Form form = (Form)obj;
                switch (sfrmName)
                {
                    case "FormAPlay":
                        priForm = (FormAPlay)form;
                        break;
                    case "FormADoublePlay":
                        dblForm = (FormADoublePlay)form;
                        break;
                    case "FormAPlayActions":
                        paForm = (FormAPlayActions)form;
                        break;
                }

                //设置该子窗体不为顶级窗体，否则不能加入到别的控件中
                form.TopLevel = false;
                form.Visible = true;
                //布满父控件
                form.Dock = DockStyle.Fill;
                //创建一个tab
                SuperTabItem item = superTabControl1.CreateTab(tabName);
                //设置显示名和控件名
                item.Text = tabName;
                item.Name = tabName;

                //将子窗体添加到Tab中
                item.AttachedControl.Controls.Add(form);
                //选择该子窗体。
                superTabControl1.SelectedTab = item;
            }
        }

        public void btnMap_Click(object sender, EventArgs e)
        {
            SetTabShow("视频播放", "FormAPlay");
        }


        private void ribbonTabItem1_Click(object sender, EventArgs e)
        {

        }

        private void FormMainNew_FormClosing(object sender, FormClosingEventArgs e)
        {
            //            if (MessageBox.Show("您确定要关闭程序吗?", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.OK)
            //            {
            //                try
            //                {
            //                   
            //                    timerHeartbeap.Stop();
            //                    Environment.Exit(0);
            //                }
            //                catch
            //                {
            //                }
            //            }
            //            else
            //            {
            //                e.Cancel = true;
            //            }
            try
            {
                if (dblForm != null)
                {
                    dblForm.PlayerPause();
                    dblForm.Player2Pause();
                }
                if (priForm != null)
                {
                    priForm.PlayerPause();
                }
                if (paForm != null)
                {
                    paForm.PlayerPause();
                }
            }
            catch { }
        }

        private void timerHeartbeap_Tick(object sender, EventArgs e)
        {
        }

        private void buttonItem1_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show("您确定要注销登录吗？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Program.CurrentUser = null;  // updated by Ted on 20180312 .move this line here
                Application.Exit();
                System.Diagnostics.Process.Start(Assembly.GetExecutingAssembly().Location);
            }
        }

        private void buttonItem2_Click(object sender, EventArgs e)
        {
            FormChangePwd changePwd = new FormChangePwd();
            changePwd.ShowDialog();
        }

        private void btnDoublePlay_Click(object sender, EventArgs e)
        {
            SetTabShow("双屏播放", "FormADoublePlay");
        }

        private void btnEditActionType_Click(object sender, EventArgs e)
        {
            SetTabShow("动作类型编辑", "ActionTypeList");
        }

        private void buttonItem4_Click(object sender, EventArgs e)
        {
            SetTabShow("编辑速度", "SpeedEdit");
        }

        private void buttonItem5_Click(object sender, EventArgs e)
        {
            SetTabShow("单个动作标准管理", "SingleActionEdit");
        }

        private void buttonItem8_Click(object sender, EventArgs e)
        {
            SetTabShow("工厂信息维护", "FactoryEdit");
        }

        private void buttonItem9_Click(object sender, EventArgs e)
        {
            SetTabShow("车间信息维护", "WorkShopEdit");
        }

        private void buttonItem10_Click(object sender, EventArgs e)
        {
            SetTabShow("生产线信息维护", "WorkLineEdit");
        }

        private void buttonItem11_Click(object sender, EventArgs e)
        {
            SetTabShow("工序信息维护", "WorkStepEdit");
        }

        private void buttonItem12_Click(object sender, EventArgs e)
        {
            SetTabShow("组合标准管理", "CombinationList");
        }

        private void buttonItem14_Click(object sender, EventArgs e)
        {
            SetTabShow("用户管理", "FormUser");
        }

        private void buttonItem15_Click(object sender, EventArgs e)
        {
            SetTabShow("权限管理", "FormRoleList");
        }

        private void buttonItem27_Click(object sender, EventArgs e)
        {
            FormAbout about = new FormAbout();
            about.ShowDialog();
        }
        private void buttonItem22_Click(object sender, EventArgs e)
        {
            SetTabShow("录像数据管理", "VideoManage");
        }

        private void buttonItem19_Click(object sender, EventArgs e)//标记播放
        {
            if (priForm == null) return;
            //privlcForm.IsAnalyzePlay = false;
            //var pl = privlcForm.Controls.Find("panelEx2", false)[0];
            //(privlcForm as Form1).panelBorderColor = System.Drawing.Color.Red;
            //pl.Invalidate();
            //pl.Update();
            var video = priForm?.CurrentVideo;
            var filePath = priForm?.filePath;
            if (video == null) return;
            var form = new FormActionsPlay(video, filePath);
            form.ShowDialog();
        }

        private void buttonItem24_Click(object sender, EventArgs e)
        {
            if (priForm == null)
            {
                MessageBox.Show("请分析视频后使用导出报表功能", "提示");
                return;
            }
            if (priForm.VideoID == 0)
            {
                MessageBox.Show("请分析视频后使用导出报表功能", "提示");
                return;
            }
            ExportForm export = new ExportForm(1, priForm.VideoID);
            export.ShowDialog();
        }

        private void buttonItem25_Click(object sender, EventArgs e)
        {
            if (priForm == null)
            {
                MessageBox.Show("请分析视频后使用导出报表功能", "提示");
                return;
            }
            if (priForm.VideoID == 0)
            {
                MessageBox.Show("请分析视频后使用导出报表功能", "提示");
                return;
            }
            ExportForm export = new ExportForm(2, priForm.VideoID);
            export.ShowDialog();
        }

        private void PlayAction_Click(object sender, EventArgs e)
        {
            SetTabShow("播放标记", "FormAPlayActions");
        }

        private void buttonItem16_Click(object sender, EventArgs e)
        {
            if (priForm == null) return;
            var name = DateTime.Now.ToFileTime() + ".png";
            saveFileDialog1.FileName = name;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                var fileName = saveFileDialog1.FileName;
                priForm.SaveSnapshot(fileName);
            }
        }

        private void buttonItem23_Click(object sender, EventArgs e)
        {
            SetTabShow("系统整合", "FormSystemIntegration");
        }

        private void buttonItem20_Click(object sender, EventArgs e)
        {
            if (dblForm == null) return;
            dblForm.PlayerPlay();
            dblForm.Player2Play();
            dblForm.CompareActions();
        }

        private void UploadData()
        {
            if (Program.CurrentZone != null)
            {
                var ip = Program.CurrentZone.FTPAddress;
                var user = Program.CurrentZone.FTPUserName;
                var pwd = Program.CurrentZone.FTPPassword;
                var port = (int)Program.CurrentZone.FTPPort;
                FtpHelper ftpop = new FtpHelper(ip, port, user, pwd);
                ftpop.Upload();
            }
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            backgroundWorker1.RunWorkerAsync();
        }


        public void LoadActions(int i) //加载视频对应的动作
        {
            priForm.LoadActionsFromSystemIntegration(i);
        }

        public void SetVideoPath(string VideoPath) //设置视频路径
        {
            SetTabShow("视频播放", "FormAPlay");

            priForm?.SetVideoPath(VideoPath);
        }

        private delegate void lblUploadingDelegate(string str, bool blVisible);
        private void lblUploadingMethod(string str, bool blVisible)
        {
            if (this.lblUploading.InvokeRequired)
            {
                lblUploadingDelegate lblUploadingDelegate = new lblUploadingDelegate(lblUploadingMethod);
                this.lblUploading.Invoke(lblUploadingDelegate, str, blVisible);
            }
            else
            {
                this.lblUploading.Text = str;
                this.lblUploading.Visible = blVisible;
            }
        }

        private delegate void lblUploadImageDelegate(bool blVisible);
        private void lblUploadImageMethod(bool blVisible)
        {
            if (this.lblUploadImage.InvokeRequired)
            {
                lblUploadImageDelegate lblUploadImageDelegate = new lblUploadImageDelegate(lblUploadImageMethod);
                this.lblUploadImage.Invoke(lblUploadImageDelegate, blVisible);
            }
            else
            {
                this.lblUploadImage.Visible = blVisible;
            }
        }

        public void SetLoadStatus(bool blStatus) //设置视频路径
        {
            lblUploadingMethod("正在从服务器下载视频", blStatus);
            lblUploadImageMethod(blStatus);
        }

        public void SetLoadStatusDirectly(bool blStatus, string downType="视频") //设置视频路径
        {
            this.lblUploading.Text = $"正在从服务器下载{downType}";
            this.lblUploadImage.Visible = blStatus;
            this.lblUploading.Visible = blStatus;
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            lblUploading.Text = "正在与服务器同步数据";
            UploadData();
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lblUploading.Visible = false;
            lblUploadImage.Visible = false;
        }

        private void buttonItem26_Click(object sender, EventArgs e)
        {

        }
        private void buttonItem3_Click(object sender, EventArgs e)
        {
            SetTabShow("车种维护", "CarTypeEdit");
        }

        private void btnProd_Click(object sender, EventArgs e)
        {
            SetTabShow("产品维护", "ProdEdit");
        }

        private void superTabControl1_TabItemClose(object sender, SuperTabStripTabItemCloseEventArgs e)
        {
            try
            {
                if (e.Tab.Name == "双屏播放")
                {
                    dblForm.PlayerPause();
                    dblForm.Player2Pause();
                }
                else if (e.Tab.Name == "视频播放")
                {
                    priForm.PlayerPause();
                }
                else if (e.Tab.Name == "播放标记")
                {
                    paForm.PlayerPause();
                }
            }
            catch { }
        }

        private void buttonItem18_Click(object sender, EventArgs e)
        {
            SetTabShow("区域管理", "FormZone");
        }

    }
}
