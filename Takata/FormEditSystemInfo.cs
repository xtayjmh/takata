﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Microsoft.Win32;

namespace Takata
{
    public partial class FormEditSystemInfo : Office2007Form
    {
        Configuration config = ConfigurationManager.OpenExeConfiguration(Environment.CurrentDirectory + "\\Joyson.exe");
        public FormEditSystemInfo()
        {
            InitializeComponent();
            GetConfigInfo();
            LoadValues();
        }

        private void GetConfigInfo()
        {
            var settings = config.AppSettings.Settings;
            //tbxFtpAddr.Text = settings["ftpaddr"].Value;
            //tbxFtpUser.Text = settings["ftpuser"].Value;
            //tbxFtpPwd.Text = settings["ftppwd"].Value;
            tbxDbAddr.Text = settings["dbaddr"].Value;
            tbxDbUser.Text = settings["dbuser"].Value;
            tbxDbPwd.Text = settings["dbpwd"].Value;
            tbxDbName.Text = settings["dbname"].Value;
        }

        private void btnOk_Click(object sender, System.EventArgs e)
        {
            //var ftpAddr = tbxFtpAddr.Text.Trim();
            //var ftpUser = tbxFtpUser.Text.Trim();
            //var ftpPwd = tbxFtpPwd.Text.Trim();
            var dbAddr = tbxDbAddr.Text.Trim();
            var dbUser = tbxDbUser.Text.Trim();
            var dbPwd = tbxDbPwd.Text.Trim();
            var dbName = tbxDbName.Text.Trim();

            //config.AppSettings.Settings["ftpaddr"].Value = ftpAddr;
            //config.AppSettings.Settings["ftpuser"].Value = ftpUser;
            //config.AppSettings.Settings["ftppwd"].Value = ftpPwd;
            config.AppSettings.Settings["dbaddr"].Value = dbAddr;
            config.AppSettings.Settings["dbuser"].Value = dbUser;
            config.AppSettings.Settings["dbpwd"].Value = dbPwd;
            config.AppSettings.Settings["dbname"].Value = dbName;

            config.ConnectionStrings.ConnectionStrings["TakataEntities"].ConnectionString = $"metadata=res://*/Takata.csdl|res://*/Takata.ssdl|res://*/Takata.msl;provider=System.Data.SqlClient;provider connection string=\"data source={dbAddr};initial catalog={dbName};persist security info=True;user id={dbUser};password={dbPwd};MultipleActiveResultSets=True;App=EntityFramework\"";
            config.ConnectionStrings.ConnectionStrings["Takata.Properties.Settings.TakataConnectionString"].ConnectionString = $"Data Source={dbAddr};Initial Catalog={dbName};Persist Security Info=True;User ID={dbUser};Password={dbPwd}";
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
            ConfigurationManager.RefreshSection("connectionStrings");

            RegistryKey regKey = Registry.CurrentUser.CreateSubKey("SOFTWARE\\JoysonRegedit");

            if (regKey != null)
            {
                //regKey.SetValue("ftpaddr", ftpAddr);
                //regKey.SetValue("ftpuser", ftpUser);
                //regKey.SetValue("ftppwd", ftpPwd);

                regKey.SetValue("dbaddr", dbAddr);
                regKey.SetValue("dbuser", dbUser);
                regKey.SetValue("dbpwd", dbPwd);
                regKey.SetValue("dbname", dbName);
            }



            MessageBoxEx.Show("修改成功");
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancle_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            var dbAddr = tbxDbAddr.Text.Trim();
            var dbUser = tbxDbUser.Text.Trim();
            var dbPwd = tbxDbPwd.Text.Trim();
            var dbName = tbxDbName.Text.Trim();
            var sqlConnection = new SqlConnection($"Data Source={dbAddr};Initial Catalog={dbName};Persist Security Info=True;User ID={dbUser};Password={dbPwd}");
            try
            {
                sqlConnection.Open();
                MessageBoxEx.Show("连接成功");
            }
            catch (Exception ex)
            {
                MessageBoxEx.Show($"无法连接，请确认数据配置信息，错误信息{ex.Message}");
            }
            finally { sqlConnection.Close(); }
        }

        private void LoadValues()
        {
            RegistryKey regKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\\JoysonRegedit", false);
            if (regKey != null)
            {
                bool blFlag = true;
                //if (regKey.GetValue("ftpaddr") != null)
                //{
                //    tbxFtpAddr.Text = regKey.GetValue("ftpaddr").ToString();
                //}
                
                //if (regKey.GetValue("ftpuser") != null)
                //{
                //    tbxFtpUser.Text = regKey.GetValue("ftpuser").ToString();
                //}
                //if (regKey.GetValue("ftppwd") != null)
                //{
                //    tbxFtpPwd.Text = regKey.GetValue("ftppwd").ToString();
                //}
                if (regKey.GetValue("dbaddr") != null)
                {
                    tbxDbAddr.Text = regKey.GetValue("dbaddr").ToString();
                }
                if (regKey.GetValue("dbuser") != null)
                {
                    tbxDbUser.Text = regKey.GetValue("dbuser").ToString();
                }
                if (regKey.GetValue("dbpwd") != null)
                {
                    tbxDbPwd.Text = regKey.GetValue("dbpwd").ToString();
                }
                if (regKey.GetValue("dbname") != null)
                {
                    tbxDbName.Text = regKey.GetValue("dbname").ToString();
                }
            }
        }
    }
}
