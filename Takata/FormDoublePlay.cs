﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DAL;
using DAL.SystemManage;
using Model;
using Takata.Properties;

namespace Takata
{
    public partial class FormDoublePlay : Form
    {
        public VlcPlayer vlcPlayer;
        private string address;
        private bool is_playing;
        private bool is_playing2;

        private bool media_is_open;//标记媒体文件是否打开，若未打开则tsbtn_play读ini打开之前的文件，若打开则跳过这步(避免每次都打开文件造成屏幕跳动)
        private bool media_is_open2;
        private float Speed = 1;
        private float Speed2 = 1;
        private decimal rate1;
        private decimal rate2;
        public VlcPlayer vlcPlayer2;
        private decimal startPoint1;//对比的起始点
        private decimal endPoint1= 10000.0000m;//对比的结束点
        private decimal startPoint2;//对比的起始点
        private decimal endPoint2= 10000.0000m;//对比的结束点
        private t_Video video1;
        private t_Video video2;
        private List<t_ActionType> actionTypes = new List<t_ActionType>();
        private int StartIndex1 = 999;
        private int EndIndex1 = 999;
        private int StartIndex2 = 999;
        private int EndIndex2 = 999;
        private List<t_Action> actions1 = new List<t_Action>();
        private List<t_Action> actions2 = new List<t_Action>();

        ComponentResourceManager resources = new ComponentResourceManager(typeof(FormDoublePlay));
        public FormDoublePlay()
        {
            InitializeComponent();
            address = Environment.CurrentDirectory;
            actionTypes = Common.GetList<t_ActionType>(null);
            dgv1.AutoGenerateColumns = false;
            dgv2.AutoGenerateColumns = false;
            var list = Common.GetList<t_PlaySpeed>(null);
            list.ForEach(i =>
            {
                tSDDBtnSpeed.DropDownItems.Add(i.PlaySpeed.ToString());
                tsDDLSpeed2.DropDownItems.Add(i.PlaySpeed.ToString());
            });
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!is_playing) return;
            var playTime = (int) (vlcPlayer.GetPlayTime() * 1000);
            if (trackBar1.Value == trackBar1.Maximum || playTime >= trackBar1.Maximum)
            {
                vlcPlayer.Pause();
                timer1.Stop();
                tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(trackBar1.Maximum), GetTimeString(trackBar1.Maximum));
                trackBar1.Value = trackBar1.Maximum;
            }
            else
            {
                if (endPoint1 != 10000.0000m && vlcPlayer.GetPlayTime() >= endPoint1)
                {
                    trackBar1.Value = (int) (endPoint1 * 1000);
                    vlcPlayer.SetPlayTime(startPoint1);
                }
                else
                {
                    trackBar1.Value = playTime;
                    tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(trackBar1.Value), GetTimeString(trackBar1.Maximum));
                }
            }
        }
        private string GetTimeString(decimal val)
        {
            val = val / 1000;
            var hour = val / 3600;
            val %= 3600;
            var minute = val / 60;
            var second = val % 60;
            var fps = vlcPlayer.GetFps();
            if (fps == 0)
                fps = 25;
            var frame = (val % 1) / (decimal)(1 / fps); //帧
            return $"{hour:00}:{minute:00}:{(int)second:00}({(int)frame})";
        }
        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            if (is_playing)
            {
                vlcPlayer.SetPlayTime(trackBar1.Value / 1000);
                trackBar1.Value = (int)vlcPlayer.GetPlayTime() * 1000;
            }
        }
        private void tSBtn_play_ButtonClick(object sender, EventArgs e)
        {
            if (endPoint1 != 10000.0000m) //如果有结束点
            {
                vlcPlayer.SetPlayTime(startPoint1);
                trackBar1.Value = (int)(startPoint1 * 1000);
                PlayerPlay();
            }
            
            else
            {
                PlayerPlay();
            }
        }

        private void Player2Play()
        {
            vlcPlayer2.Play();
            timer2.Start();
            is_playing2 = true;
        }
        private void PlayerPlay()
        {
            vlcPlayer.Play();
            timer1.Start();
            is_playing = true;
        }
        private void tSBtn_openfile_Click(object sender, EventArgs e)
        {
            if (OpenFile(vlcPlayer))
            {
                if (vlcPlayer.IsPlaying())
                {
                    vlcPlayer.Pause();
                    timer1.Stop();
                }
                var fps = vlcPlayer.GetFps(); //获取视频的码率29.97003
                if (fps == 0)
                    fps = 25;
                rate1 = 1 / (decimal)fps;//初始化默认帧数为1
                lblProcess1.Visible = false;
            }
        }
        private bool OpenFile(VlcPlayer player, bool isSecondPlayer = false)
        {
            //bool isSame = false;
            openFileDialog1.FileName = "";
            if (this.openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                StreamWriter s = new StreamWriter(address + "\\Menu.ini", true);
                s.WriteLine(openFileDialog1.FileName);
                s.Flush();
                s.Close();
                string[] text = File.ReadAllLines(address + "\\Menu.ini");
                int row = text.Length;//行
                int rowcount;
                string[] tempdata = new string[] { "", "", "" };
                if (row > 3)// 若记录行数超过3个，则先记录后三个数据，再重新建一个Menu.ini（清除数据）,接着讲记录的后三个数据写入
                {
                    StreamReader sr1 = new StreamReader(address + "\\Menu.ini", true);
                    while (sr1.Peek() > -1)
                    {
                        sr1.ReadLine();//空读，跳过原始的第一个数据，从第二个数据开始读
                        for (rowcount = 0; rowcount < 3; rowcount++)
                        {
                            tempdata[rowcount] = sr1.ReadLine();
                        }
                    }
                    sr1.Close();
                    FileStream fs = new FileStream(address + "\\Menu.ini", FileMode.Create, FileAccess.Write);
                    fs.Close();
                    StreamWriter s1 = new StreamWriter(address + "\\Menu.ini", true);
                    s1.WriteLine(tempdata[0]);
                    s1.WriteLine(tempdata[1]);
                    s1.WriteLine(tempdata[2]);
                    s1.Flush();
                    s1.Close();
                }
                // StreamReader sr2 = new StreamReader(address + "\\Menu.ini", true);
                // while(sr2.Pee)
                player.PlayFile(openFileDialog1.FileName);

                if (isSecondPlayer)
                {
                    trackBar2.SetRange(0, (int)player.Duration() * 1000);
                    trackBar2.Value = 0;
                    media_is_open2 = true;
                    is_playing2 = true;
                    timer2.Start();
                }
                else
                {
                    trackBar1.SetRange(0, (int)player.Duration() * 1000);
                    trackBar1.Value = 0;
                    timer1.Start();
                    is_playing = true;
                    media_is_open = true;
                }
                return SetVideoInfo(openFileDialog1.FileName, isSecondPlayer);
 
            }
            return false;
        }
        private void tSB_backward_Click(object sender, EventArgs e)
        {
            vlcPlayer.Pause();
            int time = (int) vlcPlayer.GetPlayTime() - 5;
            if (time > 0)
            {
                vlcPlayer.SetPlayTime(time);
            }
            else
            {
                vlcPlayer.SetPlayTime(0);
            }
            vlcPlayer.Play();
            trackBar1.Value = (int)vlcPlayer.GetPlayTime();
        }
        private void toolStripDropDownButton1_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            tSDDBtnSpeed.Text = e.ClickedItem.Text;
            Speed = float.Parse(e.ClickedItem.Text);
            vlcPlayer.SetSpeed(Speed);//设置播放速度
            timer1.Interval = (int)(1 / Speed * 1000);//设置timer的间隔，使进度条和播放进度匹配
        }
        private void PlayerPause()
        {
            if (vlcPlayer.IsPlaying()) vlcPlayer.Pause();
            timer1.Stop();
        }
        private void Player2Pause()
        {
            if (vlcPlayer2.IsPlaying()) vlcPlayer2.Pause();
            timer2.Stop();
        }
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (vlcPlayer.IsPlaying())
            {
                PlayerPause();
                is_playing = false;
            }

            var playingTime = vlcPlayer.GetPlayTime();
            if ((playingTime - rate1) * 1000 > trackBar1.Minimum)
            {
                vlcPlayer.SetPlayTime(playingTime - rate1);
                trackBar1.Value = (int)vlcPlayer.GetPlayTime() * 1000;
                tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(vlcPlayer.GetPlayTime() * 1000), GetTimeString(vlcPlayer.Duration() * 1000));
            }
            else
            {
                vlcPlayer.SetPlayTime(trackBar1.Minimum);
                trackBar1.Value = trackBar1.Minimum;
                tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(vlcPlayer.GetPlayTime() * 1000), GetTimeString(vlcPlayer.Duration() * 1000));
            }

            if (vlcPlayer.IsPlaying()) vlcPlayer.Pause();
            is_playing = false;
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (vlcPlayer.IsPlaying())
            {
                PlayerPause();
                is_playing = false;
            }

            var playingTime = vlcPlayer.GetPlayTime();
            if (playingTime + rate1 < vlcPlayer.Duration() - 0.2m)
            {
                vlcPlayer.SetPlayTime(playingTime + rate1);
                trackBar1.Value = (int)vlcPlayer.GetPlayTime() * 1000;
                tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString((playingTime + rate1) * 1000), GetTimeString(vlcPlayer.Duration() * 1000));
            }
            else
            {
                vlcPlayer.SetPlayTime(vlcPlayer.Duration() - 0.1m);
                trackBar1.Value = trackBar1.Maximum;
                tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(vlcPlayer.Duration() * 1000), GetTimeString(vlcPlayer.Duration() * 1000));
            }

            if (vlcPlayer.IsPlaying()) vlcPlayer.Pause();
            is_playing = false;
        }

        private void tSDDBtnStep_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            tSDDBtnStep.Text = e.ClickedItem.Text;
            var fps = vlcPlayer.GetFps();//获取视频的码率29.97003
            if (fps == 0)
                fps = 25;
            rate1 = 1 / (decimal)fps;
        }
        private void tSB_forward_Click(object sender, EventArgs e)
        {
            vlcPlayer.Pause();
            int time = ((int) vlcPlayer.GetPlayTime() + 5) * 1000;
            if (time < trackBar1.Maximum)
            {
                vlcPlayer.Play();
                trackBar1.Value = (int)vlcPlayer.GetPlayTime() * 1000;
                vlcPlayer.SetPlayTime(time);

            }
            else
            {
                vlcPlayer.SetPlayTime(trackBar1.Maximum);
                vlcPlayer.Pause();
                trackBar1.Value = trackBar1.Maximum;
                timer1.Stop();
            }
            
        }
        private void tSBtn_stop_Click(object sender, EventArgs e)
        {
            if (vlcPlayer.IsPlaying())
            {
                vlcPlayer.Pause();
            }

            timer1.Stop();
            is_playing = false;
            media_is_open = true;
            trackBar1.Value = 0;
            vlcPlayer.Play();
            vlcPlayer.SetPlayTime(0.01m);
            tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(trackBar1.Value), GetTimeString(trackBar1.Maximum));
            if (vlcPlayer.IsPlaying()) vlcPlayer.Pause();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            if (!File.Exists(address + "\\Menu.ini"))
            {
                FileStream fs = new FileStream(address + "\\Menu.ini", FileMode.Create, FileAccess.Write);
                fs.Close();
            }
            string pluginPath = System.Environment.CurrentDirectory + "\\plugins\\";
            vlcPlayer = new VlcPlayer(pluginPath);
            IntPtr render_wnd = this.panelEx1.Handle;
            vlcPlayer.SetRenderWindow((int)render_wnd);

            vlcPlayer2 = new VlcPlayer(pluginPath);
            var render_wnd2 = pE2.Handle;
            vlcPlayer2.SetRenderWindow((int)render_wnd2);

            tbVideoTime.Text = "00:00:00/00:00:00";
            is_playing = false;
            media_is_open = false;
            this.Size = new Size(800, 600);
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            if (OpenFile(vlcPlayer2, true))
            {
                if (vlcPlayer2.IsPlaying())
                {
                    vlcPlayer2.Pause();
                    timer2.Stop();
                }
                var fps = vlcPlayer2.GetFps(); //获取视频的码率29.97003
                if (fps == 0)
                    fps = 25;
                rate2 = 1 / (decimal)fps;//初始化默认帧数为1
                lblProcess2.Visible = false;
            }
        }

        private void timer2_Tick(object sender, EventArgs e)//第二个播放器的定时器
        {
            //if (!is_playing2) return;
            //var playTime = (int)(vlcPlayer2.GetPlayTime() * 1000);
            //if (trackBar2.Value == trackBar2.Maximum || playTime >= trackBar2.Maximum || (trackBar2.Maximum-playTime) < 500)
            //{
            //    vlcPlayer2.Pause();
            //    timer2.Stop();
            //    tbVideoTime2.Text = string.Format("{0}/{1}", GetTimeString(trackBar2.Maximum), GetTimeString(trackBar2.Maximum));
            //    trackBar2.Value = trackBar2.Maximum;
            //}
            //else
            //{
            //    trackBar2.Value = playTime;
            //    tbVideoTime2.Text = string.Format("{0}/{1}", GetTimeString(trackBar2.Value), GetTimeString(trackBar2.Maximum));
            //}
            if (!is_playing2) return;
            var playTime = (int)(vlcPlayer2.GetPlayTime() * 1000);
            if (trackBar2.Value == trackBar2.Maximum || playTime >= trackBar2.Maximum)
            {
                vlcPlayer2.Pause();
                timer2.Stop();
                tbVideoTime2.Text = string.Format("{0}/{1}", GetTimeString(trackBar2.Maximum), GetTimeString(trackBar2.Maximum));
                trackBar2.Value = trackBar2.Maximum;
            }
            else
            {
                if (endPoint2 != 10000.0000m && vlcPlayer2.GetPlayTime() >= endPoint2)
                {
                    trackBar2.Value = (int)(endPoint2 * 1000);
                    vlcPlayer2.SetPlayTime(startPoint2);
                }
                else
                {
                    trackBar2.Value = playTime;
                    tbVideoTime2.Text = string.Format("{0}/{1}", GetTimeString(trackBar2.Value), GetTimeString(trackBar2.Maximum));
                }
            }
        }

        private void tSBtn_play2_ButtonClick(object sender, EventArgs e)
        {
            if (endPoint2 != 10000.0000m) //如果有结束点
            {
                vlcPlayer2.SetPlayTime(startPoint2);
                trackBar2.Value = (int)(startPoint2 * 1000);
                Player2Play();
            }

            else
            {
                Player2Play();
            }
        }

        private void tbForPanel2_Scroll(object sender, EventArgs e)
        {
            if (is_playing2)
            {
                vlcPlayer2.SetPlayTime(trackBar2.Value / 1000);
                trackBar2.Value = (int)vlcPlayer2.GetPlayTime() * 1000;
            }
        }

        private void tSBtn_stop2_Click(object sender, EventArgs e)
        {
            if (vlcPlayer2.IsPlaying())
            {
                vlcPlayer2.Pause();
            }

            timer2.Stop();
            is_playing2 = false;
            media_is_open2 = true;
            trackBar2.Value = 0;
            vlcPlayer2.Play();
            vlcPlayer2.SetPlayTime(0.01m);
            tbVideoTime2.Text = string.Format("{0}/{1}", GetTimeString(trackBar2.Value), GetTimeString(trackBar2.Maximum));
            if (vlcPlayer2.IsPlaying()) vlcPlayer2.Pause();
        }

        private void btnForward2_Click(object sender, EventArgs e)
        {
            vlcPlayer2.Pause();
            int time = ((int) vlcPlayer2.GetPlayTime() + 5) * 1000;
            if (time < trackBar2.Maximum)
            {
                vlcPlayer2.SetPlayTime(time);
                vlcPlayer2.Play();
                trackBar2.Value = (int)vlcPlayer2.GetPlayTime() * 1000;
            }
            else
            {
                vlcPlayer2.SetPlayTime(trackBar2.Maximum);
                vlcPlayer2.Pause();
                trackBar2.Value = trackBar2.Maximum;
            }
            
        }

        private void btnBackward2_Click(object sender, EventArgs e)
        {
            vlcPlayer2.Pause();
            int time = (int)vlcPlayer2.GetPlayTime() - 5;
            if (time > 0)
            {
                vlcPlayer2.SetPlayTime(time);
            }
            else
            {
                vlcPlayer2.SetPlayTime(0);
            }
            vlcPlayer2.Play();
            trackBar2.Value = (int)vlcPlayer2.GetPlayTime() * 1000;
        }

        private void tsDDLSpeed2_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            tsDDLSpeed2.Text = e.ClickedItem.Text;
            Speed2 = float.Parse(e.ClickedItem.Text);
            vlcPlayer2.SetSpeed(Speed2);//设置播放速度
            timer2.Interval = (int)(1 / Speed2 * 1000);//设置timer的间隔，使进度条和播放进度匹配
        }

        private void btnContrastStart_Click(object sender, EventArgs e)
        {
            ClearSelection2();
            if (vlcPlayer2.IsPlaying())
            {
                vlcPlayer2.Pause();
                timer2.Stop();
            }

            if (dgv2.SelectedRows.Count == 0) return;
            var startVal = decimal.Parse(dgv2.SelectedRows[0].Cells["StartTime2"].Value.ToString())/1000 + 0.01m;
            StartIndex2 = dgv2.SelectedRows[0].Index;
            startPoint2 = startVal;
            trackBar2.Value = (int)(startPoint2 * 1000);
            vlcPlayer2.SetPlayTime(startVal);
            if (vlcPlayer2.IsPlaying()) vlcPlayer2.Pause();
            btnContrastEnd.Enabled = true;
        }

        private void btnContrastEnd_Click(object sender, EventArgs e)
        {
            if (vlcPlayer2.IsPlaying()) vlcPlayer2.Pause();
            timer2.Stop();
            if (dgv2.SelectedRows.Count == 0) return;
            var endVal = dgv2.SelectedRows[0].Cells["EndTime2"].Value;
            EndIndex2 = dgv2.SelectedRows[0].Index;
            endPoint2 = decimal.Parse(endVal.ToString()) / 1000;
            var pct = endPoint2 - startPoint2;
            lblProcess2.Width = (int)((pct / vlcPlayer2.Duration()) * 581);
            lblProcess2.Location = new Point((int)(startPoint2 / vlcPlayer2.Duration() * 581) + 597, lblProcess2.Location.Y);
            lblProcess2.Visible = true;
            vlcPlayer2.SetPlayTime(startPoint2);
            trackBar2.Value = (int)(startPoint2 * 1000);
            if (vlcPlayer2.IsPlaying()) vlcPlayer2.Pause();
            for (int i = StartIndex2; i <= EndIndex2; i++)
            {
                dgv2.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(255, 0, 120, 215);
            }
        }
        private bool SetVideoInfo(string filePath,bool isSecondPlayer=false)
        {
            var fileName = Path.GetFileName(filePath);
            var mode = Common.GetSingleModel<t_Video>(i => i.FileName == fileName);
            if (mode == null) return false;
            if (isSecondPlayer) LoadActionsForDgv2(mode.VideoId);
            else
            {
                LoadActionsForDgv1(mode.VideoId);
            }
            return true;
        }
        private void LoadActionsForDgv1(int videoId)
        {
            var list = ActionManage.GetActions(videoId).Where(i => i.TypeId != 3).OrderBy(i => i.EndTime).ToList();
            actions1 = list;
            dgv1.DataSource = list;
            //1无效2有效3无关
            for (var i = 0; i < dgv1.Rows.Count; i++)
            {
                var row = dgv1.Rows[i];
                switch ((int)row.Cells["TypeId"].Value)
                {
                    case 1:
                        row.Cells[8].Style.BackColor = Color.Red;
                        row.Cells[8].Value = "×";
                        break;
                    case 2:
                        row.Cells[8].Style.BackColor = Color.GreenYellow;
                        row.Cells[8].Value = "○";
                        break;
                    case 3:
                        row.Cells[8].Style.BackColor = Color.Yellow;
                        row.Cells[8].Value = "△";
                        break;
                }

                if (row.Cells["ActionTypeId"].Value != null && !string.IsNullOrEmpty(row.Cells["ActionTypeId"].Value.ToString()))
                {
                    var actionType = actionTypes.FirstOrDefault(j => j.ActionTypeId == (int)row.Cells["ActionTypeId"].Value);
                    if (actionType != null)
                    {
                        var color = actionType.Color.Split(',');
                        row.Cells[7].Value = actionType.ChineseName;
                        row.Cells[7].Style.BackColor = Color.FromArgb(int.Parse(color[0]), int.Parse(color[1]), int.Parse(color[2]), int.Parse(color[3]));
                    }
                }
                if (new[] { "1", "3" }.Contains(row.Cells["TypeId"].Value.ToString())) row.Cells[5].Value = decimal.Parse("0.00");//无关录像ST为0
            }
            dgv1.ClearSelection();
        }
        private void LoadActionsForDgv2(int videoId)
        {
            var list = ActionManage.GetActions(videoId).Where(i => i.TypeId != 3).OrderBy(i => i.EndTime).ToList();
            actions2 = list;
            dgv2.DataSource = list;
            //1无效2有效3无关
            for (var i = 0; i < dgv2.Rows.Count; i++)
            {
                var row = dgv2.Rows[i];
                switch ((int)row.Cells["TypeId2"].Value)
                {
                    case 1:
                        row.Cells[8].Style.BackColor = Color.Red;
                        row.Cells[8].Value = "×";
                        break;
                    case 2:
                        row.Cells[8].Style.BackColor = Color.GreenYellow;
                        row.Cells[8].Value = "○";
                        break;
                    case 3:
                        row.Cells[8].Style.BackColor = Color.Yellow;
                        row.Cells[8].Value = "△";
                        break;
                }

                if (row.Cells["ActionTypeId2"].Value != null && !string.IsNullOrEmpty(row.Cells["ActionTypeId2"].Value.ToString()))
                {
                    var actionType = actionTypes.FirstOrDefault(j => j.ActionTypeId == (int)row.Cells["ActionTypeId2"].Value);
                    if (actionType != null)
                    {
                        var color = actionType.Color.Split(',');
                        row.Cells[7].Value = actionType.ChineseName;
                        row.Cells[7].Style.BackColor = Color.FromArgb(int.Parse(color[0]), int.Parse(color[1]), int.Parse(color[2]), int.Parse(color[3]));
                    }
                }
                if (new[] { "1", "3" }.Contains(row.Cells["TypeId2"].Value.ToString())) row.Cells[5].Value = decimal.Parse("0.00");//无关录像ST为0
            }
            dgv2.ClearSelection();
        }

        private void btnCancle2_Click(object sender, EventArgs e)
        {
            startPoint2 = 0.0000m;
            endPoint2 = 10000.0000m;
            lblProcess2.Visible = false;
            ClearSelection2();
            if (vlcPlayer2.IsPlaying()) vlcPlayer2.Pause();
        }

        private void btnCancle1_Click(object sender, EventArgs e)
        {
            startPoint1 = 0.0000m;
            endPoint1 = 10000.0000m;
            lblProcess1.Visible = false;
            ClearSelection1();
            if (vlcPlayer.IsPlaying()) vlcPlayer.Pause();
        }

        private void ClearSelection1()
        {
            for (int i = 0; i < dgv1.RowCount; i++)
            {
                dgv1.Rows[i].DefaultCellStyle.BackColor = Color.White;
            }
        }
        private void ClearSelection2()
        {
            for (int i = 0; i < dgv2.RowCount; i++)
            {
                dgv2.Rows[i].DefaultCellStyle.BackColor = Color.White;
            }
        }

        private void btnSetStart1_Click(object sender, EventArgs e)
        {
            ClearSelection1();
            if (vlcPlayer.IsPlaying())
            {
                vlcPlayer.Pause();
                timer1.Stop();
            }

            if (dgv1.SelectedRows.Count == 0) return;
            var startVal = decimal.Parse(dgv1.SelectedRows[0].Cells["StartTime"].Value.ToString()) / 1000 + 0.01m;
            StartIndex1 = dgv1.SelectedRows[0].Index;
            startPoint1 = startVal;
            trackBar1.Value = (int)(startPoint1 * 1000);
            vlcPlayer.SetPlayTime(startVal);
            if (vlcPlayer.IsPlaying()) vlcPlayer.Pause();
            btnSetEnd1.Enabled = true;
        }

        private void btnSetEnd1_Click(object sender, EventArgs e)
        {
            if (vlcPlayer.IsPlaying()) vlcPlayer.Pause();
            if (dgv1.SelectedRows.Count == 0) return;
            var endVal = dgv1.SelectedRows[0].Cells["EndTime"].Value;
            EndIndex1 = dgv1.SelectedRows[0].Index;
            endPoint1 = decimal.Parse(endVal.ToString()) / 1000;
            var pct = endPoint1 - startPoint1;
            lblProcess1.Width = (int)((pct / vlcPlayer.Duration()) * 581);
            lblProcess1.Location = new Point((int)(startPoint1 / vlcPlayer.Duration() * 581) + 15, lblProcess1.Location.Y);
            lblProcess1.Visible = true;
            vlcPlayer.SetPlayTime(startPoint1);
            trackBar1.Value = (int)(startPoint1 * 1000);
            timer1.Stop();
            if (vlcPlayer.IsPlaying()) vlcPlayer.Pause();
            for (int i = StartIndex1; i <= EndIndex1; i++)
            {
                dgv1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(255, 0, 120, 215);
            }
        }

        public void CompareActions()
        {
            var list = new List<CompareDoubleMod>();
            var listLeft = actions1.Skip(StartIndex1).Take(EndIndex1 + 1 - StartIndex1).Where(i => i.TypeId != 3).ToList();//去掉无关录像
            var listRight = actions2.Skip(StartIndex2).Take(EndIndex2 + 1 - StartIndex2).Where(i => i.TypeId != 3).ToList();
            listLeft.ForEach(l =>
            {
                if (!listRight.Select(i => i.ActionName).Contains(l.ActionName))
                {
                    list.Add(new CompareDoubleMod()
                    {
                        IdA = (listLeft.IndexOf(l)+1).ToString(),
                        IdB = "X",
                        NameA = l.ActionName,
                        NameB = "X X X X",
                        TimeA = l.ActionTime / 1000,
                        TimeB = 0
                    });
                }
                else
                {
                    listRight.ForEach(r =>
                    {
                        if (l.ActionName == r.ActionName) //动作标记名称相同
                        {
                            list.Add(new CompareDoubleMod()
                            {
                                IdA = (listLeft.IndexOf(l)+1).ToString(),
                                IdB = (listRight.IndexOf(r)+1).ToString(),
                                NameA = l.ActionName,
                                NameB = r.ActionName,
                                TimeB = r.ActionTime / 1000,
                                TimeA = l.ActionTime / 1000
                            });
                        }

                    });
                }
            });
            listRight.ForEach(i =>
            {
                if (!listLeft.Select(j => j.ActionName).Contains(i.ActionName))
                {
                    list.Add(new CompareDoubleMod()
                    {
                        IdA = "X",
                        IdB = listRight.IndexOf(i).ToString(),
                        NameA = "X X X X",
                        NameB = i.ActionName,
                        TimeA = 0,
                        TimeB = i.ActionTime / 1000
                    });
                }
            });
            /*else
            {
                list.Add(new CompareDoubleMod()
                {
                    IdA = listLeft.IndexOf(l).ToString(),
                    IdB = "X",
                    NameA = l.ActionName,
                    NameB = "X",
                    TimeA = l.ActionTime,
                    TimeB = 0
                });
                list.Add(new CompareDoubleMod()
                {
                    IdA = "X",
                    IdB = listRight.IndexOf(r).ToString(),
                    NameA = "X",
                    NameB = r.ActionName,
                    TimeA = 0,
                    TimeB = r.ActionTime
                });
            }*/
            list.Add(new CompareDoubleMod()
            {
                NameA = "a-b总和", TimeA = list.Sum(i => i.TimeA), TimeB = list.Sum(i => i.TimeB)
            });
            dgv3.DataSource = list;
        }

        private void tsBtnPause_Click(object sender, EventArgs e)
        {
            if (!vlcPlayer.IsPlaying()) return;
            vlcPlayer.Pause();
            timer2.Stop();
        }

        private void tsBtnPause2_Click(object sender, EventArgs e)
        {
            if (!vlcPlayer2.IsPlaying()) return;
            vlcPlayer2.Pause();
            timer2.Stop();
        }

        private void toolStripDropDownButton2_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            var stepNum = e.ClickedItem.Text;
            toolStripDropDownButton2.Text = stepNum;
            var fps = vlcPlayer2.GetFps(); //获取视频的码率29.97003
            if (fps == 0)
                fps = 25;
            rate2 = 1 * decimal.Parse(stepNum) / (decimal)fps;
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            if (vlcPlayer2.IsPlaying())
            {
                Player2Pause();
                is_playing2 = false;
            }

            var playingTime = vlcPlayer2.GetPlayTime();
            if ((playingTime - rate2) * 1000 > trackBar2.Minimum)
            {
                vlcPlayer2.SetPlayTime(playingTime - rate2);
                trackBar2.Value = (int)vlcPlayer2.GetPlayTime() * 1000;
                tbVideoTime2.Text = string.Format("{0}/{1}", GetTimeString(vlcPlayer2.GetPlayTime() * 1000), GetTimeString(vlcPlayer2.Duration() * 1000));
            }
            else
            {
                vlcPlayer2.SetPlayTime(trackBar2.Minimum);
                trackBar2.Value = trackBar2.Minimum;
                tbVideoTime2.Text = string.Format("{0}/{1}", GetTimeString(vlcPlayer2.GetPlayTime() * 1000), GetTimeString(vlcPlayer2.Duration() * 1000));
            }

            if (vlcPlayer2.IsPlaying()) vlcPlayer2.Pause();
            is_playing2 = false;
        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            if (vlcPlayer2.IsPlaying())
            {
                Player2Pause();
                is_playing2 = false;
            }

            var playingTime = vlcPlayer2.GetPlayTime();
            if (playingTime + rate2 < vlcPlayer2.Duration() - 0.2m)
            {
                vlcPlayer2.SetPlayTime(playingTime + rate2);
                trackBar2.Value = (int)vlcPlayer2.GetPlayTime() * 1000;
                tbVideoTime2.Text = string.Format("{0}/{1}", GetTimeString((playingTime + rate2) * 1000), GetTimeString(vlcPlayer2.Duration() * 1000));
            }
            else
            {
                vlcPlayer2.SetPlayTime(vlcPlayer2.Duration() - 0.1m);
                trackBar2.Value = trackBar2.Maximum;
                tbVideoTime2.Text = string.Format("{0}/{1}", GetTimeString(vlcPlayer2.Duration() * 1000), GetTimeString(vlcPlayer2.Duration() * 1000));
            }

            if (vlcPlayer2.IsPlaying()) vlcPlayer2.Pause();
            is_playing2 = false;
        }
    }
}
