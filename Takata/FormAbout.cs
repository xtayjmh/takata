﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Takata
{
    public partial class FormAbout : Form
    {
        public FormAbout()
        {
            InitializeComponent();
            var updateInfo = File.ReadAllText($"{Environment.CurrentDirectory}\\UpdateLog.txt");
            textBoxX1.Text = updateInfo;
        }
    }
}
