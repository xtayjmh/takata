﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DAL;
using DAL.SystemManage;
using Model;
using Takata.Properties;

namespace Takata
{
    public partial class FormADoublePlay : Form
    {
  
        private string address;
        private bool is_playing;
        private bool is_playing2;

        private bool media_is_open;//标记媒体文件是否打开，若未打开则tsbtn_play读ini打开之前的文件，若打开则跳过这步(避免每次都打开文件造成屏幕跳动)
        private bool media_is_open2;
        private decimal Speed = 1;
        private decimal Speed2 = 1;

        private int step = 1;
        private int step2 = 1;

        private int startPoint1;//对比的起始点
        private int endPoint1 = 10000000;//对比的结束点
        private int startPoint2;//对比的起始点
        private int endPoint2 = 10000000;//对比的结束点
        private t_Video video1;
        private t_Video video2;
        private List<t_ActionType> actionTypes = new List<t_ActionType>();
        private int StartIndex1 = 999;
        private int EndIndex1 = 999;
        private int StartIndex2 = 999;
        private int EndIndex2 = 999;
        private List<t_Action> actions1 = new List<t_Action>();
        private List<t_Action> actions2 = new List<t_Action>();
        public  AxAPlayer3Lib.AxPlayer _VlcPlayer;
        public  AxAPlayer3Lib.AxPlayer _VlcPlayer2;
        private static int VScrollIndex = 0;
        private static int VScrollIndex2 = 0;
        private List<t_Action> invalidActions1;
        private List<t_Action> invalidActions2;
        private List<t_Action> validActions1;
        private List<t_Action> validActions2;

        ComponentResourceManager resources = new ComponentResourceManager(typeof(FormADoublePlay));
        public FormADoublePlay()
        {
            InitializeComponent();
            address = Environment.CurrentDirectory;
            actionTypes = Common.GetList<t_ActionType>(null);
            dgv1.AutoGenerateColumns = false;
            dgv2.AutoGenerateColumns = false;
            var list = Common.GetList<t_PlaySpeed>(null);
            list.ForEach(i =>
            {
                tSDDBtnSpeed.DropDownItems.Add(i.PlaySpeed.ToString());
                tsDDLSpeed2.DropDownItems.Add(i.PlaySpeed.ToString());
            });
            _VlcPlayer = this.vlcPlayer;
            _VlcPlayer2 = this.vlcPlayer2;
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!is_playing) return;
            var duration = vlcPlayer.GetDuration();
            var playTime = vlcPlayer.GetPosition();
            if (trackBar1.Value == trackBar1.Maximum || playTime >= trackBar1.Maximum)
            {
                vlcPlayer.Pause();
                timer1.Stop();
                tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(duration), GetTimeString(duration));
                trackBar1.Value = trackBar1.Maximum;
            }
            else
            {
                var skip = false;
                if (invalidActions1 == null) return;
                invalidActions1.ForEach(i =>
                {
                    if (playTime >= i.StartTime && playTime <= i.EndTime)
                    {
                        vlcPlayer.Pause();
                        vlcPlayer.SetPosition((int)i.EndTime + 10);
                        vlcPlayer.SetConfig(104, "100");
                        vlcPlayer.Play();
                        skip = true;
                    }
                });

                if (endPoint1 != 10000000 && playTime >= endPoint1)
                {
                    //timer1.Stop();
                    trackBar1.Value = (int)(startPoint1);
                    vlcPlayer.SetPosition(startPoint1);
                    //vlcPlayer.Pause();  
                }
                else
                {
                    trackBar1.Value = playTime;
                    tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(trackBar1.Value), GetTimeString(trackBar1.Maximum));
                }
                
                if (skip) return;
                SetPlayer1SelectionByTime();


            }
        }
        private string GetTimeString(int val)
        {
            var currentMS = val;
            val = val / 1000;
            var hour = val / 3600;
            val %= 3600;
            var minute = val / 60;
            var second = val % 60;

            int frameInterval = 40;
            int.TryParse(vlcPlayer.GetConfig(117), out frameInterval);

            var allTime = vlcPlayer.GetDuration();
            if (frameInterval == 0)
                frameInterval = 40;
            var frame = (int)(currentMS % 1000 / frameInterval); //帧
            return $"{hour:00}:{minute:00}:{(int)second:00}({(int)frame})";
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            if (vlcPlayer.GetState() == 0)
                return;
            if (is_playing)
            {
                PlayerPause();
            }
            var duration = vlcPlayer.GetDuration();
            vlcPlayer.SetPosition(trackBar1.Value >= duration ? duration - 180 : trackBar1.Value);
            trackBar1.Value = (int)vlcPlayer.GetPosition();

        }
        private void tSBtn_play_ButtonClick(object sender, EventArgs e)
        {
            if (vlcPlayer.GetState() == 0)
                return;
            if (endPoint1 != 10000000) //如果有结束点
            {
                vlcPlayer.SetPosition(startPoint1);
                trackBar1.Value = startPoint1;
                PlayerPlay();
            }

            else
            {
                PlayerPlay();
            }
        }

        public void Player2Play()
        {
            vlcPlayer2.Play();
            timer2.Start();
            int tempSpeed = (int)(Speed2 * 100);
            vlcPlayer2.SetConfig(104, tempSpeed.ToString()); //设置播放速度
            is_playing2 = true;
        }
        public  void PlayerPlay()
        {
            vlcPlayer.Play();
            timer1.Start();
            int tempSpeed = (int)(Speed * 100);
            vlcPlayer.SetConfig(104, tempSpeed.ToString()); //设置播放速度
            is_playing = true;
        }
        private void tSBtn_openfile_Click(object sender, EventArgs e)
        {
            if (OpenFile(vlcPlayer))
            {
                if (vlcPlayer.GetState() == 5)
                {
                    vlcPlayer.Pause();
                    timer1.Stop();
                }
                lblProcess1.Visible = false;
            }
        }
        private bool OpenFile(AxAPlayer3Lib.AxPlayer player , bool isSecondPlayer = false)
        {
            //bool isSame = false;
            openFileDialog1.FileName = "";
            if (this.openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                StreamWriter s = new StreamWriter(address + "\\Menu.ini", true);
                s.WriteLine(openFileDialog1.FileName);
                s.Flush();
                s.Close();
                string[] text = File.ReadAllLines(address + "\\Menu.ini");
                int row = text.Length;//行
                int rowcount;
                string[] tempdata = new string[] { "", "", "" };
                if (row > 3)// 若记录行数超过3个，则先记录后三个数据，再重新建一个Menu.ini（清除数据）,接着讲记录的后三个数据写入
                {
                    StreamReader sr1 = new StreamReader(address + "\\Menu.ini", true);
                    while (sr1.Peek() > -1)
                    {
                        sr1.ReadLine();//空读，跳过原始的第一个数据，从第二个数据开始读
                        for (rowcount = 0; rowcount < 3; rowcount++)
                        {
                            tempdata[rowcount] = sr1.ReadLine();
                        }
                    }
                    sr1.Close();
                    FileStream fs = new FileStream(address + "\\Menu.ini", FileMode.Create, FileAccess.Write);
                    fs.Close();
                    StreamWriter s1 = new StreamWriter(address + "\\Menu.ini", true);
                    s1.WriteLine(tempdata[0]);
                    s1.WriteLine(tempdata[1]);
                    s1.WriteLine(tempdata[2]);
                    s1.Flush();
                    s1.Close();
                }
                // StreamReader sr2 = new StreamReader(address + "\\Menu.ini", true);
                // while(sr2.Pee)
                player.Open(openFileDialog1.FileName);

                if (isSecondPlayer)
                {
                    trackBar2.SetRange(0, (int)vlcPlayer2.GetDuration());
                    trackBar2.Value = 0;
                    media_is_open2 = true;
                    var filePath = openFileDialog1.FileName;
                    lblFileName2.Text = Path.GetFileName(filePath);
                    toolTipFileName2.SetToolTip(lblFileName2, filePath);
                    VScrollIndex2 = 0;
                }
                else
                {
                    trackBar1.SetRange(0, (int)vlcPlayer.GetDuration());
                    trackBar1.Value = 0;
                    media_is_open = true;
                    var filePath = openFileDialog1.FileName;
                    lblFileName1.Text = Path.GetFileName(filePath);
                    toolTipFileName.SetToolTip(lblFileName1, filePath);
                    VScrollIndex = 0;
                }
                return SetVideoInfo(openFileDialog1.FileName, isSecondPlayer);

            }
            return false;
        }
        private void tSB_backward_Click(object sender, EventArgs e)
        {
            if (vlcPlayer.GetState() == 0)
                return;
            vlcPlayer.Pause();
            int time = (int)vlcPlayer.GetPosition() - 5000;
            if (time > 0)
            {
                vlcPlayer.SetPosition(time);
            }
            else
            {
                vlcPlayer.SetPosition(0);
            }
            vlcPlayer.Play();
            trackBar1.Value = (int)vlcPlayer.GetPosition();
            timer1.Start();
        }
        private void toolStripDropDownButton1_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            int frameInterval = 40;
            int.TryParse(vlcPlayer.GetConfig(117), out frameInterval);
            int factor = frameInterval / 20;  //40是因为数据库中速度的分为40等分
            tSDDBtnSpeed.Text = e.ClickedItem.Text;
            Speed = decimal.Parse(e.ClickedItem.Text);
        }
        public void PlayerPause()
        {
            if (vlcPlayer.GetState() == 5)
                vlcPlayer.Pause();
            timer1.Stop();
        }
        public void Player2Pause()
        {
            if (vlcPlayer2.GetState() == 5)
                vlcPlayer2.Pause();
            timer2.Stop();
        }

        private void toolStripButton1_MouseDown(object sender, EventArgs e)
        {
            if (vlcPlayer.GetState() == 0)
                return;
            if (vlcPlayer.GetState() == 5)
            {
                timer1.Stop();
                vlcPlayer.Pause();
                is_playing = false;
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (vlcPlayer.GetState() == 0)
                return;
            var playingTime = 0;

            if (vlcPlayer.GetConfig(111) == "1")
            {
                vlcPlayer.SetConfig(113, "-" + step.ToString());
            }
            else
            {
                int frameInterval = 40;
                int.TryParse(vlcPlayer.GetConfig(117), out frameInterval);
                playingTime = vlcPlayer.GetPosition();// vlcPlayer.GetPlayTime();
                playingTime = playingTime - step * frameInterval; //updated by Ted
                if (playingTime > trackBar1.Minimum)
                {
                    vlcPlayer.SetPosition(playingTime);
                }
                else
                {
                    vlcPlayer.SetPosition(trackBar1.Minimum);
                }
            }
            playingTime = vlcPlayer.GetPosition();
            if (playingTime > trackBar1.Minimum)
            {
                trackBar1.Value = playingTime;
                tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(playingTime), GetTimeString(vlcPlayer.GetDuration()));
            }
            else
            {
                trackBar1.Value = trackBar1.Minimum;
                tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(playingTime), GetTimeString(vlcPlayer.GetDuration()));
            }

            if (vlcPlayer.GetState() == 5) vlcPlayer.Pause();
            is_playing = false;
 
        }
        private void toolStripButton2_MouseDown(object sender, EventArgs e)
        {
            if (vlcPlayer.GetState() == 0)
                return;
            if (vlcPlayer.GetState() == 5)
            {
                timer1.Stop();
                vlcPlayer.Pause();
                is_playing = false;
            }
        }
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (vlcPlayer.GetState() == 0)
                return;
            var playingTime = 0;

            if (vlcPlayer.GetConfig(109) == "1")
            {
                vlcPlayer.SetConfig(113, step.ToString());
            }

            else
            {
                int frameInterval = 40;
                int.TryParse(vlcPlayer.GetConfig(117), out frameInterval);
                playingTime = vlcPlayer.GetPosition();// vlcPlayer.GetPlayTime();
                playingTime = playingTime + step * frameInterval; //updated by Ted
                if (playingTime < vlcPlayer.GetDuration() - 200)
                {
                    vlcPlayer.SetPosition(playingTime);
                }
                else
                {
                    vlcPlayer.SetPosition(vlcPlayer.GetDuration() - 100);
                }
            }
            playingTime = vlcPlayer.GetPosition();

            if (playingTime < vlcPlayer.GetDuration() - 200)
            {
                trackBar1.Value = playingTime;
                tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(playingTime), GetTimeString(vlcPlayer.GetDuration()));
            }
            else
            {
                trackBar1.Value = trackBar1.Maximum;
                tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(playingTime), GetTimeString(vlcPlayer.GetDuration()));
            }

            if (vlcPlayer.GetState() == 5) vlcPlayer.Pause();
            is_playing = false;
        }

        private void tSDDBtnStep_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            var stepNum = e.ClickedItem.Text;
            tSDDBtnStep.Text = stepNum;
            step = int.Parse(stepNum);
        }
        private void tSB_forward_Click(object sender, EventArgs e)
        {
            if (vlcPlayer.GetState() == 0)
                return;
            vlcPlayer.Pause();
            int time = (int)vlcPlayer.GetPosition() + 5000;
            if (time < trackBar1.Maximum)
            {
                vlcPlayer.SetPosition(time);
                trackBar1.Value = (int)vlcPlayer.GetPosition();
                vlcPlayer.Play();
                timer1.Start();
            }
            else
            {
                timer1.Stop();
                var duration = vlcPlayer.GetDuration();
                vlcPlayer.SetPosition(duration - 1);
                trackBar1.Value = trackBar1.Maximum;
            }

        }
        private void tSBtn_stop_Click(object sender, EventArgs e)
        {
            if (vlcPlayer.GetState() == 0)
                return;
            if (vlcPlayer.GetState() == 5)
            {
                vlcPlayer.Pause();
            }

            timer1.Stop();
            is_playing = false;
            media_is_open = true;
            trackBar1.Value = 0;
            VScrollIndex = 0;
            vlcPlayer.SetPosition(0);

            tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(trackBar1.Value), GetTimeString(trackBar1.Maximum));
            if (vlcPlayer.GetState() == 5) vlcPlayer.Pause();
        }

 

        private void Form1_Load(object sender, EventArgs e)
        {
            if (!File.Exists(address + "\\Menu.ini"))
            {
                FileStream fs = new FileStream(address + "\\Menu.ini", FileMode.Create, FileAccess.Write);
                fs.Close();
            }

            vlcPlayer.SetConfig(105, "0");
            vlcPlayer.SetConfig(120, "1");
            vlcPlayer.SetConfig(37, "b:\\aaaaa1.jpg");
            vlcPlayer.SetConfig(209, "1");
            vlcPlayer.SetConfig(707, "3");
            vlcPlayer.SetConfig(8, "0");

            vlcPlayer2.SetConfig(105, "0");
            vlcPlayer2.SetConfig(120, "1");
            vlcPlayer2.SetConfig(37, "b:\\aaaaa1.jpg");
            vlcPlayer2.SetConfig(209, "1");
            vlcPlayer2.SetConfig(707, "3");
            vlcPlayer2.SetConfig(8, "0");


            tbVideoTime.Text = "00:00:00/00:00:00";
            tbVideoTime2.Text = "00:00:00/00:00:00";
            is_playing = false;
            media_is_open = false;
            this.Size = new Size(800, 600);
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            if (OpenFile(vlcPlayer2, true))
            {
                if (vlcPlayer.GetState() == 5)
                {
                    vlcPlayer2.Pause();
                    timer2.Stop();
                }
                lblProcess2.Visible = false;
            }
        }

        private void timer2_Tick(object sender, EventArgs e)//第二个播放器的定时器
        {
            if (!is_playing2) return;
            var playTime = vlcPlayer2.GetPosition();
            if (trackBar2.Value == trackBar2.Maximum || playTime >= trackBar2.Maximum)
            {
                vlcPlayer2.Pause();
                timer2.Stop();
                tbVideoTime2.Text = string.Format("{0}/{1}", GetTimeString(trackBar2.Maximum), GetTimeString(trackBar2.Maximum));
                trackBar2.Value = trackBar2.Maximum;
            }
            else
            {
                var skip = false;
                if (invalidActions2 == null) return;
                invalidActions2.ForEach(i =>
                {
                    if (playTime >= i.StartTime && playTime <= i.EndTime)
                    {
                        vlcPlayer2.Pause();
                        vlcPlayer2.SetPosition((int)i.EndTime + 10);
                        vlcPlayer2.SetConfig(104, "100");
                        vlcPlayer2.Play();
                        skip = true;
                    }
                });
                if (endPoint2 != 10000000 && playTime >= endPoint2)
                {
                    //timer2.Stop();
                    trackBar2.Value = startPoint2;
                    vlcPlayer2.SetPosition(startPoint2);
                    //vlcPlayer2.Pause();   
                }
                else
                {
                    trackBar2.Value = playTime;
                    tbVideoTime2.Text = string.Format("{0}/{1}", GetTimeString(trackBar2.Value), GetTimeString(trackBar2.Maximum));
                }
                
                if (skip) return;
                SetPlayer2SelectionByTime();

            }
        }

        private void tSBtn_play2_ButtonClick(object sender, EventArgs e)
        {
            if (vlcPlayer2.GetState() == 0)
                return;
            if (endPoint2 != 10000000) //如果有结束点
            {
                vlcPlayer2.SetPosition(startPoint2);
                trackBar2.Value = startPoint2;
                Player2Play();
            }

            else
            {
                Player2Play();
            }
        }

        private void tbForPanel2_Scroll(object sender, EventArgs e)
        {
            if (vlcPlayer2.GetState() == 0)
                return;

            if (is_playing2)
            {
                PlayerPause();
            }
            var duration = vlcPlayer2.GetDuration();
            vlcPlayer2.SetPosition(trackBar2.Value >= duration ? duration - 180 : trackBar2.Value);
            trackBar2.Value = vlcPlayer2.GetPosition();

        }

        private void tSBtn_stop2_Click(object sender, EventArgs e)
        {
            if (vlcPlayer2.GetState() == 0)
                return;
            if (vlcPlayer2.GetState() == 5)
            {
                vlcPlayer2.Pause();
            }

            timer2.Stop();
            is_playing2 = false;
            media_is_open2 = true;
            trackBar2.Value = 0;
            VScrollIndex2 = 0;
            vlcPlayer2.SetPosition(0);

            tbVideoTime2.Text = string.Format("{0}/{1}", GetTimeString(trackBar2.Value), GetTimeString(trackBar2.Maximum));
            if (vlcPlayer2.GetState() == 5) vlcPlayer2.Pause();

        }

        private void btnForward2_Click(object sender, EventArgs e)
        {
            if (vlcPlayer2.GetState() == 0)
                return;
            vlcPlayer2.Pause();
            int time = (int)vlcPlayer2.GetPosition() + 5000;
            if (time < trackBar2.Maximum)
            {
                vlcPlayer2.SetPosition(time);
                trackBar2.Value = (int)vlcPlayer2.GetPosition();
                vlcPlayer2.Play();
                timer2.Start();
            }
            else
            {
                timer2.Stop();
                var duration = vlcPlayer2.GetDuration();
                vlcPlayer2.SetPosition(duration - 1);
                trackBar2.Value = trackBar2.Maximum;
            }
        }

        private void btnBackward2_Click(object sender, EventArgs e)
        {
            if (vlcPlayer2.GetState() == 0)
                return;
            vlcPlayer2.Pause();
            int time = (int)vlcPlayer2.GetPosition() - 5000;
            if (time > 0)
            {
                vlcPlayer2.SetPosition(time);
            }
            else
            {
                vlcPlayer2.SetPosition(0);
            }
            vlcPlayer2.Play();
            trackBar2.Value = (int)vlcPlayer2.GetPosition();
            timer2.Start();
        }

        private void tsDDLSpeed2_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            var stepNum = e.ClickedItem.Text;
            tsDDLSpeed2.Text = stepNum;
            step2 = int.Parse(stepNum);
        }

        private void btnContrastStart_Click(object sender, EventArgs e)
        {
            if (vlcPlayer2.GetState() == 0)
                return;
            ClearSelection2();
            if (vlcPlayer2.GetState() == 5)
            {
                vlcPlayer2.Pause();
                timer2.Stop();
            }

            if (dgv2.SelectedRows.Count == 0) return;
            var startVal = (int)(decimal.Parse(dgv2.SelectedRows[0].Cells["StartTime2"].Value.ToString()));
            StartIndex2 = dgv2.SelectedRows[0].Index;
            startPoint2 = startVal;
            trackBar2.Value = startPoint2;
            vlcPlayer2.SetPosition(startVal);
            if (vlcPlayer2.GetState() == 5) vlcPlayer2.Pause();
            btnContrastEnd.Enabled = true;
        }

        private void btnContrastEnd_Click(object sender, EventArgs e)
        {
            if (vlcPlayer2.GetState() == 0)
                return;
            if (vlcPlayer2.GetState() == 5) vlcPlayer2.Pause();
            timer2.Stop();
            if (dgv2.SelectedRows.Count == 0) return;
            var endVal = (int)decimal.Parse(dgv2.SelectedRows[0].Cells["EndTime2"].Value.ToString());
            EndIndex2 = dgv2.SelectedRows[0].Index;
            endPoint2 = endVal;
            var pct = endPoint2 - startPoint2;
            var duration = vlcPlayer2.GetDuration();
            lblProcess2.Width = (int)((float)pct / (float)duration * 581);
            lblProcess2.Location = new Point((int)((float)startPoint2 / (float)duration * 581) + 602, lblProcess2.Location.Y);
            lblProcess2.Visible = true;
            lblProcess2.BringToFront();
            vlcPlayer2.SetPosition(startPoint2);
            trackBar2.Value = (int)(startPoint2);
            for (int i = StartIndex2; i <= EndIndex2; i++)
            {
                dgv2.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(255, 0, 120, 215);
            }
        }
        private bool SetVideoInfo(string filePath, bool isSecondPlayer = false)
        {
            var fileName = Path.GetFileName(filePath);
            var mode = Common.GetSingleModel<t_Video>(i => i.FileName == fileName);
            if (mode == null) return false;
            if (isSecondPlayer) LoadActionsForDgv2(mode.VideoId);
            else
            {
                LoadActionsForDgv1(mode.VideoId);
            }
            return true;
        }
        private void LoadActionsForDgv1(int videoId)
        {
            var list = ActionManage.GetActions(videoId).OrderBy(i => i.EndTime).ToList();
            invalidActions1 = list.Where(i => i.TypeId != 2).ToList();
            validActions1 = list.Where(i => i.TypeId == 2).ToList();

            actions1 = list;
            dgv1.DataSource = list;
            //1无效2有效3无关
            for (var i = 0; i < dgv1.Rows.Count; i++)
            {
                var row = dgv1.Rows[i];
                switch ((int)row.Cells["TypeId"].Value)
                {
                    case 1:
                        row.Cells[8].Style.BackColor = Color.Red;
                        row.Cells[8].Value = "×";
                        break;
                    case 2:
                        row.Cells[8].Style.BackColor = Color.GreenYellow;
                        row.Cells[8].Value = "○";
                        break;
                    case 3:
                        row.Cells[8].Style.BackColor = Color.Yellow;
                        row.Cells[8].Value = "△";
                        break;
                }

                if (row.Cells["ActionTypeId"].Value != null && !string.IsNullOrEmpty(row.Cells["ActionTypeId"].Value.ToString()))
                {
                    var actionType = actionTypes.FirstOrDefault(j => j.ActionTypeId == (int)row.Cells["ActionTypeId"].Value);
                    if (actionType != null)
                    {
                        var color = actionType.Color.Split(',');
                        row.Cells[7].Value = actionType.ChineseName;
                        row.Cells[7].Style.BackColor = Color.FromArgb(int.Parse(color[0]), int.Parse(color[1]), int.Parse(color[2]), int.Parse(color[3]));
                    }
                }
                if (new[] { "1", "3" }.Contains(row.Cells["TypeId"].Value.ToString())) row.Cells[5].Value = decimal.Parse("0.00");//无关录像ST为0
            }
            dgv1.ClearSelection();
        }
        private void LoadActionsForDgv2(int videoId)
        {
            var list = ActionManage.GetActions(videoId).OrderBy(i => i.EndTime).ToList();
            invalidActions2 = list.Where(i => i.TypeId != 2).ToList();
            validActions2 = list.Where(i => i.TypeId == 2).ToList();
            actions2 = list;
            dgv2.DataSource = list;
            //1无效2有效3无关
            for (var i = 0; i < dgv2.Rows.Count; i++)
            {
                var row = dgv2.Rows[i];
                switch ((int)row.Cells["TypeId2"].Value)
                {
                    case 1:
                        row.Cells[8].Style.BackColor = Color.Red;
                        row.Cells[8].Value = "×";
                        break;
                    case 2:
                        row.Cells[8].Style.BackColor = Color.GreenYellow;
                        row.Cells[8].Value = "○";
                        break;
                    case 3:
                        row.Cells[8].Style.BackColor = Color.Yellow;
                        row.Cells[8].Value = "△";
                        break;
                }

                if (row.Cells["ActionTypeId2"].Value != null && !string.IsNullOrEmpty(row.Cells["ActionTypeId2"].Value.ToString()))
                {
                    var actionType = actionTypes.FirstOrDefault(j => j.ActionTypeId == (int)row.Cells["ActionTypeId2"].Value);
                    if (actionType != null)
                    {
                        var color = actionType.Color.Split(',');
                        row.Cells[7].Value = actionType.ChineseName;
                        row.Cells[7].Style.BackColor = Color.FromArgb(int.Parse(color[0]), int.Parse(color[1]), int.Parse(color[2]), int.Parse(color[3]));
                    }
                }
                if (new[] { "1", "3" }.Contains(row.Cells["TypeId2"].Value.ToString())) row.Cells[5].Value = decimal.Parse("0.00");//无关录像ST为0
            }
            dgv2.ClearSelection();
        }

        private void btnCancle2_Click(object sender, EventArgs e)
        {
            if (vlcPlayer2.GetState() == 0)
                return;
            startPoint2 = 0;
            endPoint2 = 10000000;
            lblProcess2.Visible = false;
            lblProcess2.SendToBack();
            ClearSelection2();
            if (vlcPlayer2.GetState() == 5) vlcPlayer2.Pause();
        }

        private void btnCancle1_Click(object sender, EventArgs e)
        {
            if (vlcPlayer.GetState() == 0)
                return;
            startPoint1 = 0;
            endPoint1 = 10000000;
            lblProcess1.Visible = false;
            lblProcess1.SendToBack();
            ClearSelection1();
            if (vlcPlayer.GetState() == 5) vlcPlayer.Pause();
        }

        private void ClearSelection1()
        {
            for (int i = 0; i < dgv1.RowCount; i++)
            {
                dgv1.Rows[i].DefaultCellStyle.BackColor = Color.White;
            }
        }
        private void ClearSelection2()
        {
            for (int i = 0; i < dgv2.RowCount; i++)
            {
                dgv2.Rows[i].DefaultCellStyle.BackColor = Color.White;
            }
        }

        private void btnSetStart1_Click(object sender, EventArgs e)
        {
            if (vlcPlayer.GetState() == 0)
                return;
            ClearSelection1();
            if (vlcPlayer.GetState() == 5)
            {
                vlcPlayer.Pause();
                timer1.Stop();
            }

            if (dgv1.SelectedRows.Count == 0) return;
            var startVal = (int)(decimal.Parse(dgv1.SelectedRows[0].Cells["StartTime"].Value.ToString()));
            StartIndex1 = dgv1.SelectedRows[0].Index;
            startPoint1 = startVal;
            trackBar1.Value = startPoint1;
            vlcPlayer.SetPosition(startVal);
            if (vlcPlayer.GetState() == 5) vlcPlayer.Pause();
            btnSetEnd1.Enabled = true;
        }

        private void btnSetEnd1_Click(object sender, EventArgs e)
        {
            if (vlcPlayer.GetState() == 0)
                return;
            if (vlcPlayer.GetState() == 5) vlcPlayer.Pause();
            timer1.Stop();
            if (dgv1.SelectedRows.Count == 0) return;
            var endVal = (int)decimal.Parse(dgv1.SelectedRows[0].Cells["EndTime"].Value.ToString());
            EndIndex1 = dgv1.SelectedRows[0].Index;
            endPoint1 = (int)endVal;
            var pct = endPoint1 - startPoint1;
            var duration = vlcPlayer.GetDuration();
            lblProcess1.Width = (int)((float)pct / (float)duration * 581);
            lblProcess1.Location = new Point((int)((float)startPoint1 / (float)duration * 581) + 19, lblProcess1.Location.Y);
            lblProcess1.Visible = true;
            lblProcess1.BringToFront();
            vlcPlayer.SetPosition(startPoint1);
            trackBar1.Value = startPoint1;
            for (int i = StartIndex1; i <= EndIndex1; i++)
            {
                dgv1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(255, 0, 120, 215);
            }
        }

        public void CompareActions()
        {
            if (vlcPlayer.GetState() == 0|| vlcPlayer2.GetState() == 0)
                return;

            var list = new List<CompareDoubleMod>();
            var listLeft = actions1.Skip(StartIndex1).Take(EndIndex1 + 1 - StartIndex1).Where(i => i.TypeId == 2).ToList();//去掉无关录像
            var listRight = actions2.Skip(StartIndex2).Take(EndIndex2 + 1 - StartIndex2).Where(i => i.TypeId == 2).ToList();

            listLeft.ForEach(l =>
            {
                if (!listRight.Select(i => i.ActionName).Contains(l.ActionName))
                {
                    list.Add(new CompareDoubleMod()
                    {
                        IdA = (listLeft.IndexOf(l) + 1).ToString(),
                        IdB = "X",
                        NameA = l.ActionName,
                        NameB = "X X X X",
                        TimeA = l.ManualStandardTime / 1000,
                        TimeB = 0
                    });
                }
                else
                {
                    listRight.ForEach(r =>
                    {
                        if (l.ActionName == r.ActionName) //动作标记名称相同
                        {
                            list.Add(new CompareDoubleMod()
                            {
                                IdA = (listLeft.IndexOf(l) + 1).ToString(),
                                IdB = (listRight.IndexOf(r) + 1).ToString(),
                                NameA = l.ActionName,
                                NameB = r.ActionName,
                                TimeB = r.ManualStandardTime / 1000,
                                TimeA = l.ManualStandardTime / 1000
                            });
                        }

                    });
                }
            });
            listRight.ForEach(i =>
            {
                if (!listLeft.Select(j => j.ActionName).Contains(i.ActionName))
                {
                    list.Add(new CompareDoubleMod()
                    {
                        IdA = "X",
                        IdB = listRight.IndexOf(i).ToString(),
                        NameA = "X X X X",
                        NameB = i.ActionName,
                        TimeA = 0,
                        TimeB = i.ManualStandardTime / 1000
                    });
                }
            });
            list.Add(new CompareDoubleMod()
            {
                NameA = "a-b总和",
                TimeA = list.Sum(i => i.TimeA),
                TimeB = list.Sum(i => i.TimeB)
            });
            dgv3.DataSource = list;
        }

        private void tsBtnPause_Click(object sender, EventArgs e)
        {
            if (vlcPlayer.GetState() == 0)
                return;
            if (vlcPlayer.GetState() == 5)
                vlcPlayer.Pause();
            timer2.Stop();
        }

        private void tsBtnPause2_Click(object sender, EventArgs e)
        {
            if (vlcPlayer2.GetState() == 0)
                return;
            if (vlcPlayer2.GetState() == 5)
                vlcPlayer2.Pause();
            timer2.Stop();
        }

        private void toolStripDropDownButton2_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            int frameInterval = 40;
            int.TryParse(vlcPlayer.GetConfig(117), out frameInterval);
            int factor = frameInterval / 20;  //40是因为数据库中速度的分为40等分
            toolStripDropDownButton2.Text = e.ClickedItem.Text;
            Speed2 = decimal.Parse(e.ClickedItem.Text);

 
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            if (vlcPlayer2.GetState() == 0)
                return;
            var playingTime = 0;

            if (vlcPlayer2.GetConfig(111) == "1")
            {
                vlcPlayer2.SetConfig(113, "-" + step2.ToString());
            }
            else
            {
                int frameInterval = 40;
                int.TryParse(vlcPlayer2.GetConfig(117), out frameInterval);
                playingTime = vlcPlayer2.GetPosition();// vlcPlayer.GetPlayTime();
                playingTime = playingTime - step * frameInterval; //updated by Ted
                if (playingTime > trackBar2.Minimum)
                {
                    vlcPlayer2.SetPosition(playingTime);
                }
                else
                {
                    vlcPlayer2.SetPosition(trackBar2.Minimum);
                }
            }
            playingTime = vlcPlayer2.GetPosition();
            if (playingTime > trackBar2.Minimum)
            {
                trackBar2.Value = playingTime;
                tbVideoTime2.Text = string.Format("{0}/{1}", GetTimeString(playingTime), GetTimeString(vlcPlayer2.GetDuration()));
            }
            else
            {
                trackBar2.Value = trackBar2.Minimum;
                tbVideoTime2.Text = string.Format("{0}/{1}", GetTimeString(playingTime), GetTimeString(vlcPlayer2.GetDuration()));
            }

            if (vlcPlayer2.GetState() == 5) vlcPlayer2.Pause();
            is_playing2 = false;

             
        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            if (vlcPlayer2.GetState() == 0)
                return;

            var playingTime = 0;

            if (vlcPlayer2.GetConfig(109) == "1")
            {
                vlcPlayer2.SetConfig(113, step2.ToString());
            }

            else
            {
                int frameInterval = 40;
                int.TryParse(vlcPlayer2.GetConfig(117), out frameInterval);
                playingTime = vlcPlayer2.GetPosition();// vlcPlayer.GetPlayTime();
                playingTime = playingTime + step * frameInterval; //updated by Ted
                if (playingTime < vlcPlayer2.GetDuration() - 200)
                {
                    vlcPlayer2.SetPosition(playingTime);
                }
                else
                {
                    vlcPlayer2.SetPosition(vlcPlayer2.GetDuration() - 100);
                }
            }
            playingTime = vlcPlayer2.GetPosition();

            if (playingTime < vlcPlayer2.GetDuration() - 200)
            {
                trackBar2.Value = playingTime;
                tbVideoTime2.Text = string.Format("{0}/{1}", GetTimeString(playingTime), GetTimeString(vlcPlayer2.GetDuration()));
            }
            else
            {
                trackBar2.Value = trackBar2.Maximum;
                tbVideoTime2.Text = string.Format("{0}/{1}", GetTimeString(playingTime), GetTimeString(vlcPlayer2.GetDuration()));
            }

            if (vlcPlayer2.GetState() == 5) vlcPlayer2.Pause();
            is_playing2 = false;

        }

        private void vlcPlayer_OnMessage(object sender, AxAPlayer3Lib._IPlayerEvents_OnMessageEvent e)
        {
            if (e.nMessage == 0x0201)
            {

                if (vlcPlayer.GetState() == 5)
                {
                    timer1.Stop();
 
                    vlcPlayer.Pause();
                }
                else
                {
                    PlayerPlay();
                }
            }
        }

        private void vlcPlayer_OnOpenSucceeded(object sender, EventArgs e)
        {
        
                vlcPlayer.Pause();
      
            timer1.Stop();
            is_playing = false;
            media_is_open = true;
            trackBar1.Value = 0;

            vlcPlayer.SetPosition(0);

            tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(trackBar1.Value), GetTimeString(trackBar1.Maximum));
            if (vlcPlayer.GetState() == 5) vlcPlayer.Pause();

            var tbMax = vlcPlayer.GetDuration();
            vlcPlayer.SetConfig(103, tbMax.ToString());
            trackBar1.SetRange(0, tbMax);
            trackBar1.Value = 0;
        }

        private void vlcPlayer2_OnMessage(object sender, AxAPlayer3Lib._IPlayerEvents_OnMessageEvent e)
        {
            if (e.nMessage == 0x0201)
            {

                if (vlcPlayer2.GetState() == 5)
                {
                    timer2.Stop();

                    vlcPlayer2.Pause();
                }
                else
                {
                    Player2Play();
                }
            }
        }

        private void vlcPlayer2_OnOpenSucceeded(object sender, EventArgs e)
        {
      
                vlcPlayer2.Pause();
    
            timer2.Stop();
            is_playing2 = false;
            media_is_open2 = true;
            trackBar2.Value = 0;

            vlcPlayer2.SetPosition(0);

            tbVideoTime2.Text = string.Format("{0}/{1}", GetTimeString(trackBar2.Value), GetTimeString(trackBar2.Maximum));
            if (vlcPlayer2.GetState() == 5) vlcPlayer2.Pause();

            var tbMax = vlcPlayer2.GetDuration();
            vlcPlayer2.SetConfig(103, tbMax.ToString());
            trackBar2.SetRange(0, tbMax);
            trackBar2.Value = 0;
        }

        private void SetPlayer1SelectionByTime()
        {
            var playTime = vlcPlayer.GetPosition();  
            var action = actions1.FirstOrDefault(a => playTime >= a.StartTime && playTime < a.EndTime);
            if (action == null) return;
            vlcPlayer.SetConfig(104, ((int)((decimal)action.Speed * 100)).ToString());
            for (int i = 0; i < dgv1.Rows.Count; i++)
            {
                DataGridViewRow dgvr = dgv1.Rows[i];
                if ((int)dgvr.Cells[0].Value == action.ActionId)
                {
                    if (dgv1.SelectedRows.Count > 0 && dgv1.SelectedRows[0].Index == i)
                        return;
                    dgv1.ClearSelection();
                    dgvr.Selected = true;

                    // added by Ted
                    if (dgvr.Index - VScrollIndex >= 6)
                    {
                        VScrollIndex = dgvr.Index - 5;
                    }

                    if (VScrollIndex < dgv1.RowCount)//解决指定的参数已超出有效值的范围2018-5-31
                    {
                        dgv1.FirstDisplayedScrollingRowIndex = VScrollIndex;
                    }
                }
            }
        }

        private void SetPlayer2SelectionByTime()
        {
            var playTime = vlcPlayer2.GetPosition();
            var action = actions2.FirstOrDefault(a => playTime >= a.StartTime && playTime < a.EndTime);
            if (action == null) return;
            vlcPlayer2.SetConfig(104, ((int)((decimal)action.Speed * 100)).ToString());
            for (int i = 0; i < dgv2.Rows.Count; i++)
            {
                DataGridViewRow dgvr = dgv2.Rows[i];
                if ((int)dgvr.Cells[0].Value == action.ActionId)
                {
                    if (dgv2.SelectedRows.Count > 0 && dgv2.SelectedRows[0].Index == i)
                        return;
                    dgv2.ClearSelection();
                    dgvr.Selected = true;

                    // added by Ted
                    if (dgvr.Index - VScrollIndex2 >= 6)
                    {
                        VScrollIndex2 = dgvr.Index - 5;
                    }

                    if (VScrollIndex2 < dgv2.RowCount)//解决指定的参数已超出有效值的范围2018-5-31
                    {
                        dgv2.FirstDisplayedScrollingRowIndex = VScrollIndex2;
                    }
                }
            }
        }

        private void SetPlayer1Process()
        {
            if (vlcPlayer.GetState() == 5) vlcPlayer.Pause();
            if (dgv1.SelectedRows.Count == 0) return;
            var startVal = dgv1.SelectedRows[0].Cells["StartTime"].Value;
            var currentStartTime = (int)decimal.Parse(startVal.ToString());
            vlcPlayer.SetPosition(currentStartTime);
            trackBar1.Value = (int)decimal.Parse(startVal.ToString()) < trackBar1.Maximum ? (int)decimal.Parse(startVal.ToString()) : 0;
            tbVideoTime.Text = string.Format("{0}/{1}", GetTimeString(trackBar1.Value), GetTimeString(trackBar1.Maximum));
            PlayerPause();
        }

        private void SetPlayer2Process()
        {
            if (vlcPlayer2.GetState() == 5) vlcPlayer2.Pause();
            if (dgv2.SelectedRows.Count == 0) return;
            var startVal = dgv2.SelectedRows[0].Cells["StartTime2"].Value;
            var currentStartTime = (int)decimal.Parse(startVal.ToString());
            vlcPlayer2.SetPosition(currentStartTime);
            trackBar2.Value = (int)decimal.Parse(startVal.ToString()) < trackBar2.Maximum ? (int)decimal.Parse(startVal.ToString()) : 0;
            tbVideoTime2.Text = string.Format("{0}/{1}", GetTimeString(trackBar2.Value), GetTimeString(trackBar2.Maximum));
            Player2Pause();
        }

        private void dgv1_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex == -1)
                return;
            timer1.Stop();
            if (vlcPlayer.GetState() == 5) vlcPlayer.Pause();
        }

        private void dgv2_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex == -1)
                return;
            timer2.Stop();
            if (vlcPlayer2.GetState() == 5) vlcPlayer2.Pause();
        }

        private void dgv1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
                return;
            if (vlcPlayer.GetState() == 5)
                vlcPlayer.Pause();
            VScrollIndex = dgv1.FirstDisplayedScrollingRowIndex;//获取当前显示的第一条的索引值，用于重新刷新数据的时候控制滚动条
 
            timer1.Stop();

            var selectedRows = dgv1.SelectedRows;
            if (selectedRows.Count == 0) return;
            var id = (int)selectedRows[0].Cells[0].Value;
            var actionForUpdate = actions1.FirstOrDefault(i => i.ActionId == id); //加载动作数据到窗体内对应的控件
            var upEndTime = actionForUpdate.EndTime;
            btnSetStart1.Enabled = true;
            if (actionForUpdate != null)
            {
                SetPlayer1Process();
            }
            else
            {
                btnSetStart1.Enabled = false;
            }

            btnSetStart1.Enabled = true;
        }
        private void dgv2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
                return;
            if (vlcPlayer2.GetState() == 5)
                vlcPlayer2.Pause();
            VScrollIndex = dgv2.FirstDisplayedScrollingRowIndex;//获取当前显示的第一条的索引值，用于重新刷新数据的时候控制滚动条

            timer2.Stop();

            var selectedRows = dgv2.SelectedRows;
            if (selectedRows.Count == 0) return;
            var id = (int)selectedRows[0].Cells[0].Value;
            var actionForUpdate = actions2.FirstOrDefault(i => i.ActionId == id); //加载动作数据到窗体内对应的控件
            var upEndTime = actionForUpdate.EndTime;
            btnContrastStart.Enabled = true;
            if (actionForUpdate != null)
            {
                SetPlayer2Process();
            }
            else
            {
                btnContrastStart.Enabled = false;
            }

            btnContrastStart.Enabled = true;
        }

        private void FormADoublePlay_FormClosing(object sender, FormClosingEventArgs e)
        {
            PlayerPause();
            Player2Pause();
        }
    }
}
