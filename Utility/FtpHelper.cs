﻿using DAL;
using EnterpriseDT.Net.Ftp;
using EnterpriseDT.Util.Debug;
using Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Utility
{
    public class FtpHelper
    {
        #region 字段
        string ftpURI;
        string ftpUserID;
        string ftpServerIP;
        int ftpServerPort;
        string ftpPassword;
        #endregion

        public FtpHelper(string ip, int port, string userId, string pwd)
        {
            FTPConnection.LogLevel = LogLevel.All;
            FTPConnection.LogToConsole = false;
            FTPConnection.LogToTrace = true;
            FTPConnection.LogFile = "ftplog.log";

            ftpServerIP = ip;
            ftpServerPort = port;
            ftpUserID = userId;
            ftpPassword = pwd;
            ftpURI = "ftp://" + ftpServerIP + "/";
        }

        public bool TestFTP()
        {
            using (var ftpConn = new FTPConnection
            {
                //连接地址、端口号（默认为21）、用户名、密码
                ServerAddress = ftpServerIP,
                ServerPort = ftpServerPort,
                UserName = ftpUserID,
                Password = ftpPassword,

                //编码方式一定要和服务器端保持一致，否则后面获取的文件名可能为乱码
                ConnectMode = FTPConnectMode.PASV,
                TransferType = FTPTransferType.BINARY,
                CommandEncoding = Encoding.GetEncoding("GBK"),
                DataEncoding = Encoding.GetEncoding("GBK")
            })
            {
                try
                {
                    //建立FTP连接
                    ftpConn.Connect();
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return false;
                }
            }
        }

        public void UpLoadEditReport(string filePath, string prodId, string updateFileName)
        {
            using (var ftpConn = new FTPConnection
            {
                //连接地址、端口号（默认为21）、用户名、密码
                ServerAddress = ftpServerIP,
                ServerPort = ftpServerPort,
                UserName = ftpUserID,
                Password = ftpPassword,

                //编码方式一定要和服务器端保持一致，否则后面获取的文件名可能为乱码
                ConnectMode = FTPConnectMode.PASV,
                TransferType = FTPTransferType.BINARY,
                CommandEncoding = Encoding.GetEncoding("GBK"),
                DataEncoding = Encoding.GetEncoding("GBK")
            })
            {
                try
                {
                    //建立FTP连接
                    ftpConn.Connect();
                    if (!ftpConn.DirectoryExists("Resource")) ftpConn.CreateDirectory("Resource");
                    if (!ftpConn.DirectoryExists($"Resource/{prodId}/3/")) ftpConn.CreateDirectory($"Resource/{prodId}/3/");
                    var newChildPath = $"Resource/{prodId}/3/{updateFileName}";
                    ftpConn.UploadFile(filePath, newChildPath);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    //关闭FTP连接
                    ftpConn.Close();
                }
            }
        }
        public void Upload()
        {
            using (var ftpConn = new FTPConnection
            {
                //连接地址、端口号（默认为21）、用户名、密码
                ServerAddress = ftpServerIP,
                ServerPort = ftpServerPort,
                UserName = ftpUserID,
                Password = ftpPassword,

                //编码方式一定要和服务器端保持一致，否则后面获取的文件名可能为乱码
                ConnectMode = FTPConnectMode.PASV,
                TransferType = FTPTransferType.BINARY,
                CommandEncoding = Encoding.GetEncoding("GBK"),
                DataEncoding = Encoding.GetEncoding("GBK")
            })
            {
                try
                {
                    //建立FTP连接
                    ftpConn.Connect();
                    if (!ftpConn.DirectoryExists("Resource"))
                        ftpConn.CreateDirectory("Resource");

                    var ParentFolderPath = Environment.CurrentDirectory + "\\Resource\\";

                    DirectoryInfo ParentFolder = new DirectoryInfo(ParentFolderPath);
                    var FileList = FileHelper.GetFiles(ParentFolder, "*.*");
                    foreach (var fileFullName in FileList)
                    {
                        var ChildPath = fileFullName.Substring(fileFullName.IndexOf("Resource"));
                        var fileInfo = new FileInfo(fileFullName);
                        var fileName = fileInfo.Name;
                        var ext = fileInfo.Extension;
                        var mode = Common.GetSingleModel<t_Video>(i => i.FileName == fileName);
                        if (ext.ToLower() != ".xls" && ext.ToLower() != ".xlsx")
                        {
                            if (mode != null)
                            {
                                string FolderName = mode.PartNo == null ? "1" : mode.PartNo;
                                if (!(bool)(mode.IsUploaded == null ? false : mode.IsUploaded))
                                {
                                    if (!ftpConn.DirectoryExists("Resource/" + FolderName))
                                        ftpConn.CreateDirectory("Resource/" + FolderName);
                                    if (!ftpConn.DirectoryExists("Resource/" + FolderName + "/1"))
                                        ftpConn.CreateDirectory("Resource/" + FolderName + "/1");

                                    var ftpFilePath = "Resource/" + FolderName + "/1/" + fileName;
                                    if (!ftpConn.Exists(ftpFilePath))
                                    {
                                        ftpConn.UploadFile(fileFullName, ftpFilePath);
                                    }
                                }
                            }
                        }
                        else
                        {
                            var folderNameArr = ChildPath.Split('\\');
                            if (!ftpConn.DirectoryExists("Resource/" + folderNameArr[1]))
                                ftpConn.CreateDirectory("Resource/" + folderNameArr[1]);
                            if (!ftpConn.DirectoryExists("Resource/" + folderNameArr[1] + "/" + folderNameArr[2]))
                                ftpConn.CreateDirectory("Resource/" + folderNameArr[1] + "/" + folderNameArr[2]);

                            var newChildPath = $"Resource/{folderNameArr[1]}/{folderNameArr[2]}/{fileName}";

                            ftpConn.UploadFile(fileFullName, newChildPath);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    //关闭FTP连接
                    ftpConn.Close();
                }
            }
        }

        public void UploadExcelFile()
        {
            using (var ftpConn = new FTPConnection
            {
                //连接地址、端口号（默认为21）、用户名、密码
                ServerAddress = ftpServerIP,
                ServerPort = ftpServerPort,
                UserName = ftpUserID,
                Password = ftpPassword,

                //编码方式一定要和服务器端保持一致，否则后面获取的文件名可能为乱码
                ConnectMode = FTPConnectMode.PASV,
                TransferType = FTPTransferType.BINARY,
                CommandEncoding = Encoding.GetEncoding("GBK"),
                DataEncoding = Encoding.GetEncoding("GBK")
            })
            {
                try
                {
                    //建立FTP连接
                    ftpConn.Connect();
                    if (!ftpConn.DirectoryExists("Resource"))
                        ftpConn.CreateDirectory("Resource");

                    var ParentFolderPath = Environment.CurrentDirectory + "\\Resource\\";

                    DirectoryInfo ParentFolder = new DirectoryInfo(ParentFolderPath);
                    var FileList = FileHelper.GetFiles(ParentFolder, "*.*");
                    foreach (var fileFullName in FileList)
                    {
                        var ChildPath = fileFullName.Substring(fileFullName.IndexOf("Resource"));
                        var fileInfo = new FileInfo(fileFullName);
                        var fileName = fileInfo.Name;
                        var ext = fileInfo.Extension;
                        if (ext.ToLower() != ".xls" && ext.ToLower() != ".xlsx")
                        {

                        }
                        else
                        {
                            var folderNameArr = ChildPath.Split('\\');
                            if (!ftpConn.DirectoryExists("Resource/" + folderNameArr[1]))
                                ftpConn.CreateDirectory("Resource/" + folderNameArr[1]);
                            if (!ftpConn.DirectoryExists("Resource/" + folderNameArr[1] + "/" + folderNameArr[2]))
                                ftpConn.CreateDirectory("Resource/" + folderNameArr[1] + "/" + folderNameArr[2]);

                            var newChildPath = $"Resource/{folderNameArr[1]}/{folderNameArr[2]}/{fileName}";
                            ftpConn.UploadFile(fileFullName, newChildPath);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    //关闭FTP连接
                    ftpConn.Close();
                }
            }
        }

        public void UploadExcelFile(List<string> reportPath)
        {
            using (var ftpConn = new FTPConnection
            {
                //连接地址、端口号（默认为21）、用户名、密码
                ServerAddress = ftpServerIP,
                ServerPort = ftpServerPort,
                UserName = ftpUserID,
                Password = ftpPassword,

                //编码方式一定要和服务器端保持一致，否则后面获取的文件名可能为乱码
                ConnectMode = FTPConnectMode.PASV,
                TransferType = FTPTransferType.BINARY,
                CommandEncoding = Encoding.GetEncoding("GBK"),
                DataEncoding = Encoding.GetEncoding("GBK")
            })
            {
                try
                {
                    //建立FTP连接
                    ftpConn.Connect();
                    if (!ftpConn.DirectoryExists("Resource"))
                        ftpConn.CreateDirectory("Resource");

                    var ParentFolderPath = Environment.CurrentDirectory + "\\Resource\\";

                    DirectoryInfo ParentFolder = new DirectoryInfo(ParentFolderPath);
                    var FileList = FileHelper.GetFiles(ParentFolder, "*.*");
                    foreach (var fileFullName in reportPath)
                    {
                        var ChildPath = fileFullName.Substring(fileFullName.IndexOf("Resource"));
                        var fileInfo = new FileInfo(fileFullName);
                        var fileName = fileInfo.Name;
                        var ext = fileInfo.Extension;
                        if (ext.ToLower() != ".xls" && ext.ToLower() != ".xlsx")
                        {

                        }
                        else
                        {
                            var folderNameArr = ChildPath.Split('\\');
                            if (!ftpConn.DirectoryExists("Resource/" + folderNameArr[1]))
                                ftpConn.CreateDirectory("Resource/" + folderNameArr[1]);
                            if (!ftpConn.DirectoryExists("Resource/" + folderNameArr[1] + "/" + folderNameArr[2]))
                                ftpConn.CreateDirectory("Resource/" + folderNameArr[1] + "/" + folderNameArr[2]);

                            var newChildPath = $"Resource/{folderNameArr[1]}/{folderNameArr[2]}/{fileName}";
                            ftpConn.UploadFile(fileFullName, newChildPath);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    //关闭FTP连接
                    ftpConn.Close();
                }
            }
        }

        public string Download(string filePathName)
        {
            using (var ftpConn = new FTPConnection
            {
                //连接地址、端口号（默认为21）、用户名、密码
                ServerAddress = ftpServerIP,
                ServerPort = ftpServerPort,
                UserName = ftpUserID,
                Password = ftpPassword,

                //编码方式一定要和服务器端保持一致，否则后面获取的文件名可能为乱码
                ConnectMode = FTPConnectMode.PASV,
                TransferType = FTPTransferType.BINARY,
                CommandEncoding = Encoding.GetEncoding("GBK"),
                DataEncoding = Encoding.GetEncoding("GBK")
            })
            {
                try
                {
                    ftpConn.Connect();
                    string localPath = Environment.CurrentDirectory + "\\" + filePathName;
                    var fileInfo = new FileInfo(localPath);
                    var lastEditTime = fileInfo.LastWriteTime;//本地文件的最后一次修改时间
                    //var remoteLastWriteTime = ftpConn.GetLastWriteTime(filePathName);
                    //if (lastEditTime > remoteLastWriteTime) return "本地文件的更新时间晚于服务器文件的更新时间";//如果本地的文件更新时间晚于服务器文件的更新时间则不下载。
                    var path = localPath.Substring(0, localPath.LastIndexOf('\\'));
                    if (!Directory.Exists(path)) Directory.CreateDirectory(path);
                    if (ftpConn.Exists(filePathName))
                    {
                        ftpConn.DownloadFile(localPath, filePathName);
                        return "";
                    }
                    else
                    {
                        return "FTP服务器不存在该文件";
                    }
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }
        }
    }
}
