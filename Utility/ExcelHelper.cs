﻿using Microsoft.Office.Interop.Excel;
using OfficeOpenXml;
using OfficeOpenXml.Drawing.Chart;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

namespace Utility
{
    public class ImproveReportModel
    {
        public ImproveReportModel()
        {
            HeaderModel = new HeaderInfoModel();
            DataList = new List<DataBodyModel>();

        }
        public int workStepId { get; set; } //added by Ted for order workstep on 20180620
        public string SheetName { get; set; }
        public string Title { get; set; }
        public HeaderInfoModel HeaderModel { get; set; }
        public List<DataBodyModel> DataList { get; set; }

        /// <summary>
        /// 人数，总结报表用到
        /// </summary>
        public int PeopleCount { get; set; } = 1;
        public class HeaderInfoModel
        {
            public string WorkDepart { get; set; }
            public string WorkLine { get; set; }
            public string CarType { get; set; }
            public string ProductName { get; set; }
            public string ActionName { get; set; }
            public string WorkMan { get; set; }
            public string Researcher { get; set; }
            public string CreateDate { get; set; }
        }
        public class DataBodyModel
        {
            public int Index { get; set; }
            public string Before_ActionDes { get; set; }
            public decimal? Before_StandTime { get; set; }

            public string Anaysis_ActionType { get; set; }

            public string Anaysis_IsValid { get; set; }
            public string Anaysis_ECRS { get; set; }

            public decimal? After_Time { get; set; }
            public string After_ActionDes { get; set; }
            public string After_Instruction { get; set; }
        }

    }

    public class HumanMatchingModel
    {
        public HumanMatchingModel()
        {
            HeaderModel = new HeaderInfoModel();
            DataList = new List<DataBodyModel>();
            Summary = new SummaryModel();
        }
        public int workStepId { get; set; } //added by Ted for order workstep on 20180620
        public string SheetName { get; set; }
        public string Title { get; set; }
        public HeaderInfoModel HeaderModel { get; set; }

        public List<DataBodyModel> DataList { get; set; }

        public SummaryModel Summary { get; set; }
        public class HeaderInfoModel
        {
            public string WorkDepart { get; set; }
            public string WorkLine { get; set; }
            public string CarType { get; set; }
            public string ProductName { get; set; }
            public string ActionName { get; set; }
            public string WorkMan { get; set; }
            public string Researcher { get; set; }
            public string CreateDate { get; set; }
        }
        public class DataBodyModel
        {
            public int Index { get; set; }
            public int TypeId { get; set; }
            public string Operator_ActionDes { get; set; }
            public decimal? Operator_StandTime { get; set; }

            public string Operator_ActionType { get; set; }

            public Operator_StatusEnum Operator_Status { get; set; }

            public Matching_StatusEnum Matching_Status { get; set; }

            public decimal? Matching_Time { get; set; }
            public string Matching_ActionDes { get; set; }
        }
        public class SummaryModel
        {
            public string Operater_FreeTime { get; set; }
            public string Operater_ActionTime { get; set; }
            public string Operater_Rate { get; set; }

            public string Matching_FreeTime { get; set; }
            public string Matching_ActionTime { get; set; }
            public string Matching_Rate { get; set; }


        }
        public enum Operator_StatusEnum
        {
            First = 1,
            Second = 2,
            Third = 3,
            Other = 4,
        }
        public enum Matching_StatusEnum
        {
            Free = 1,
            Running = 2,
            Other = 3
        }
    }
    public class ExcelHelper
    {
        public static void ImproveReport(List<ImproveReportModel> modelList, string FileName, string OpenPassword = "")
        {

            FileInfo file = new FileInfo(FileName);
            if (!file.Directory.Exists)
            {
                file.Directory.Create();
            }
            if (file.Exists)
            {

                file.Delete();
            }

            List<string> positionList = new List<string>();
            List<string> positionAfterList = new List<string>();
            using (ExcelPackage ep = new ExcelPackage(file, OpenPassword))
            {
                foreach (var model in modelList)
                {
                    if (ep.Workbook.Worksheets.Any(r => r.Name == model.SheetName))
                    {
                        model.SheetName = model.SheetName + "_" + ep.Workbook.Worksheets.Count(r => r.Name.Contains(model.SheetName));
                    }

                    ExcelWorksheet ws = ep.Workbook.Worksheets.Add(string.IsNullOrEmpty(model.SheetName) ? "NoName" + Guid.NewGuid().ToString("N") : model.SheetName);
                    ws.DefaultColWidth = 20;
                    ws.Cells.Style.Font.Name = "Arial Unicode MS";//字体 
                    ws.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    #region 标题
                    ws.Cells[1, 1].Value = model.Title;


                    ws.Cells[1, 1].Style.Font.Size = 26;//字体大小
                    ws.Cells[1, 1, 1, 9].Merge = true;
                    #endregion

                    #region 表头信息
                    ws.Cells[2, 1].Value = "工作部门：" + model.HeaderModel.WorkDepart;
                    ws.Cells[3, 1].Value = "生产线：" + model.HeaderModel.WorkLine;
                    ws.Cells[4, 1].Value = "产品名称：" + model.HeaderModel.CarType+" "+model.HeaderModel.ProductName;
                    ws.Cells[5, 1].Value = "作业名称：" + model.HeaderModel.ActionName;
                    ws.Cells[6, 1].Value = "工作人员：" + model.HeaderModel.WorkMan;
                    ws.Cells[7, 1].Value = "研究者：" + model.HeaderModel.Researcher;
                    ws.Cells[8, 1].Value = "日期：" + model.HeaderModel.CreateDate;
                    for (int i = 2; i <= 8; i++)
                    {
                        ws.Row(i).Height = 22;
                        ws.Cells[i, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;//水平居中
                        ws.Cells[i, 1, i, 4].Merge = true;
                        ws.Cells[i, 1, i, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.FromArgb(0, 0, 0));
                    }
                    ws.Cells[2, 5].Value = "工\n作\n地\n布\n置\n图";
                    ws.Cells[2, 5].Style.WrapText = true;
                    ws.Cells[2, 5, 8, 5].Merge = true;
                    ws.Cells[2, 5, 8, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.FromArgb(0, 0, 0));
                    ws.Cells[2, 6, 8, 9].Merge = true;

                    ws.Cells[1, 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;//单独设置单元格底部边框样式和颜色（上下左右均可分开设置）
                    ws.Cells[1, 1].Style.Border.Bottom.Color.SetColor(Color.FromArgb(191, 191, 191));
                    ws.Cells[2, 1, 8, 9].Style.Border.BorderAround(ExcelBorderStyle.Medium, Color.FromArgb(0, 0, 0));//设置单元格所有边框
                    #endregion

                    #region 数据循环
                    ws.Row(9).Height = 20;
                    ws.Row(9).Style.Font.Bold = true;
                    ws.Row(9).Style.Font.Size = 12;


                    ws.Cells[9, 1].Value = "改善前";
                    ws.Cells[9, 1, 9, 3].Merge = true;
                    ws.Cells[9, 1, 9, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[9, 1, 9, 3].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 255, 204));//设置单元格背景色

                    ws.Cells[9, 4].Value = "动作分析";
                    ws.Cells[9, 4, 9, 6].Merge = true;
                    ws.Cells[9, 4, 9, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[9, 4, 9, 6].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(228, 223, 236));//设置单元格背景色

                    ws.Cells[9, 7].Value = "改善后";
                    ws.Cells[9, 7, 9, 9].Merge = true;
                    ws.Cells[9, 7, 9, 9].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[9, 7, 9, 9].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(204, 255, 102));//设置单元格背景色


                    ws.Row(10).Height = 15;
                    ws.Cells[10, 1, 10, 9].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[10, 1, 10, 9].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(128, 128, 128));//设置单元格背景色
                    ws.Cells[10, 1, 10, 9].Style.Font.Color.SetColor(Color.White);//字体颜色
                    ws.Cells[10, 1].Value = "序号";
                    ws.Cells[10, 2].Value = "动作叙述";
                    ws.Cells[10, 3].Value = "时间/S";
                    ws.Cells[10, 4].Value = "动作类型";
                    ws.Cells[10, 5].Value = "有效/无效";
                    ws.Cells[10, 6].Value = "ECRS原则";
                    ws.Cells[10, 7].Value = "标准时间";
                    ws.Cells[10, 8].Value = "动作叙述";
                    ws.Cells[10, 9].Value = "说明";
                    for (int i = 1; i <= 9; i++)
                    {
                        ws.Cells[10, i].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                        ws.Cells[10, i].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                        ws.Cells[10, i].Style.Border.Bottom.Color.SetColor(Color.Black);
                    }
                    int startIndex = 11;
                    foreach (var item in model.DataList)
                    {
                        ws.Row(startIndex).Height = 15;
                        ws.Cells[startIndex, 1].Value = item.Index;
                        ws.Cells[startIndex, 2].Value = item.Before_ActionDes;
                        ws.Cells[startIndex, 3].Value = item.Before_StandTime.Value;
                        ws.Cells[startIndex, 4].Value = item.Anaysis_ActionType;
                        ws.Cells[startIndex, 5].Value = item.Anaysis_IsValid;
                        ws.Cells[startIndex, 6].Value = item.Anaysis_ECRS;
                        ws.Cells[startIndex, 7].Value = item.After_Time.Value;
                        ws.Cells[startIndex, 8].Value = item.After_ActionDes;
                        ws.Cells[startIndex, 9].Value = item.After_Instruction;
                        for (int i = 1; i < 10; i++)
                        {
                            ws.Cells[startIndex, i].Style.Border.BorderAround(ExcelBorderStyle.Dotted, Color.FromArgb(0, 0, 0));
                            if (item.Anaysis_IsValid == "无效")
                            {
                                ws.Cells[startIndex, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                ws.Cells[startIndex, 6].Style.Fill.BackgroundColor.SetColor(Color.Yellow);//设置单元格背景色
                                ws.Cells[startIndex, 8].Style.Font.Strike = true;
                            }
                            else if (item.Anaysis_ECRS == "简化")
                            {
                                ws.Cells[startIndex, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                ws.Cells[startIndex, 6].Style.Fill.BackgroundColor.SetColor(Color.Aqua);//设置单元格背景色
                            }
                        }
                        startIndex++;
                    }

                    #endregion

                    #region 数据footer
                    ws.Row(startIndex).Height = 15;
                    ws.Row(startIndex).Style.Font.Bold = true;
                    ws.Cells[startIndex, 2].Value = "实际节拍（s）";
                    positionList.Add("'" + ws.Name.Replace("'","''") + "'" + "!" + ws.Cells[startIndex, 3].Address);
                    positionAfterList.Add("'" + ws.Name.Replace("'", "''") + "'" + "!" + ws.Cells[startIndex, 7].Address);
                    ws.Cells[startIndex, 3].Formula = "Sum(" + ws.Cells[11, 3].Address + ":" + ws.Cells[11 + model.DataList.Count - 1, 3].Address + ")";
                    ws.Cells[startIndex, 7].Formula = "Sum(" + ws.Cells[11, 7].Address + ":" + ws.Cells[11 + model.DataList.Count - 1, 7].Address + ")";
                    ws.Cells[startIndex, 8].Value = "标准节拍（s）";

                    ws.Cells[startIndex, 2, startIndex, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[startIndex, 2, startIndex, 3].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(217, 217, 217));//设置单元格背景色

                    ws.Cells[startIndex, 7, startIndex, 9].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[startIndex, 7, startIndex, 9].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(217, 217, 217));//设置单元格背景色
                    ws.Cells[startIndex, 8, startIndex, 9].Merge = true;
                    for (int i = 1; i < 10; i++)
                    {
                        ws.Cells[startIndex, i].Style.Border.BorderAround(ExcelBorderStyle.Dotted, Color.FromArgb(0, 0, 0));
                    }

                    #endregion

                    ws.Column(1).Width = 8;

                    ws.Column(2).Width = 40;
                    ws.Column(3).Width = 10;
                    ws.Column(4).Width = 12;
                    ws.Column(5).Width = 12;
                    ws.Column(6).Width = 12;
                    ws.Column(7).Width = 10;
                    ws.Column(8).Width = 40;
                    ws.Column(9).Width = 20;
                    ws.Row(3).Style.Numberformat.Format = "0.000";
                    ws.Row(7).Style.Numberformat.Format = "0.000";
                    ws.Cells[9, 4, model.DataList.Count + 11, 6].Style.Border.BorderAround(ExcelBorderStyle.Medium, Color.Black);
                    ws.Cells[9, 1, model.DataList.Count + 11, 9].Style.Border.BorderAround(ExcelBorderStyle.Medium, Color.FromArgb(0, 0, 0));


                    #region 右边图例
                    ws.Cells[2, 11].Value = "E";
                    ws.Cells[3, 11].Value = "C";
                    ws.Cells[4, 11].Value = "R";
                    ws.Cells[5, 11].Value = "S";

                    ws.Cells[2, 12].Value = "取消";
                    ws.Cells[3, 12].Value = "合并";
                    ws.Cells[4, 12].Value = "重排";
                    ws.Cells[5, 12].Value = "简化";

                    ws.Column(11).Width = 4;

                    ws.Cells[2, 12].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[3, 12].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[4, 12].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[5, 12].Style.Fill.PatternType = ExcelFillStyle.Solid;

                    ws.Cells[2, 12].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 255, 0));//设置单元格背景色
                    ws.Cells[3, 12].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(228, 223, 236));//设置单元格背景色
                    ws.Cells[4, 12].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 153, 255));//设置单元格背景色
                    ws.Cells[5, 12].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(102, 255, 255));//设置单元格背景色

                    ws.Cells[2, 11, 2, 12].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    ws.Cells[3, 11, 3, 12].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    ws.Cells[4, 11, 4, 12].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    ws.Cells[5, 11, 5, 12].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);

                    ws.Cells[2, 11].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    ws.Cells[3, 11].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    ws.Cells[4, 11].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    ws.Cells[5, 11].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    #endregion
                }


                #region 总结报表

                ExcelWorksheet wsSummary = ep.Workbook.Worksheets.Add("总结报表");
                wsSummary.DefaultColWidth = 10;
                wsSummary.Cells.Style.Font.Name = "Arial Unicode MS";//字体 
                wsSummary.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSummary.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                #region 标题
                var dataCoumn = modelList.Count + 6;
                wsSummary.Cells[1, 1].Value = "改善对比";


                wsSummary.Cells[1, 1].Style.Font.Size = 26;//字体大小
                wsSummary.Cells[1, 1, 1, dataCoumn].Merge = true;
                #endregion
                #region 数据表头
                wsSummary.Row(2).Height = 15;
                wsSummary.Cells[2, 1, 2, dataCoumn].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSummary.Cells[2, 1, 2, dataCoumn].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(128, 128, 128));//设置单元格背景色
                wsSummary.Cells[2, 1, 2, dataCoumn].Style.Font.Color.SetColor(Color.White);//字体颜色
                wsSummary.Cells[2, 1].Value = "工位";

                var dataCount = modelList.Count;
                for (int i = 0; i < dataCount; i++)
                {
                    wsSummary.Cells[2, i + 2].Value = modelList[i].SheetName;
                }

                wsSummary.Cells[2, dataCount + 2].Value = "节拍";
                wsSummary.Cells[2, dataCount + 3].Value = "平衡度";
                wsSummary.Cells[2, dataCount + 4].Value = "作业人数";
                wsSummary.Cells[2, dataCount + 5].Value = "一人出来高";
                wsSummary.Cells[2, dataCount + 6].Value = "效率提升比率";

                #endregion

                #region 数据
                wsSummary.Cells[3, 1].Value = "改善前";
                wsSummary.Cells[4, 1].Value = "改善后";
                var maxBeforeTime = 0.00m;
                var maxAfterTime = 0.00m;
                var peopleCount = 0;
                for (int i = 0; i < dataCount; i++)
                {
                    var sumBeforeTime = modelList[i].DataList.Sum(d => d.Before_StandTime);
                    var sumAfterTime = modelList[i].DataList.Sum(d => d.After_Time);
                    if (sumAfterTime != null && sumAfterTime != 0.00m) peopleCount++;
                    if (sumBeforeTime > maxBeforeTime) maxBeforeTime = (decimal)sumBeforeTime;
                    if (sumAfterTime > maxAfterTime) maxAfterTime = (decimal)sumAfterTime;
                    //                    wsSummary.Cells[3, i + 2].Value = modelList[i].DataList.Sum(r => r.Before_StandTime);
                    //                    wsSummary.Cells[4, i + 2].Value = modelList[i].DataList.Sum(r => r.After_Time);
                    wsSummary.Cells[3, i + 2].Formula = positionList[i];
                    wsSummary.Cells[4, i + 2].Formula = positionAfterList[i];
                    wsSummary.Cells[3, i + 2].Style.Numberformat.Format = "0.000";
                    wsSummary.Cells[4, i + 2].Style.Numberformat.Format = "0.000";

                    wsSummary.Cells[3, dataCount + 4].Formula = "COUNTIF(B3:" + wsSummary.Cells[3, dataCount + 1].Address + ",\">0\")";
                    wsSummary.Cells[4, dataCount + 4].Formula = "COUNTIF(B4:" + wsSummary.Cells[4, dataCount + 1].Address + ",\">0\")";
                }

                for (int i = 3; i <= 4; i++)
                {
                    wsSummary.Cells[i, dataCount + 2].Formula = "Max(" + wsSummary.Cells[i, 2].Address + ":" + wsSummary.Cells[i, dataCount + 1].Address + ")";
                    //wsSummary.Cells[i, dataCount + 3].Formula = $"AVERAGE(${wsSummary.Cells[i, 2].Address}:${wsSummary.Cells[i, dataCount + 1].Address })/MAX(${wsSummary.Cells[i, 2].Address}:${wsSummary.Cells[i, dataCount + 1].Address })";
                    wsSummary.Cells[i, dataCount + 3].Formula = $"SUM(${wsSummary.Cells[i, 2].Address}:${wsSummary.Cells[i, dataCount + 1].Address })/{wsSummary.Cells[i, dataCount + 4].Address}/MAX(${wsSummary.Cells[i, 2].Address}:${wsSummary.Cells[i, dataCount + 1].Address })";//修改平衡度公式2018-5-29
                    wsSummary.Cells[i, dataCount + 5].Formula = $"3600/${wsSummary.Cells[i, dataCount + 2]}/${wsSummary.Cells[i, dataCount + 4]}";
                    wsSummary.Cells[i, dataCount + 5].Style.Numberformat.Format = "0.000";
                    wsSummary.Cells[i, dataCount + 2].Style.Numberformat.Format = "0.000";
                    wsSummary.Cells[i, dataCount + 3].Style.Numberformat.Format = "0.00%";
                }
                for (int i = 3; i <= 4; i++)
                {
                    for (int j = 2; j <= 5; j++)
                    {
                        wsSummary.Cells[i, dataCount + j].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        wsSummary.Cells[i, dataCount + j].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 255, 204));//设置单元格背景色
                    }

                }
                wsSummary.Cells[3, dataCount + 6, 4, dataCount + 6].Merge = true;
                wsSummary.Cells[3, dataCount + 6, 4, dataCount + 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSummary.Cells[3, dataCount + 6, 4, dataCount + 6].Style.Fill.BackgroundColor.SetColor(maxBeforeTime < maxAfterTime ? Color.Red : Color.FromArgb(153, 255, 102));


                wsSummary.Cells[3, dataCount + 6].Formula = $"(${wsSummary.Cells[4, dataCount + 5].Address} - ${wsSummary.Cells[3, dataCount + 5].Address}) / ${wsSummary.Cells[3, dataCount + 5].Address}";
                wsSummary.Cells[3, dataCount + 6].Style.Numberformat.Format = "0.00%";

                var great = wsSummary.Cells[3, dataCount + 6].ConditionalFormatting.AddGreaterThan();
                great.Formula = "0";
                great.Style.Fill.BackgroundColor.Color = Color.GreenYellow;

                var less = wsSummary.Cells[3, dataCount + 6].ConditionalFormatting.AddLessThan();
                less.Formula = "0";
                less.Style.Fill.BackgroundColor.Color = Color.Red;

                for (int i = 1; i <= 4; i++)
                {
                    for (int j = 1; j < dataCoumn + 1; j++)
                    {
                        wsSummary.Cells[i, j].Style.Border.BorderAround(ExcelBorderStyle.Medium, Color.Black);
                    }
                }
                #endregion
                #endregion
                ExcelBarChart chart = wsSummary.Drawings.AddChart("chart", eChartType.ColumnClustered) as ExcelBarChart;//eChartType中可以选择图表类型
                ExcelChartSerie serie = chart.Series.Add(wsSummary.Cells[3, 2, 3, modelList.Count + 1], wsSummary.Cells[2, 2, 2, modelList.Count + 1]);//设置图表的x轴和y轴
                serie.HeaderAddress = wsSummary.Cells[3, 1];//设置图表的图例
                serie.Fill.Style = eFillStyle.SolidFill;
                serie.Fill.Color = Color.FromArgb(255, 255, 102);
                ExcelChartSerie serie2 = chart.Series.Add(wsSummary.Cells[4, 2, 4, modelList.Count + 1], wsSummary.Cells[2, 2, 2, modelList.Count + 1]);//设置图表的x轴和y轴
                serie2.HeaderAddress = wsSummary.Cells[4, 1];//设置图表的图例
                serie2.Fill.Color = Color.FromArgb(179, 162, 199);

                chart.SetPosition(150, 10);//设置位置
                chart.SetSize(800, 300);//设置大小
                chart.Style = eChartStyle.Style15;//设置图表的样式
                chart.ShowHiddenData = true;
                chart.DataLabel.ShowValue = true;

                ep.Save(OpenPassword);
            }
        }
        public static void HumanMatchingReport(List<HumanMatchingModel> modelList, string FileName, string OpenPassword = "")
        {
            FileInfo file = new FileInfo(FileName);
            if (!file.Directory.Exists)
            {
                file.Directory.Create();
            }
            if (file.Exists)
            {

                file.Delete();
            }

            using (ExcelPackage ep = new ExcelPackage(file, OpenPassword))
            {
                foreach (var model in modelList)
                {
                    if (ep.Workbook.Worksheets.Any(r => r.Name == model.SheetName))
                    {
                        model.SheetName = model.SheetName + "_" + ep.Workbook.Worksheets.Count(r => r.Name.Contains(model.SheetName));
                    }
                    ExcelWorksheet ws = ep.Workbook.Worksheets.Add(model.SheetName);
                    ws.DefaultColWidth = 10;

                    ws.Cells.Style.Font.Name = "Arial Unicode MS";//字体 
                    ws.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;



                    #region 标题
                    ws.Cells[1, 1].Value = model.Title;


                    ws.Cells[1, 1].Style.Font.Size = 26;//字体大小
                    ws.Cells[1, 1, 1, 8].Merge = true;
                    #endregion

                    #region 表头信息
                    ws.Cells[2, 1].Value = "工作部门：" + model.HeaderModel.WorkDepart;
                    ws.Cells[3, 1].Value = "生产线：" + model.HeaderModel.WorkLine;
                    ws.Cells[4, 1].Value = "产品名称：" + model.HeaderModel.CarType + " " + model.HeaderModel.ProductName;
                    ws.Cells[5, 1].Value = "作业名称：" + model.HeaderModel.ActionName;
                    ws.Cells[6, 1].Value = "工作人员：" + model.HeaderModel.WorkMan;
                    ws.Cells[7, 1].Value = "研究者：" + model.HeaderModel.Researcher;
                    ws.Cells[8, 1].Value = "日期：" + model.HeaderModel.CreateDate;
                    for (int i = 2; i <= 8; i++)
                    {
                        ws.Row(i).Height = 22;
                        ws.Cells[i, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;//水平居中
                        ws.Cells[i, 1, i, 4].Merge = true;
                        ws.Cells[i, 1, i, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.FromArgb(0, 0, 0));
                    }
                    ws.Cells[2, 5].Value = "工\n作\n地\n布\n置\n图";
                    ws.Cells[2, 5].Style.WrapText = true;
                    ws.Cells[2, 5, 8, 5].Merge = true;
                    ws.Cells[2, 5, 8, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.FromArgb(0, 0, 0));
                    ws.Cells[2, 6, 8, 8].Merge = true;

                    ws.Cells[1, 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;//单独设置单元格底部边框样式和颜色（上下左右均可分开设置）
                    ws.Cells[1, 1].Style.Border.Bottom.Color.SetColor(Color.FromArgb(191, 191, 191));
                    ws.Cells[2, 1, 8, 8].Style.Border.BorderAround(ExcelBorderStyle.Medium, Color.FromArgb(0, 0, 0));//设置单元格所有边框
                    #endregion

                    #region 数据循环
                    ws.Row(9).Height = 20;
                    ws.Row(9).Style.Font.Bold = true;
                    ws.Row(9).Style.Font.Size = 12;


                    ws.Cells[9, 1].Value = "操作者";
                    ws.Cells[9, 1, 9, 5].Merge = true;


                    ws.Cells[9, 6].Value = "设备";
                    ws.Cells[9, 6, 9, 8].Merge = true;

                    ws.Row(10).Height = 15;
                    ws.Row(9).Style.Font.Bold = true;
                    ws.Row(9).Style.Font.Size = 12;

                    ws.Row(10).Style.Font.Bold = true;
                    ws.Cells[10, 1].Value = "序号";
                    ws.Cells[10, 2].Value = "动作叙述";
                    ws.Cells[10, 3].Value = "标准时间";
                    ws.Cells[10, 4].Value = "动作类型";
                    ws.Cells[10, 5].Value = "人-状态";
                    ws.Cells[10, 6].Value = "设备-状态";
                    ws.Cells[10, 7].Value = "时间";
                    ws.Cells[10, 8].Value = "设备动作叙述";
 

                    int startIndex = 11;
                    foreach (var item in model.DataList)
                    {
                        ws.Row(startIndex).Height = 15;
                        ws.Cells[startIndex, 1].Value = item.Index;
                        ws.Cells[startIndex, 2].Value = item.Operator_ActionDes;
                        ws.Cells[startIndex, 3].Value = item.Operator_StandTime.Value.ToString("F2");
                        ws.Cells[startIndex, 4].Value = item.Operator_ActionType;

                        if (item.Operator_Status == HumanMatchingModel.Operator_StatusEnum.First)
                        {
                            // ws.Cells[startIndex, 5].Value = "第一类动素";
                            ws.Cells[startIndex, 5].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells[startIndex, 5].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(204, 255, 153));//设置单元格背景色
                        }
                        else if (item.Operator_Status == HumanMatchingModel.Operator_StatusEnum.Second)
                        {
                            // ws.Cells[startIndex, 5].Value = "第二类动素";
                            ws.Cells[startIndex, 5].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells[startIndex, 5].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 255, 153));//设置单元格背景色

                        }
                        else if (item.Operator_Status == HumanMatchingModel.Operator_StatusEnum.Third)
                        {
                            // ws.Cells[startIndex, 5].Value = "第三类动素";
                            ws.Cells[startIndex, 5].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells[startIndex, 5].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(218, 238, 243));//设置单元格背景色

                        }

                        // ws.Cells[startIndex, 5].Value = item.Operator_Status;


                        if (item.Matching_Status == HumanMatchingModel.Matching_StatusEnum.Free)
                        {
                            //ws.Cells[startIndex, 6].Value = "空闲";
                            ws.Cells[startIndex, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells[startIndex, 6].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(218, 238, 243));//设置单元格背景色
                        }
                        else if (item.Matching_Status == HumanMatchingModel.Matching_StatusEnum.Running)
                        {
                            //ws.Cells[startIndex, 6].Value = "运行";
                            ws.Cells[startIndex, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells[startIndex, 6].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(177, 160, 199));//设置单元格背景色

                        }


                        ws.Cells[startIndex, 7].Value = item.Matching_Time.Value.ToString("F2");
                        ws.Cells[startIndex, 8].Value = item.Matching_ActionDes;

                        for (int i = 1; i < 9; i++)
                        {
                            ws.Cells[startIndex, i].Style.Border.BorderAround(ExcelBorderStyle.Dotted, Color.FromArgb(0, 0, 0));
                        }

                        startIndex++;
                    }
                    for (int i = 1; i < 9; i++)
                    {
                        ws.Cells[10, i].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.FromArgb(0, 0, 0));
                    }

                    ws.Cells[9, 1, model.DataList.Count + 10, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.FromArgb(0, 0, 0));
                    ws.Cells[9, 1, model.DataList.Count + 10, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.FromArgb(0, 0, 0));
                    #endregion
                    ws.Column(1).Width = 8;

                    ws.Column(2).Width = 40;
                    ws.Column(3).Width = 10;
                    ws.Column(4).Width = 15;
                    ws.Column(8).Width = 40;
                    ws.Column(9).Width = 20;


                    #region 统计
                    ws.Row(startIndex).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Row(startIndex).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[startIndex, 1, startIndex + 2, 1].Merge = true;
                    ws.Cells[startIndex, 1].Value = "统\r\n计";
                    for (int i = 0; i < 20; i++)
                    {
                        ws.Cells[startIndex + i, 3, startIndex + i, 4].Merge = true;
                        ws.Cells[startIndex + i, 5, startIndex + i, 6].Merge = true;
                        ws.Cells[startIndex + i, 7, startIndex + i, 8].Merge = true;
                    }
                    ws.Cells[startIndex, 3].Value = "空闲时间";
                    ws.Cells[startIndex, 5].Value = "操作时间";
                    ws.Cells[startIndex, 7].Value = "利用率";

                    ws.Cells[startIndex + 1, 2].Value = "操作者";
                    ws.Cells[startIndex + 2, 2].Value = "设备";

                    ws.Cells[startIndex + 1, 3].Value = double.Parse(model.Summary.Operater_FreeTime??"0");
                    ws.Cells[startIndex + 1, 5].Value = double.Parse(model.Summary.Operater_ActionTime ?? "0");
                    ws.Cells[startIndex + 2, 3].Value = double.Parse(model.Summary.Operater_FreeTime ?? "0");
                    ws.Cells[startIndex + 2, 5].Value = double.Parse(model.Summary.Operater_ActionTime ?? "0");
                    ws.Cells[startIndex + 1, 5].Style.Numberformat.Format = "0.000";
                    ws.Cells[startIndex + 1, 3].Style.Numberformat.Format = "0.000";
                    ws.Cells[startIndex + 1, 7].Formula = $"${ws.Cells[startIndex + 1, 5]}/SUM(${ws.Cells[startIndex + 1, 3].Address}:${ws.Cells[startIndex + 1, 5].Address})";
                    ws.Cells[startIndex + 1, 7].Style.Numberformat.Format = "0.00%";


                    ws.Cells[startIndex + 2, 3].Value = double.Parse(model.Summary.Matching_FreeTime ?? "0");
                    ws.Cells[startIndex + 2, 5].Value = double.Parse(model.Summary.Matching_ActionTime ?? "0");
                    ws.Cells[startIndex + 2, 7].Formula = $"${ws.Cells[startIndex + 2, 5]}/SUM(${ws.Cells[startIndex + 2, 3].Address}:${ws.Cells[startIndex + 2, 5].Address})";
                    ws.Cells[startIndex + 2, 7].Style.Numberformat.Format = "0.00%";

                    for (int i = 0; i <= 2; i++)
                    {
                        for (int j = 1; j <= 8; j++)
                        {
                            ws.Cells[startIndex + i, j].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.FromArgb(0, 0, 0));
                            if (i != 0 && j > 2)
                            {
                                ws.Cells[startIndex + i, j].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                ws.Cells[startIndex + i, j].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(217, 217, 217));//设置单元格背景色
                            }

                        }

                    }
                    ws.Cells[startIndex, 1, startIndex + 2, 8].Style.Border.BorderAround(ExcelBorderStyle.Medium, Color.FromArgb(0, 0, 0));//设置单元格所有边框

                    #endregion

                    #region 备注
                    var markIndex = startIndex + 5;
                    ws.Cells[markIndex, 2].Value = "备注：“人-状态”：";
                    ws.Cells[markIndex, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                    ws.Cells[markIndex + 0, 3].Value = "第一类动素";
                    ws.Cells[markIndex + 1, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[markIndex + 1, 3].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(204, 255, 153));//设置单元格背景色

                    ws.Cells[markIndex + 3, 3].Value = "第二类动素";
                    ws.Cells[markIndex + 4, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[markIndex + 4, 3].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 255, 153));//设置单元格背景色


                    ws.Cells[markIndex + 6, 3].Value = "第三类动素";
                    ws.Cells[markIndex + 7, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[markIndex + 7, 3].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(218, 238, 243));//设置单元格背景色


                    for (int j = 0; j <= 7; j++)
                    {
                        if (!(new int[] { 2, 5 }).Contains(j))
                        {
                            ws.Cells[markIndex + j, 3, markIndex + j, 4].Style.Border.BorderAround(ExcelBorderStyle.Medium, Color.Black);
                        }
                    }


                    ws.Cells[markIndex, 5].Value = "备注：“设备-状态”：";
                    ws.Cells[markIndex, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                    ws.Cells[markIndex + 0, 7].Value = "空闲";
                    ws.Cells[markIndex + 1, 7].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[markIndex + 1, 7].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(218, 238, 243));//设置单元格背景色

                    ws.Cells[markIndex + 3, 7].Value = "运行";
                    ws.Cells[markIndex + 4, 7].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[markIndex + 4, 7].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(177, 160, 199));//设置单元格背景色



                    for (int j = 0; j <= 4; j++)
                    {
                        if (!(new int[] { 2, 5 }).Contains(j))
                        {
                            ws.Cells[markIndex + j, 7, markIndex + j, 8].Style.Border.BorderAround(ExcelBorderStyle.Medium, Color.Black);
                        }
                    }
                }
                #endregion

                ep.Save(OpenPassword);
            }

        }

    }
}
